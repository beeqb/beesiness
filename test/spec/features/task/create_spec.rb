feature 'Tasks', type: :feature, js: true do
  scenario 'creation' do
    page.driver.set_cookie('lang="ru"; domain=http.beeqb.local')

    sign_in('glukhota@beeqb.com', '12345678')

    within('.app-aside') do
      click_on 'BeeQB'
      find('a', text: 'Dashboard', visible: true).click
    end

    within('.app-header') do
      click_on 'Создать'
      click_on 'Задачу'
    end

    expect(page).to have_current_path('/companies/3/tasks/new')

    fill_in 'Название задачи', with: 'Задача 1'
  end
end
