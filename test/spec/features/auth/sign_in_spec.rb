feature 'Auth', :type => :feature, :js => true do

  scenario 'ru' do
    page.driver.set_cookie('lang="ru"; domain=http.beeqb.local')
    visit '/'
    expect(page).to have_text 'Войти'
  end

  scenario 'en' do
    page.driver.set_cookie('lang="en"; domain=http.beeqb.local')
    visit '/'
    expect(page).to have_text 'Sign in'
  end

  scenario 'sign in' do
    page.driver.set_cookie('lang="ru"; domain=http.beeqb.local')

    sign_in('glukhota@beeqb.com', '12345678')

    expect(page).to have_text 'Мои компании'
  end

end
