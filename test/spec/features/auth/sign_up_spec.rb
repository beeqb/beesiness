feature 'Auth', :type => :feature, :js => true do

  scenario 'ru' do
    page.driver.set_cookie('lang="ru"; domain=http.beeqb.local')
    visit '/users/sign_up'
    expect(page).to have_text 'Зарегистрироваться'
  end

  scenario 'en' do
    page.driver.set_cookie('lang="en"; domain=http.beeqb.local')
    visit '/users/sign_up'
    expect(page).to have_text 'Sign up'
  end

  scenario 'sign up' do
    page.driver.set_cookie('lang="ru"; domain=http.beeqb.local')
    visit '/users/sign_up'

    fill_in 'name', :with => 'Test'
    fill_in 'email', :with => 'test_'+Time.now.to_i.to_s+'@test.com'
    fill_in 'password', :with => '12345678'
    check('agree')

    click_button 'Зарегистрироваться'

    expect(page).to have_text 'Вам было выслано письмо с ссылкой для подтверждения аккаунта.'
  end
end
