module AuthMacros
  def sign_in(email, password)
    visit '/'

    fill_in 'E-mail', :with => email
    fill_in 'Пароль', :with => password

    click_button 'Войти'
  end
end
