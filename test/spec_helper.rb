require 'capybara/rspec'
require 'capybara-webkit'
require 'capybara-screenshot/rspec'

Dir['./spec/support/**/*.rb'].each {|f| require f}

Encoding.default_external = Encoding::UTF_8
Encoding.default_internal = Encoding::UTF_8

Capybara.configure do |config|
  config.javascript_driver = :webkit
  config.app_host = 'http://http.beeqb.local'
  config.server_host = 'http.beeqb.local'
  config.server_port = 80
  config.default_max_wait_time = 5
end

Capybara::Screenshot.autosave_on_failure = true

Capybara::Webkit.configure do |config|
  config.allow_unknown_urls
end

RSpec.configure do |config|
  config.include AuthMacros, type: :feature
end
