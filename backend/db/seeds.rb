if Role.all.blank?
  Role.roles.each do |role_name|
    Role.create({name: role_name})
  end
end

if Appeal.all.blank?
  Appeal.create([
    {name: 'Mr.'},
    {name: 'Mrs.'},
    {name: 'Miss.'},
    {name: 'Ms.'},
    {name: 'Dr.'},
  ])
end

if Country.all.blank?
  Country.create([
    {name: 'Россия'},
  ])
end

if City.all.blank?
  City.create([
    {name: 'Москва', country_id: 1},
    {name: 'Санкт-Петербург', country_id: 1},
  ])
end

if UserActivity.all.blank?
  UserActivity.create([
    {name: 'Работает', color: '27c24c'},
    {name: 'В отпуске', color: '337ab7'},
    {name: 'Уволен', color: '98a6ad'},
  ])
end

if CountryPhoneCode.all.blank?
  CountryPhoneCode.create([
    {country_name:'Австралия', code: '+61'},
    {country_name:'Австрия', code: '+43'},
    {country_name:'Азербайджан', code: '+994'},
    {country_name:'Албания', code: '+355'},
    {country_name:'Алжир', code: '+213'},
    {country_name:'Ангола', code: '+244'},
    {country_name:'Андорра', code: '+376'},
    {country_name:'Антигуа и Барбуда', code: '+1268'},
    {country_name:'Аргентина', code: '+54'},
    {country_name:'Армения', code: '+374'},
    {country_name:'Афганистан', code: '+93'},
    {country_name:'Багамы', code: '+1242'},
    {country_name:'Бангладеш', code: '+880'},
    {country_name:'Барбадос', code: '+1246'},
    {country_name:'Бахрейн', code: '+973'},
    {country_name:'Беларусь', code: '+375'},
    {country_name:'Белиз', code: '+501'},
    {country_name:'Бельгия', code: '+32'},
    {country_name:'Бенин', code: '+229'},
    {country_name:'Болгария', code: '+359'},
    {country_name:'Боливия', code: '+591'},
    {country_name:'Босния и Герцеговина', code: '+387'},
    {country_name:'Ботсвана', code: '+267'},
    {country_name:'Бразилия', code: '+55'},
    {country_name:'Бруней', code: '+673'},
    {country_name:'Буркина Фасо', code: '+226'},
    {country_name:'Бурунди', code: '+257'},
    {country_name:'Бутан', code: '+975'},
    {country_name:'Вануату', code: '+678'},
    {country_name:'Ватикан', code: '+39'},
    {country_name:'Великобритания', code: '+44'},
    {country_name:'Венгрия', code: '+36'},
    {country_name:'Венесуэла', code: '+58'},
    {country_name:'Восточный Тимор', code: '+670'},
    {country_name:'Вьетнам', code: '+84'},
    {country_name:'Габон', code: '+241'},
    {country_name:'Гаити', code: '+509'},
    {country_name:'Гайана', code: '+592'},
    {country_name:'Гамбия', code: '+220'},
    {country_name:'Гана', code: '+233'},
    {country_name:'Гватемала', code: '+502'},
    {country_name:'Гвинея', code: '+224'},
    {country_name:'Гвинея-Бисау', code: '+245'},
    {country_name:'Германия', code: '+49'},
    {country_name:'Гондурас', code: '+504'},
    {country_name:'Гренада', code: '+1473'},
    {country_name:'Греция', code: '+30'},
    {country_name:'Грузия', code: '+995'},
    {country_name:'Дания', code: '+45'},
    {country_name:'Джибути', code: '+253'},
    {country_name:'Доминика', code: '+1767'},
    {country_name:'Доминиканская Республика', code: '+1809'},
    {country_name:'Египет', code: '+20'},
    {country_name:'Замбия', code: '+260'},
    {country_name:'Зимбабве', code: '+263'},
    {country_name:'Израиль', code: '+972'},
    {country_name:'Индия', code: '+91'},
    {country_name:'Индонезия', code: '+62'},
    {country_name:'Иордания', code: '+962'},
    {country_name:'Ирак', code: '+964'},
    {country_name:'Иран', code: '+98'},
    {country_name:'Ирландия', code: '+353'},
    {country_name:'Исландия', code: '+354'},
    {country_name:'Испания', code: '+34'},
    {country_name:'Италия', code: '+39'},
    {country_name:'Йемен', code: '+967'},
    {country_name:'Кабо-Верде', code: '+238'},
    {country_name:'Казахстан', code: '+77'},
    {country_name:'Камбоджа', code: '+855'},
    {country_name:'Камерун', code: '+237'},
    {country_name:'Канада', code: '+1'},
    {country_name:'Катар', code: '+974'},
    {country_name:'Кения', code: '+254'},
    {country_name:'Кипр', code: '+357'},
    {country_name:'Киргизия', code: '+996'},
    {country_name:'Кирибати', code: '+686'},
    {country_name:'Китай', code: '+86'},
    {country_name:'Колумбия', code: '+57'},
    {country_name:'Коморы', code: '+269'},
    {country_name:'Конго, демократическая республика', code: '+243'},
    {country_name:'Конго, республика', code: '+242'},
    {country_name:'Коста-Рика', code: '+506'},
    {country_name:'Кот-д’Ивуар', code: '+225'},
    {country_name:'Куба', code: '+53'},
    {country_name:'Кувейт', code: '+965'},
    {country_name:'Лаос', code: '+856'},
    {country_name:'Латвия', code: '+371'},
    {country_name:'Лесото', code: '+266'},
    {country_name:'Либерия', code: '+231'},
    {country_name:'Ливан', code: '+961'},
    {country_name:'Ливия', code: '+218'},
    {country_name:'Литва', code: '+370'},
    {country_name:'Лихтенштейн', code: '+423'},
    {country_name:'Люксембург', code: '+352'},
    {country_name:'Маврикий', code: '+230'},
    {country_name:'Мавритания', code: '+222'},
    {country_name:'Мадагаскар', code: '+261'},
    {country_name:'Македония', code: '+389'},
    {country_name:'Малави', code: '+265'},
    {country_name:'Малайзия', code: '+60'},
    {country_name:'Мали', code: '+223'},
    {country_name:'Мальдивы', code: '+960'},
    {country_name:'Мальта', code: '+356'},
    {country_name:'Марокко', code: '+212'},
    {country_name:'Маршалловы Острова', code: '+692'},
    {country_name:'Мексика', code: '+52'},
    {country_name:'Мозамбик', code: '+259'},
    {country_name:'Молдавия', code: '+373'},
    {country_name:'Монако', code: '+377'},
    {country_name:'Монголия', code: '+976'},
    {country_name:'Мьянма', code: '+95'},
    {country_name:'Намибия', code: '+264'},
    {country_name:'Науру', code: '+674'},
    {country_name:'Непал', code: '+977'},
    {country_name:'Нигер', code: '+227'},
    {country_name:'Нигерия', code: '+234'},
    {country_name:'Нидерланды', code: '+31'},
    {country_name:'Никарагуа', code: '+505'},
    {country_name:'Новая Зеландия', code: '+64'},
    {country_name:'Норвегия', code: '+47'},
    {country_name:'Объединенные Арабские Эмираты', code: '+971'},
    {country_name:'Оман', code: '+968'},
    {country_name:'Пакистан', code: '+92'},
    {country_name:'Палау', code: '+680'},
    {country_name:'Панама', code: '+507'},
    {country_name:'Папуа - Новая Гвинея', code: '+675'},
    {country_name:'Парагвай', code: '+595'},
    {country_name:'Перу', code: '+51'},
    {country_name:'Польша', code: '+48'},
    {country_name:'Португалия', code: '+351'},
    {country_name:'Россия', code: '+7'},
    {country_name:'Руанда', code: '+250'},
    {country_name:'Румыния', code: '+40'},
    {country_name:'Сальвадор', code: '+503'},
    {country_name:'Самоа', code: '+685'},
    {country_name:'Сан-Марино', code: '+378'},
    {country_name:'Сан-Томе и Принсипи', code: '+239'},
    {country_name:'Саудовская Аравия', code: '+966'},
    {country_name:'Свазиленд', code: '+268'},
    {country_name:'Северная Корея', code: '+850'},
    {country_name:'Сейшелы', code: '+248'},
    {country_name:'Сенегал', code: '+221'},
    {country_name:'Сент-Винсент и Гренадины', code: '+1784'},
    {country_name:'Сент-Китс и Невис', code: '+1869'},
    {country_name:'Сент-Люсия', code: '+1758'},
    {country_name:'Сербия', code: '+381'},
    {country_name:'Сингапур', code: '+65'},
    {country_name:'Сирия', code: '+963'},
    {country_name:'Словакия', code: '+421'},
    {country_name:'Словения', code: '+986'},
    {country_name:'Соединенные Штаты Америки', code: '+1'},
    {country_name:'Соломоновы Острова', code: '+677'},
    {country_name:'Сомали', code: '+252'},
    {country_name:'Судан', code: '+249'},
    {country_name:'Суринам', code: '+597'},
    {country_name:'Сьерра-Леоне', code: '+232'},
    {country_name:'Таджикистан', code: '+992'},
    {country_name:'Таиланд', code: '+66'},
    {country_name:'Танзания', code: '+255'},
    {country_name:'Того', code: '+228'},
    {country_name:'Тонга', code: '+676'},
    {country_name:'Тринидад и Тобаго', code: '+1868'},
    {country_name:'Тувалу', code: '+688'},
    {country_name:'Тунис', code: '+216'},
    {country_name:'Туркмения', code: '+993'},
    {country_name:'Турция', code: '+90'},
    {country_name:'Уганда', code: '+256'},
    {country_name:'Узбекистан', code: '+998'},
    {country_name:'Украина', code: '+380'},
    {country_name:'Уругвай', code: '+598'},
    {country_name:'Федеративные штаты Микронезии', code: '+691'},
    {country_name:'Фиджи', code: '+679'},
    {country_name:'Филиппины', code: '+63'},
    {country_name:'Финляндия', code: '+358'},
    {country_name:'Франция', code: '+33'},
    {country_name:'Хорватия', code: '+385'},
    {country_name:'Центрально-Африканская Республика', code: '+236'},
    {country_name:'Чад', code: '+235'},
    {country_name:'Черногория', code: '+381'},
    {country_name:'Чехия', code: '+420'},
    {country_name:'Чили', code: '+56'},
    {country_name:'Швейцария', code: '+41'},
    {country_name:'Швеция', code: '+46'},
    {country_name:'Шри-Ланка', code: '+94'},
    {country_name:'Эквадор', code: '+593'},
    {country_name:'Экваториальная Гвинея', code: '+240'},
    {country_name:'Эритрея', code: '+291'},
    {country_name:'Эстония', code: '+372'},
    {country_name:'Эфиопия', code: '+251'},
    {country_name:'Южная Корея', code: '+82'},
    {country_name:'Южно-Африканская Республика', code: '+27'},
    {country_name:'Ямайка', code: '+1876'},
    {country_name:'Япония', code: '+81'},
  ])



end

# create admin
if User.where(email: 'admin@beeqb.com').blank?
  admin = User.new
  admin.name = 'admin'
  admin.email = 'admin@beeqb.com'
  admin.password = '12345678'
  admin.confirmation_sent_at = Time.now
  admin.confirmed_at = Time.now
  admin.save!

  admin.add_role :admin
end

# creating user for super-company
if User.where(email: 'glukhota@beeqb.com').blank?
  user = User.new
  user.email = 'glukhota@beeqb.com'
  user.password = '12345678'
  user.confirmation_sent_at = Time.now
  user.confirmed_at = Time.now
  user.name = 'Сергей'
  user.surname = 'Глухота'
  user.cell_phone_code_id = 18 # Russia (+7)
  user.cell_phone = '999-890-12-34'
  user.lang = 'ru'
  user.appeal_id = 1
  user.city_id = 1
  user.sex = 'm'
  user.save!

  user.add_role :admin
end

# creating super-company
if Company.where(is_super: true).blank?
  user = User.find_by(email: 'glukhota@beeqb.com')

  company = Company.new
  company.creator_id = user.id
  company.name = 'Name will be replaced later' # replaced to beeqb for skip validation
  company.description = 'BeeQB'
  company.address = 'Софийская наб., 12, Москва, Россия, 119072'
  company.site = 'http://glukhota.com'
  company.email = 'beeqb@beeqb.com'
  company.phone_code_id = 18 # Russia (+7)
  company.phone = '999-999-99-99'
  company.currency = 'usd'
  company.vat = 0
  company.is_super = true
  company.inn = 0
  company.save!

  company.update_attribute(:name, 'BeeQB') # skip validation name

  company.add_user(user, :employee_director)
  company.add_user(user, :employee_head_of_sales_department)

  # add product category
  category = ProductCategory.new
  category.company_id = company.id
  category.name = 'Subscriptions'
  category.color = 'ddffaa'
  category.save!

  # add product
  product = Product.new
  product.name = 'Workplace'
  product.price = 4.99
  product.vendor_code = '121323232'
  product.activity = 'yes'
  product.barcode = 123456789
  product.category = category
  product.save!

  # add departments
  department_sales = Department.new
  department_sales.name = 'Отдел продаж'
  department_sales.company_id = company.id
  department_sales.color = 'eeefff'

  department_sales.save!

  employee = Employee.where(
    company_id: company.id,
    user_id: user.id
  ).first

  unless employee.nil?
    department_sales.add_employee(
      employee,
      I18n.t('roles.' + :employee_head_of_sales_department.to_s)
    )
  end
end

if GenericStatus.all.blank?
  LeadStatus.create(
    id: :lead_new,
    color: '#f05050',
    system: true
  )
  LeadStatus.create(
    id: :lead_in_work,
    color: '#27c24c',
    system: true
  )
  LeadStatus.create(
    id: :lead_trash,
    color: '#edf1f2',
    system: true
  )

  ClientStatus.create(
    id: :client_new,
    color: '#f05050',
    system: true
  )
  ClientStatus.create(
    id: :client_current,
    color: '#23b7e5',
    system: true
  )
  ClientStatus.create(
    id: :client_permanent,
    color: '#7266ba',
    system: true
  )
  ClientStatus.create(
    id: :client_special,
    color: '#27c24c',
    system: true
  )
  ClientStatus.create(
    id: :client_no,
    color: '#58666e',
    system: true
  )

  DealStatus.create(
    id: :in_progress,
    color: '#7266ba',
    system: true
  )
  DealStatus.create(
    id: :contract_conclusion,
    color: '#fad733',
    system: true
  )
  DealStatus.create(
    id: :contract_signed,
    color: '#fad733',
    system: true
  )
  DealStatus.create(
    id: :won,
    color: '#27c24c',
    system: true
  )
  DealStatus.create(
    id: :payed,
    color: '#27c24c',
    system: true
  )
  DealStatus.create(
    id: :lost,
    color: '#f05050',
    system: true
  )
  DealStatus.create(
    id: :returned,
    color: '#f05050',
    system: true
  )

  SupplierStatus.create(
    id: :supplier_new,
    color: '#f05050',
    system: true
  )
  SupplierStatus.create(
    id: :supplier_current,
    color: '#23b7e5',
    system: true
  )
  SupplierStatus.create(
    id: :supplier_permanent,
    color: '#7266ba',
    system: true
  )
  SupplierStatus.create(
    id: :supplier_special,
    color: '#27c24c',
    system: true
  )
  SupplierStatus.create(
    id: :supplier_no,
    color: '#58666e',
    system: true
  )
end

unless CashBookCategory.exists?
  CashBookCategory.create(
    id: :income,
    system: true,
    expense: false
  )
  CashBookCategory.create(
    id: :outcome,
    system: true,
    expense: true
  )
  CashBookCategory.create(
    id: :loan,
    system: true,
    expense: false
  )
  CashBookCategory.create(
    id: :loan_return,
    system: true,
    expense: true
  )
end
