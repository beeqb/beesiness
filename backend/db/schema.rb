# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170510141607) do

  create_table "appeals", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_appeals_on_name", unique: true, using: :btree
  end

  create_table "calendar_events", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "title"
    t.datetime "start"
    t.datetime "end"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "user_id"
    t.datetime "deleted_at"
    t.string   "recurring"
    t.integer  "original_event_id"
    t.date     "recurring_end_at"
    t.index ["original_event_id"], name: "index_calendar_events_on_original_event_id", using: :btree
    t.index ["user_id"], name: "index_calendar_events_on_user_id", using: :btree
  end

  create_table "cash_book_categories", id: :string, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "name"
    t.boolean  "system",     default: false
    t.boolean  "expense",    default: false
    t.integer  "company_id"
    t.datetime "deleted_at"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.index ["company_id"], name: "index_cash_book_categories_on_company_id", using: :btree
  end

  create_table "cash_book_records", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "category",                                                                            null: false
    t.integer  "bill_kind"
    t.string   "currency",                                                                            null: false
    t.decimal  "balance",                                    precision: 18, scale: 2
    t.decimal  "amount",                                     precision: 18, scale: 2,                 null: false
    t.text     "comment",                      limit: 65535
    t.integer  "creator_id",                                                                          null: false
    t.integer  "company_id"
    t.datetime "created_at",                                                                          null: false
    t.datetime "updated_at",                                                                          null: false
    t.integer  "deal_id"
    t.datetime "deleted_at"
    t.boolean  "personal",                                                            default: false
    t.decimal  "personal_balance",                           precision: 18, scale: 2
    t.integer  "user_id"
    t.string   "reporting_document"
    t.integer  "cash_flow_budget_category_id"
    t.string   "deal_display_name"
    t.string   "source_type"
    t.integer  "source_id"
    t.index ["cash_flow_budget_category_id"], name: "index_cash_book_records_on_cash_flow_budget_category_id", using: :btree
    t.index ["category"], name: "fk_rails_2b09ab2dc1", using: :btree
    t.index ["company_id"], name: "index_cash_book_records_on_company_id", using: :btree
    t.index ["creator_id"], name: "index_cash_book_records_on_creator_id", using: :btree
    t.index ["deal_id"], name: "index_cash_book_records_on_deal_id", using: :btree
    t.index ["source_type", "source_id"], name: "index_cash_book_records_on_source_type_and_source_id", using: :btree
    t.index ["user_id"], name: "index_cash_book_records_on_user_id", using: :btree
  end

  create_table "cash_flow_budget_categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "name"
    t.integer  "company_id"
    t.boolean  "system"
    t.datetime "deleted_at"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "ancestry"
    t.integer  "kind"
    t.string   "cash_book_category"
    t.index ["ancestry"], name: "index_cash_flow_budget_categories_on_ancestry", using: :btree
    t.index ["company_id"], name: "index_cash_flow_budget_categories_on_company_id", using: :btree
  end

  create_table "cities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "country_id"
    t.index ["country_id"], name: "index_cities_on_country_id", using: :btree
  end

  create_table "clients", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "name"
    t.string   "surname"
    t.datetime "birthday"
    t.integer  "country_id"
    t.integer  "city_id"
    t.integer  "company_id"
    t.integer  "user_id"
    t.string   "address"
    t.string   "email"
    t.string   "work_phone"
    t.string   "cell_phone"
    t.string   "site"
    t.string   "skype"
    t.string   "facebook"
    t.string   "viber"
    t.string   "current_job"
    t.string   "current_department"
    t.string   "current_position"
    t.boolean  "current_is_head"
    t.text     "relations",          limit: 65535
    t.datetime "deleted_at"
    t.datetime "created_at",                                                                    null: false
    t.datetime "updated_at",                                                                    null: false
    t.string   "status"
    t.integer  "manager_id"
    t.integer  "second_manager_id"
    t.string   "visibility"
    t.string   "legal_form"
    t.integer  "discount"
    t.text     "notes",              limit: 65535
    t.string   "legal_entity_name"
    t.string   "legal_entity_email"
    t.integer  "legal_entity_id"
    t.string   "type",                                                       default: "Client"
    t.decimal  "potential_amount",                 precision: 18, scale: 2
    t.string   "source"
    t.string   "recommender"
    t.decimal  "latitude",                         precision: 20, scale: 14
    t.decimal  "longitude",                        precision: 20, scale: 14
    t.integer  "work_phone_code_id"
    t.integer  "cell_phone_code_id"
    t.string   "work_phone_full"
    t.string   "cell_phone_full"
    t.string   "rf_status"
    t.string   "rm_status"
    t.decimal  "deposit",                          precision: 18, scale: 2
    t.index ["cell_phone_code_id"], name: "fk_rails_31ba0611e7", using: :btree
    t.index ["city_id"], name: "index_clients_on_city_id", using: :btree
    t.index ["company_id"], name: "index_clients_on_company_id", using: :btree
    t.index ["country_id"], name: "index_clients_on_country_id", using: :btree
    t.index ["legal_entity_id"], name: "fk_rails_a16ad296f7", using: :btree
    t.index ["manager_id"], name: "fk_rails_21c055f70b", using: :btree
    t.index ["second_manager_id"], name: "fk_rails_056285c6ec", using: :btree
    t.index ["user_id"], name: "index_clients_on_user_id", using: :btree
    t.index ["work_phone_code_id"], name: "fk_rails_319a15d9da", using: :btree
  end

  create_table "companies", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "logo"
    t.string   "name"
    t.text     "description",      limit: 65535
    t.text     "address",          limit: 65535
    t.decimal  "latitude",                       precision: 20, scale: 14
    t.decimal  "longitude",                      precision: 20, scale: 14
    t.string   "site"
    t.string   "email"
    t.string   "phone"
    t.string   "currency"
    t.datetime "created_at",                                                               null: false
    t.datetime "updated_at",                                                               null: false
    t.string   "vat"
    t.integer  "phone_code_id"
    t.boolean  "is_super",                                                 default: false
    t.datetime "deleted_at"
    t.integer  "creator_id"
    t.string   "inn",                                                      default: "0"
    t.string   "kpp"
    t.float    "interest_rate",    limit: 24
    t.json     "receipt_template"
    t.index ["creator_id"], name: "fk_rails_44c88e59f7", using: :btree
    t.index ["phone_code_id"], name: "fk_rails_2c9925806a", using: :btree
  end

  create_table "company_statistics", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "leads_count"
    t.integer  "deals_count"
    t.integer  "clients_count"
    t.integer  "products_count"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "company_id"
    t.integer  "employee_id"
    t.index ["company_id"], name: "index_company_statistics_on_company_id", using: :btree
    t.index ["employee_id"], name: "index_company_statistics_on_employee_id", using: :btree
  end

  create_table "countries", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "country_phone_codes", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "country_name"
    t.string   "code"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "country_code"
    t.index ["country_code"], name: "index_country_phone_codes_on_country_code", unique: true, using: :btree
  end

  create_table "deal_monthly_payments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.float    "amount",  limit: 24
    t.date     "date"
    t.integer  "deal_id"
    t.datetime "paid_at"
    t.index ["deal_id"], name: "index_deal_monthly_payments_on_deal_id", using: :btree
  end

  create_table "deal_products", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "deal_id"
    t.integer  "product_id"
    t.integer  "count"
    t.datetime "deleted_at"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.datetime "returned_at"
    t.decimal  "price",       precision: 18, scale: 2
    t.index ["deal_id"], name: "index_deal_products_on_deal_id", using: :btree
    t.index ["product_id"], name: "index_deal_products_on_product_id", using: :btree
  end

  create_table "deal_products_returns", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "company_id"
    t.integer  "deal_id"
    t.integer  "deal_product_id"
    t.decimal  "sum",             precision: 18, scale: 2
    t.integer  "count"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.index ["company_id"], name: "index_deal_products_returns_on_company_id", using: :btree
    t.index ["deal_id"], name: "index_deal_products_returns_on_deal_id", using: :btree
    t.index ["deal_product_id"], name: "index_deal_products_returns_on_deal_product_id", using: :btree
  end

  create_table "deals", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "number"
    t.string   "contract_number"
    t.string   "check"
    t.string   "invoice"
    t.integer  "company_id"
    t.integer  "client_id"
    t.string   "legal_form"
    t.string   "email"
    t.string   "phone"
    t.string   "address"
    t.datetime "started_at"
    t.datetime "closed_at"
    t.string   "notes"
    t.string   "visibility"
    t.integer  "discount"
    t.decimal  "total_sum",                                  precision: 18, scale: 2
    t.decimal  "check_sum",                                  precision: 18, scale: 2
    t.decimal  "discount_sum",                               precision: 18, scale: 2
    t.decimal  "vat_sum",                                    precision: 18, scale: 2
    t.string   "payment_method"
    t.string   "payment_period"
    t.string   "status"
    t.datetime "single_payment_date"
    t.datetime "monthly_payment_started_at"
    t.datetime "monthly_payment_ended_at"
    t.datetime "contract_started_at"
    t.datetime "contract_ended_at"
    t.integer  "manager_id"
    t.integer  "second_manager_id"
    t.datetime "in_progress_at"
    t.datetime "contract_conclusion_at"
    t.datetime "contract_signed_at"
    t.datetime "won_at"
    t.datetime "payed_at"
    t.datetime "lost_at"
    t.datetime "deleted_at"
    t.datetime "created_at",                                                           null: false
    t.datetime "updated_at",                                                           null: false
    t.decimal  "latitude",                                   precision: 20, scale: 14
    t.decimal  "longitude",                                  precision: 20, scale: 14
    t.integer  "phone_code_id"
    t.datetime "returned_at"
    t.string   "payment_status"
    t.string   "payment_option"
    t.float    "interest_rate",                limit: 24
    t.string   "name"
    t.integer  "cash_flow_budget_category_id"
    t.text     "receipt",                      limit: 65535
    t.decimal  "paid_cash_amount",                           precision: 18, scale: 2
    t.index ["cash_flow_budget_category_id"], name: "index_deals_on_cash_flow_budget_category_id", using: :btree
    t.index ["client_id"], name: "index_deals_on_client_id", using: :btree
    t.index ["company_id"], name: "index_deals_on_company_id", using: :btree
    t.index ["manager_id"], name: "fk_rails_f3f36014cd", using: :btree
    t.index ["phone_code_id"], name: "fk_rails_9f412be0ea", using: :btree
    t.index ["second_manager_id"], name: "fk_rails_0344778ccf", using: :btree
  end

  create_table "departments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "name"
    t.string   "color"
    t.datetime "deleted_at"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "company_id"
    t.boolean  "editable",   default: true
    t.index ["company_id"], name: "index_departments_on_company_id", using: :btree
  end

  create_table "deposit_fees", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.decimal  "amount",     precision: 18, scale: 2
    t.integer  "client_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["client_id"], name: "index_deposit_fees_on_client_id", using: :btree
  end

  create_table "employee_forbidden_product_categories", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer "employee_id",         null: false
    t.integer "product_category_id", null: false
  end

  create_table "employee_phones", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "employee_id"
    t.integer  "country_phone_code_id"
    t.string   "phone"
    t.string   "kind"
    t.datetime "deleted_at"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.index ["country_phone_code_id"], name: "index_employee_phones_on_country_phone_code_id", using: :btree
    t.index ["employee_id"], name: "index_employee_phones_on_employee_id", using: :btree
  end

  create_table "employees", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "pass_number"
    t.integer  "chief_id"
    t.decimal  "salary",                 precision: 18, scale: 2
    t.string   "salary_period"
    t.datetime "activity_changed_at"
    t.datetime "job_started_at"
    t.string   "participation_in_sales",                           default: "no"
    t.datetime "deleted_at"
    t.datetime "created_at",                                                      null: false
    t.datetime "updated_at",                                                      null: false
    t.integer  "user_id"
    t.integer  "company_id"
    t.integer  "user_activity_id"
    t.string   "identity"
    t.string   "card_number"
    t.decimal  "latitude",               precision: 20, scale: 14
    t.decimal  "longitude",              precision: 20, scale: 14
    t.string   "address"
    t.string   "email"
    t.date     "vacation_start"
    t.date     "vacation_end"
    t.string   "cashbox_name"
    t.index ["company_id"], name: "index_employees_on_company_id", using: :btree
    t.index ["user_activity_id"], name: "index_employees_on_user_activity_id", using: :btree
    t.index ["user_id"], name: "index_employees_on_user_id", using: :btree
  end

  create_table "employees_departments", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "department_id"
    t.integer  "employee_id"
    t.boolean  "is_head"
    t.string   "position"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.index ["department_id"], name: "index_employees_departments_on_department_id", using: :btree
    t.index ["employee_id"], name: "index_employees_departments_on_employee_id", using: :btree
  end

  create_table "generic_statuses", id: :string, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "name"
    t.string   "color"
    t.boolean  "system",     default: false
    t.datetime "deleted_at"
    t.integer  "company_id"
    t.string   "type"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "weight",     default: 0
    t.index ["company_id"], name: "index_generic_statuses_on_company_id", using: :btree
  end

  create_table "lead_products", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "lead_id"
    t.integer  "product_id"
    t.integer  "count"
    t.datetime "deleted_at"
    t.datetime "returned_at"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["lead_id"], name: "index_lead_products_on_lead_id", using: :btree
    t.index ["product_id"], name: "index_lead_products_on_product_id", using: :btree
  end

  create_table "notifications", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "type"
    t.integer  "user_id"
    t.string   "content"
    t.integer  "subject_id"
    t.datetime "viewed_at"
    t.datetime "email_sent_at"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["user_id"], name: "index_notifications_on_user_id", using: :btree
  end

  create_table "personal_cash_book_balances", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer "user_id"
    t.integer "record_id",                          null: false
    t.decimal "balance",   precision: 18, scale: 2
    t.index ["record_id"], name: "index_personal_cash_book_balances_on_record_id", using: :btree
    t.index ["user_id"], name: "index_personal_cash_book_balances_on_user_id", using: :btree
  end

  create_table "product_categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "name"
    t.string   "color"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.integer  "company_id"
    t.datetime "deleted_at"
    t.boolean  "has_part_in_average_price", default: true
    t.index ["company_id"], name: "index_product_categories_on_company_id", using: :btree
  end

  create_table "products", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "name"
    t.string   "vendor_code"
    t.string   "barcode"
    t.text     "description",    limit: 65535
    t.decimal  "price",                        precision: 18, scale: 2
    t.datetime "created_at",                                                            null: false
    t.datetime "updated_at",                                                            null: false
    t.datetime "deleted_at"
    t.integer  "company_id"
    t.string   "activity"
    t.string   "delivery_time"
    t.json     "images"
    t.boolean  "is_recommended",                                        default: false
    t.integer  "preview_index",                                         default: 0
    t.decimal  "quantity",                     precision: 5,  scale: 2, default: "0.0"
    t.integer  "category_id"
    t.decimal  "first_cost",                   precision: 18, scale: 2
    t.index ["category_id"], name: "index_products_on_category_id", using: :btree
    t.index ["company_id"], name: "index_products_on_company_id", using: :btree
  end

  create_table "receipts", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "deal_id"
    t.text     "content",       limit: 65535
    t.decimal  "amount",                      precision: 18, scale: 2
    t.json     "deal_products"
    t.integer  "kind"
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
    t.index ["deal_id"], name: "index_receipts_on_deal_id", using: :btree
  end

  create_table "roles", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "name"
    t.string   "resource_type"
    t.integer  "resource_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
    t.index ["name"], name: "index_roles_on_name", using: :btree
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id", using: :btree
  end

  create_table "sales_plan_employees", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "sales_plan_id"
    t.integer  "employee_id"
    t.integer  "part",                comment: "Part of plan assigned to employee"
    t.datetime "deleted_at"
    t.integer  "product_category_id"
    t.index ["employee_id"], name: "index_sales_plan_employees_on_employee_id", using: :btree
    t.index ["product_category_id"], name: "index_sales_plan_employees_on_product_category_id", using: :btree
    t.index ["sales_plan_id"], name: "index_sales_plan_employees_on_sales_plan_id", using: :btree
  end

  create_table "sales_plan_product_categories", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer "sales_plan_id"
    t.integer "product_category_id"
    t.decimal "sum",                 precision: 18, scale: 2
    t.decimal "quantity",            precision: 18, scale: 2
    t.index ["product_category_id"], name: "index_sales_plan_product_categories_on_product_category_id", using: :btree
    t.index ["sales_plan_id"], name: "index_sales_plan_product_categories_on_sales_plan_id", using: :btree
  end

  create_table "sales_plan_products", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "sales_plan_id"
    t.integer  "product_id"
    t.decimal  "quantity",      precision: 10, scale: 2, default: "0.0"
    t.decimal  "sum",           precision: 10, scale: 2, default: "0.0"
    t.datetime "deleted_at"
    t.index ["product_id"], name: "index_sales_plan_products_on_product_id", using: :btree
    t.index ["sales_plan_id"], name: "index_sales_plan_products_on_sales_plan_id", using: :btree
  end

  create_table "sales_plans", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer  "company_id"
    t.integer  "kind",                                                comment: "Plan by quantity/sum"
    t.integer  "year",                                                comment: "Year of plan"
    t.integer  "month",                                               comment: "Month of plan"
    t.datetime "deleted_at"
    t.decimal  "sum",        precision: 10, scale: 2, default: "0.0"
    t.decimal  "quantity",   precision: 10, scale: 2, default: "0.0"
    t.index ["company_id"], name: "index_sales_plans_on_company_id", using: :btree
  end

  create_table "system_events", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "kind"
    t.integer  "user_id"
    t.integer  "company_id"
    t.string   "subject_type"
    t.integer  "subject_id"
    t.text     "general_description", limit: 65535
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.text     "local_description",   limit: 65535
    t.json     "meta_data"
    t.index ["company_id"], name: "index_system_events_on_company_id", using: :btree
    t.index ["subject_type", "subject_id"], name: "index_system_events_on_subject_type_and_subject_id", using: :btree
    t.index ["user_id"], name: "index_system_events_on_user_id", using: :btree
  end

  create_table "tasks", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "title"
    t.text     "description",   limit: 65535
    t.integer  "creator_id"
    t.integer  "executor_id"
    t.integer  "company_id"
    t.json     "attachments"
    t.boolean  "important"
    t.boolean  "urgent"
    t.string   "assessment"
    t.string   "category"
    t.date     "start_date"
    t.date     "due_date"
    t.integer  "estimate"
    t.json     "checklist"
    t.integer  "client_id"
    t.string   "status"
    t.datetime "started_at"
    t.datetime "done_at"
    t.float    "spent_time",    limit: 24
    t.integer  "renew_count",                 default: 0
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "assistant_id"
    t.datetime "last_pause_at"
    t.index ["assistant_id"], name: "index_tasks_on_assistant_id", using: :btree
    t.index ["client_id"], name: "index_tasks_on_client_id", using: :btree
    t.index ["company_id"], name: "index_tasks_on_company_id", using: :btree
    t.index ["creator_id"], name: "index_tasks_on_creator_id", using: :btree
    t.index ["executor_id"], name: "index_tasks_on_executor_id", using: :btree
  end

  create_table "tasks_watchers", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer "task_id",     null: false
    t.integer "employee_id", null: false
    t.index ["employee_id"], name: "index_tasks_watchers_on_employee_id", using: :btree
    t.index ["task_id"], name: "index_tasks_watchers_on_task_id", using: :btree
  end

  create_table "user_activities", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "name"
    t.string   "color"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_company_invitations", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.datetime "sent_at"
    t.datetime "confirmed_at"
    t.integer  "company_id"
    t.string   "role"
    t.string   "email"
    t.string   "password"
    t.string   "position"
    t.integer  "department_id"
    t.index ["company_id"], name: "index_user_company_invitations_on_company_id", using: :btree
    t.index ["department_id"], name: "fk_rails_e34ce9da7c", using: :btree
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.string   "provider",                                                       default: "email", null: false
    t.string   "uid",                                                            default: "",      null: false
    t.string   "encrypted_password",                                             default: "",      null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                                                  default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "image"
    t.string   "name"
    t.string   "surname"
    t.string   "email"
    t.string   "public_email"
    t.date     "birthday"
    t.string   "skype"
    t.string   "whatsapp"
    t.string   "viber"
    t.string   "facebook_link"
    t.string   "cell_phone"
    t.string   "lang"
    t.string   "status"
    t.text     "tokens",                 limit: 65535
    t.datetime "created_at",                                                                       null: false
    t.datetime "updated_at",                                                                       null: false
    t.integer  "appeal_id"
    t.string   "sex"
    t.integer  "city_id"
    t.decimal  "balance",                              precision: 10,            default: 0
    t.decimal  "bonus",                                precision: 10,            default: 0
    t.string   "facebook_identity"
    t.integer  "cell_phone_code_id"
    t.datetime "deleted_at"
    t.datetime "last_seen"
    t.string   "facebook_image_url"
    t.decimal  "latitude",                             precision: 20, scale: 14
    t.decimal  "longitude",                            precision: 20, scale: 14
    t.string   "address"
    t.boolean  "fully_registered",                                               default: false
    t.index ["appeal_id"], name: "index_users_on_appeal_id", using: :btree
    t.index ["cell_phone_code_id"], name: "fk_rails_adc9b170f0", using: :btree
    t.index ["city_id"], name: "index_users_on_city_id", using: :btree
    t.index ["email"], name: "index_users_on_email", using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true, using: :btree
  end

  create_table "users_roles", id: false, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci" do |t|
    t.integer "user_id"
    t.integer "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id", using: :btree
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree
    t.index ["user_id"], name: "index_users_roles_on_user_id", using: :btree
  end

  add_foreign_key "calendar_events", "calendar_events", column: "original_event_id"
  add_foreign_key "cash_book_records", "cash_book_categories", column: "category"
  add_foreign_key "cash_book_records", "cash_flow_budget_categories"
  add_foreign_key "cash_book_records", "employees", column: "creator_id"
  add_foreign_key "cash_flow_budget_categories", "companies"
  add_foreign_key "clients", "companies", column: "legal_entity_id"
  add_foreign_key "clients", "country_phone_codes", column: "cell_phone_code_id"
  add_foreign_key "clients", "country_phone_codes", column: "work_phone_code_id"
  add_foreign_key "clients", "employees", column: "manager_id"
  add_foreign_key "clients", "employees", column: "second_manager_id"
  add_foreign_key "companies", "country_phone_codes", column: "phone_code_id"
  add_foreign_key "companies", "users", column: "creator_id"
  add_foreign_key "company_statistics", "companies"
  add_foreign_key "company_statistics", "employees"
  add_foreign_key "deal_products_returns", "companies"
  add_foreign_key "deal_products_returns", "deal_products"
  add_foreign_key "deal_products_returns", "deals"
  add_foreign_key "deals", "cash_flow_budget_categories"
  add_foreign_key "deals", "country_phone_codes", column: "phone_code_id"
  add_foreign_key "deals", "employees", column: "manager_id"
  add_foreign_key "deals", "employees", column: "second_manager_id"
  add_foreign_key "departments", "companies"
  add_foreign_key "employee_phones", "country_phone_codes"
  add_foreign_key "employee_phones", "employees"
  add_foreign_key "employees", "companies"
  add_foreign_key "employees", "user_activities"
  add_foreign_key "employees", "users"
  add_foreign_key "employees_departments", "employees"
  add_foreign_key "notifications", "users"
  add_foreign_key "personal_cash_book_balances", "cash_book_records", column: "record_id"
  add_foreign_key "product_categories", "companies"
  add_foreign_key "products", "companies"
  add_foreign_key "products", "product_categories", column: "category_id"
  add_foreign_key "receipts", "deals"
  add_foreign_key "system_events", "companies"
  add_foreign_key "system_events", "users"
  add_foreign_key "tasks", "clients"
  add_foreign_key "tasks", "companies"
  add_foreign_key "tasks", "employees", column: "assistant_id"
  add_foreign_key "tasks", "employees", column: "executor_id"
  add_foreign_key "tasks", "users", column: "creator_id"
  add_foreign_key "user_company_invitations", "companies"
  add_foreign_key "user_company_invitations", "departments"
  add_foreign_key "users", "country_phone_codes", column: "cell_phone_code_id"
end
