class CreateSalesPlanProductCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :sales_plan_product_categories do |t|
      t.references :sales_plan
      t.references :product_category
      t.decimal :sum, precision: 18, scale: 2
    end
  end
end
