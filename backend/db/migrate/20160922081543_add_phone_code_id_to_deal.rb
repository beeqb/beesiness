class AddPhoneCodeIdToDeal < ActiveRecord::Migration[5.0]
  def change
    add_column :deals, :phone_code_id, :integer
    add_foreign_key :deals, :country_phone_codes, column: :phone_code_id, primary_key: 'id'
  end
end
