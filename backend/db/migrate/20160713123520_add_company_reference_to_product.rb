class AddCompanyReferenceToProduct < ActiveRecord::Migration[5.0]
  def change
    add_reference :products, :company, index: true
    add_foreign_key :products, :companies
  end
end
