class AddFullyRegisteredToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :fully_registered, :boolean, default: false

    execute "UPDATE users SET fully_registered = 1"
  end
end
