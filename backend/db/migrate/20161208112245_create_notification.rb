class CreateNotification < ActiveRecord::Migration[5.0]
  def change
    create_table :notifications do |t|
      t.string :type
      t.references :user, foreign_key: true, index: true
      t.string :content

      t.integer :subject_id

      t.datetime :viewed_at
      t.datetime :email_sent_at

      t.timestamps
    end
  end
end
