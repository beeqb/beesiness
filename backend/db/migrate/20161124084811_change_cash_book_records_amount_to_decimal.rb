class ChangeCashBookRecordsAmountToDecimal < ActiveRecord::Migration[5.0]
  def change
    change_column(
      :cash_book_records,
      :amount,
      :decimal,
      precision: 18,
      scale: 2
    )

    change_column(
      :cash_book_records,
      :balance,
      :decimal,
      precision: 18,
      scale: 2
    )
  end
end
