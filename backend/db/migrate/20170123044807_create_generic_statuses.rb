class CreateGenericStatuses < ActiveRecord::Migration[5.0]
  def change
    drop_table :lead_statuses, if_exists: true

    create_table :generic_statuses, id: :string do |t|
      t.string :name
      t.string :color
      t.boolean :system, default: false
      t.datetime :deleted_at
      t.references :company
      t.string :type

      t.timestamps
    end

    reversible do |dir|
      dir.up do
        execute \
          "INSERT INTO generic_statuses (id,
            color,
            system,
            created_at,
            updated_at,
            type
          ) "\
          "VALUES ('lead_new', '#f05050', 1, NOW(), NOW(), 'LeadStatus'), "\
          "('lead_in_work', '#27c24c', 1, NOW(), NOW(), 'LeadStatus'), "\
          "('lead_trash', '#edf1f2', 1, NOW(), NOW(), 'LeadStatus')"

        execute \
          "INSERT INTO generic_statuses (id,
            color,
            system,
            created_at,
            updated_at,
            type
          ) "\
          "VALUES ('client_new', '#f05050', 1, NOW(), NOW(), 'ClientStatus'), "\
          "('client_current', '#23b7e5', 1, NOW(), NOW(), 'ClientStatus'), "\
          "('client_permanent', '#7266ba', 1, NOW(), NOW(), 'ClientStatus'), "\
          "('client_special', '#27c24c', 1, NOW(), NOW(), 'ClientStatus'), "\
          "('client_no', '#58666e', 1, NOW(), NOW(), 'ClientStatus')"

        execute \
          "INSERT INTO generic_statuses (id,
            color,
            system,
            created_at,
            updated_at,
            type
          ) "\
          "VALUES ('in_progress', '#7266ba', 1, NOW(), NOW(), 'DealStatus'), "\
          "('contract_conclusion', '#fad733', 1, NOW(), NOW(), 'DealStatus'), "\
          "('contract_signed', '#fad733', 1, NOW(), NOW(), 'DealStatus'), "\
          "('won', '#27c24c', 1, NOW(), NOW(), 'DealStatus'), "\
          "('payed', '#27c24c', 1, NOW(), NOW(), 'DealStatus'), "\
          "('lost', '#f05050', 1, NOW(), NOW(), 'DealStatus'), "\
          "('returned', '#f0505f', 1, NOW(), NOW(), 'DealStatus')"

        execute \
          "INSERT INTO generic_statuses (id,
            color,
            system,
            created_at,
            updated_at,
            type
          ) "\
          "VALUES ('supplier_new', '#f05050', 1, NOW(), NOW(), 'SupplierStatus'), "\
          "('supplier_current', '#23b7e5', 1, NOW(), NOW(), 'SupplierStatus'), "\
          "('supplier_permanent', '#7266ba', 1, NOW(), NOW(), 'SupplierStatus'), "\
          "('supplier_special', '#27c24c', 1, NOW(), NOW(), 'SupplierStatus'), "\
          "('supplier_no', '#58666e', 1, NOW(), NOW(), 'SupplierStatus')"
      end
    end
  end
end
