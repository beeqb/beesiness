class CreateJoinTableCashBookRecordsProductCategories < ActiveRecord::Migration[5.0]
  def change
    create_join_table(
      :cash_book_records,
      :product_categories,
      column_options: {null: false}
    ) do |t|
      t.index(
        [:cash_book_record_id, :product_category_id],
        name: 'by_cash_book_product_category'
      )
    end
  end
end
