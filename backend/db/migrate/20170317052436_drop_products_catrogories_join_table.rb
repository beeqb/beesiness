class DropProductsCatrogoriesJoinTable < ActiveRecord::Migration[5.0]
  def up
    execute "UPDATE products p SET category_id = (SELECT product_category_id
      FROM products_product_categories WHERE product_id = p.id LIMIT 1)"
    
    drop_table :products_product_categories
  end
end
