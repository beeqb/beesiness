class AddPaidAtToDealMonthlyPayments < ActiveRecord::Migration[5.0]
  def change
    add_column :deal_monthly_payments, :paid_at, :datetime
  end
end
