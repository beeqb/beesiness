class AddCityIdToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :sex, :string
    add_reference :users, :city
  end
end
