class AddIsoCodeToCountryPhoneCodes < ActiveRecord::Migration[5.0]
  def change
    add_column :country_phone_codes, :country_code, :string
    add_index :country_phone_codes, :country_code, unique: true
  end
end
