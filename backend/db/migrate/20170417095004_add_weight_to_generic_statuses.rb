class AddWeightToGenericStatuses < ActiveRecord::Migration[5.0]
	def change
		add_column :generic_statuses, :weight, :integer, default: 0
	end
end
