class CreateCompanies < ActiveRecord::Migration[5.0]
  def change
    create_table :companies do |t|
      t.string :logo
      t.string :name
      t.string :description
      t.text :address
      t.decimal :latitude
      t.decimal :longitude
      t.string :site
      t.string :email
      t.string :phone
      t.string :currency

      t.timestamps
    end
  end
end
