class CreateDealProductsReturns < ActiveRecord::Migration[5.0]
  def change
    create_table :deal_products_returns do |t|
      t.references :company, foreign_key: true
      t.references :deal, foreign_key: true
      t.references :deal_product, foreign_key: true
      t.decimal :sum, precision: 18, scale: 2
      t.integer :count

      t.timestamps
    end
  end
end
