class MakeCashBookRecordBillKindNullable < ActiveRecord::Migration[5.0]
  def change
    change_column_null(:cash_book_records, :bill_kind, true)
  end
end
