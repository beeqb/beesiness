class AddFullPhonesToClients < ActiveRecord::Migration[5.0]
  def up
    add_column :clients, :work_phone_full, :string
    add_column :clients, :cell_phone_full, :string


    db.execute "UPDATE clients c"\
      " INNER JOIN country_phone_codes wpc ON c.work_phone_code_id = wpc.id"\
      " INNER JOIN country_phone_codes cpc ON c.cell_phone_code_id = cpc.id"\
      " SET c.work_phone_full = CONCAT_WS(' ', wpc.code, c.work_phone),"\
      " c.cell_phone_full = CONCAT_WS(' ', cpc.code, c.cell_phone)"
  end

  def down
    remove_column :clients, :work_phone_full
    remove_column :clients, :cell_phone_full
  end

  private

  def db
    ActiveRecord::Base.connection
  end
end
