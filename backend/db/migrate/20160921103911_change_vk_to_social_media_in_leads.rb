class ChangeVkToSocialMediaInLeads < ActiveRecord::Migration[5.0]
  def up
    db.execute "UPDATE clients"\
      " SET source = 'social_media'"\
      " WHERE type = 'Lead' AND source = 'vk'"
  end

  def down
    db.execute "UPDATE clients"\
      " SET source = 'vk'"\
      " WHERE type = 'Lead' AND source = 'social_media'"
  end

  private

  def db
    ActiveRecord::Base.connection
  end
end
