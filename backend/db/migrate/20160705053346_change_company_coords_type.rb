class ChangeCompanyCoordsType < ActiveRecord::Migration[5.0]
  def change
    change_column :companies, :latitude, :decimal, precision: 20, scale: 14
    change_column :companies, :longitude, :decimal, precision: 20, scale: 14
  end
end
