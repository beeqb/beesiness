class AddDeletedAtColumnToTables < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :deleted_at, :datetime
    add_column :product_categories, :deleted_at, :datetime
    add_column :companies, :deleted_at, :datetime
    add_column :users, :deleted_at, :datetime
  end
end
