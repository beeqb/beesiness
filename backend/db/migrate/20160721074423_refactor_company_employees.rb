class RefactorCompanyEmployees < ActiveRecord::Migration[5.0]
  def change
    rename_table :business_cards, :employees
    drop_table :users_companies
    rename_table :users_departments, :employees_departments

    remove_foreign_key :users, :user_activities
    remove_column :users, :user_activity_id

    remove_foreign_key :users, :country_phone_codes

    remove_column :users, :work_phone_code_id
    remove_column :users, :work_phone
    remove_column :users, :identity
    remove_column :users, :card_number

    add_column :employees, :work_phone, :string
    add_column :employees, :identity, :string
    add_column :employees, :card_number, :string

    add_reference :employees, :country_phone_code, index: true
    add_foreign_key :employees, :country_phone_codes

    remove_column :employees_departments, :user_id

    add_reference :employees_departments, :employee, index: true
    add_foreign_key :employees_departments, :employees

  end
end
