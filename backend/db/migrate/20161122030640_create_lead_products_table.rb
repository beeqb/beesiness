class CreateLeadProductsTable < ActiveRecord::Migration[5.0]
  def change
    create_table :lead_products do |t|
      t.references :lead
      t.references :product
      t.integer :count

      t.datetime :deleted_at
      t.datetime :returned_at
      t.timestamps
    end
  end
end
