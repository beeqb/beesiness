class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :name
      t.string :currency
      t.string :vendor_code
      t.string :barcode
      t.string :description
      t.decimal :price, precision: 18, scale: 2

      t.timestamps
    end

    add_reference :products, :product_category, index: true
    add_foreign_key :products, :product_categories
  end
end
