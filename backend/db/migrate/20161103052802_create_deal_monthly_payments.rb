class CreateDealMonthlyPayments < ActiveRecord::Migration[5.0]
  def change
    create_table :deal_monthly_payments do |t|
      t.float :amount
      t.date :date
      t.references :deal
    end
  end
end
