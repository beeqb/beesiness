class AddKindToCashFlowBudgetCategories < ActiveRecord::Migration[5.0]
  def change
    add_column :cash_flow_budget_categories, :kind, :integer
  end
end
