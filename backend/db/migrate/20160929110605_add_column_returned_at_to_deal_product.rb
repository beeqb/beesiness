class AddColumnReturnedAtToDealProduct < ActiveRecord::Migration[5.0]
  def change
    add_column :deal_products, :returned_at, :datetime
    add_column :deals, :returned_at, :datetime
  end
end
