class AddReceiptToDeals < ActiveRecord::Migration[5.0]
  def change
    add_column :deals, :receipt, :text
  end
end
