class CreateCountryPhoneCodes < ActiveRecord::Migration[5.0]
  def change
    create_table :country_phone_codes do |t|
      t.string :country_name
      t.string :code
      t.timestamps
    end
  end
end
