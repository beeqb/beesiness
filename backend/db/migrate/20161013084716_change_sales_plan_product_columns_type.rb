class ChangeSalesPlanProductColumnsType < ActiveRecord::Migration[5.0]
  def change
    change_column :sales_plan_products, :sum, :decimal,
                  precision: 10,
                  scale: 2,
                  default: 0
    change_column :sales_plan_products, :quantity, :decimal,
                  precision: 10,
                  scale: 2,
                  default: 0
  end
end
