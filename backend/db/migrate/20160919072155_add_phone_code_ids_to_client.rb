class AddPhoneCodeIdsToClient < ActiveRecord::Migration[5.0]
  def change
    add_column :clients, :work_phone_code_id, :integer
    add_column :clients, :cell_phone_code_id, :integer

    add_foreign_key :clients, :country_phone_codes, column: :work_phone_code_id
    add_foreign_key :clients, :country_phone_codes, column: :cell_phone_code_id
  end
end
