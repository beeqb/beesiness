class AddReceiptTemplateToCompanies < ActiveRecord::Migration[5.0]
  def change
    add_column :companies, :receipt_template, :json
  end
end
