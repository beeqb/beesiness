class SetPhoneCodeIdToAllDeals < ActiveRecord::Migration[5.0]
  def change
    Deal.all.each do |deal|
      if deal.phone_code_id.nil?
        deal.phone_code_id = CountryPhoneCode.first.id
        deal.save!
      end
    end
  end
end
