class ChangeBackCalendarEventsStartAndEnd < ActiveRecord::Migration[5.0]
  def change
    change_column :calendar_events, :start, :datetime
    change_column :calendar_events, :end, :datetime
  end
end
