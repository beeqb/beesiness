class CreateSystemEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :system_events do |t|
      t.string :kind
      t.references :user, foreign_key: true, index: true
      t.references :company, foreign_key: true, index: true
      t.references :subject, polymorphic: true
      t.text :description
      t.timestamps
    end
  end
end
