class AddAppealToUser < ActiveRecord::Migration[5.0]
  def change
    add_reference :users, :appeal, index: true
    add_foreign_key :users, :appeals
  end
end
