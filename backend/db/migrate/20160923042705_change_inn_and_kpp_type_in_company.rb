class ChangeInnAndKppTypeInCompany < ActiveRecord::Migration[5.0]
  def change
    change_column :companies, :inn, :string
    change_column :companies, :kpp, :string
  end
end
