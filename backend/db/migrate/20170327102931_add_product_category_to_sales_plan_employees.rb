class AddProductCategoryToSalesPlanEmployees < ActiveRecord::Migration[5.0]
  def change
    add_reference :sales_plan_employees, :product_category
  end
end
