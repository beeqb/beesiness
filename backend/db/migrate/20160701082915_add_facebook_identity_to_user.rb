class AddFacebookIdentityToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :facebook_identity, :string
  end
end
