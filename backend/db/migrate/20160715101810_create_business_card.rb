class CreateBusinessCard < ActiveRecord::Migration[5.0]
  def change
    create_table :business_cards do |t|
      t.string :pass_number
      t.integer :chief_id, :default => nil
      t.decimal :salary, precision: 18, scale: 2
      t.string :salary_period
      t.datetime :activity_changed_at
      t.datetime :job_started_at
      t.string :participation_in_sales, :default => 'no'
      t.datetime :deleted_at

      t.timestamps
    end

    add_reference :business_cards, :user, index: true
    add_foreign_key :business_cards, :users

    add_reference :business_cards, :company, index: true
    add_foreign_key :business_cards, :companies

    add_reference :business_cards, :user_activity, index: true
    add_foreign_key :business_cards, :user_activities

    remove_column :users, :chief_id
    remove_column :users, :pass_number
  end
end
