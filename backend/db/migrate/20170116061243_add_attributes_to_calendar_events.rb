class AddAttributesToCalendarEvents < ActiveRecord::Migration[5.0]
  def change
    add_reference(
      :calendar_events,
      :original_event,
      foreign_key: {to_table: :calendar_events}
    )
    add_column :calendar_events, :recurring_end_at, :date
  end
end
