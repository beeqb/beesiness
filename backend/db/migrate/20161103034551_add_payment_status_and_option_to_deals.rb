class AddPaymentStatusAndOptionToDeals < ActiveRecord::Migration[5.0]
  def change
    # статус оплаты: "оплачено/ожидает оплаты"
    add_column :deals, :payment_status, :string
    # вариант оплаты: "наличные/карта/счет"
    add_column :deals, :payment_option, :string
  end
end
