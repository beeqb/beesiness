class AddDealToCashBookRecords < ActiveRecord::Migration[5.0]
  def change
    add_reference :cash_book_records, :deal, index: true
  end
end
