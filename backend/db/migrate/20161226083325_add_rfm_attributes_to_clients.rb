class AddRfmAttributesToClients < ActiveRecord::Migration[5.0]
  def change
    add_column :clients, :rf_status, :string
    add_column :clients, :rm_status, :string
  end
end
