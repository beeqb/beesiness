class AlterUserCompanyInvitations < ActiveRecord::Migration[5.0]
  def change
    add_column :user_company_invitations, :position, :string

    add_column :user_company_invitations, :department_id, :integer
    add_foreign_key :user_company_invitations, :departments
  end
end
