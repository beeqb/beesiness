class AddSumToSalesPlan < ActiveRecord::Migration[5.0]
  def change
    add_column :sales_plans, :sum, :decimal,
               precision: 10,
               scale: 2,
               default: 0

    add_column :sales_plans, :quantity, :decimal,
               precision: 10,
               scale: 2,
               default: 0
  end
end
