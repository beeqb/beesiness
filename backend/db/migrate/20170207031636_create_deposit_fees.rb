class CreateDepositFees < ActiveRecord::Migration[5.0]
  def change
    create_table :deposit_fees do |t|
      t.decimal :amount, precision: 18, scale: 2
      t.references :client

      t.timestamps
    end
  end
end
