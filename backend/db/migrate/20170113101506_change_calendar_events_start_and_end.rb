class ChangeCalendarEventsStartAndEnd < ActiveRecord::Migration[5.0]
  def change
    change_column :calendar_events, :start, :date
    change_column :calendar_events, :end, :date
  end
end
