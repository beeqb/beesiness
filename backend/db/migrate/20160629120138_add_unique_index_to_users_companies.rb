class AddUniqueIndexToUsersCompanies < ActiveRecord::Migration[5.0]
  def change
    add_index :users_companies, [:user_id, :company_id], :unique => true
  end
end
