class CreateEmployeePhones < ActiveRecord::Migration[5.0]
  def change
    create_table :employee_phones do |t|
      t.references :employee, foreign_key: true
      t.references :country_phone_code, foreign_key: true
      t.string :phone
      t.string :kind
      t.datetime :deleted_at

      t.timestamps
    end

    remove_foreign_key :employees, :country_phone_codes
    remove_column :employees, :country_phone_code_id

    remove_column :employees, :work_phone
  end
end
