class CreateProductsProductCategories < ActiveRecord::Migration[5.0]
  def change
    create_table :products_product_categories do |t|
      t.references :product
      t.references :product_category
    end

    remove_foreign_key :products, :product_categories
    remove_column :products, :product_category_id

    add_foreign_key :products_product_categories, :products
    add_foreign_key :products_product_categories, :product_categories
  end
end
