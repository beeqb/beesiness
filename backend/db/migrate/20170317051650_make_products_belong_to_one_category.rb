class MakeProductsBelongToOneCategory < ActiveRecord::Migration[5.0]
  def change
    add_reference(
      :products,
      :category,
      foreign_key: { to_table: :product_categories }
    )
  end
end
