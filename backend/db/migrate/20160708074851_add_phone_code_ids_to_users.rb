class AddPhoneCodeIdsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :work_phone_code_id, :integer
    add_column :users, :cell_phone_code_id, :integer
    add_column :companies, :phone_code_id, :integer

    add_foreign_key :users, :country_phone_codes, column: :work_phone_code_id, primary_key: "id"
    add_foreign_key :users, :country_phone_codes, column: :cell_phone_code_id, primary_key: "id"
    add_foreign_key :companies, :country_phone_codes, column: :phone_code_id, primary_key: "id"
  end
end
