class CreateDepartments < ActiveRecord::Migration[5.0]
  def change
    create_table :departments do |t|
      t.string :name
      t.string :color
      t.datetime :deleted_at

      t.timestamps
    end

    add_reference :departments, :company, index: true
    add_foreign_key :departments, :companies
  end
end
