class AddSourceToCashBookRecord < ActiveRecord::Migration[5.0]
  def change
    add_reference(
      :cash_book_records,
      :source,
      polymorphic: true,
      index: true
    )
  end
end
