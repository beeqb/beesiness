class AddUserAndDeletedAtToCalendarEvents < ActiveRecord::Migration[5.0]
  def change
    add_reference :calendar_events, :user
    add_column :calendar_events, :deleted_at, :datetime
  end
end
