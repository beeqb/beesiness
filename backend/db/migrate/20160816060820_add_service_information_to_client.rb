class AddServiceInformationToClient < ActiveRecord::Migration[5.0]
  def change
    add_column :clients, :manager_id, :integer
    add_column :clients, :second_manager_id, :integer
    add_column :clients, :visibility, :string
    add_column :clients, :legal_form, :string
    add_column :clients, :discount, :integer
    add_column :clients, :notes, :text

    add_foreign_key :clients, :employees, column: :manager_id
    add_foreign_key :clients, :employees, column: :second_manager_id

  end
end
