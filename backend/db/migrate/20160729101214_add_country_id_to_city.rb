class AddCountryIdToCity < ActiveRecord::Migration[5.0]
  def change
    add_reference :cities, :country
  end
end
