class CreatePersonalCashBookBalance < ActiveRecord::Migration[5.0]
  def change
    create_table :personal_cash_book_balances do |t|
      t.references :user
      t.references(
        :record,
        null: false,
        foreign_key: {
          to_table: :cash_book_records,
        }
      )
      t.decimal :balance, precision: 18, scale: 2
    end
  end
end
