class AddFieldsToClient < ActiveRecord::Migration[5.0]
  def change
    add_column :clients, :legal_entity_name, :string
    add_column :clients, :legal_entity_email, :string

    add_column :clients, :legal_entity_id, :integer
    add_foreign_key :clients, :companies, column: :legal_entity_id
  end
end
