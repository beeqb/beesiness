class CreateLeadStatusesTable < ActiveRecord::Migration[5.0]
  def change
    create_table :lead_statuses, id: :string do |t|
      t.string :name
      t.string :color
      t.boolean :system, default: false
      t.datetime :deleted_at
      t.references :company

      t.timestamps
    end

    reversible do |dir|
      dir.up do
        execute \
          "INSERT INTO lead_statuses (id, color, system, created_at, updated_at) "\
          "VALUES ('lead_new', '#f05050', 1, NOW(), NOW()), "\
          "('lead_in_work', '#27c24c', 1, NOW(), NOW()), "\
          "('lead_trash', '#edf1f2', 1, NOW(), NOW())"
      end
    end
  end
end
