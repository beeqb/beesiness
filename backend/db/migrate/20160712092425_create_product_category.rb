class CreateProductCategory < ActiveRecord::Migration[5.0]
  def change
    create_table :product_categories do |t|
      t.string :name
      t.string :color
      t.timestamps
    end

    add_reference :product_categories, :company, index: true
    add_foreign_key :product_categories, :companies
  end
end
