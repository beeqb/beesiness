class AddEditableToDepartment < ActiveRecord::Migration[5.0]
  def change
    add_column :departments, :editable, :boolean, default: 1
  end
end
