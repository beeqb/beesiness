class CreateUserCompanyInvitation < ActiveRecord::Migration[5.0]
  def change
    create_table :user_company_invitations do |t|
      t.datetime :invitation_sent_at
      t.datetime :invitation_confirmed_at
    end

    add_reference :user_company_invitations, :user, index: true
    add_foreign_key :user_company_invitations, :users

    add_reference :user_company_invitations, :company, index: true
    add_foreign_key :user_company_invitations, :companies

    add_reference :user_company_invitations, :role, index: true
    add_foreign_key :user_company_invitations, :roles

  end
end
