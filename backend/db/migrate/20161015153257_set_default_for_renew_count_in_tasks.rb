class SetDefaultForRenewCountInTasks < ActiveRecord::Migration[5.0]
  def up
    change_column :tasks, :renew_count, :integer, default: 0

    db.execute 'UPDATE tasks SET renew_count = 0 WHERE renew_count IS NULL'
  end

  def down
    change_column :tasks, :renew_count, :integer

    db.execute 'UPDATE tasks SET renew_count = NULL WHERE renew_count = 0'
  end

  private

  def db
    ActiveRecord::Base.connection
  end
end
