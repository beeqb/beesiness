class CreateCashBookCategories < ActiveRecord::Migration[5.0]
  def up
    create_table :cash_book_categories, id: :string do |t|
      t.string :name
      t.boolean :system, default: false
      t.boolean :expense, default: false
      t.references :company
      t.datetime :deleted_at

      t.timestamps
    end

    change_column :cash_book_records, :category, :string

    execute \
      "INSERT INTO cash_book_categories (
        id,
        system,
        expense,
        created_at,
        updated_at
      ) "\
      "VALUES ('income', 1, 0, NOW(), NOW()), "\
      "('outcome', 1, 1, NOW(), NOW()), "\
      "('loan', 1, 0, NOW(), NOW()), "\
      "('loan_return', 1, 1, NOW(), NOW())"

    execute \
      "UPDATE cash_book_records "\
      "SET category = 'income' "\
      "WHERE category = '0'"

    execute \
      "UPDATE cash_book_records "\
      "SET category = 'outcome' "\
      "WHERE category = '1'"

    execute \
      "UPDATE cash_book_records "\
      "SET category = 'loan' "\
      "WHERE category = '2'"

    execute \
      "UPDATE cash_book_records "\
      "SET category = 'loan_return' "\
      "WHERE category = '3'"

    add_foreign_key(
      :cash_book_records,
      :cash_book_categories,
      column: :category
    )
  end

  def down
    execute \
      "UPDATE cash_book_records "\
      "SET category = 0 "\
      "WHERE category = 'income'"

    execute \
      "UPDATE cash_book_records "\
      "SET category = 1 "\
      "WHERE category = 'outcome'"

    execute \
      "UPDATE cash_book_records "\
      "SET category = 2 "\
      "WHERE category = 'loan'"

    execute \
      "UPDATE cash_book_records "\
      "SET category = 3 "\
      "WHERE category = 'loan_return'"

    change_column :cash_book_records, :category, :integer

    drop_foreign_key(:cash_book_records, :cash_book_categories)
  end
end
