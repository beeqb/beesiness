class AddAncestryToCashFlowBudgetCategories < ActiveRecord::Migration[5.0]
  def change
    add_column :cash_flow_budget_categories, :ancestry, :string
    add_index :cash_flow_budget_categories, :ancestry
  end
end
