class AddPreviewIndexToProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :preview_index, :integer, default: 0
  end
end
