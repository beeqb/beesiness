class AddAttributesToCashBookRecord < ActiveRecord::Migration[5.0]
  def change
    add_column :cash_book_records, :personal, :boolean, default: false
    add_column(
      :cash_book_records,
      :personal_balance,
      :decimal,
      precision: 18,
      scale: 2
    )
    add_reference :cash_book_records, :user
  end
end
