class CreateCompanyStatistics < ActiveRecord::Migration[5.0]
  def change
    create_table :company_statistics do |t|
      t.integer :leads_count
      t.integer :deals_count
      t.integer :clients_count
      t.integer :products_count
      t.timestamps
    end

    add_reference :company_statistics, :company, foreign_key: true
    add_reference :company_statistics, :employee, foreign_key: true
  end
end
