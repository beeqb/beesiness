class AddActivityAndDeliveryTimeToProduct < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :activity, :string
    add_column :products, :delivery_time, :string
  end
end
