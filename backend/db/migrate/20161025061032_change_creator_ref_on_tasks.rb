class ChangeCreatorRefOnTasks < ActiveRecord::Migration[5.0]
  def change
    remove_foreign_key :tasks, column: :creator_id
    add_foreign_key :tasks, :users, column: :creator_id
  end
end
