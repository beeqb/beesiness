class AddHasPartInAveragePriceToProductCategories < ActiveRecord::Migration[5.0]
  def change
    add_column(
      :product_categories,
      :has_part_in_average_price,
      :boolean,
      default: true
    )
  end
end
