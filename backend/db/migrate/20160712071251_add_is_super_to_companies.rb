class AddIsSuperToCompanies < ActiveRecord::Migration[5.0]
  def change
    add_column :companies, :is_super, :boolean, :default => false
  end
end
