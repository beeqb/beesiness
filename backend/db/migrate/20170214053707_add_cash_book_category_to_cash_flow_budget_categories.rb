class AddCashBookCategoryToCashFlowBudgetCategories < ActiveRecord::Migration[5.0]
  def change
    add_column :cash_flow_budget_categories, :cash_book_category, :string
  end
end
