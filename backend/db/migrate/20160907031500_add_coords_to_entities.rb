class AddCoordsToEntities < ActiveRecord::Migration[5.0]
  def change
    add_column :clients, :latitude, :decimal, precision: 20, scale: 14
    add_column :clients, :longitude, :decimal, precision: 20, scale: 14

    add_column :deals, :latitude, :decimal, precision: 20, scale: 14
    add_column :deals, :longitude, :decimal, precision: 20, scale: 14
  end
end
