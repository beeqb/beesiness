class AddCreatorToCompany < ActiveRecord::Migration[5.0]
  def change
    add_column :companies, :creator_id, :integer
    add_foreign_key :companies, :users, column: :creator_id, primary_key: 'id'
  end
end
