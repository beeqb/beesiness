class CreateDeals < ActiveRecord::Migration[5.0]
  def change
    create_table :deals do |t|
      t.integer :number
      t.string :contract_number
      t.string :check
      t.string :invoice
      t.references :company
      t.references :client
      t.string :legal_form
      t.string :email
      t.string :phone
      t.string :address

      t.datetime :started_at
      t.datetime :closed_at

      t.string :notes
      t.string :visibility
      t.integer :discount

      t.decimal :total_sum, precision: 18, scale: 2
      t.decimal :check_sum, precision: 18, scale: 2
      t.decimal :discount_sum, precision: 18, scale: 2
      t.decimal :vat_sum, precision: 18, scale: 2

      t.string :payment_method
      t.string :payment_period
      t.string :status

      t.datetime :single_payment_date
      t.datetime :monthly_payment_started_at
      t.datetime :monthly_payment_ended_at

      t.datetime :contract_started_at
      t.datetime :contract_ended_at

      t.integer :manager_id
      t.integer :second_manager_id

      t.datetime :in_progress_at
      t.datetime :contract_conclusion_at
      t.datetime :contract_signed_at
      t.datetime :won_at
      t.datetime :payed_at
      t.datetime :lost_at
      t.datetime :deleted_at

      t.timestamps
    end

    add_foreign_key :deals, :users, column: :manager_id, primary_key: "id"
    add_foreign_key :deals, :users, column: :second_manager_id, primary_key: "id"

    create_table :deal_products do |t|
      t.references :deal
      t.references :product
      t.integer :count

      t.datetime :deleted_at
      t.timestamps
    end
  end
end