class CreateUserActivity < ActiveRecord::Migration[5.0]
  def change
    create_table :user_activities do |t|
      t.string :name
      t.string :color
      t.timestamps
    end

    add_reference :users, :user_activity, index: true
    add_foreign_key :users, :user_activities
  end
end
