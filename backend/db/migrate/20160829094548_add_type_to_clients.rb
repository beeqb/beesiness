class AddTypeToClients < ActiveRecord::Migration[5.0]
  def change
    add_column :clients, :type, :string, :default => 'Client'
    add_column :clients, :potential_amount, :decimal, precision: 18, scale: 2
    add_column :clients, :source, :string
    add_column :clients, :recommender, :string
  end
end
