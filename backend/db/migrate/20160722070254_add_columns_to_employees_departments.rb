class AddColumnsToEmployeesDepartments < ActiveRecord::Migration[5.0]
  def change
    add_column :employees_departments, :is_head, :boolean
    add_column :employees_departments, :position, :string
  end
end
