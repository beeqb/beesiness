class CreateCashFlowBudgetCategories < ActiveRecord::Migration[5.0]
  def up
    create_table :cash_flow_budget_categories do |t|
      t.string :name
      t.references :company, foreign_key: true
      t.boolean :system
      t.datetime :deleted_at

      t.timestamps
    end

    execute \
      "INSERT INTO cash_flow_budget_categories(
        name, system, created_at, updated_at
      ) VALUES ('Статья 1', 1, NOW(), NOW()),
        ('Статья 2', 1, NOW(), NOW()),
        ('Статья 3', 1, NOW(), NOW()),
        ('Статья 4', 1, NOW(), NOW())
      "
  end

  def down
    drop_table :cash_flow_budget_categories
  end
end
