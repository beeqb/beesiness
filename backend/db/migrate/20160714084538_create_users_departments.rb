class CreateUsersDepartments < ActiveRecord::Migration[5.0]
  def change
    create_table(:users_departments, :id => false) do |t|
      t.references :user
      t.references :department
    end
  end
end
