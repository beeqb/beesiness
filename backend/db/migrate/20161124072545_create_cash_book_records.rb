class CreateCashBookRecords < ActiveRecord::Migration[5.0]
  def change
    create_table :cash_book_records do |t|
      t.integer :category, null: false, comment: 'Income/outcome/loan'
      t.integer :bill_kind, null: false, comment: 'Cash/casless'
      t.string :currency, null: false
      t.float :balance, null: false
      t.float :amount, null: false

      t.text :comment

      t.references :creator, null: false, foreign_key: {to_table: :employees}
      t.references :company

      t.timestamps
    end
  end
end
