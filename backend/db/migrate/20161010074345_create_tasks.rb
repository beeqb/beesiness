class CreateTasks < ActiveRecord::Migration[5.0]
  def change
    create_table :tasks do |t|
      t.string :title
      t.text :description
      t.references :creator, foreign_key: {to_table: :employees}
      t.references :executor, foreign_key: {to_table: :employees}
      t.references :watcher, foreign_key: {to_table: :employees}
      t.references :company, foreign_key: true
      t.json :attachments
      t.boolean :important
      t.boolean :urgent
      t.string :assessment
      t.string :category
      t.date :start_date
      t.date :due_date
      t.integer :estimate
      t.json :checklist
      t.references :client, foreign_key: true
      t.string :status
      t.datetime :started_at
      t.datetime :done_at
      t.float :spent_time
      t.integer :renew_count

      t.timestamps
    end
  end
end
