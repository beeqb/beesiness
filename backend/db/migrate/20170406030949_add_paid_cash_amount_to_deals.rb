class AddPaidCashAmountToDeals < ActiveRecord::Migration[5.0]
  def change
    add_column :deals, :paid_cash_amount, :decimal, precision: 18, scale: 2
  end
end
