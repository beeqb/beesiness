class AddMetaDataToSystemEvents < ActiveRecord::Migration[5.0]
  def change
    add_column :system_events, :meta_data, :json
  end
end
