class AddColumnsToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :latitude, :decimal, precision: 20, scale: 14
    add_column :users, :longitude, :decimal, precision: 20, scale: 14
    add_column :users, :address, :string
  end
end
