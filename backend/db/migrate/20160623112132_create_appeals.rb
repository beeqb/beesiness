class CreateAppeals < ActiveRecord::Migration[5.0]
  def change
    create_table :appeals do |t|
      t.string :name
      t.timestamps
    end

    add_index :appeals, :name, :unique => true
  end
end
