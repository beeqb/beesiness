class AddDealDisplayNameToCashBookRecords < ActiveRecord::Migration[5.0]
  def change
    add_column :cash_book_records, :deal_display_name, :string
  end
end
