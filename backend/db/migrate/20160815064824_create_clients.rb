class CreateClients < ActiveRecord::Migration[5.0]
  def change
    create_table :clients do |t|
      t.string :name
      t.string :surname
      t.datetime :birthday
      t.references :country
      t.references :city
      t.references :company
      t.references :user
      t.string :address
      t.string :email
      t.string :work_phone
      t.string :cell_phone
      t.string :site
      t.string :skype
      t.string :facebook
      t.string :viber
      t.string :current_job
      t.string :current_department
      t.string :current_position
      t.boolean :current_is_head
      t.text :relations

      t.datetime :deleted_at
      t.timestamps
    end
  end
end
