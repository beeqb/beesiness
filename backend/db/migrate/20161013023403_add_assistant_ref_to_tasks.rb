class AddAssistantRefToTasks < ActiveRecord::Migration[5.0]
  def change
      add_reference :tasks, :assistant, foreign_key: {to_table: :employees}
  end
end
