class AddRecurringToCalendarEvents < ActiveRecord::Migration[5.0]
  def change
    add_column :calendar_events, :recurring, :string
  end
end
