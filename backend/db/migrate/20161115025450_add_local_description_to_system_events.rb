class AddLocalDescriptionToSystemEvents < ActiveRecord::Migration[5.0]
  def change
    add_column :system_events, :local_description, :text
    rename_column :system_events, :description, :general_description
  end
end
