class AddFirstCostToProducts < ActiveRecord::Migration[5.0]
  def change
    add_column(
      :products,
      :first_cost,
      :decimal,
      precision: 18,
      scale: 2
    )
  end
end
