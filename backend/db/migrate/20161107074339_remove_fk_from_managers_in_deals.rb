class RemoveFkFromManagersInDeals < ActiveRecord::Migration[5.0]
  def change
    remove_foreign_key :deals, column: :manager_id
    remove_foreign_key :deals, column: :second_manager_id

    db.execute(
      'UPDATE deals d '\
      'JOIN users u ON u.id = d.manager_id '\
      'JOIN employees e ON e.user_id = u.id AND e.company_id = d.company_id '\
      'SET manager_id = e.id'
    )

    db.execute(
      'UPDATE deals d '\
      'JOIN users u ON u.id = d.second_manager_id '\
      'JOIN employees e ON e.user_id = u.id AND e.company_id = d.company_id '\
      'SET second_manager_id = e.id'
    )

    add_foreign_key :deals, :employees, column: :manager_id
    add_foreign_key :deals, :employees, column: :second_manager_id
  end

  private

  def db
    ActiveRecord::Base.connection
  end
end
