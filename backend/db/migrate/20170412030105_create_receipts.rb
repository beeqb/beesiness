class CreateReceipts < ActiveRecord::Migration[5.0]
  def change
    create_table :receipts do |t|
      t.references :deal, foreign_key: true
      t.text :content
      t.decimal :amount, precision: 18, scale: 2
      t.json :deal_products
      t.integer :kind
      t.timestamps
    end
  end
end
