class CreateUsersCompanies < ActiveRecord::Migration[5.0]
  def change
    create_table(:users_companies, :id => false) do |t|
      t.references :user
      t.references :company
    end
  end
end
