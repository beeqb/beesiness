class AddPriceToDealProducts < ActiveRecord::Migration[5.0]
  def up
    add_column :deal_products, :price, :decimal, precision: 18, scale: 2
    db.execute 'UPDATE deal_products dp '\
               'INNER JOIN products p ON dp.product_id = p.id '\
               'SET dp.price = p.price'
  end

  def down
    remove_column :deal_products, :price
  end

  private

  def db
    ActiveRecord::Base.connection
  end
end
