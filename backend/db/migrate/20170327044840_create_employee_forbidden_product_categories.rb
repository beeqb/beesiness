class CreateEmployeeForbiddenProductCategories < ActiveRecord::Migration[5.0]
  def change
    create_join_table(
      :employees,
      :product_categories,
      table_name: :employee_forbidden_product_categories
    )
  end
end
