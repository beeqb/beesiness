class AddInterestRateToCompaniesAndDeals < ActiveRecord::Migration[5.0]
  def change
    add_column :companies, :interest_rate, :float
    add_column :deals, :interest_rate, :float
  end
end
