class AddColumnsToCompany < ActiveRecord::Migration[5.0]
  def change
    add_column :companies, :inn, :int, default: 0
    add_column :companies, :kpp, :int
  end
end
