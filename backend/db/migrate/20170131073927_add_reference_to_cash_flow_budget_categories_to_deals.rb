class AddReferenceToCashFlowBudgetCategoriesToDeals < ActiveRecord::Migration[5.0]
  def change
    add_reference :deals, :cash_flow_budget_category, foreign_key: true
  end
end
