class AddQuantityToSalesPlanProductsCategories < ActiveRecord::Migration[5.0]
  def change
    add_column(
      :sales_plan_product_categories,
      :quantity,
      :decimal,
      precision: 18,
      scale: 2
    )
  end
end
