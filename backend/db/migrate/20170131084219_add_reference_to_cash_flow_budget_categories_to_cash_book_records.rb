class AddReferenceToCashFlowBudgetCategoriesToCashBookRecords < ActiveRecord::Migration[5.0]
  def change
    add_reference(
      :cash_book_records,
      :cash_flow_budget_category,
      foreign_key: true
    )
    drop_table :cash_book_records_product_categories
  end
end
