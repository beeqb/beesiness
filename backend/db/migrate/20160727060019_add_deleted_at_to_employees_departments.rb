class AddDeletedAtToEmployeesDepartments < ActiveRecord::Migration[5.0]
  def change
    add_column :employees_departments, :id, :primary_key
    add_column :employees_departments, :created_at, :datetime
    add_column :employees_departments, :updated_at, :datetime
    add_column :employees_departments, :deleted_at, :datetime
  end
end
