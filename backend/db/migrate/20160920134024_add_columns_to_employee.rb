class AddColumnsToEmployee < ActiveRecord::Migration[5.0]
  def change
    add_column :employees, :latitude, :decimal, precision: 20, scale: 14
    add_column :employees, :longitude, :decimal, precision: 20, scale: 14
    add_column :employees, :address, :string
  end
end
