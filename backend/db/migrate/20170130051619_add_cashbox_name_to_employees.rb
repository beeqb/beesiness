class AddCashboxNameToEmployees < ActiveRecord::Migration[5.0]
  def change
    add_column :employees, :cashbox_name, :string
  end
end
