class CreateTasksWatchersJoinTable < ActiveRecord::Migration[5.0]
  def change
    create_join_table :tasks, :employees, table_name: :tasks_watchers do |t|
      t.index :task_id
      t.index :employee_id
    end

    db.execute "INSERT INTO tasks_watchers(task_id, employee_id)"\
      " SELECT id, watcher_id FROM tasks WHERE watcher_id IS NOT NULL"

    remove_foreign_key :tasks, column: :watcher_id
    remove_column  :tasks, :watcher_id
  end

  private

  def db
    ActiveRecord::Base.connection
  end
end
