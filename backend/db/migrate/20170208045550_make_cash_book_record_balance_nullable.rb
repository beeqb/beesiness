class MakeCashBookRecordBalanceNullable < ActiveRecord::Migration[5.0]
  def change
    change_column_null(:cash_book_records, :balance, true)
    change_column_null(:cash_book_records, :personal_balance, true)
  end
end
