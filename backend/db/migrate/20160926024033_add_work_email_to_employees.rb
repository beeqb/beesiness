class AddWorkEmailToEmployees < ActiveRecord::Migration[5.0]
  def up
    add_column :employees, :email, :string

    db.execute "UPDATE employees e"\
      " INNER JOIN users u ON e.user_id = u.id"\
      " SET e.email = u.email"
  end

  def down
    remove_column :employees, :email
  end

  private

  def db
    ActiveRecord::Base.connection
  end
end
