class AddReportingDocumentToCashBookRecord < ActiveRecord::Migration[5.0]
  def change
    add_column :cash_book_records, :reporting_document, :string
  end
end
