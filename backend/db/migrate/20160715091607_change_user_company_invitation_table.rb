class ChangeUserCompanyInvitationTable < ActiveRecord::Migration[5.0]
  def change
    remove_foreign_key :user_company_invitations, :roles
    remove_column :user_company_invitations, :role_id


    remove_foreign_key :user_company_invitations, :users
    remove_column :user_company_invitations, :user_id

    add_column :user_company_invitations, :role, :string
    add_column :user_company_invitations, :email, :string

    rename_column :user_company_invitations, :invitation_sent_at, :sent_at
    rename_column :user_company_invitations, :invitation_confirmed_at, :confirmed_at

    add_column :user_company_invitations, :password, :string
  end
end
