class AddDeletedAtToCashBookRecords < ActiveRecord::Migration[5.0]
  def change
    add_column :cash_book_records, :deleted_at, :datetime
  end
end
