class CreateSalesPlan < ActiveRecord::Migration[5.0]
  def change
    create_table :sales_plans do |t|
      t.references :company
      t.integer :kind, comment: 'Plan by quantity/sum'
      t.integer :year, comment: 'Year of plan'
      t.integer :month, comment: 'Month of plan'
      t.datetime :deleted_at
    end

    create_table :sales_plan_products do |t|
      t.references :sales_plan
      t.references :product
      t.integer :quantity
      t.integer :sum
      t.datetime :deleted_at
    end

    create_table :sales_plan_employees do |t|
      t.references :sales_plan
      t.references :employee
      t.integer :part, comment: 'Part of plan assigned to employee'
      t.datetime :deleted_at
    end
  end
end
