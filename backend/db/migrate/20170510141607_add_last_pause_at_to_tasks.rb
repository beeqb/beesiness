class AddLastPauseAtToTasks < ActiveRecord::Migration[5.0]
  def change
    add_column :tasks, :last_pause_at, :datetime
  end
end
