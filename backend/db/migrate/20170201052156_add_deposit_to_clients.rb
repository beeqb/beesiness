class AddDepositToClients < ActiveRecord::Migration[5.0]
  def change
    add_column :clients, :deposit, :decimal, precision: 18, scale: 2
  end
end
