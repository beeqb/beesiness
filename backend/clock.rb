require 'clockwork'
require './config/boot'
require './config/environment'

module Clockwork
  configure do |config|
    config[:sleep_timeout] = 1
  end

  every(1.day, 'Every day in 12:00', at: '12:00') do
    Notification::TaskConditions.notify
    Notification::TaskNew.notify
    Notification::DealWaitsPayment.notify
    Notification::DealContractIsEnding.notify
  end
end
