namespace :deal do
  desc 'Преобразует сделки в записи кассовой книги'
  task map_to_cash_book: :environment do
    Deal.find_each(batch_size: 100) do |deal|
      next if CashBookRecord.where(deal_id: deal.id).exists?

      puts "Mapping deal wint id = #{deal.id}..."
      cash_book_record = DealToCashBookMap.call(deal)

      if cash_book_record
        cash_book_record.save!
        puts 'Done!'
      else
        puts 'Total sum is 0, skipping'
      end
    end
  end
end
