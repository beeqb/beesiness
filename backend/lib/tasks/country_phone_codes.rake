require 'csv'

namespace :country_phone_codes do
  desc 'Импорт кодов ISO стран в таблицу country_phone_codes'
  task import_iso: :environment do
    f = File.new('lib/tasks/countries.txt')

    CSV.foreach('lib/tasks/countries.txt', col_sep: ', ', quote_char: "'") do |row|
      iso_code, *names = row
      ru_name = names[2].strip

      models = CountryPhoneCode.where(country_name: ru_name)

      if models.size > 1
        raise 'More than one country for ' + ru_name
      end

      models.update_all(country_code: iso_code)
    end
  end

end
