namespace :cash_book do
  desc 'Импортирует статьи БДСС в БД для всех компаний'
  task seed_cash_flow_budget_categories: :environment do
    Company.pluck(:id).each do |company_id|
      SeedCashFlowBudgetCategoriesWorker.new.perform(company_id)
    end
  end
end
