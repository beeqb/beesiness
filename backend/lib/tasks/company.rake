namespace :company do
  desc 'Для всех сотрудников сохраняет кол-во сделок, лидов, клиентов'\
       ' и товаров в их компании на текущий момент'
  task update_statistics: :environment do
    Employee.find_each(batch_size: 100) do |employee|
      company = employee.company

      CompanyStatistic.create!(
        company_id: company.id,
        employee_id: employee.id,
        leads_count: company.leads_for(employee).count,
        clients_count: company.clients_for(employee).count,
        deals_count: company.deals_for(employee).count,
        products_count: company.products_for(employee).count
      )
    end
  end
end
