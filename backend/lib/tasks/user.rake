namespace :user do
  desc 'Downloads profile images for users, signed up via Facebook'

  task :download_fb_images => :environment do
    User.find_each(batch_size: 100) do |user|
      if user.image.present? or user.facebook_image_url.nil?
        puts "Skipping user #{user.fullname} with id #{user.id}"
        next
      end

      user.remote_image_url = user.facebook_image_url.sub('http:', 'https:')
      user.facebook_image_url = nil

      puts "Going to update user #{user.fullname} with id #{user.id}..."

      user.save!

      puts "Success!"
    end
  end

  task destroy: :environment do
    ARGV.each { |a| task a.to_sym do ; end }

    db = ActiveRecord::Base.connection
    user_id = if ARGV[1]
                db.quote(ARGV[1])
              else
                User.last.id
              end

    db.execute(
      "DELETE ed FROM employees_departments ed "\
      "JOIN employees e ON e.id = ed.employee_id "\
      "WHERE e.user_id = #{user_id}"
    )
    db.execute(
      "DELETE d FROM departments d "\
      "JOIN companies c ON c.id = d.company_id "\
      "WHERE c.creator_id = #{user_id}"
    )
    db.execute("DELETE FROM employees WHERE user_id = #{user_id}")
    db.execute("DELETE FROM companies WHERE creator_id = #{user_id}")
    db.execute("DELETE FROM users WHERE id = #{user_id}")
  end
end
