namespace :dev do
  desc 'Add role to user with email glukhota@beeqb.com in company BeeQB'

  task add_role: :environment do
    ARGV.each { |a| task a.to_sym do ; end }

    user = User.find_by(email: 'glukhota@beeqb.com')
    company = Company.find_by(name: 'BeeQB')

    user.add_role(ARGV[1], company)
  end

  task remove_role: :environment do
    ARGV.each { |a| task a.to_sym do ; end }

    user = User.find_by(email: 'glukhota@beeqb.com')
    company = Company.find_by(name: 'BeeQB')

    user.remove_role(ARGV[1], company)
  end

  task gen_events: :environment do
    all = SystemEvent::Registry.all
    base_path = Rails.root.join('app/models/system_event/events')

    all.each_key do |root|
      dir_path = base_path.join(root.to_s)

      Dir.mkdir(dir_path) unless Dir.exist?(dir_path)

      all[root].each_key do |key|
        file_path = dir_path.join("#{key.to_s}.rb")

        next if File.exist?(file_path)

        File.open(file_path, 'w') do |file|
          file.write <<-EOF
module SystemEvent::Events
  module #{root.to_s.camelize}
    class #{key.to_s.camelize} < BaseEvent
      def name
        '#{root.to_s}:#{key.to_s}'
      end
    end
  end
end
          EOF
        end
      end
    end
  end
end
