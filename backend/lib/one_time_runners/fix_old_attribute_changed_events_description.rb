SystemEvent.where(kind: 'product:attribute_changed').find_each(batch_size: 100) do |event|
  event.update(local_description: event.general_description.sub(/в товаре.+?a>\s/, ''))
end
