CashBookRecord.where(source_type: 'NilClass').update_all(source_type: nil)

CashBookRecord.where(source_type: nil).find_each(batch_size: 100).each do |record|
  next unless record.deal

  if record.deal.client
    record.source = record.deal.client
  else
    record.source_type = 'Client'
  end

  if record.valid?
    record.save
  else
    puts "Record##{record.id} is invalid"
  end
end
