supercompany = Company.find_by_name('BeeQB')

puts '=' * 20
puts 'Processing companies'
puts '=' * 20

Company.where.not(id: supercompany.id).find_each(batch_size: 100) do |company|
  next if Lead.where(company_id: supercompany.id, legal_entity: company).exists?

  puts "Converting company##{company.id} \"#{company.name}\"..."

  lead = UserOrCompanyToLeadMap.call(company)
  lead.save!

  puts "Done"
end

puts '=' * 20
puts 'Processing users'
puts '=' * 20

supercompany_employees = supercompany.employees.pluck(:user_id)

User.where.not(id: supercompany_employees).find_each(batch_size: 100) do |user|
  next if Lead.where(company_id: supercompany.id, user: user).exists?

  puts "Converting user##{user.id} \"#{user.fullname}\"..."

  lead = UserOrCompanyToLeadMap.call(user)
  
  if lead.valid? then lead.save else puts 'Invalid' end

  puts "Done"
end
