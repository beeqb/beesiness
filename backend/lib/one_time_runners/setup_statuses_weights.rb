lead_statuses = {
	lead_new: { weight: -3 },
	lead_in_work: { weight: -2 },
	lead_trash: { weight: -1 },
}

deal_statuses = {
	in_progress: { weight: -7 },
	contract_conclusion: { weight: -6 },
	contract_signed: { weight: -5 },
	won: { weight: -4 },
	payed: { weight: -3 },
	returned: { weight: -2 },
	lost: { weight: -1 },
}

client_statuses = {
	client_new: { weight: -5 },
	client_current: { weight: -4 },
	client_permanent: { weight: -3 },
	client_special: { weight: -2 },
	client_no: { weight: -1 },
}

supplier_statuses = {
	supplier_new: { weight: -5 },
	supplier_current: { weight: -4 },
	supplier_permanent: { weight: -3 },
	supplier_special: { weight: -2 },
	supplier_no: { weight: -1 },
}

LeadStatus.update(lead_statuses.keys, lead_statuses.values)
DealStatus.update(deal_statuses.keys, deal_statuses.values)
ClientStatus.update(client_statuses.keys, client_statuses.values)
SupplierStatus.update(supplier_statuses.keys, supplier_statuses.values) 
