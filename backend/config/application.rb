require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
# require "sprockets/railtie"
require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module App
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Only loads a smaller set of middleware suitable for API only apps.
    # Middleware like session, flash, cookies can be added back manually.
    # Skip views, helpers and assets when generating a new resource.
    config.api_only = true

    config.time_zone = 'Moscow'
    config.i18n.default_locale = :ru
    config.encoding = 'utf-8'

    config.autoload_paths += [
      Rails.root.join('lib'),
      Rails.root.join('app/validators'),
      Rails.root.join('app/services'),
      Rails.root.join('app/listeners'),
      Rails.root.join('app/workers'),
    ]

    config.middleware.use ActionDispatch::Flash # for devise works

    config.to_prepare do
      Wisper.clear if Rails.env.development?
      Wisper.subscribe(
        DealListener.new,
        on: 'deal:created',
        with: :deal_created
      )
      Wisper.subscribe(
        ClientListener.new,
        on: 'client:updated',
        with: :client_updated
      )
      Wisper.subscribe(
        DealListener.new,
        on: 'deal:status_changed',
        with: :deal_status_changed
      )
      Wisper.subscribe(
        DealListener.new,
        on: 'deal:returned',
        with: :deal_returned
      )
      Wisper.subscribe(
        DealListener.new,
        on: 'deal:partially_returned',
        with: :deal_partially_returned
      )
      Wisper.subscribe(
        DealListener.new,
        on: 'deal_product:created',
        with: :deal_product_created
      )
      Wisper.subscribe(
        DealListener.new,
        on: 'deal_product:updated',
        with: :deal_product_updated
      )
      Wisper.subscribe(
        ProductListener.new,
        on: 'product:attribute_changed',
        with: :product_attribute_changed
      )
      Wisper.subscribe(CashBookListener.new, prefix: :on)
      Wisper.subscribe(SystemEventListener.new)
    end

    PhonyRails.default_country_code = 'RU'

    config.active_job.queue_adapter = :sidekiq
  end
end
