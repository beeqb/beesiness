ActionMailer::Base.delivery_method = :smtp
ActionMailer::Base.smtp_settings = {
  address: ENV['MAIL_ADDRESS'],
  domain: ENV['MAIL_DOMAIN'],
  port: ENV['MAIL_PORT'],
  openssl_verify_mode: ENV['MAIL_OPENSSL_VERIFY_MODE'],
}

unless ENV['MAIL_AUTHENTICATION'].nil?
  ActionMailer::Base.smtp_settings[:authentication] = ENV['MAIL_AUTHENTICATION']
  ActionMailer::Base.smtp_settings[:user_name] = ENV['MAIL_USERNAME']
  ActionMailer::Base.smtp_settings[:password] = ENV['MAIL_PASSWORD']
end