Devise.setup do |config|
  config.mailer_sender = ENV['MAIL_FROM']
  config.secret_key = ENV['SECRET']
end
