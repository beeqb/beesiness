Rails.application.routes.draw do
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  scope '/api' do
    mount_devise_token_auth_for 'User', at: 'auth', controllers: {
      token_validations: 'auth/token_validations',
      sessions: 'auth/sessions',
      registrations: 'auth/registrations',
    }

    get '/auth/invitations/confirm'
    get '/translations' => 'translations#index'
    get '/references' => 'application#references'

    resources :users do
      collection do
        get 'new'
        post 'resend_confirmation'
        post 'validate'
        post 'search'
      end
      member do
        put 'upload_image'
      end
    end

    resources :notifications, only: [:index, :update] do
      collection do
        post 'mark_all_as_read'
      end
    end

    resources :calendar_events, only: [:index, :create] do
      collection do
        post 'remove'
      end
    end

    resources :tasks do
      collection do
        get 'new'
        post 'validate'
        get 'assigned_to_me'
        post 'finish_all'
      end

      member do
        post 'start'
        post 'suspend'
        post 'finish'
        post 'reopen'
        put 'upload_attachments'
        delete 'attachments/:index', action: 'remove_attachment'
      end
    end

    resources :companies do
      collection do
        get 'new'
        post 'validate'
        post 'search'
      end

      member do
        put 'upload_logo'
        post 'invite_user'
        get 'stream'
        get 'search_entities'
        get 'budget_statistics'
        get 'budget_statistics_by_days'
        get 'sales_report'
        get 'search/cash_book_sources' => 'search#cash_book_sources'
      end

      statuses = [
        :lead_statuses,
        :client_statuses,
        :deal_statuses,
        :supplier_statuses,
      ]

      statuses.each do |status|
        resources status do
          collection do
            get 'new'
            post 'validate'
          end

          member do
            post 'delete_safely'
          end
        end
      end

      resources :product_categories do
        collection do
          get 'new'
          post 'validate'
        end

        member do
          post 'delete_safely'
        end
      end

      resources :products do
        collection do
          get 'new'
          get 'recommended'
          post 'validate'
          post 'search'
        end

        member do
          put 'upload_images'
          delete 'images/:index', action: 'remove_image'
          post 'set_recommended'
        end
      end

      resources :clients do
        collection do
          get 'new'
          post 'validate'
          post 'search'
        end
      end

      resources :leads do
        collection   do
          get 'new'
          post 'validate'
          post 'validate_lead_as_client'
        end
        member do
          put 'set_status'
          put 'convert'
        end

        resources :lead_products do
          collection do
            get 'new'
            post 'validate'
          end

          member do
            post 'return'
          end
        end
      end

      resources :suppliers do
        collection do
          get 'new'
          post 'validate'
          post 'search'
        end
      end

      resources :departments do
        collection do
          get 'new'
          post 'validate'
        end
      end

      resources :employees do
        collection do
          get 'new'
          post 'validate'
        end
        member do
          get 'activity'
          post 'dismiss'
          get 'sales_funnel'
          get 'sales_forecast'
        end
      end

      resources :deals do
        collection do
          get 'new'
          post 'validate'
          post 'omni_search'
          post 'receipt_preview'
        end

        member do
          put 'set_status'
          post 'return'
          get 'receipt'
        end

        post 'monthly_payments/:id/confirm' => 'deal_monthly_payments#confirm'
        post 'monthly_payments/:id/discard' => 'deal_monthly_payments#discard'

        resources :deal_products do
          collection do
            get 'new'
            post 'validate'
          end

          member do
            post 'return'
          end
        end

      end

      resources :sales_plans do
        collection do
          get 'new'
          post 'validate'
        end
      end

      resources :sales_plan_employees do
        collection do
          post 'bulk_update'
        end
      end

      resources :cash_book_records do
        collection do
          get 'new'
          post 'validate'
          get 'existing_sources'
        end

        member do
          put 'upload_document'
        end
      end

      resources :cash_book_categories do
        collection do
          get 'new'
          post 'validate'
        end

        member do
          post 'delete_safely'
        end
      end

      resources :cash_flow_budget_categories do
        member do
          post 'delete_safely'
        end
      end
    end

  end

  root 'application#default'

  # Profile

  get '/profile' => 'application#default'
  get '/profile/edit' => 'application#default'

  # Users

  get '/users/sign_in' => 'application#default'
  get '/users/sign_up' => 'application#default'
  get '/users/sign_up_success' => 'application#default'
  get '/users/sign_out' => 'application#default'
  get '/users/change_password' => 'application#default'
  get '/users/restore_password' => 'application#default'
  get '/users/restore_password_success' => 'application#default'
  get '/users/:id' => 'application#default'

  # Companies

  get '/companies/new' => 'application#default'
  get '/companies/:id' => 'application#default'
  get '/companies/:id/edit' => 'application#default'

  # Company employees

  get '/companies/:id/employees' => 'application#default'
  get '/companies/:id/employees/:employee_id' => 'application#default'

  # Company products

  get '/companies/:id/products' => 'application#default'
  get '/companies/:id/products/:product_id' => 'application#default'
  get '/companies/:id/products/new' => 'application#default'

  # Company clients

  get '/companies/:id/clients' => 'application#default'
  get '/companies/:id/clients/:client_id' => 'application#default'
  get '/companies/:id/clients/new' => 'application#default'

  # Company leads

  get '/companies/:id/leads' => 'application#default'
  get '/companies/:id/leads/:lead_id' => 'application#default'
  get '/companies/:id/leads/new' => 'application#default'
  get '/companies/:id/leads/:lead_id/convert' => 'application#default'

  # Company suppliers

  get '/companies/:id/suppliers' => 'application#default'
  get '/companies/:id/suppliers/new' => 'application#default'
  get '/companies/:id/suppliers/:supplier_id' => 'application#default'

  # Company deals

  get '/companies/:id/deals' => 'application#default'
  get '/companies/:id/deals/:deal_id' => 'application#default'
  get '/companies/:id/deals/new' => 'application#default'

  get '/companies/:id/deals/:deal_id/receipt' => 'application#plain'

  # Company POS

  get '/companies/:id/pos' => 'application#default'

  # Company BA&BI

  get '/companies/:id/sales_plan' => 'application#default'
  get '/companies/:id/sales_plan/manage' => 'application#default'

  get '/companies/:id/cash_book' => 'application#default'
  get '/companies/:id/cash_book/earliest' => 'cash_book_records#get_earliest_record'
  get '/companies/:id/budget' => 'application#default'
  get '/companies/:id/break-even-analysis' => 'application#default'
  get '/companies/:id/sales-report' => 'application#default'

  # Company CRM

  get '/companies/:id/crm' => 'application#default'

  # Company search results page

  get '/companies/:id/search-results' => 'application#default'

  # Tasks

  get '/companies/:id/tasks/new' => 'application#default'
  get '/tasks' => 'application#default'

  # Calendar
  get '/calendar' => 'application#default'

  # Policy

  get '/policy' => 'application#default'

  # Translations

  get '/translations' => 'application#default'

  # Desktop

  get '/desktop' => 'application#default'

  # Budget

  get '/budget' => 'application#default'

  # Analysis

  get '/analysis' => 'application#default'

  get '/tmp/store' => 'application#default'
  get '/tmp/deals' => 'application#default'
  get '/tmp/clients' => 'application#default'
  get '/tmp/leads' => 'application#default'

end
