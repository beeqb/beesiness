class BaseController < ApplicationController
  include Wisper::Publisher

  before_action :authenticate_user!, except: [:resend_confirmation]
  before_action :set_entity, only: [:destroy, :show, :update]
  before_action :set_or_create_entity, only: [:validate]

  serialization_scope :current_user

  rescue_from ActiveRecord::RecordInvalid,
              with: :render_unprocessable_entity_response

  # GET /api/{plural_entity_name}/new
  def new
    entity = entity_class.new
    entity.assign_attributes(query_params.to_h)

    data = {
      entity: ActiveModelSerializers::SerializableResource.new(
        entity,
        custom_include: get_entity_include
      ).as_json,
    }

    render json: expand_data(data)
  end

  # POST /api/{plural_entity_name}
  def create
    set_entity(entity_class.new(entity_params))
    if get_entity.save
      on_successful_create
      render json: get_entity,
             custom_include: get_entity_include,
             status: :created
    else
      render json: { errors: get_entity.errors }, status: :unprocessable_entity
    end
  end

  def on_successful_create
    broadcast("#{entity_name}:created", subject: get_entity, user: current_user)
  end

  # DELETE /api/{plural_entity_name}/:id
  def destroy
    entity = get_entity
    if entity.respond_to?(:destroy)
      entity.destroy
      broadcast(
        "#{entity_name}:removed",
        subject: entity,
        user: current_user
      )
    end

    render json: entity
  end

  # GET /api/{plural_entity_name}
  def index
    collection = process_data_for_index(index_entities)

    data = if page_params[:page]
             paginated_data_for_index(collection)
           else
             data_for_index(collection)
           end

    render json: expand_data(data)
  end

  # Хук для переопределения в потомках
  def process_data_for_index(entities)
    entities
  end

  # GET /api/{plural_entity_name}/:id
  def show
    data = {
      entity: ActiveModelSerializers::SerializableResource.new(
        get_entity,
        custom_include: get_entity_include,
        scope: current_user
      ).as_json,
    }
    render json: expand_data(data)
  end

  # PATCH/PUT /api/{plural_entity_name}/:id
  def update
    if get_entity.update(entity_params)
      on_successful_update
      render json: get_entity, custom_include: get_entity_include
    else
      render json: { errors: get_entity.errors }, status: :unprocessable_entity
    end
  end

  # Хук для переопределения в потомках
  def on_successful_update
    broadcast("#{entity_name}:updated", subject: get_entity, user: current_user)
  end

  # POST /api/{plural_entity_name}/validate
  def validate
    entity = get_entity
    entity.assign_attributes(entity_params.to_h)
    if entity.valid?
      render json: { errors: entity.errors }
    else
      render json: { errors: entity.errors }, status: :unprocessable_entity
    end
  end

  def search
    or_query = []
    or_values = []

    (params[:or] || []).each do |key, value|
      or_query.push(key + ' = ?')
      or_values.push(value)
    end

    data = apply_scopes(entity_class)
           .where(query_params)
           .where(or_query.join(' OR '), *or_values)
           .accessible_by(current_ability)

    render json: ActiveModel::Serializer::CollectionSerializer.new(
      data,
      custom_include: get_entity_include,
      scope: current_user
    )
  end

  private

  # Returns the entity from the created instance variable
  # @return [Object]
  def get_entity
    instance_variable_get("@#{entity_name}")
  end

  # Returns the allowed parameters for searching
  # Override this method in each API controller
  # to permit additional parameters to search on
  # @return [Hash]
  def query_params
    {}
  end

  # Returns the allowed parameters for pagination
  # @return [Hash]
  def page_params
    params.permit(:page, :page_size)
  end

  # The entity class based on the controller
  # @return [Class]
  def entity_class
    @entity_class ||= entity_name.classify.constantize
  end

  # The singular name for the entity class based on the controller
  # @return [String]
  def entity_name
    @entity_name ||= controller_name.singularize
  end

  # Only allow a trusted parameter "white list" through.
  # If a single entity is loaded for #create or #update,
  # then the controller for the entity must implement
  # the method "#{entity_name}_params" to limit permitted
  # parameters for the individual model.
  def entity_params
    @entity_params ||= send("#{entity_name}_params")
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_entity(entity = nil)
    entity ||= entity_class.find(params[:id])
    instance_variable_set("@#{entity_name}", entity)
  end

  def set_or_create_entity
    entity = if params[:id]
               entity_class.find(params[:id])
             else
               entity_class.new
             end
    instance_variable_set("@#{entity_name}", entity)
  end

  def get_entity_include
    include = []
    unless params[:include].nil?
      include = params[:include].delete(' ').split(',')
    end
    include
  end

  def get_entity_expand
    expand = []
    expand = params[:expand].delete(' ').split(',') unless params[:expand].nil?
    expand
  end

  def expand_data(data)
    expand = get_entity_expand
    if expand
      expand.each do |expand_item|
        next if data.has_key?(expand_item.to_sym)
        data[expand_item] = entity_class.send(expand_item)
      end
    end
    data
  end

  def index_entities
    entities = 
      apply_scopes(entity_class)
      .where(query_params)
      .accessible_by(current_ability)

    order(entities)
  end

  def data_for_index(entities)
    {
      entities: ActiveModel::Serializer::CollectionSerializer.new(
        entities,
        custom_include: get_entity_include,
        current_company_id: params[:company_id],
        scope: current_user
      ),
      total_count: entities.count,
    }
  end

  def paginated_data_for_index(entities)
    paginated_entities = paginate(entities)

    {
      entities: ActiveModel::Serializer::CollectionSerializer.new(
        paginated_entities,
        custom_include: get_entity_include,
        current_company_id: params[:company_id],
        scope: current_user
      ),
      total_count: entities.count,
      per: page_params[:page_size] || entity_class.default_per_page,
      page: paginated_entities.current_page,
      last_page: paginated_entities.last_page?,
    }
  end

  def order(collection)
    case
    when params[:order]
      collection.order(params[:order])
    when entity_class.column_names.include?('created_at')
      collection.order('created_at DESC')
    else
      collection
    end
  end

  def paginate(collection)
    collection.page(params[:page]).per(page_params[:page_size])
  end

  def render_unprocessable_entity_response(exception)
    render(
      json: { errors: exception.record.errors },
      status: :unprocessable_entity
    )
  end
end
