#######################################
# Контроллер для событий на календаре #
#######################################
class CalendarEventsController < BaseController
  def index
    aggregator =
      Calendar::EventsAggregator.new(params[:start], params[:end])

    render json: aggregator.events_for(current_user)
  end

  def remove
    if event_remove_params[:id]
      event = CalendarEvent.find(event_remove_params[:id])
      event.destroy

      render json: event
    else
      event = CalendarEvent.find(event_remove_params[:original_event_id])
      event.update(recurring_end_at: event_remove_params[:start])

      render json: event
    end
  end

  private

  def calendar_event_params
    params
      .require(:calendar_event)
      .permit(
        :title,
        :start,
        :end,
        :user_id,
        :recurring
      )
  end

  def event_remove_params
    params
      .require(:calendar_event)
      .permit(
        :id,
        :recurring,
        :start,
        :end,
        :original_event_id
      )
  end
end
