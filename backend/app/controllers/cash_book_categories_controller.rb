class CashBookCategoriesController < BaseController
  load_and_authorize_resource :company
  load_and_authorize_resource :cash_book_category, through: :company

  def delete_safely
    category = get_entity
    new_category = params[:category_after_delete]

    if new_category
      CashBookRecord
        .where(category: category.id, company_id: category.company_id)
        .update(category: new_category)
    end

    category.destroy

    render json: category, custom_include: get_entity_include
  end

  private

  def cash_book_category_params
    params
      .require(:cash_book_category)
      .permit(:name, :expense, :company_id)
  end
end
