class TranslationsController < ApplicationController

  def index
    translations = I18n.backend.send(:translations)
    render json: {
      ru: translations[:ru],
      en: translations[:en],
    }
  end

end
