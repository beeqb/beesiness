# Tasks controller
class TasksController < BaseController
  def upload_attachments
    task = Task.find(params[:id])

    begin
      files = task.attachments
      files += params.require(:attachments).permit!.to_h.values
      task.attachments = files
    rescue ActionController::ParameterMissing
      render json: { errors: [] }, status: :unprocessable_entity
      return
    end

    if task.save
      render json: task, custom_include: get_entity_include
    else
      render json: { errors: task.errors }, status: :unprocessable_entity
    end
  end

  def remove_attachment
    task = Task.find(params[:id])

    remain_files = task.attachments
    remain_files.delete_at(params[:index].to_i)

    if remain_files.empty?
      task.remove_attachments!
    else
      task.attachments = remain_files
    end

    if task.save
      render json: task
    else
      render json: { errors: task.errors }, status: :unprocessable_entity
    end
  end

  def start
    task = Task.find(params[:id])

    task.start

    if task.save
      render json: task
    else
      render json: { errors: task.errors }, status: :unprocessable_entity
    end
  end

  def suspend
    task = Task.find(params[:id])

    task.suspend

    if task.save
      render json: task
    else
      render json: { errors: task.errors }, status: :unprocessable_entity
    end
  end

  def finish
    task = Task.find(params[:id])

    task.finish

    if task.save
      render json: task
    else
      render json: { errors: task.errors }, status: :unprocessable_entity
    end
  end

  def finish_all
    begin
      tasks = Task.finish_all(params[:ids])
    rescue ActiveRecord::RecordInvalid => invalid
      return render(
        json: { id: invalid.record.id, errors: invalid.record.errors },
        status: :unprocessable_entity
      )
    end

    render json: tasks
  end

  def reopen
    task = Task.find(params[:id])

    task.reopen

    if task.save
      render json: task
    else
      render json: { errors: task.errors }, status: :unprocessable_entity
    end
  end

  private

  # rubocop:disable Metrics/MethodLength
  def task_params
    params
      .require(:task)
      .permit(
        :title,
        :description,
        :department,
        :creator_id,
        :executor_id,
        :assistant_id,
        :company_id,
        :attachments,
        :important,
        :urgent,
        :assessment,
        :category,
        :start_date,
        :due_date,
        :estimate,
        :client_id,
        checklist: [:title, :done],
        watcher_ids: []
      )
  end

  def query_params
    params.permit(
      :company_id,
      :executor_id,
      :client_id,
      :name,
      :status
    )
  end
end
