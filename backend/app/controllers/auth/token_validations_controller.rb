module Auth
  class TokenValidationsController < DeviseTokenAuth::TokenValidationsController
    serialization_scope :current_user

    protected

    def render_validate_token_success
      render(
        json: @resource,
        root: :data,
        custom_include: %w(
          permissions
          allow_change_password
          active_tasks_count
        ),
        adapter: :json
      )
    end
  end
end
