module Auth
  class InvitationsController < ApplicationController

    def confirm
      invitation = UserCompanyInvitation
                     .where('md5(id)="' + params[:token] + '"')
                     .where(confirmed_at: nil)
                     .where.not(sent_at: nil)
                     .first

      if invitation.nil?
        redirect_to '/'
        return
      end

      success = invitation.confirm

      if success
        user = User.where(email: invitation[:email]).first

        # create client id
        client_id = SecureRandom.urlsafe_base64(nil, false)
        token = SecureRandom.urlsafe_base64(nil, false)
        token_hash = BCrypt::Password.create(token)
        expiry = (Time.now + DeviseTokenAuth.token_lifespan).to_i

        user.tokens[client_id] = {
          token: token_hash,
          expiry: expiry
        }

        user.save!

        redirect_to(user.build_auth_url('profile?edit=true', {
          token: token,
          client_id: client_id,
          account_confirmation_success: true,
          config: 'default'
        }))
      else
        redirect_to '/'
      end
    end
  end
end
