module Auth
  class SessionsController < DeviseTokenAuth::SessionsController
    serialization_scope :current_user
    before_action :clear_ability, only: [:destroy]

    def create
      if facebook_authenticate
        confirmed = (
        !@resource.respond_to?(:active_for_authentication?) or
          @resource.active_for_authentication?
        )
        if confirmed
          return sign_in_success
        elsif not confirmed
          return render_create_error_not_confirmed
        end
      end

      # Check
      field = (resource_params.keys.map(&:to_sym) & resource_class.authentication_keys).first

      @resource = nil
      if field
        q_value = resource_params[field]

        if resource_class.case_insensitive_keys.include?(field)
          q_value.downcase!
        end

        q = "#{field.to_s} = ? AND provider='email'"

        if ActiveRecord::Base.connection.adapter_name.downcase.starts_with? 'mysql'
          q = "BINARY " + q
        end

        @resource = resource_class.where(q, q_value).first
      end

      if @resource and valid_params?(field, q_value) and @resource.valid_password?(resource_params[:password]) and (!@resource.respond_to?(:active_for_authentication?) or @resource.active_for_authentication?)
        sign_in_success
      elsif @resource and not (!@resource.respond_to?(:active_for_authentication?) or @resource.active_for_authentication?)
        render_create_error_not_confirmed
      else
        render_create_error_bad_credentials
      end
    end

    private

    def sign_in_success
      # create client id
      @client_id = SecureRandom.urlsafe_base64(nil, false)
      @token = SecureRandom.urlsafe_base64(nil, false)

      @resource.tokens[@client_id] = {
        token: BCrypt::Password.create(@token),
        expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
      }
      @resource.save

      sign_in(:user, @resource, store: false, bypass: false)

      yield @resource if block_given?

      new_auth_header = @resource.build_auth_header(@token, @client_id)
      response.headers.merge!(new_auth_header)

      render_create_success
    end

    def facebook_authenticate
      if params['identity'].nil?
        return false
      end
      @resource = resource_class.where({facebook_identity: params['identity']}).first
    end

    def render_create_success
      render json: {
        data: UserSerializer.new(@resource, {
          custom_include: ['permissions']
        })
      }
    end

    def render_create_error_not_confirmed
      render json: {
        success: false,
        errors: [
          I18n.t(
            "devise_token_auth.sessions.not_confirmed",
            email: @resource.email,
          ),
        ],
        not_confirmed: true,
      }, status: 401
    end

    def clear_ability
      RequestStore.store[:"beeqb_ability_user_#{@resource.id}"] = nil
    end
  end
end
