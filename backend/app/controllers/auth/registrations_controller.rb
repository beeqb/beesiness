module Auth
  class RegistrationsController < DeviseTokenAuth::RegistrationsController
    def create
      @is_facebook = false
      @is_registration = true
      params_for_create_resource = sign_up_params
      password = params_for_create_resource[:password]

      if params_for_create_resource[:password] == '--------'
        @is_facebook = true
        if facebook_authenticate
          @is_registration = false
          return sign_in_success
        end
        password = (0...8).map { ('a'..'z').to_a[rand(26)] }.join
        params_for_create_resource[:password] = password
        params_for_create_resource[:password_confirmation] = password
      end
      @resource = resource_class.new(params_for_create_resource)
      @resource.provider = "email"


      # honor devise configuration for case_insensitive_keys
      if resource_class.case_insensitive_keys.include?(:email)
        @resource.email = sign_up_params[:email].try :downcase
      else
        @resource.email = sign_up_params[:email]
      end

      # give redirect value from params priority
      @redirect_url = params[:confirm_success_url]

      # fall back to default value if provided
      @redirect_url ||= DeviseTokenAuth.default_confirm_success_url

      # success redirect url is required
      if resource_class.devise_modules.include?(:confirmable) && !@redirect_url
        return render_create_error_missing_confirm_success_url
      end

      # if whitelist is set, validate redirect_url against whitelist
      if DeviseTokenAuth.redirect_whitelist
        unless DeviseTokenAuth.redirect_whitelist.include?(@redirect_url)
          return render_create_error_redirect_url_not_allowed
        end
      end

      begin
        # override email confirmation, must be sent manually from ctrl
        resource_class.set_callback("create", :after, :send_on_create_confirmation_instructions)
        resource_class.skip_callback("create", :after, :send_on_create_confirmation_instructions)
        if @resource.save
          yield @resource if block_given?

          unless @resource.confirmed?
            # user will require email authentication
            if @is_facebook
              if @is_registration
                @resource.confirmation_sent_at = Time.now
                @resource.confirmed_at = Time.now
                @resource.save!
              end

              sign_in_success
              return
            end
            @resource.send_confirmation_instructions({
                                                       client_config: params[:config_name],
                                                       redirect_url: @redirect_url,
                                                       password: password,
                                                     })
          else
            # email auth has been bypassed, authenticate user
            @client_id = SecureRandom.urlsafe_base64(nil, false)
            @token = SecureRandom.urlsafe_base64(nil, false)

            @resource.tokens[@client_id] = {
              token: BCrypt::Password.create(@token),
              expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
            }

            @resource.save!

            update_auth_header
          end
          render_create_success
        else
          clean_up_passwords @resource
          render_create_error
        end
      rescue ActiveRecord::RecordNotUnique
        clean_up_passwords @resource
        render_create_error_email_already_exists
      end
    end

    def sign_in_success
      # create client id
      @client_id = SecureRandom.urlsafe_base64(nil, false)
      @token = SecureRandom.urlsafe_base64(nil, false)

      @resource.tokens[@client_id] = {
        token: BCrypt::Password.create(@token),
        expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
      }
      @resource.save

      sign_in(:user, @resource, store: false, bypass: false)

      yield @resource if block_given?

      new_auth_header = @resource.build_auth_header(@token, @client_id)
      response.headers.merge!(new_auth_header)

      render_create_success
    end

    def render_create_success
      render json: {
        data: UserSerializer.new(@resource, {
          custom_include: ['permissions']
        }),
        is_facebook: @is_facebook,
        is_registration: @is_registration,
      }
    end

    def facebook_authenticate
      uid = params[:facebook_data][:uid]
      return false if uid.nil?
      @resource = resource_class.where({facebook_identity: uid}).first
    end

  end
end
