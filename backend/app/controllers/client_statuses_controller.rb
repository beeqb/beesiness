class ClientStatusesController < GenericStatusesController
  load_and_authorize_resource :company
  load_and_authorize_resource :client_status, through: :company
end
