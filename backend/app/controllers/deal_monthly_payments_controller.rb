class DealMonthlyPaymentsController < BaseController
  load_and_authorize_resource :company
  load_and_authorize_resource :deal, through: :company
  load_and_authorize_resource :deal_monthly_payment, through: :deal

  def confirm
    payment = get_entity
    payment.paid_at = Time.now
    deal = payment.deal

    if payment.save
      DealMonthlyPaymentToCashBookMap.call(payment, 'income').try(:save!)
      CreateReceipt.call(deal, :sell, [], payment.amount)

      unless deal.deal_monthly_payments.not_paid.exists?
        deal.update(status: :payed)
      end

      render json: payment
    else
      render json: { errors: payment.errors }, status: :unprocessable_entity
    end
  end

  def discard
    payment = get_entity
    payment.paid_at = nil

    if payment.save
      DealMonthlyPaymentToCashBookMap.call(payment, 'outcome').try(:save!)
      render json: payment
    else
      render json: { errors: payment.errors }, status: :unprocessable_entity
    end
  end

  private

  def deal_monthly_payment_params
    params.require(:deal_monthly_payment).permit(:paid_at)
  end
end
