class CashFlowBudgetCategoriesController < BaseController
  load_and_authorize_resource :company
  load_and_authorize_resource :cash_flow_budget_category, through: :company

  def index
    categories = CashFlowBudgetCategory.where(company_id: params[:company_id])
    serialize = Proc.new do |parent, children|
      CashFlowBudgetCategorySerializer.new(parent, children: children)
    end

    render json: {
      income: categories.income.arrange_serializable(&serialize),
      outcome: categories.outcome.arrange_serializable(&serialize),
      references: CashFlowBudgetCategory.references,
    }
  end

  def delete_safely
    category_id = params[:id]

    if params[:category_after_delete].present?
      category_after_delete = entity_class.find(params[:category_after_delete])
    end

    Deal
      .where(cash_flow_budget_category_id: category_id)
      .update(
        cash_flow_budget_category_id: params[:category_after_delete]
      )

    CashBookRecord
      .where(cash_flow_budget_category_id: category_id)
      .update(
        cash_flow_budget_category_id: params[:category_after_delete]
      )

    get_entity.destroy

    render json: get_entity
  end

  private

  def cash_flow_budget_category_params
    params
      .require(:cash_flow_budget_category)
      .permit(
        :name,
        :kind,
        :parent_id,
        :cash_book_category,
        :company_id
      )
  end
end
