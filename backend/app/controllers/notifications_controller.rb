class NotificationsController < BaseController

  load_and_authorize_resource

  def mark_all_as_read
    affected_number =
      Notification.where(viewed_at: nil).update_all(viewed_at: Time.now)

    render json: { notifications_marked: affected_number }
  end

  private

  def notification_params
    params.require(:notification)
      .permit([
                :viewed_at
              ])
  end

  def query_params
    params.permit(
      :user_id,
    ).merge(default_params)
  end

  def default_params
    { viewed_at: nil }
  end
end
