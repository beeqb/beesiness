class SearchController < ApplicationController
  def cash_book_sources
    token = params[:token]
    company_id = params[:id]

    search_query = Search::Query.new(
      token,
      company_id,
      [:clients, :employees, :suppliers]
    )
    render json: search_query.perform
  end
end
