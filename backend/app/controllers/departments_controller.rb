class DepartmentsController < BaseController

  load_and_authorize_resource :company
  load_and_authorize_resource :department, :through => :company

  private

  def department_params
    params
      .require(:department)
      .permit([
        :name,
        :color,
        :company_id,
        :employee_id,
      ])
  end

  def query_params
    params.permit(
      :name,
      :company_id,
      :employee_id,
    )
  end
end