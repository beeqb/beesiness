class DealStatusesController < GenericStatusesController
  load_and_authorize_resource :company
  load_and_authorize_resource :deal_status, through: :company
end
