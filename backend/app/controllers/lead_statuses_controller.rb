class LeadStatusesController < GenericStatusesController
  load_and_authorize_resource :company
  load_and_authorize_resource :lead_status, through: :company
end
