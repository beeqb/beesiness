class ProductsController < BaseController
  load_and_authorize_resource :company
  load_and_authorize_resource :product, through: :company

  has_scope :available, type: :boolean
  has_scope :by_categories, type: :array

  def destroy
    product = get_entity

    if product.deals.empty?
      product.destroy
      broadcast('product:removed', subject: product, user: current_user)
      render json: product
    else
      render(
        json: { errors: ['Товар участвует в сделке'] },
        status: :unprocessable_entity
      )
    end
  end

  def upload_images
    product = Product.find(params[:id])

    begin
      images = product.images
      images += params.require(:images).permit!.to_h.values
      product.images = images
    rescue ActionController::ParameterMissing
      render json: { errors: [] }, status: :unprocessable_entity
      return
    end

    success = product.save

    if success
      render json: product, custom_include: get_entity_include
    else
      render json: { errors: product.errors }, status: :unprocessable_entity
    end
  end

  def remove_image
    product = Product.find(params[:id])

    remain_images = product.images
    # По каким-то неведомым причинам
    # последняя картинка так не удаляется
    # Если это вдруг понадобится, то надо
    # использовать product.remove_images!
    remain_images.delete_at(params[:index].to_i)
    product.images = remain_images

    if product.save
      render json: product
    else
      render json: { errors: product.errors }, status: :unprocessable_entity
    end
  end

  def recommended
    forbidden_categories = current_employee.forbidden_product_category_ids
    entities = entity_class.available.recommended.where(query_params).accessible_by(current_ability).limit(6)

    if forbidden_categories.present?
      entities = entities.where.not(category_id: forbidden_categories)
    end

    data = {
      entities: ActiveModel::Serializer::CollectionSerializer.new(
        entities,
        custom_include: get_entity_include
      ),
    }

    render json: data
  end

  def set_recommended
    entity = get_entity
    entity.is_recommended = params.require(:is_recommended)
    entity.save!

    render json: entity
  end

  def on_successful_update
    get_entity.previous_changes.each_key do |attribute|
      next if attribute == 'updated_at'

      broadcast(
        'product:attribute_changed',
        subject: get_entity,
        user: current_user,
        actors: { attribute: attribute },
      )
    end
  end

  def search
    forbidden_categories = current_employee.forbidden_product_category_ids

    data = apply_scopes(entity_class)
           .where(query_params)
           .accessible_by(current_ability)

    if forbidden_categories.present? 
      data = data.where.not(category_id: forbidden_categories)
    end

    if params[:or]
      or_query = params[:or].keys.map { |key| "#{key} LIKE ?" }.join(' OR ')
      or_values = params[:or].values.map { |value| "%#{value}%" }

      data = data.where(or_query, *or_values)
    end

    render json: ActiveModel::Serializer::CollectionSerializer.new(
      data,
      custom_include: get_entity_include
    )
  end

  private

  def product_params
    params
      .require(:product)
      .permit(
        :company_id,
        :name,
        :vendor_code,
        :barcode,
        :description,
        :price,
        :first_cost,
        :activity,
        :delivery_time,
        :remove_images,
        :is_recommended,
        :preview_index,
        :quantity,
        :category_id
      )
  end

  def query_params
    params.permit(
      :company_id,
      :name,
      :vendor_code,
      :barcode,
      :price
    )
  end
end
