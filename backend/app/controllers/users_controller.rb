class UsersController < BaseController
  load_and_authorize_resource

  def update
    @user.scenario = 'profile'

    if @user.update(entity_params)
      broadcast('user:updated', subject: @user, user: current_user)
      unless @user.fully_registered
        lead = UserOrCompanyToLeadMap.call(@user)
        lead.save!
        @user.update(fully_registered: true)
      end
      render json: @user, custom_include: get_entity_include
    else
      render json: { errors: @user.errors }, status: :unprocessable_entity
    end
  end

  def upload_image
    user = User.find(params[:id])
    user.image = params[:file]
    success = user.save
    if success
      render json: user, custom_include: get_entity_include
    else
      render json: {errors: user.errors}, status: :unprocessable_entity
    end
  end

  def resend_confirmation
    user = User.where({email: params[:email]}).first
    success = false
    if user
      user.send_confirmation_instructions(
        {
          client_config: 'default',
          redirect_url: 'profile?edit=true',
          password: params[:password],
        }
      )
      success = true
    end

    render json: {
      success: success,
    }
  end

  def search
    users = entity_class.where(query_params)
    render json: users, custom_include: get_entity_include
  end

  def validate
    @user.scenario = 'profile'
    super
  end

  def confirm_invitation
    invitation = UserCompanyInvitation
                   .where('md5(id)=' + params[:token])
                   .where(confirmed_at: nil)
                   .where.not(sent_at: nil)
                   .first
    invitation.confirm

    if success
      redirect_to '/profile'
    end
  end

  private

  def user_params
    p = User.public_params << [:password, :password_confirmation]
    params.require(:user).permit(p.flatten)
  end

  def query_params
    params.permit(
      :email,
      :name,
      :surname,
    )
  end
end
