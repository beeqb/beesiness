class LeadProductsController < BaseController

  load_and_authorize_resource :company
  load_and_authorize_resource :lead, through: :company
  load_and_authorize_resource :lead_product, through: :lead

  private

  def lead_product_params
    params
      .require(:lead_product)
      .permit([
        :lead_id,
        :product_id,
        :count,
      ])
  end

  def query_params
    params.permit(
      :lead_id,
    )
  end
end
