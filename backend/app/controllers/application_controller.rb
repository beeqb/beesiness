class ApplicationController < ActionController::API
  include CanCan::ControllerAdditions
  include ActionController::MimeResponds
  include DeviseTokenAuth::Concerns::SetUserByToken
  include ActionController::Cookies

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_locale

  rescue_from CanCan::AccessDenied do |exception|
    render json: {error: exception.message}, status: :forbidden
  end

  def current_employee
    return nil unless params[:company_id]

    current_user.employee_in_company(params[:company_id])
  end

  def current_ability
    RequestStore.store[:"beeqb_ability_user_#{current_user.id}"] ||= Ability.new(current_user)
  end

  DEFAULT = '<!--# include file="/assets/views/index.html" -->'.freeze
  PLAIN = '<!--# include file="/assets/views/plain.html" -->'.freeze

  def default
    render plain: DEFAULT, content_type: 'text/html'
  end

  def plain
    render plain: PLAIN, content_type: 'text/html'
  end

  def references
    render json: References.all
  end

  def set_locale
    if cookies[:lang].present?
      I18n.locale = cookies[:lang].gsub('\\', '').gsub('"', '').to_sym
    end
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [
      :name,
      {facebook_data: [
        :photo_big,
        :last_name,
        :first_name,
        :email,
        :city,
        :profile,
        :bdate,
        :country,
        :uid,
        :sex,
        :identity,
        :phone,
      ]},
    ])
  end
end
