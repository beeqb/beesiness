class GenericStatusesController < BaseController
  def delete_safely
    status = get_entity
    new_status = params[:status_after_delete]

    if new_status
      entity_class
        .target
        .where(
          status: status,
          company_id: status.company_id
        )
        .update(status: new_status)
    end

    status.destroy

    render json: status, custom_include: get_entity_include
  end

  private

  ['lead', 'deal', 'client', 'supplier'].each do |class_name|
    define_method class_name + '_status_params' do
      params.require(class_name + '_status').permit(:name, :color, :company_id)
    end
  end
end
