class SuppliersController < BaseController
  load_and_authorize_resource :company
  load_and_authorize_resource :supplier, through: :company

  def update
    supplier = get_entity

    if supplier.update(entity_params)
      broadcast('supplier:updated', subject: supplier, user: current_user)

      if supplier.previous_changes[:status].present?
        broadcast(
          'supplier:status_changed',
          subject: supplier,
          user: current_user
        )
      end

      render json: supplier, custom_include: get_entity_include
    else
      render json: { errors: supplier.errors }, status: :unprocessable_entity
    end
  end

  private

  def supplier_params
    params
      .require(:supplier)
      .permit(
        :company_id,
        :name,
        :surname,
        :birthday,
        :country_id,
        :city_id,
        :company_id,
        :user_id,
        :address,
        :latitude,
        :longitude,
        :email,
        :work_phone_code_id,
        :work_phone,
        :work_phone_full,
        :cell_phone_code_id,
        :cell_phone,
        :cell_phone_full,
        :site,
        :skype,
        :facebook,
        :viber,
        :current_job,
        :current_department,
        :current_position,
        :current_is_head,
        :source,
        :status,
        { statuses: [] },
        :manager_id,
        :second_manager_id,
        :visibility,
        :legal_form,
        :discount,
        :notes,
        :legal_entity_id,
        :legal_entity_email,
        :legal_entity_name
      )
  end

  def query_params
    params.permit(
      :company_id,
      :name,
      :status,
      status: []
    )
  end
end
