class SupplierStatusesController < GenericStatusesController
  load_and_authorize_resource :company
  load_and_authorize_resource :supplier_status, through: :company
end
