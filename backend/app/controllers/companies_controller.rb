class CompaniesController < BaseController
  load_and_authorize_resource
  skip_load_and_authorize_resource only: :search

  def upload_logo
    company = Company.find(params[:id])
    company.logo = params[:file]
    success = company.save
    if success
      render json: company, custom_include: get_entity_include
    else
      render json: {errors: company.errors}, status: :unprocessable_entity
    end
  end

  def stream
    company = Company.find(params[:id])
    data =
      company
      .stream
      .order(created_at: :desc)
      .where(stream_params)
      .page(page_params[:page])
      .per(10)

    render json: {
      entities: data,
      total_pages: data.total_pages,
    }
  end

  def invite_user
    invitation = UserCompanyInvitation.new
    invitation.email = params[:email]
    invitation.company_id = params[:id]
    invitation.department_id = params[:department_id]
    invitation.position = params[:position]
    invitation.role = :user
    success = invitation.save

    if success
      broadcast('employee:invited', subject: invitation, user: current_user)
      render json: invitation, custom_include: get_entity_include
    else
      render json: {errors: invitation.errors}, status: :unprocessable_entity
    end
  end

  def search_entities
    search_query = Search::Query.new(
      params[:token],
      params[:id],
      params[:search_section]
    )

    render json: {
      search_results: SearchResultSerializer.new(search_query.perform).as_json,
      references: search_query.references,
    }
  end

  def create
    set_entity(entity_class.new(entity_params))
    if get_entity.save
      broadcast(
        "#{entity_name}:created",
        subject: get_entity,
        user: current_user
      )

      Lead.skip_callback(:create, :before, :invite_user)
      lead = UserOrCompanyToLeadMap.call(get_entity)
      lead.save!
      Lead.set_callback(:create, :before, :invite_user)

      render json: get_entity,
             custom_include: get_entity_include,
             status: :created
    else
      render json: { errors: get_entity.errors }, status: :unprocessable_entity
    end
  end

  def update
    old_director_id = get_entity.director_id
    director_changed =
      entity_params[:director_id] &&
      old_director_id != entity_params[:director_id]

    if get_entity.update(entity_params)
      broadcast('company:updated', subject: get_entity, user: current_user)

      if director_changed
        broadcast(
          'company:director_changed',
          subject: get_entity,
          user: current_user,
          actors: {
            new_director_id: entity_params[:director_id],
            old_director_id: old_director_id,
          }
        )
      end

      render json: get_entity, custom_include: get_entity_include
    else
      render json: {errors: get_entity.errors}, status: :unprocessable_entity
    end
  end

  def search
    companies =
      entity_class
      .where('name LIKE ?', "%#{query_params[:name]}%")

    if query_params[:email]
      companies = companies.where(email: query_params[:email])
    end

    render json: companies, custom_include: get_entity_include
  end

  def sales_report
    report = SalesReport.new(
      get_entity,
      current_user,
      params[:quarter],
      params[:year]
    )

    render json: report.build
  end

  def budget_statistics
    indicators = CompanyIndicators.new(get_entity, current_user)

    render json: {
      data: indicators.to_hash(
        *params[:indicators].delete(' ').split(',')
      ),
      references: CashBookRecord.references,
    }
  end

  def budget_statistics_by_days
    indicators = CompanyIndicators.new(get_entity, current_user)
    stats_by_days = indicators.actual_budget_stats_by_days(params[:min_date], params[:max_date])
    render json: {
        data: {
            indicators: indicators.to_hash(*params[:indicators].delete(' ').split(',')),
            stats_by_days: stats_by_days
        },
        references: CashBookRecord.references
    }
  end

  private

  def company_params
    params.require(:company).permit(
      :logo,
      :name,
      :description,
      :address,
      :latitude,
      :longitude,
      :site,
      :email,
      :phone,
      :currency,
      :vat,
      :phone_code_id,
      :creator_id,
      :director_id,
      :inn,
      :kpp,
      :interest_rate,
      receipt_template: [:header, :item, :footer]
    )
  end

  def query_params
    params.permit(
      :email,
      :name,
      id: []
    )
  end

  def stream_params
    params.permit(:subject_type, :subject_id)
  end
end
