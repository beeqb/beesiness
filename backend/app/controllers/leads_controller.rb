class LeadsController < BaseController

  load_and_authorize_resource :company
  load_and_authorize_resource :lead, :through => :company

  def convert
    lead = get_entity
    lead.assign_attributes(entity_params.to_h)
    client = lead.becomes(Client)
    client.type = 'Client'

    if client.valid?
      client.create_user_company_invitation
      client.save!

      broadcast('lead:converted', subject: lead, user: current_user)

      render json: client, custom_include: get_entity_include
    else
      render json: { errors: client.errors }, status: :unprocessable_entity
    end
  end

  def validate_lead_as_client
    entity = get_entity
    entity.assign_attributes(entity_params.to_h)
    client = entity.becomes(Client)
    if client.valid?
      render json: { errors: client.errors }
    else
      render json: { errors: client.errors }, status: :unprocessable_entity
    end
  end

  def update
    lead = get_entity

    if lead.update(entity_params)
      if lead.previous_changes[:type].present?
        broadcast('lead:converted', subject: lead, user: current_user)
      else
        broadcast('lead:updated', subject: lead, user: current_user)

        if lead.previous_changes[:status].present?
          broadcast('lead:status_changed', subject: lead, user: current_user)
        end
      end

      render json: lead, custom_include: get_entity_include
    else
      render json: {errors: lead.errors}, status: :unprocessable_entity
    end
  end

  private

  def lead_params
    params
      .require(:lead)
      .permit(
        :created_at,
        :company_id,
        :name,
        :surname,
        :birthday,
        :country_id,
        :city_id,
        :company_id,
        :user_id,
        :address,
        :latitude,
        :longitude,
        :email,
        :work_phone,
        :work_phone_code_id,
        :cell_phone,
        :cell_phone_code_id,
        :site,
        :skype,
        :facebook,
        :viber,
        :current_job,
        :current_department,
        :current_position,
        :current_is_head,
        :status,
        {statuses: []},
        :manager_id,
        :second_manager_id,
        :visibility,
        :legal_form,
        :discount,
        :notes,
        :legal_entity_id,
        :legal_entity_email,
        :legal_entity_name,
        :recommender,
        :source,
        :potential_amount,
        :type,
      )
  end

  def query_params
    params.permit(
      :company_id,
      :name,
      :status,
      {status: []},
    )
  end
end
