class CashBookRecordsController < BaseController
  has_scope :in_department, as: :department_id
  has_scope :created_at_date, as: :created_at
  has_scope :by_comment, as: :comment
  has_scope :by_deal_number, as: :deal_number

  def update
    if get_entity.update(entity_params)
      broadcast(
        "#{entity_name}:updated",
        subject: get_entity,
        user: current_user
      )
      CashBook.new(get_entity.company_id).recalculate_balance

      render json: get_entity, custom_include: get_entity_include
    else
      render json: { errors: get_entity.errors }, status: :unprocessable_entity
    end
  end

  def upload_document
    record = CashBookRecord.find(params[:id])
    record.reporting_document = params[:document]
    success = record.save
    if success
      render json: record, custom_include: get_entity_include
    else
      render json: { errors: record.errors }, status: :unprocessable_entity
    end
  end

  def existing_sources
    sources_ids_and_types =
      CashBookRecord
        .where.not(source_id: nil)
        .distinct
        .pluck(:source_id, :source_type)

    sources = Hash.new { |h, k| h[k] = Array.new }

    sources_ids_and_types.each_with_object(sources) do |(id, type), result|
      result[type.downcase.pluralize] << ActiveModelSerializers::SerializableResource.new(type.constantize.find(id))
    end

    render json: sources
  end

  # TODO: Rewrite this
  def earliest_record
      record = CashBookRecord.order(:created_at).where(company_id: params[:id]).limit(1)
      render json: record
  end

  private

  def cash_book_record_params
    params
      .require(:cash_book_record)
      .permit(
        :amount,
        :bill_kind,
        :category,
        :comment,
        :company_id,
        :creator_id,
        :user_id,
        :personal,
        :created_at,
        :currency,
        :reporting_document,
        :cash_flow_budget_category_id,
        :deal_display_name,
        :source_type,
        :source_id
      )
  end

  def query_params
    params.permit(
      :bill_kind,
      :category,
      :company_id,
      :creator_id,
      :amount,
      :balance,
      :cash_flow_budget_category_id,
      :source_type,
      :source_id,
      { id: [] },
    )
  end
end
