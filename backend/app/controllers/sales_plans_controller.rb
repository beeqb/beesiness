class SalesPlansController < BaseController
  # load_and_authorize_resource :company
  # load_and_authorize_resource :sales_plan, :through => :company

  private

  def sales_plan_params
    params
      .require(:sales_plan)
      .permit(
        :company_id,
        :month,
        :year,
        :kind,
        :sum,
        sales_plan_products_attributes: [
          :id,
          :sales_plan_id,
          :product_id,
          :quantity,
          :sum,
          :_destroy,
        ],
        sales_plan_product_categories_attributes: [
          :id,
          :sales_plan_id,
          :product_category_id,
          :sum,
          :_destroy,
        ],
        sales_plan_employees_attributes: [
          :id,
          :sales_plan_id,
          :employee_id,
          :part,
        ]
      )
  end

  def query_params
    params.permit(
      :company_id,
      :year,
      :month,
      year: [],
      month: [],
    )
  end
end
