class DealProductsController < BaseController
  load_and_authorize_resource :company
  load_and_authorize_resource :deal, through: :company
  load_and_authorize_resource :deal_product, through: :deal

  def return
    entity = get_entity
    deal = entity.deal

    products_return = entity.return(params[:count])

    broadcast(
      'deal:partially_returned',
      subject: deal,
      user: current_user,
      actors: { return: products_return }
    )

    render json: entity, custom_include: get_entity_include
  end

  private

  def deal_product_params
    params
      .require(:deal_product)
      .permit([
        :deal_id,
        :product_id,
        :count,
        :price,
      ])
  end

  def query_params
    params.permit(
      :deal_id,
    )
  end
end
