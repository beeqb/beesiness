class ClientsController < BaseController
  load_and_authorize_resource :company
  load_and_authorize_resource :client, through: :company

  def update
    if get_entity.update(entity_params)
      broadcast('client:updated', subject: get_entity, user: current_user)

      if get_entity.previous_changes[:status].present?
        broadcast(
          'client:status_changed',
          subject: get_entity,
          user: current_user
        )
      end

      render json: get_entity, custom_include: get_entity_include
    else
      render json: { errors: get_entity.errors }, status: :unprocessable_entity
    end
  end

  def search
    data = entity_class
           .where(query_params)
           .accessible_by(current_ability)

    if params[:or]
      or_query = params[:or].keys.map { |key| "#{key} LIKE ?" }.join(' OR ')
      or_values = params[:or].values.map { |value| "%#{value}%" }

      data = data.where(or_query, *or_values)
    end

    render json: ActiveModel::Serializer::CollectionSerializer.new(
      data,
      custom_include: get_entity_include
    )
  end

  private

  def client_params
    params
      .require(:client)
      .permit(
        :address,
        :birthday,
        :cell_phone,
        :cell_phone_code_id,
        :cell_phone_full,
        :city_id,
        :company_id,
        :company_id,
        :country_id,
        :current_department,
        :current_is_head,
        :current_job,
        :current_position,
        :deposit,
        :discount,
        :email,
        :facebook,
        :latitude,
        :legal_entity_email,
        :legal_entity_id,
        :legal_entity_name,
        :legal_form,
        :longitude,
        :manager_id,
        :name,
        :notes,
        :second_manager_id,
        :site,
        :skype,
        :source,
        :status,
        :surname,
        :user_id,
        :viber,
        :visibility,
        :work_phone,
        :work_phone_code_id,
        :work_phone_full,
        statuses: []
      )
  end

  def query_params
    params.permit(
      :company_id,
      :name,
      :status,
      status: []
    )
  end
end
