########################################
# Products categories controller class #
########################################
class ProductCategoriesController < BaseController
  load_and_authorize_resource :company
  load_and_authorize_resource :product_category, through: :company

  def delete_safely
    entity = get_entity

    if params[:category_after_delete].present?
      category = ProductCategory.find(params[:category_after_delete])
    end

    entity.products.each do |product|
      if params[:delete_with_products].present?
        product.categories = product.product_categories.ids
        product.destroy
        next
      end

      next unless category

      product.product_categories.delete(entity)
      product.categories = product.product_categories.ids
      product.categories.push(category.id)
      product.categories.uniq!
      product.save!
    end

    if entity.respond_to?(:destroy)
      entity.destroy
      broadcast(
        'product_category:removed',
        subject: entity,
        user: current_user
      )
    end

    render json: entity, custom_include: get_entity_include
  end

  private

  def product_category_params
    params
      .require(:product_category)
      .permit([
        :name,
        :color,
        :company_id,
        :has_part_in_average_price,
      ])
  end

  def query_params
    params.permit(
      :name,
      :company_id
    )
  end
end
