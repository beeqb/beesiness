class DealsController < BaseController
  load_and_authorize_resource :company
  load_and_authorize_resource :deal, through: :company

  has_scope :by_client_name
  has_scope :by_manager
  has_scope :by_contract_conclusion_date, using: [:min, :max], type: :hash
  has_scope :by_started_at, using: [:min, :max], type: :hash
  has_scope :by_closed_at, using: [:min, :max], type: :hash
  has_scope :by_total_sum, using: [:min, :max], type: :hash

  def on_successful_create
    deal = get_entity
    broadcast('deal:created', subject: deal, user: current_user)
    if deal.client
      broadcast(
        'client:deal_made',
        subject: deal.client,
        user: current_user,
        actors: { deal: deal }
      )
    end
  end

  def set_status
    deal = Deal.find(params[:id])
    deal.status = params[:status]
    deal[deal.status.to_s + '_at'] = Time.now

    success = deal.save

    if success
      broadcast('deal:status_changed', subject: deal, user: current_user)
      render json: deal, custom_include: get_entity_include
    else
      render json: { errors: deal.errors }, status: :unprocessable_entity
    end
  end

  def return
    entity = get_entity
    entity.return
    broadcast('deal:returned', subject: entity, user: current_user)

    render json: entity, custom_include: get_entity_include
  end

  def omni_search
    q = params.require(:q)

    render json: entity_class
      .left_outer_joins(:client, :products)
      .where(query_params)
      .where(
        'clients.name LIKE :like_q OR
        clients.surname LIKE :like_q OR
        clients.legal_entity_name LIKE :like_q  OR
        clients.cell_phone LIKE :like_q OR
        clients.work_phone LIKE :like_q OR
        clients.legal_entity_name LIKE :like_q OR
        DATE_FORMAT(deals.created_at, \'%d.%m.%Y\') = :q OR
        deals.number LIKE :like_q OR
        products.vendor_code LIKE :like_q OR
        products.barcode LIKE :like_q',
        q: q,
        like_q: "%#{q}%"
      )
      .group('deals.id')
  end

  def receipt
    render json: { content: get_entity.receipt }
  end

  def receipt_preview
    company = Company.find(params[:company_id])
    dummy_deal = Deal.dummy(company)
    dummy_deal.manager = current_employee
    dummy_deal.payed_at = Time.now

    receipt = Receipt.new(deal: dummy_deal, kind: :sell)

    render json: {
      content: ReceiptContent.new(receipt).html(receipt_preview_params.to_h),
    }
  end

  private

  def receipt_preview_params
    params.require(:templates).permit(:header, :item, :footer)
  end

  # rubocop:disable Metrics/MethodLength
  def deal_params
    params
      .require(:deal)
      .permit(
        :name,
        :contract_number,
        :company_id,
        :legal_form,
        :email,
        :phone,
        :phone_code_id,
        :address,
        :latitude,
        :longitude,
        :started_at,
        :closed_at,
        :notes,
        :visibility,
        :discount,
        :client_id,
        :discount_sum,
        :manager_id,
        :vat_sum,
        :payment_method,
        :second_manager_id,
        :payment_period,
        :status,
        :single_payment_date,
        :monthly_payment_started_at,
        :monthly_payment_ended_at,
        :total_sum,
        :check_sum,
        :contract_started_at,
        :contract_ended_at,
        :payment_status,
        :payment_option,
        :interest_rate,
        :cash_flow_budget_category_id,
        :paid_cash_amount,
        {
          deal_monthly_payments_attributes: [
            :id,
            :amount,
            :date,
            :_destroy,
          ],
        },
        selected_products: [
          :product_id,
          :count,
          :price,
        ]
      )
  end
  # rubocop:enable Metrics/MethodLength

  def query_params
    params.permit(
      :company_id,
      :status,
      status: []
    )
  end
end
