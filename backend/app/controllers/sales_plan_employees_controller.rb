class SalesPlanEmployeesController < BaseController
  def bulk_update
    entities = []

    bulk_update_params[:sales_plan_employees].each do |data|
      unless data[:id]
        entities << SalesPlanEmployee.create(data) 
        next
      end

      if data[:part].blank?
        SalesPlanEmployee.destroy(data[:id])
        next
      end

      entities << SalesPlanEmployee.update(data[:id], data)
    end 

    render json: entities, custom_include: 'employee'
  end

  private

  def bulk_update_params
    params
      .permit(
        sales_plan_employees: [
          :id,
          :sales_plan_id,
          :part,
          :employee_id,
          :product_category_id,
        ]
      )
  end
end
