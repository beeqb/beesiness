class EmployeesController < BaseController
  load_and_authorize_resource :company
  load_and_authorize_resource :employee, through: :company

  def process_data_for_index(entities)
    return entities unless params[:departments]

    entities
      .distinct
      .joins(:employees_departments)
      .where('department_id IN (?)', params[:departments].split(','))
  end

  def sales_funnel
    funnel = SalesFunnel.new(get_entity)
    funnel.deals_scopes = params[:deals_scopes] if params[:deals_scopes]
    funnel.start_date = params[:start_date] if params[:start_date]
    funnel.end_date = params[:end_date] if params[:end_date] 

    render json: {
      funnel_data: funnel.data,
      average_time_until_statuses: funnel.average_time_until_statuses,
      employee_activity: funnel.employee_activity,
    }
  end

  def sales_forecast
    forecast = SalesForecast.new(get_entity)

    render json: forecast.data
  end

  def dismiss
    employee = get_entity
    Task.transfer(employee, current_employee)
    employee.dismiss

    broadcast('employee:dismissed', subject: get_entity, user: current_user)

    render json: employee
  end

  def update
    get_entity.assign_attributes(entity_params)

    dismissed =
      UserActivity.dismissed?(entity_params[:user_activity_id]) &&
      get_entity.user_activity_id_changed?

    if get_entity.save
      broadcast('employee:updated', subject: get_entity, user: current_user)

      if dismissed
        broadcast('employee:dismissed', subject: get_entity, user: current_user)
        Task.transfer(get_entity, current_employee)
        employee.dismiss
      end

      render json: get_entity, custom_include: get_entity_include
    else
      render json: { errors: get_entity.errors }, status: :unprocessable_entity
    end
  end

  def activity
    render json: EmployeeActivity.new(get_entity).data
  end

  private

  def employee_params
    params
      .require(:employee)
      .permit(
        :latitude,
        :longitude,
        :address,
        :email,
        :user_id,
        :company_id,
        :user_activity_id,
        :pass_number,
        :chief_id,
        :salary,
        :activity_changed_at,
        :participation_in_sales,
        :salary_period,
        :job_started_at,
        :vacation_start,
        :vacation_end,
        :identity,
        :card_number,
        :cashbox_name,
        { selected_roles: [] },
        { selected_employees_departments: [
          :id,
          :employee_id,
          :department_id,
          :is_head,
          :position,
        ], },
        { selected_phones: [
          :id,
          :employee_id,
          :country_phone_code_id,
          :phone,
          :kind,
        ], },
        { user_attributes: [:name, :surname, :birthday] },
        forbidden_product_category_ids: []
      )
  end

  def query_params
    params.permit(
      :user_id,
      :company_id,
      :department_id,
      :participation_in_sales,
    )
  end
end
