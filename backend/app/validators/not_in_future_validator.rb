class NotInFutureValidator < ActiveModel::EachValidator
  def validate_each(record, attribute, value)
    return unless value

    error_i18n_key =
      'activerecord.errors.models.%{class}.attributes.%{attribute}.in_future' %
      {class: record.class.name.underscore, attribute: attribute}

    if value.to_date > Date.today
      record.errors.add(attribute, I18n.t(error_i18n_key))
    end
  end
end
