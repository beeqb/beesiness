class SeedCashFlowBudgetCategoriesWorker
  include Sidekiq::Worker

  def perform(company_id)
    income_categories.each do |category_info|
      attributes =
        category_info.merge(
          cash_book_category: :income,
          company_id: company_id,
        )
      create_category(attributes)
    end

    outcome_categories.each do |category_info|
      attributes = category_info.merge(
        cash_book_category: :outcome,
        company_id: company_id,
      )
      create_category(attributes)
    end
  end

  def income_categories
    [
      { name: 'Доход от оказания услуг', kind: :operating },
      {
        name: 'Поступления от финансовой деятельности',
        kind: :financial,
        children: [
          { name: 'Доход по выданному займу', kind: :financial },
          { name: 'Выплаты по депозиту', kind: :financial },
        ]
      },
      { name: 'Доход от продажи товаров', kind: :operating },
      { name: 'Помощь учредителя', kind: :financial },
      { name: 'Возврат средств', kind: :operating },
    ]
  end

  def outcome_categories
    [
      {
        name: 'Персонал',
        kind: :operating,
        children: [
          { name: 'Налоги на ФОТ', kind: :operating },
          { name: 'Обязательное страхование', kind: :operating },
          { name: 'Подбор персонала', kind: :financial },
        ],
      },
      {
        name: 'Хозяйственные расходы',
        kind: :operating,
        children: [
          { name: 'Аренда', kind: :operating },
          { name: 'Уборка помещений', kind: :operating },
          { name: 'Офисные расходы', kind: :operating },
          { name: 'Охрана объектов', kind: :operating },
          { name: 'Коммунальные платежи', kind: :operating },
        ],
      },
      {
        name: 'Маркетинг',
        kind: :operating,
        children: [
          { name: 'Реклама', kind: :operating },
          { name: 'Командировочные расходы', kind: :operating },
        ],
      },
      {
        name: 'IT и связь',
        kind: :operating,
        children: [
          {
            name: 'Информационные технологии',
            kind: :operating,
            children: [
              { name: 'Оплата труда', kind: :operating },
            ],
          },
          { name: 'Услуги связи', kind: :operating },
        ],
      },
      {
        name: 'Инвестиционные расходы',
        kind: :investment,
        children: [
          { name: 'Покупка ОС', kind: :investment }
        ],
      },
      {
        name: 'Налоги',
        kind: :operating,
        children: [
          { name: 'Налог на прибыль', kind: :operating },
          { name: 'НДС', kind: :operating },
          { name: 'Налог УСН', kind: :operating },
          { name: 'Налог по ЕНВД', kind: :operating },
        ]
      },
      {
        name: 'Расходы по основной деятельности',
        kind: :operating,
        children: [
          { name: 'Услуги поставщиков', kind: :operating },
          { name: 'Лизинг', kind: :operating },
          { name: 'Курьерские услуги', kind: :operating },
          { name: 'Транспортный налог', kind: :operating },
          { name: 'Оплата за продукцию', kind: :operating },
          { name: 'Доставка грузов', kind: :operating },
        ],
      },
      {
        name: 'Выплаты по финансовой деятельности',
        kind: :financial,
        children: [
          { name: 'Платеж по кредиту', kind: :financial },
          { name: 'Выдача займов', kind: :financial },
          { name: 'Перевод на депозит', kind: :financial },
          { name: 'Возврат займа', kind: :financial },
        ],
      },
      {
        name: 'Выплаты учредителям и акционерам',
        kind: :financial,
        children: [
          { name: 'Выплата дивидендов', kind: :operating },
          { name: 'Взнос в капитал', kind: :financial },
          { name: 'Распределение прибыли', kind: :financial },
        ],
      },
      {
        name: 'Управленческие расходы',
        kind: :operating,
        children: [
          { name: 'Банковское обслуживание', kind: :operating },
          { name: 'Бухгалтерские услуги', kind: :operating },
          { name: 'Юридические услуги', kind: :operating },
          { name: 'Плата за операции с наличными', kind: :operating },
        ],
      },
      { name: 'Выплата физическим лицам', kind: :operating },
      { name: 'Дивиденды', kind: :operating },
    ]
  end

  def create_category(attributes, parent = nil)
    children = attributes.delete(:children)

    category = CashFlowBudgetCategory.create(attributes.merge(parent: parent))

    if children.present?
      children.each do |category_info|
        child_attributes =
          category_info.merge(
            cash_book_category: category.cash_book_category,
            company_id: category.company_id
          )

        create_category(child_attributes, category)
      end
    end

    category
  end
end
