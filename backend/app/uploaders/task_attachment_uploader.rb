class TaskAttachmentUploader < BaseUploader
  def as_json(options = nil)
    serializable_hash(options).merge({name: File.basename(path)})
  end
end
