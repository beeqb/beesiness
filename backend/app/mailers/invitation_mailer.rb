class InvitationMailer < ApplicationMailer

  def instructions(invitation)

    @invitation = invitation
    @token = Digest::MD5.hexdigest(@invitation.id.to_s)

    mail(
      to: @invitation.email,
      subject: t('mail.invitation_instructions.subject')
    )
  end

end
