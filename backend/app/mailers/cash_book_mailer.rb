class CashBookMailer < ApplicationMailer

  def cash_gap(company)
    @company = company

    employees = @company.employees_with_roles(
      :employee_accountant,
      :employee_director
    )

    mail(
      to: employees.pluck(:email),
      subject: t('mail.cash_book_cash_gap.subject')
    )
  end
end
