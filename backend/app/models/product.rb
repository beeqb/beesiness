# == Schema Information
#
# Table name: products
#
#  id             :integer          not null, primary key
#  name           :string(255)
#  vendor_code    :string(255)
#  barcode        :string(255)
#  description    :string(255)
#  price          :decimal(18, 2)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  deleted_at     :datetime
#  company_id     :integer
#  activity       :string(255)
#  delivery_time  :string(255)
#  images         :json
#  is_recommended :boolean          default(FALSE)
#  preview_index  :integer          default(0)
#  quantity       :decimal(5, 2)    default(0.0)
#
#  Товар
class Product < Base::ResourceRecord
  include CommonScopes

  belongs_to :company
  belongs_to :category, class_name: 'ProductCategory', inverse_of: :products
  has_and_belongs_to_many :deals,
                          join_table: :deal_products

  mount_uploaders :images, ProductImageUploader

  validates :activity, :name, :category, presence: true
  validates :barcode,
            numericality: { only_integer: true },
            length: { minimum: 9 },
            allow_nil: true

  validates :quantity, numericality: { greater_than_or_equal_to: 0 }

  before_save :move_to_archive,
              on: :update,
              if: proc { |product| product.quantity_changed? && product.quantity.zero? }

  scope :recommended, -> { where(is_recommended: true) }
  scope :available, -> { where.not(activity: [:no, :archive]) }
  scope :by_categories, lambda { |categories_ids|
    where(category_id: categories_ids)
  }

  def self.activities
    [
      {
        id: 'yes',
        name: I18n.t('activerecord.product.activities.active'),
      },
      {
        id: 'no',
        name: I18n.t('activerecord.product.activities.disabled'),
      },
      {
        id: 'archive',
        name: I18n.t('activerecord.product.activities.archive'),
      },
      {
        id: 'to_order',
        name: I18n.t('activerecord.product.activities.to_order'),
      },
    ]
  end

  def self.references
    {
      activities: activities,
    }
  end

  def self.labels
    {
      id: '#',
      company_id: I18n.t('activerecord.product.company_id'),
      remove_images: I18n.t('activerecord.product.remove_images'),
      name: I18n.t('activerecord.product.name'),
      vendor_code: I18n.t('activerecord.product.vendor_code'),
      barcode: I18n.t('activerecord.product.barcode'),
      description: I18n.t('activerecord.product.description'),
      price: I18n.t('activerecord.product.price'),
      first_cost: I18n.t('activerecord.product.first_cost'),
      created_at: I18n.t('activerecord.product.created_at'),
      updated_at: I18n.t('activerecord.product.updated_at'),
      deleted_at: I18n.t('activerecord.product.deleted_at'),
      activity: I18n.t('activerecord.product.activity'),
      delivery_time: I18n.t('activerecord.product.delivery_time'),
      category_id: I18n.t('activerecord.product.category'),
      images: I18n.t('activerecord.product.images'),
      xedit_title: I18n.t('common.edit'),
      quantity: I18n.t('activerecord.product.quantity'),
    }
  end

  def move_to_archive
    self.activity = 'archive'
  end
end
