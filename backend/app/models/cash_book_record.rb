# == Schema Information
#
# Table name: cash_book_records
#
#  id                 :integer          not null, primary key
#  category           :integer          not null
#  bill_kind          :integer          not null
#  currency           :string(255)      not null
#  balance            :decimal(18, 2)   not null
#  amount             :decimal(18, 2)   not null
#  comment            :text(65535)
#  creator_id         :integer          not null
#  company_id         :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  deal_id            :integer
#  deleted_at         :datetime
#  personal           :boolean          default(FALSE)
#  personal_balance   :decimal(18, 2)
#  user_id            :integer
#  reporting_document :string(255)
#

######################################
# Модель для записи в кассовой книге #
######################################
class CashBookRecord < Base::ResourceRecord
  enum bill_kind: [:cash, :cashless]

  belongs_to :creator, class_name: 'Employee'
  belongs_to :company
  belongs_to :category_info,
             class_name: 'CashBookCategory',
             foreign_key: :category
  belongs_to :cash_flow_budget_category
  belongs_to :deal
  belongs_to :source, polymorphic: true

  has_one :personal_cash_book_balance, inverse_of: :cash_book_record

  before_validation :set_source_type

  after_create :calculate_balance
  before_save :set_amount_sign

  validates :currency,
            :category,
            :creator,
            :amount,
            presence: true

  validates :amount, numericality: true
  validate :amount, :not_more_than_loan, if: :loan_return?

  mount_uploader :reporting_document, CashBookDocumentUploader

  scope :income, -> { where(category: :income) }
  scope :outcome, -> { where(category: :outcome) }
  scope :loan, -> { where(category: :loan) }
  scope :loan_return, -> { where(category: :loan_return) }

  scope :not_personal, ->() { where(personal: false) }

  scope :including_personal_for, (lambda do |user_id|
    where(personal: false)
      .or(where(personal: true, user_id: user_id))
  end)

  scope :in_department, (lambda do |department_id|
    joins(creator: :employees_departments)
      .where(
        creator: {
          employees_departments: {
            department_id: department_id,
          },
        }
      )
  end)

  scope :by_comment, ->(term) { where('comment LIKE ?', "%#{term}%") }
  scope :by_deal_number, (lambda do |number|
    joins(:deal).where(deals: { number: number })
  end)

  scope :created_at_date, (lambda do |string_date|
    where('DATE(created_at) = ?', string_date)
  end)

  scope :between_dates, (lambda do |min, max|
    max ||= Time.zone.now
    min ||= '1970-01-01 00:00:00'

    where('started_at BETWEEN ? AND ?', min, max)
  end)

  # rubocop:disable Metrics/MethodLength
  def self.labels
    {
      personal: I18n.t('activerecord.cash_book_record.personal'),
      bill_kind: I18n.t('activerecord.cash_book_record.bill_kind'),
      category: I18n.t('activerecord.cash_book_record.category'),
      created_at: I18n.t('activerecord.cash_book_record.created_at'),
      cash_flow_budget_category: \
        I18n.t('activerecord.cash_book_record.cash_flow_budget_category'),
      department: I18n.t('activerecord.cash_book_record.department'),
      creator_id: I18n.t('activerecord.cash_book_record.creator_id'),
      reporting_document: \
        I18n.t('activerecord.cash_book_record.reporting_document'),
      comment: I18n.t('activerecord.cash_book_record.comment'),
      amount: I18n.t('activerecord.cash_book_record.amount'),
      balance: I18n.t('activerecord.cash_book_record.balance'),
      source: I18n.t('activerecord.cash_book_record.source'),
    }
  end

  def self.references
    {
      bill_kinds: [
        {
          id: :cash,
          name: I18n.t('activerecord.cash_book_record.bill_kinds.cash'),
        },
        {
          id: :cashless,
          name: I18n.t('activerecord.cash_book_record.bill_kinds.cashless'),
        },
      ],
    }
  end
  # rubocop:enable Metrics/MethodLength

  def loan_return?
    category == 'loan_return'
  end

  def personal_balance_for(user_id)
    PersonalCashBookBalance
      .find_by(record_id: id, user_id: user_id)
      .try(:balance)
  end

  private

  def calculate_balance
    CashBook.new(company_id).recalculate_balance
  end

  def set_amount_sign
    self.amount *= -1 if category_info.expense? && amount >= 0
  end

  def not_more_than_loan
    records = CashBookRecord.including_personal_for(user_id)
    loan = records.loan.sum(:amount)
    returned = records.loan_return.sum(:amount).abs

    return if loan >= returned + amount

    errors.add(
      :amount,
      I18n.t(
        'activerecord.errors.models.cash_book_record.not_more_than_loan'
      )
    )
  end

  # Суть этого фикса см. в комментарии к #set_subject_type в
  # классе SystemEvent
  def set_source_type
    self.source_type = source.class.name if source_id
  end
end
