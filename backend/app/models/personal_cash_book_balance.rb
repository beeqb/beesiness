# == Schema Information
#
# Table name: personal_cash_book_balances
#
#  id        :integer          not null, primary key
#  user_id   :integer
#  record_id :integer          not null
#  balance   :decimal(18, 2)
#

class PersonalCashBookBalance < ActiveRecord::Base
  belongs_to :user
  belongs_to :cash_book_record, foreign_key: 'record_id'
end
