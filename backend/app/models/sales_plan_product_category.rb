class SalesPlanProductCategory < ApplicationRecord
  belongs_to :product_category
  belongs_to :sales_plan

  scope :by_date, (lambda do |months, year|
    joins(:sales_plan).where(sales_plans: { month: months, year: year })
  end)

  scope :by_year, (lambda do |year|
    joins(:sales_plan).where(sales_plans: { year: year })
  end)

end
