# == Schema Information
#
# Table name: companies
#
#  id            :integer          not null, primary key
#  logo          :string(255)
#  name          :string(255)
#  description   :text(65535)
#  address       :text(65535)
#  latitude      :decimal(20, 14)
#  longitude     :decimal(20, 14)
#  site          :string(255)
#  email         :string(255)
#  phone         :string(255)
#  currency      :string(255)
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  vat           :string(255)
#  phone_code_id :integer
#  is_super      :boolean          default(FALSE)
#  deleted_at    :datetime
#  creator_id    :integer
#  inn           :string(255)      default("0")
#  kpp           :string(255)
#  interest_rate :float(24)
#

##############################
# The class of company model #
##############################
class Company < Base::ResourceRecord
  resourcify

  has_many :product_categories, -> { order(created_at: :desc) }
  has_many :products
  has_many :departments
  has_many :employees

  # select clients only, not leads
  has_many :clients, -> { where(type: 'Client') }
  has_many :client_statuses

  has_many :suppliers, -> { where(type: 'Supplier') }
  has_many :supplier_statuses

  has_many :leads
  has_many :lead_statuses

  has_many :deals
  has_many :deal_statuses

  has_many :statistics,
           -> { order(created_at: :desc) },
           class_name: 'CompanyStatistic'

  has_many :stream,
           -> { order(created_at: :desc) },
           class_name: 'SystemEvent'

  has_many :tasks

  belongs_to :phone_code,
             class_name: 'CountryPhoneCode',
             foreign_key: 'phone_code_id'

  belongs_to :creator,
             class_name: 'User',
             foreign_key: 'creator_id'

  has_many :cash_book_categories
  has_many :cash_flow_budget_categories
  has_many :sales_plans

  validates :inn, :kpp, format: {
    with: /\A[a-zA-Zа-яА-Я0-9]+\z/,
    allow_blank: true,
  }

  validates :creator_id,
            :name,
            :email,
            :vat,
            :currency,
            :phone,
            :phone_code_id,
            :address,
            presence: true

  validates :email, uniqueness: true
  validates :name, format: {
    with: /\A[a-zA-Zа-яА-Я0-9\s"\-']+\z/,
    # message: :invalid_company_name, # custom message not supported by I18.t
  }
  validates :name, format: {
    without: /(beeqb)+/i,
    # message: :not_beeqb_name, # custom message not supported by I18.t
  }, if: proc { |obj| obj.new_record? }
  validates :email, email: true
  validates_length_of :description, :name, maximum: 256, allow_blank: true
  validates :site, url: { allow_blank: true }
  validates :interest_rate, numericality: {allow_nil: true}

  mount_uploader :logo, CompanyLogoUploader

  attr_accessor :director_id # need for company form

  after_find :load_director_id
  after_save :save_director_id
  after_create :create_department_and_creator, :seed_cash_flow_budget_categories
  before_validation :strip_name

  def strip_name
    name.strip! unless name.nil?
  end

  def after_destroy
    employees.each do |employee|
      employee.update_attribute(:deleted_at, Time.now)
    end
  end

  # get sales managers, between who
  # sales plan will be distributed
  def sales_managers
    employees_with_roles(
      :employee_head_of_sales_department,
      :employee_manager_of_sales_department,
    )
    .where(participation_in_sales: 'yes')
  end

  def employees_with_roles(*roles)
    user_ids = []
    roles.each do |role|
      user_ids.push(User.with_role(role, self).pluck(:id))
    end

    user_ids.flatten!

    employees.where(user_id: user_ids)
  end

  def add_user(user, role = nil, department = nil, position = nil)
    employee = Employee.where(
      company_id: id,
      user_id: user.id
    ).first

    if employee.blank?
      # create employee
      employee = Employee.new
      employee.user_id = user.id
      employee.email = user.email
      employee.company_id = id
      employee.user_activity_id = 1
      employee.activity_changed_at = Time.now
      employee.participation_in_sales = 'no'
      employee.job_started_at = Time.now
      employee.address = address
      employee.latitude = latitude
      employee.longitude = longitude
      employee.save!
    end

    if role
      user.add_role(role, self)
      department.add_employee(employee, position) if department
    end
  end

  def remove_user(user)
    # @TODO implement this method:
    # revoke current role
    # remove employee object from company
  end

  def director
    User.with_role(:employee_director, self).first
  end

  def leads_for(person)
    user = person.is_a?(Employee) ? person.user : person
    leads.accessible_by(ability_for(user))
  end

  def deals_for(person)
    user = person.is_a?(Employee) ? person.user : person
    deals.accessible_by(ability_for(user))
  end

  def clients_for(person)
    user = person.is_a?(Employee) ? person.user : person

    clients
      .where
      .not(status: [:lead_new, :lead_in_work, :lead_trash])
      .accessible_by(ability_for(user))
  end

  def products_for(person)
    user = person.is_a?(Employee) ? person.user : person

    products.accessible_by(ability_for(user))
  end

  def clients_count_by_statuses(user)
    statuses = ClientStatus.for_company(id).pluck(:id)

    statuses.map do |status|
      [
        status.to_sym,
        clients.where(status: status).accessible_by(ability_for(user)).count,
      ]
    end.to_h
  end

  def deals_count_by_statuses(user)
    {
      in_progress:
        deals.where(status: 'in_progress')
             .accessible_by(ability_for(user))
             .count,
      contract_conclusion:
        deals.where(status: 'contract_conclusion')
             .accessible_by(ability_for(user))
             .count,
      contract_signed:
        deals.where(status: 'contract_signed')
             .accessible_by(ability_for(user))
             .count,
      won:
        deals.where(status: 'won')
             .accessible_by(ability_for(user))
             .count,
      payed:
        deals.where(status: 'payed')
             .accessible_by(ability_for(user))
             .count,
      lost:
        deals.where(status: 'lost')
             .accessible_by(ability_for(user))
             .count,
      returned:
        deals.where(status: 'returned')
             .accessible_by(ability_for(user))
             .count,
    }
  end

  def suppliers_count_by_statuses(user)
    {
      supplier_new:
        suppliers.where(status: :supplier_new)
               .accessible_by(ability_for(user))
               .count,
      supplier_current:
        suppliers.where(status: :supplier_current)
               .accessible_by(ability_for(user))
               .count,
      supplier_permanent:
        suppliers.where(status: :supplier_permanent)
               .accessible_by(ability_for(user))
               .count,
      supplier_special:
        suppliers.where(status: :supplier_special)
               .accessible_by(ability_for(user))
               .count,
      supplier_no:
        suppliers.where(status: :supplier_no)
               .accessible_by(ability_for(user))
               .count,
    }
  end

  def leads_count_by_statuses(user)
    {
      lead_new:
        leads.where(status: :lead_new)
             .accessible_by(ability_for(user))
             .count,
      lead_in_work:
        leads.where(status: :lead_in_work)
             .accessible_by(ability_for(user))
             .count,
      lead_trash:
        leads.where(status: :lead_trash)
             .accessible_by(ability_for(user))
             .count,
    }
  end

  def plan_income
    current = SalesPlan.current(id)
    current.try(:sum) || 0.0
  end

  def has_supplier?(company)
    Supplier.exists?(legal_entity_id: company.id, company_id: id)
  end

  def self.client_statuses
    ActiveModel::Serializer::CollectionSerializer.new(ClientStatus.system)
  end

  def self.country_phone_codes
    ActiveModel::Serializer::CollectionSerializer.new(
      CountryPhoneCode.all
    )
  end

  def self.deal_statuses
    ActiveModel::Serializer::CollectionSerializer.new(DealStatus.system)
  end

  def self.lead_statuses
    ActiveModel::Serializer::CollectionSerializer.new(LeadStatus.system)
  end

  def self.references
    {
      country_phone_codes: country_phone_codes,
      vat: [
        {
          id: '18',
          name: '18%',
        },
        {
          id: '10',
          name: '10%',
        },
        {
          id: 'not_provided',
          name: 'Не предусмотрен',
        },
        {
          id: '0',
          name: 'Без НДС',
        },
      ],
      currencies: [
        {
          id: 'rub',
          name: 'RUB',
        },
        {
          id: 'eur',
          name: 'EUR',
        },
        {
          id: 'usd',
          name: 'USD',
        },
      ],
      client_statuses: client_statuses,
      deal_statuses: deal_statuses,
      lead_statuses: lead_statuses,
      supplier_statuses: ActiveModel::Serializer::CollectionSerializer.new(
        SupplierStatus.system
      ),
    }
  end

  def self.labels
    {
      id: '#',
      logo: I18n.t('activerecord.company.logo'),
      name: I18n.t('activerecord.company.name'),
      description: I18n.t('activerecord.company.description'),
      address: I18n.t('activerecord.company.address'),
      latitude: I18n.t('activerecord.company.latitude'),
      longitude: I18n.t('activerecord.company.longitude'),
      site: I18n.t('activerecord.company.site'),
      email: I18n.t('activerecord.company.email'),
      phone: I18n.t('activerecord.company.phone'),
      currency: I18n.t('activerecord.company.currency'),
      created_at: I18n.t('activerecord.company.created_at'),
      updated_at: I18n.t('activerecord.company.updated_at'),
      creator: I18n.t('activerecord.company.creator'),
      director: I18n.t('activerecord.company.director'),
      contacts: I18n.t('activerecord.company.contacts'),
      vat: I18n.t('activerecord.company.vat'),
      add_department: I18n.t('activerecord.company.add_department'),
      add_category: I18n.t('activerecord.company.add_category'),
      inn: I18n.t('activerecord.company.inn'),
      kpp: I18n.t('activerecord.company.kpp'),
      interest_rate: I18n.t('activerecord.company.interest_rate'),
    }
  end

  def self.selectable_roles
    Role
      .roles
      .reject { |role_name| role_name.to_s.index('employee_').nil? }
      .select { |role_name| role_name.to_s.index('employee_dismissed').nil? }
      .select { |role_name| role_name.to_s.index('employee_creator').nil? }
      .map { |role_name| { id: role_name, name: I18n.t("roles.#{role_name}") } }
  end

  private

  def create_department_and_creator
    department = Department.new
    department.company_id = id
    department.name = I18n.t('demo.department.name')
    department.color = I18n.t('demo.department.color')
    department.editable = false
    department.save!

    add_user(
      creator,
      :employee_director,
      department,
      I18n.t('roles.' + :employee_director.to_s)
    )
    creator.add_role(:employee_admin, self)
    creator.add_role(:employee_creator, self)
  end

  def load_director_id
    self.director_id = director.try(:id)
  end

  def save_director_id
    return if director_id.nil?

    User.with_role(:employee_director, self).each do |director|
      director.remove_role(:employee_director, self)
    end

    user = User.find(director_id)
    user.add_role(:employee_director, self) if user
  end

  def seed_cash_flow_budget_categories
    SeedCashFlowBudgetCategoriesWorker.perform_async(id)
  end
end
