# == Schema Information
#
# Table name: country_phone_codes
#
#  id           :integer          not null, primary key
#  country_name :string(255)
#  code         :string(255)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  country_code :string(255)
#

class CountryPhoneCode < Base::ApplicationRecord

end
