# == Schema Information
#
# Table name: lead_products
#
#  id          :integer          not null, primary key
#  lead_id     :integer
#  product_id  :integer
#  count       :integer
#  deleted_at  :datetime
#  returned_at :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class LeadProduct < Base::ResourceRecord
  belongs_to :lead
  belongs_to :product
end
