# == Schema Information
#
# Table name: employee_phones
#
#  id                    :integer          not null, primary key
#  employee_id           :integer
#  country_phone_code_id :integer
#  phone                 :string(255)
#  kind                  :string(255)
#  deleted_at            :datetime
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#

class EmployeePhone < Base::ResourceRecord
  belongs_to :country_phone_code
  belongs_to :employee

  validates :phone, :kind, presence: true

  enum kind: {work: 'work', cell: 'cell'}
end
