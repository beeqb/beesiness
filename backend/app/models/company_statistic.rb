# == Schema Information
#
# Table name: company_statistics
#
#  id             :integer          not null, primary key
#  leads_count    :integer
#  deals_count    :integer
#  clients_count  :integer
#  products_count :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  company_id     :integer
#  employee_id    :integer
#

class CompanyStatistic < ActiveRecord::Base
  belongs_to :company
  belongs_to :employee
end
