# == Schema Information
#
# Table name: tasks
#
#  id           :integer          not null, primary key
#  title        :string(255)
#  description  :text(65535)
#  creator_id   :integer
#  executor_id  :integer
#  company_id   :integer
#  attachments  :json
#  important    :boolean
#  urgent       :boolean
#  assessment   :string(255)
#  category     :string(255)
#  start_date   :date
#  due_date     :date
#  estimate     :integer
#  checklist    :json
#  client_id    :integer
#  status       :string(255)
#  started_at   :datetime
#  done_at      :datetime
#  spent_time   :float(24)
#  renew_count  :integer          default(0)
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  assistant_id :integer
#

# Tasks model
class Task < Base::ApplicationRecord
  belongs_to :creator,
             class_name: 'User',
             foreign_key: 'creator_id'
  belongs_to :executor,
             class_name: 'Employee',
             foreign_key: 'executor_id'
  belongs_to :assistant,
             class_name: 'Employee',
             foreign_key: 'assistant_id'

  has_and_belongs_to_many :watchers,
                          class_name: 'Employee',
                          join_table: :tasks_watchers

  belongs_to :client
  belongs_to :company

  validates :title, presence: true
  validates :creator_id, presence: true
  validates :executor_id, presence: true
  validates :start_date, presence: true
  validates :status, presence: true

  mount_uploaders :attachments, TaskAttachmentUploader

  scope :assigned_to, (lambda do |user|
    employee_ids = user.employees.pluck(:id)
    where(executor_id: employee_ids)
  end)

  scope :assigned_to_employee, ->(employee) { where(executor_id: employee.id) }

  scope :created_by, ->(user) { where(creator_id: user.id) }

  scope :active, -> { where.not(status: :done) }
  scope :in_progress, -> { where(status: :in_progress) }
  scope :done, -> { where(status: :done) }

  scope :overdue, -> { where('due_date < ?', Date.today) }

  after_initialize :set_defaults

  def self.finish_all(ids)
    tasks = active.where(id: ids)
    tasks.each(&:finish)

    transaction do
      tasks.each(&:save!)
    end

    tasks
  end

  def self.transfer(from, to)
    active.where(executor_id: from.id).update_all(executor_id: to.id)
  end

  def self.in_this_month
    today = Date.today

    where(
      'created_at >= ? AND created_at <= ?',
      today.beginning_of_month,
      today.end_of_month,
    )
  end

  def start
    self.status = :in_progress
    self.started_at ||= DateTime.now
    self.last_pause_at = DateTime.now
  end

  def suspend
    self.status = :suspended
    self.spent_time ||= 0
    self.spent_time += (DateTime.now.to_i - self.last_pause_at.to_i ) / 3600.0
    self.last_pause_at = DateTime.now
  end

  def finish
    return if status == :done

    self.status = :done
    self.done_at = DateTime.now
  end

  def reopen
    self.status = :new
    self.renew_count += 1
  end

  def set_defaults
    self.status ||= :new
    self.checklist ||= []
    self.important = false if important.nil?
    self.urgent = false if urgent.nil?
  end

  def overdue?
    due_date.try(:past?) || false
  end

  def self.labels
    {
      id: '#',
      title: I18n.t('activerecord.task.title'),
      description: I18n.t('activerecord.task.description'),
      department: I18n.t('activerecord.task.department'),
      creator_id: I18n.t('activerecord.task.creator_id'),
      executor_id: I18n.t('activerecord.task.executor_id'),
      watchers: I18n.t('activerecord.task.watchers'),
      company_id: I18n.t('activerecord.task.company_id'),
      assistant_id: I18n.t('activerecord.task.assistant_id'),
      attachments: I18n.t('activerecord.task.attachments'),
      rating: I18n.t('activerecord.task.rating'),
      assessment: I18n.t('activerecord.task.assessment'),
      category: I18n.t('activerecord.task.category'),
      start_datetime: I18n.t('activerecord.task.start_datetime'),
      deadline: I18n.t('activerecord.task.deadline'),
      due_datetime: I18n.t('activerecord.task.due_datetime'),
      estimate: I18n.t('activerecord.task.estimate'),
      checklist: I18n.t('activerecord.task.checklist'),
      client: I18n.t('activerecord.task.client'),
      status: I18n.t('activerecord.task.status'),
      started_at: I18n.t('activerecord.task.started_at'),
      done_at: I18n.t('activerecord.task.done_at'),
      created_at: I18n.t('activerecord.task.created_at'),
      spent_time: I18n.t('activerecord.task.spent_time'),
      renew_count: I18n.t('activerecord.task.renew_count'),
    }
  end

  def self.references
    References.task
  end
end
