###############
# Статья БДДС #
###############
class CashFlowBudgetCategory < Base::ResourceRecord
  has_ancestry

  enum kind: [:operating, :financial, :investment]

  validates :name, presence: true

  has_many :deals

  scope :income, ->() { where(cash_book_category: :income) }
  scope :outcome, ->() { where(cash_book_category: :outcome) }

  after_save :update_subtree_cash_book_category,
             if: :cash_book_category_changed?

  def self.references
    {
      kinds: [
        {
          id: :operating,
          name: 'Операционный',
        },
        {
          id: :financial,
          name: 'Финансовый',
        },
        {
          id: :investment,
          name: 'Инвестиционный',
        },
      ]
    }
  end

  private

  def update_subtree_cash_book_category
    subtree.update(cash_book_category: cash_book_category)
  end
end
