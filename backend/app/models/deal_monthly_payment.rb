# == Schema Information
#
# Table name: deal_monthly_payments
#
#  id      :integer          not null, primary key
#  amount  :float(24)
#  date    :date
#  deal_id :integer
#  paid_at :datetime
class DealMonthlyPayment < ActiveRecord::Base
  belongs_to :deal

  scope :not_paid, ->() { where(paid_at: nil) }

  def paid?
    !!paid_at
  end
end
