# == Schema Information
#
# Table name: deals
#
#  id                         :integer          not null, primary key
#  number                     :integer
#  contract_number            :string(255)
#  check                      :string(255)
#  invoice                    :string(255)
#  company_id                 :integer
#  client_id                  :integer
#  legal_form                 :string(255)
#  email                      :string(255)
#  phone                      :string(255)
#  address                    :string(255)
#  started_at                 :datetime
#  closed_at                  :datetime
#  notes                      :string(255)
#  visibility                 :string(255)
#  discount                   :integer
#  total_sum                  :decimal(18, 2)
#  check_sum                  :decimal(18, 2)
#  discount_sum               :decimal(18, 2)
#  vat_sum                    :decimal(18, 2)
#  payment_method             :string(255)
#  payment_period             :string(255)
#  status                     :string(255)
#  single_payment_date        :datetime
#  monthly_payment_started_at :datetime
#  monthly_payment_ended_at   :datetime
#  contract_started_at        :datetime
#  contract_ended_at          :datetime
#  manager_id                 :integer
#  second_manager_id          :integer
#  in_progress_at             :datetime
#  contract_conclusion_at     :datetime
#  contract_signed_at         :datetime
#  won_at                     :datetime
#  payed_at                   :datetime
#  lost_at                    :datetime
#  deleted_at                 :datetime
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  latitude                   :decimal(20, 14)
#  longitude                  :decimal(20, 14)
#  phone_code_id              :integer
#  returned_at                :datetime
#  payment_status             :string(255)
#  payment_option             :string(255)
#  interest_rate              :float(24)
#  name                       :string(255)
#

##############################
# The class of deal model #
##############################
class Deal < Base::ResourceRecord
  include CommonScopes

  has_many :deal_products
  has_and_belongs_to_many :products,
                          join_table: :deal_products
  belongs_to :company
  belongs_to :client,
             class_name: 'Client',
             foreign_key: 'client_id'

  belongs_to :manager,
             class_name: 'Employee',
             foreign_key: 'manager_id'

  belongs_to :second_manager,
             class_name: 'Employee',
             foreign_key: 'second_manager_id'

  belongs_to :cash_flow_budget_category

  has_many :deal_monthly_payments
  has_many :receipts, -> { order(created_at: :desc) }

  accepts_nested_attributes_for :deal_monthly_payments, allow_destroy: true

  belongs_to :status_info,
             class_name: 'DealStatus',
             foreign_key: 'status'

  attr_accessor :selected_products

  validates :legal_form,
            :started_at,
            :manager_id,
            :visibility,
            presence: true

  validates :started_at, not_in_future: true
  validates :discount, numericality: { only_integer: true, allow_nil: true }
  validates :interest_rate, numericality: { allow_nil: true }
  validates :interest_rate, presence: true, if: :credit?
  validates :payment_method, presence: true, unless: :single_payment?

  before_create :set_number, :set_initial_status
  before_create :set_close_time,
                if: proc { |deal|
                  deal.status.in?(%w(payed lost returned))
                }
  after_save :save_products, :convert_if_client_is_lead
  before_update :set_status_change_time, if: :status_changed?
  before_update :set_close_time,
                if: proc { |deal|
                  deal.status_changed? &&
                    deal.status.in?(%w(payed lost returned))
                }

  scope :in_progress, ->() { where(status: 'in_progress') }
  scope :won, ->() { where(status: %w(won payed)) }
  scope :lost, ->() { where(status: %w(lost returned)) }
  scope :not_closed, ->() { where('closed_at IS NULL') }
  scope :in_status, ->(status) { where(status: status) }
  scope :by_client_name, (lambda do |client_name|
    joins(:client).where("CONCAT_WS(' ', clients.name, clients.surname) LIKE '%#{client_name}%'")
  end)
  scope :by_contract_conclusion_date, (lambda do |min, max|
    max ||= Time.now
    min ||= '1970-01-01 00:00:00'

    where('contract_conclusion_at BETWEEN ? AND ?', min, max)
  end)

  scope :by_started_at, (lambda do |min, max|
    max ||= Time.now
    min ||= '1970-01-01 00:00:00'

    where('started_at BETWEEN ? AND ?', min, max)
  end)

  scope :not_closed_between, (lambda do |min, max|
    where.not('closed_at BETWEEN ? AND ?', min, max).or(where(closed_at: nil))
  end)

  scope :by_closed_at, (lambda do |min, max|
    max ||= Time.now
    min ||= '1970-01-01 00:00:00'

    where('closed_at BETWEEN ? AND ?', min, max)
  end)

  scope :by_total_sum, (lambda do |min, max|
    max ||= 1_000_000_000
    min ||= 0

    where('total_sum BETWEEN ? AND ?', min, max)
  end)

  scope :by_manager, ->(manager_id) { where(manager_id: manager_id) }

  scope :closed_in_current_month, (lambda do
    now = Time.now
    where(closed_at: now.beginning_of_month..now.end_of_month)
  end)

  scope :paid_in_current_month, (lambda do
    now = Time.now
    in_status(:payed).where(payed_at: now.beginning_of_month..now.end_of_month)
  end)

  scope :paid_in, (lambda do |range|
    in_status(:payed).where(payed_at: range)
  end)

  scope :visible_for, (lambda do |person|
    return unless person

    user_employees_ids = if person.is_a?(Employee)
                           person.id
                         else
                           person.employees.pluck(:id)
                         end

    where(visibility: :all)
      .or(
        where(visibility: :me, manager_id: user_employees_ids)
      )
      .or(
        where(second_manager_id: user_employees_ids)
      )
  end)

  def return
    deal_products.each do |deal_product|
      deal_product.returned_at = Time.now
      deal_product.save!
    end

    self.status = :returned
    save!
  end

  def self.dummy(company)
    dummy_deal = new
    dummy_deal.company = company
    dummy_deal.payment_option = 'cash'

    products = [company.products.first, company.products.second]
    products.each do |product|
      dummy_deal.deal_products.build(
        price: product.price,
        product: product,
        count: 1
      )
    end
    dummy_deal.update_sums
    dummy_deal.paid_cash_amount = dummy_deal.total_sum + 600
    dummy_deal
  end

  # rubocop:disable Metrics/MethodLength
  def self.references
    {
      client_sources: Client.sources,
      rfm_categories: Client.rfm_categories,
      visibilities: [
        {
          id: 'all',
          name: I18n.t('activerecord.deal.visibilities.all'),
        },

        {
          id: 'me',
          name: I18n.t('activerecord.deal.visibilities.me'),
        },
      ],
      legal_forms: [
        {
          id: 'legal_entity',
          name: I18n.t('activerecord.deal.legal_forms.legal_entity'),
        },

        {
          id: 'individual',
          name: I18n.t('activerecord.deal.legal_forms.individual'),
        },
      ],
      payment_periods: [
        {
          id: 'single',
          name: I18n.t('activerecord.deal.payment_periods.single'),
        },
        {
          id: 'monthly',
          name: I18n.t('activerecord.deal.payment_periods.monthly'),
        },
      ],
      payment_methods: [
        {
          id: 'credit',
          name: I18n.t('activerecord.deal.payment_methods.credit'),
        },
        {
          id: 'by_installments',
          name: I18n.t('activerecord.deal.payment_methods.by_installments'),
        },
      ],
      country_phone_codes: ActiveModel::Serializer::CollectionSerializer.new(
        CountryPhoneCode.all
      ),
      payment_options: [
        {
          id: 'cash',
          name: I18n.t('activerecord.deal.payment_options.cash'),
        },

        {
          id: 'card',
          name: I18n.t('activerecord.deal.payment_options.card'),
        },
        {
          id: 'account_charge',
          name: I18n.t('activerecord.deal.payment_options.account_charge'),
        },
      ],
    }
  end

  def self.labels
    {
      id: '#',
      name: I18n.t('activerecord.deal.name'),
      number: I18n.t('activerecord.deal.number'),
      contract_number: I18n.t('activerecord.deal.contract_number'),
      client_id: I18n.t('activerecord.deal.client_id'),
      company_id: I18n.t('activerecord.deal.company_id'),
      address: I18n.t('activerecord.deal.address'),
      email: I18n.t('activerecord.deal.email'),
      phone: I18n.t('activerecord.deal.phone'),
      created_at: I18n.t('activerecord.deal.created_at'),
      status: I18n.t('activerecord.deal.status'),
      manager_id: I18n.t('activerecord.deal.manager_id'),
      second_manager_id: I18n.t('activerecord.deal.second_manager_id'),
      visibility: I18n.t('activerecord.deal.visibility'),
      legal_form: I18n.t('activerecord.deal.legal_form'),
      discount: I18n.t('activerecord.deal.discount'),
      notes: I18n.t('activerecord.deal.notes'),
      started_at: I18n.t('activerecord.deal.started_at'),
      total_sum: I18n.t('activerecord.deal.total_sum'),
      check_sum: I18n.t('activerecord.deal.check_sum'),
      discount_sum: I18n.t('activerecord.deal.discount_sum'),
      vat_sum: I18n.t('activerecord.deal.vat_sum'),
      payment_period: I18n.t('activerecord.deal.payment_period'),
      payment_method: I18n.t('activerecord.deal.payment_method'),
      payment_option: I18n.t('activerecord.deal.payment_option'),
      product_category_id: I18n.t('activerecord.deal.product_category_id'),
      product_id: I18n.t('activerecord.deal.product_id'),
      product_count: I18n.t('activerecord.deal.product_count'),
      closed_at: I18n.t('activerecord.deal.closed_at'),
      client_status: I18n.t('activerecord.deal.client_status'),
      contract_is_active: I18n.t('activerecord.deal.contract_is_active'),
      contract_signed: I18n.t('activerecord.deal.contract_signed'),
      xedit_title: I18n.t('common.edit'),
      interest_rate: I18n.t('activerecord.deal.interest_rate'),
      cash_flow_budget_category: \
        I18n.t('activerecord.deal.cash_flow_budget_category'),
    }
  end
  # rubocop:enable Metrics/MethodLength

  def credit?
    payment_method == 'credit'
  end

  def cash?
    payment_method == 'cash'
  end

  def single_payment?
    payment_period.nil? || payment_period == 'single'
  end

  def update_sums
    self.check_sum = deal_products.inject(0) do |sum, deal_product|
      if deal_product.was_returned?
        sum
      else
        deal_product.price * deal_product.count_minus_returns + sum
      end
    end

    self.discount_sum = check_sum * ((discount || 0) / 100)
    self.total_sum = check_sum - discount_sum

    self.vat_sum = total_sum * ((company.vat.to_f || 0) / 100)
  end

  def update_sums!
    update_sums
    save
  end

  private

  def set_initial_status
    if status
      set_status_change_time
      return
    end

    self.status = 'in_progress'
    self.in_progress_at = Time.now
  end

  def set_status_change_time
    self[status.to_s + '_at'] = Time.now if status_info.system?
  end

  def set_close_time
    self.closed_at = Time.now
  end

  def set_number
    self.number = company.deals.count + 1
  end

  def save_products
    return if selected_products.nil?

    deal_products.delete_all

    selected_products.each { |product| deal_products.create!(product) }
  end

  def convert_if_client_is_lead
    return if client.nil?
    client.convert! if client.type == 'Lead'
  end
end
