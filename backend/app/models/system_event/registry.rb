class SystemEvent::Registry
  def self.include?(event_kind)
    scope, event = parse_kind(event_kind)
    !!all.dig(scope, event)
  end

  def self.local_message_for(event_kind)
    message_for(event_kind, :local)
  end

  def self.general_message_for(event_kind)
    message_for(event_kind, :general)
  end

  def self.all
    {
      company: {
        updated: '%{user_name} изменил (а) информацию о компании',
        director_changed: '%{user_name} поменял (а) директора с %{old_director_link} на %{new_director_link}',
      },
      employee: {
        invited: '%{user_name} пригласил (а) сотрудника в компанию',
        updated: '%{user_name} изменил (а) информацию о сотруднике %{employee_name_link}',
        dismissed: '%{employee_name_link} был (а) уволен (а)',
      },
      department: {
        created: '%{user_name} создал (а) отдел %{department_name}',
        updated: '%{user_name} изменил (а) информацию об отделе %{department_name}',
        removed: '%{user_name} удалил (а) отдел %{department_name}',
      },
      product: {
        created: {
          general: '%{user_name} создал (а) товар %{product_name_link}',
          local: 'создан пользователем %{user_name}',
        },
        updated: {
          general: '%{user_name} изменил (а) информацию о товаре %{product_name_link}',
          local: '%{user_name} изменил (а) информацию',
        },
        attribute_changed: {
          general: '%{user_name} изменил (а) поле «%{attribute_label}» '\
                   'в товаре %{product_name_link} '\
                   'с «%{old_value}» на «%{new_value}»',
          local: '%{user_name} изменил (а) поле «%{attribute_label}» '\
                   'с «%{old_value}» на «%{new_value}»',
        },
        removed: '%{user_name} удалил (а) товар %{product_name}',
        category_changed: {
          general: '%{user_name} перенес (ла) товар %{product_name_link}' \
                   'из категории «%{old_category_name}» ' \
                   'в категорию «%{new_category_name}»',
                    
          local: '%{user_name} перенес (ла) товар из категории ' \
                 '«%{old_category_name}» в категорию «%{new_category_name}»',
        },
      },
      product_category: {
        created: '%{user_name} создал (а) категорию товаров %{category_name}',
        updated: '%{user_name} изменил (а) информацию о категории товаров %{category_name}',
        removed: '%{user_name} удалил (а) категорию товаров %{category_name}',
      },
      lead: {
        created: {
          general: '%{user_name} создал (а) лида %{lead_name_link}',
          local: 'создан пользователем %{user_name}',
        },
        updated: {
          general: '%{user_name} изменил (а) информацию о лиде %{lead_name_link}',
          local: '%{user_name} изменил (а) информацию',
        },
        converted: '%{user_name} конвертировал (а) лида %{lead_name_link} в клиенты',
        status_changed: {
          general: '%{user_name} изменил (а) статус лида %{lead_name_link} с "%{old_status}" на "%{new_status}"',
          local: '%{user_name} изменил (а) статус с "%{old_status}" на "%{new_status}"',
        },
      },
      client: {
        created: {
          general: '%{user_name} создал (а) клиента %{client_name_link}',
          local: 'создан пользователем %{user_name}',
        },
        updated: {
          general: '%{user_name} изменил (а) информацию о клиенте %{client_name_link}',
          local: '%{user_name} изменил (а) информацию',
        },
        deal_made: {
          general: '%{user_name} заключил (а) с клиентом %{client_name_link} сделку %{deal_number_link}',
          local: '%{user_name} заключил (а) сделку %{deal_number_link}',
        },
        status_changed: {
          general: '%{user_name} изменил (а) статус клиента %{client_name_link} с "%{old_status}" на "%{new_status}"',
          local: '%{user_name} изменил (а) статус с "%{old_status}" на "%{new_status}"',
        },
      },
      supplier: {
        created: {
          general: '%{user_name} создал (а) поставщика %{supplier_name_link}',
          local: 'создан пользователем %{user_name}',
        },
        updated: {
          general: '%{user_name} изменил (а) информацию о поставщике %{supplier_name_link}',
          local: '%{user_name} изменил (а) информацию',
        },
        status_changed: {
          general: '%{user_name} изменил (а) статус поставщика %{supplier_name_link} с "%{old_status}" на "%{new_status}"',
          local: '%{user_name} изменил (а) статус с "%{old_status}" на "%{new_status}"',
        },
      },
      deal: {
        created: {
          general: '%{user_name} создал (а) сделку %{deal_number_link}',
          local: 'создана пользователем %{user_name}',
        },
        updated: {
          general: '%{user_name} изменил (а) информацию о сделке %{deal_number_link}',
          local: '%{user_name} изменил (а) информацию',
        },
        status_changed: {
          general: '%{user_name} изменил (а) статус сделки %{deal_number_link} с "%{old_status}" на "%{new_status}"',
          local: '%{user_name} изменил (а) статус с "%{old_status}" на "%{new_status}"',
        },
        returned: {
          general: '%{user_name} оформил (а) возврат сделки %{deal_number_link}',
          local: '%{user_name} оформил (а) возврат',
        },
        partially_returned: {
          general: '%{user_name} оформил (а) частичный возврат сделки %{deal_number_link}',
          local: '%{user_name} оформил (а) частичный возврат',
        },
      }
    }
  end

  def self.parse_kind(event_kind)
    event_kind.split(':').map(&:to_sym)
  end

  private

  def self.message_for(event_kind, level)
    scope, event = parse_kind(event_kind)

    message = all.dig(scope, event)

    if message.is_a?(Hash)
      message[level]
    else
      message
    end
  end
end
