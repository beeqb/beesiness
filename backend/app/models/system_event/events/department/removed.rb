module SystemEvent::Events
  module Department
    class Removed < BaseEvent
      def name
        'department:removed'
      end

      private

      def department_name
        subject.name
      end
    end
  end
end
