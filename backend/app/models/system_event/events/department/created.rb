module SystemEvent::Events
  module Department
    class Created < BaseEvent
      def name
        'department:created'
      end

      private

      def department_name
        subject.name
      end
    end
  end
end
