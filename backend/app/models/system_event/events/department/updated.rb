module SystemEvent::Events
  module Department
    class Updated < BaseEvent
      def name
        'department:updated'
      end

      private

      def department_name
        subject.name
      end
    end
  end
end
