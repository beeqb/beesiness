module SystemEvent::Events
  module Lead
    class Updated < BaseEvent
      def name
        'lead:updated'
      end

      private

      def lead_name_link
        lead = ::Lead.find(subject.id)

        name = if subject.individual?
                 subject.fullname
               else
                 subject.legal_entity_name
               end

        link_to(lead, name)
      end
    end
  end
end
