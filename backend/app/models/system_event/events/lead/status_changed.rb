module SystemEvent::Events
  module Lead
    class StatusChanged < BaseEvent
      def name
        'lead:status_changed'
      end

      private

      def lead_name_link
        lead = ::Lead.find(subject.id)

        lead_name = if subject.legal_form == 'individual'
                      subject.name + ' ' + subject.surname
                    else
                      subject.legal_entity_name
                    end

        link_to(lead, lead_name)
      end

      def new_status
        new_status = statuses.find do |status|
          status.id == subject.previous_changes[:status].last
        end

        new_status ? new_status.name : ''
      end

      def old_status
        old_status = statuses.find do |status|
          status.id == subject.previous_changes[:status].first
        end

        old_status ? old_status.name : ''
      end

      def statuses
        ::LeadStatus.for_company(subject.company_id)
      end
    end
  end
end
