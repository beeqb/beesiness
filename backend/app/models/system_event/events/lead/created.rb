module SystemEvent::Events
  module Lead
    class Created < BaseEvent
      def name
        'lead:created'
      end

      private

      def lead_name_link
        lead = ::Lead.find(subject.id)
        name = if subject.legal_form == 'individual'
                 if  subject.surname
                   subject.name + ' ' + subject.surname
                 else
                   subject.name
                 end
               else
                 subject.legal_entity_name
               end

        link_to(lead, name)
      end
    end
  end
end
