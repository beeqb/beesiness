module SystemEvent::Events
  module Lead
    class Converted < BaseEvent
      def name
        'lead:converted'
      end

      private

      def lead_name_link
        client = ::Client.find(subject.id)

        lead_name = if subject.legal_form == 'individual'
                      if  subject.surname
                        subject.name + ' ' + subject.surname
                      else
                        subject.name
                      end
                    else
                      subject.legal_entity_name
                    end

        link_to(client, lead_name)
      end
    end
  end
end
