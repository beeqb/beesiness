class SystemEvent
  module Events
    # Base class for all events
    class BaseEvent
      attr_accessor :event, :actors

      def initialize(event, actors)
        @event = event
        @actors = actors
      end

      def general_description
        raw_description = SystemEvent::Registry.general_message_for(name)
        raw_description % params_from(raw_description)
      end

      def local_description
        raw_description = SystemEvent::Registry.local_message_for(name)
        raw_description % params_from(raw_description)
      end

      def name
        # override in child
      end

      # override in child
      def meta_data
        {}
      end

      protected

      def params_from(raw_description)
        required_params = raw_description.scan(/%\{(\w+?)\}/).flatten

        format_params = required_params.map do |param_name|
          key = param_name.to_sym
          value = send(param_name)

          [key, value]
        end

        format_params.to_h
      end

      def subject
        @event.subject
      end

      def link_to(entity, text)
        entity_class = entity.class.name.downcase
        url = "/companies/#{@event.company_id}/"\
              "#{entity_class.pluralize}/#{entity.id}"

        "<a href=\"#{url}\">#{text}</a>"
      end

      def user_name
        link_to(
          @event.user.employee_in_company(@event.company_id),
          @event.user.fullname
        )
      end
    end
  end
end
