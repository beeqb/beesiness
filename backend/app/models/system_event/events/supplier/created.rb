module SystemEvent::Events
  module Supplier
    class Created < BaseEvent
      def name
        'supplier:created'
      end

      private

      def supplier_name_link
        supplier = ::Supplier.find(subject.id)
        name = if subject.legal_form == 'individual'
                 subject.name + ' ' + subject.surname
               else
                 subject.legal_entity_name
               end

        link_to(supplier, name)
      end
    end
  end
end
