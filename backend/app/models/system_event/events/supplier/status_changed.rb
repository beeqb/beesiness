module SystemEvent::Events
  module Supplier
    class StatusChanged < BaseEvent
      def name
        'supplier:status_changed'
      end

      private

      def supplier_name_link
        supplier = ::Supplier.find(subject.id)
        name = if subject.legal_form == 'individual'
                 subject.name + ' ' + subject.surname
               else
                 subject.legal_entity_name
               end

        link_to(supplier, name)
      end

      def new_status
        new_status = statuses.find do |status|
          status.id == subject.previous_changes[:status].last
        end

        new_status ? new_status.name : ''
      end

      def old_status
        old_status = statuses.find do |status|
          status.id == subject.previous_changes[:status].first
        end

        old_status ? old_status.name : ''
      end

      def statuses
        ::SupplierStatus.for_company(subject.company_id)
      end
    end
  end
end
