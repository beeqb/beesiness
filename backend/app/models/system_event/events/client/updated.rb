module SystemEvent::Events
  module Client
    class Updated < BaseEvent
      def name
        'client:updated'
      end

      private

      def client_name_link
        client = ::Client.find(subject.id)
        name = if subject.legal_form == 'individual'
                 subject.name + ' ' + subject.surname
               else
                 subject.legal_entity_name
               end

        link_to(client, name)
      end
    end
  end
end
