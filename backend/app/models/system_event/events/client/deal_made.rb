module SystemEvent::Events
  module Client
    class DealMade < BaseEvent
      def name
        'client:deal_made'
      end

      private

      def client_name_link
        client = ::Client.find(subject.id)
        name = if subject.legal_form == 'individual'
                 subject.name + ' ' + subject.surname
               else
                 subject.legal_entity_name
               end

        link_to(client, name)
      end

      def deal_number_link
        link_to(@actors[:deal], @actors[:deal].number)
      end
    end
  end
end
