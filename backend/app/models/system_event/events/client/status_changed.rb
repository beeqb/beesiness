module SystemEvent::Events
  module Client
    class StatusChanged < BaseEvent
      def name
        'client:status_changed'
      end

      private

      def client_name_link
        client = ::Client.find(subject.id)
        name = if subject.legal_form == 'individual'
                 subject.name + ' ' + subject.surname
               else
                 subject.legal_entity_name
               end

        link_to(client, name)
      end

      def new_status
        new_status = statuses.find do |status|
          status.id == subject.previous_changes[:status].last
        end

        new_status ? new_status.name : ''
      end

      def old_status
        old_status = statuses.find do |status|
          status.id == subject.previous_changes[:status].first
        end

        old_status ? old_status.name : ''
      end

      def statuses
        ::ClientStatus.for_company(subject.company_id)
      end
    end
  end
end
