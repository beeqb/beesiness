module SystemEvent::Events
  module Employee
    class Invited < BaseEvent
      def name
        'employee:invited'
      end
    end
  end
end
