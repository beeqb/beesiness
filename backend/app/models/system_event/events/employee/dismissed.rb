module SystemEvent::Events
  module Employee
    class Dismissed < BaseEvent
      def name
        'employee:dismissed'
      end

      private

      def employee_name_link
        link_to(subject, subject.user.fullname)
      end
    end
  end
end
