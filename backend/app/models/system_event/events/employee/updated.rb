module SystemEvent::Events
  module Employee
    class Updated < BaseEvent
      def name
        'employee:updated'
      end

      private

      def employee_name_link
        link_to(subject, subject.user.fullname)
      end
    end
  end
end
