module SystemEvent::Events
  module ProductCategory
    class Removed < BaseEvent
      def name
        'product_category:removed'
      end

      private

      def category_name
        subject.name
      end
    end
  end
end
