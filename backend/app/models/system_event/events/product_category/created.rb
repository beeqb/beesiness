module SystemEvent::Events
  module ProductCategory
    class Created < BaseEvent
      def name
        'product_category:created'
      end

      private

      def category_name
        subject.name
      end
    end
  end
end
