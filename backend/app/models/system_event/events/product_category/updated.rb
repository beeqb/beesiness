module SystemEvent::Events
  module ProductCategory
    class Updated < BaseEvent
      def name
        'product_category:updated'
      end

      private

      def category_name
        subject.name
      end
    end
  end
end
