module SystemEvent::Events
  module Deal
    class PartiallyReturned < BaseEvent
      def name
        'deal:partially_returned'
      end

      private

      def deal_number_link
        link_to(subject, subject.number)
      end
    end
  end
end
