module SystemEvent::Events
  module Deal
    class Returned < BaseEvent
      def name
        'deal:returned'
      end

      private

      def deal_number_link
        link_to(subject, subject.number)
      end
    end
  end
end
