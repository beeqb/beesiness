module SystemEvent::Events
  module Deal
    # Deal creation event
    class Created < BaseEvent
      def name
        'deal:created'
      end

      def meta_data
        {
          new_status: subject.status,
          total_sum: subject.total_sum,
        }
      end

      private

      def deal_number_link
        link_to(subject, subject.number)
      end

    end
  end
end
