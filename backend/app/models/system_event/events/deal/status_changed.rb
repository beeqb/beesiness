module SystemEvent::Events
  module Deal
    class StatusChanged < BaseEvent
      def name
        'deal:status_changed'
      end

      def meta_data
        {
          old_status: subject.previous_changes[:status].first,
          new_status: subject.previous_changes[:status].last,
          total_sum: subject.total_sum,
        }
      end

      private

      def deal_number_link
        link_to(subject, subject.number)
      end

      def new_status
        new_status = statuses.find do |status|
          status.id == subject.previous_changes[:status].last
        end

        new_status ? new_status.name : ''
      end

      def old_status
        old_status = statuses.find do |status|
          status.id == subject.previous_changes[:status].first
        end

        old_status ? old_status.name : ''
      end

      def statuses
        ::DealStatus.for_company(subject.company_id)
      end
    end
  end
end
