module SystemEvent::Events
  module Deal
    class Updated < BaseEvent
      def name
        'deal:updated'
      end

      private

      def deal_number_link
        link_to(subject, subject.number)
      end
    end
  end
end
