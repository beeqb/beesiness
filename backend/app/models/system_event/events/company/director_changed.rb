module SystemEvent::Events
  module Company
    class DirectorChanged < BaseEvent
      def name
        'company:director_changed'
      end

      private

      def new_director_link
        new_director = ::User.find(@actors[:new_director_id])

        link_to(new_director, new_director.name + ' ' + new_director.surname)
      end

      def old_director_link
        old_director = ::User.find(@actors[:old_director_id])

        link_to(old_director, old_director.name + ' ' + old_director.surname)
      end
    end
  end
end
