module SystemEvent::Events
  module Company
    class Updated < BaseEvent
      def name
        'company:updated'
      end
    end
  end
end
