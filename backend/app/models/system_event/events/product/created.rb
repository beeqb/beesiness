module SystemEvent::Events
  module Product
    class Created < BaseEvent
      def name
        'product:created'
      end

      private

      def product_name_link
        link_to(subject, subject.name)
      end
    end
  end
end
