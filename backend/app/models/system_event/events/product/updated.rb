module SystemEvent::Events
  module Product
    class Updated < BaseEvent
      def name
        'product:updated'
      end

      def meta_data
        subject.previous_changes
      end

      private

      def product_name_link
        link_to(subject, subject.name)
      end
    end
  end
end
