module SystemEvent::Events
  module Product
    class Removed < BaseEvent
      def name
        'product:removed'
      end

      private

      def product_name
         subject.name
      end
    end
  end
end
