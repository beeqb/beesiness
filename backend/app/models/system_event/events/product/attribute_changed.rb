module SystemEvent::Events
  module Product
    class AttributeChanged < BaseEvent
      def name
        'product:attribute_changed'
      end

      private

      def old_value
        expand(subject.previous_changes[attribute].first)
      end

      def new_value
        expand(subject.previous_changes[attribute].last)
      end

      def attribute_label
        ::Product.labels[attribute.to_sym]
      end

      def attribute
        @actors[:attribute]
      end

      def product_name_link
        link_to(subject, subject.name)
      end

      # "Раскрывает" значения аттрибута
      # Это нужно для полей, ввод которых производится через select,
      # чтобы в историю попадали человекопонятные значения, а не
      # системные идентификаторы.
      # Для остальных полей просто возвращается значение, как есть
      def expand(value)
        case attribute
        when 'category_id' then ::ProductCategory.find(value).name
        when 'activity'
          ::Product.activities.detect { |activity| activity[:id] == value }[:name]
        else value
        end
      end
    end
  end
end
