module SystemEvent::Events
  module Product
    class CategoryChanged < BaseEvent
      def name
        'product:category_changed'
      end

      def meta_data
        {
          old_category_name: old_category_name,
          new_category_name: new_category_name,
        }
      end

      private

      def product_name_link
        link_to(subject, subject.name)
      end

      def old_category_name
        category_id = subject.previous_changes[:category_id].first
        ProductCategory.find(category_id).name
      end

      def new_category_name
        category_id = subject.previous_changes[:category_id].last
        ProductCategory.find(category_id).name
      end
    end
  end
end
