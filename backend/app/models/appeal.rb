# == Schema Information
#
# Table name: appeals
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Appeal < Base::ApplicationRecord
  resourcify
end
