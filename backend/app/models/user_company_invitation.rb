# == Schema Information
#
# Table name: user_company_invitations
#
#  id            :integer          not null, primary key
#  sent_at       :datetime
#  confirmed_at  :datetime
#  company_id    :integer
#  role          :string(255)
#  email         :string(255)
#  password      :string(255)
#  position      :string(255)
#  department_id :integer
#

class UserCompanyInvitation < Base::ApplicationRecord
  belongs_to :company
  belongs_to :department

  attr_accessor :force_create_user
  attr_accessor :created_user_id # if user force created, contain his id

  validates :email, :role, :company_id, presence: true

  before_create :check_user_existence
  after_create :send_invitation

  def confirm
    user = User.find_by(email: email) || create_user

    if role.to_sym != :client_new
      company.add_user(user, role, department, position)
    end

    self.confirmed_at = Time.now
    save
  end

  def create_user
    user = User.new
    user.provider = 'email'
    user.email = email
    user.name = 'Your name'
    user.password = password
    user.password_confirmation = password
    user.confirmation_sent_at = Time.now
    user.confirmed_at = Time.now
    user.confirmation_token = 'invitation'
    user.save!

    user
  end

  def send_invitation
    InvitationMailer
      .instructions(self)
      .deliver_now

    self.sent_at = Time.now
    save
  end

  private

  def check_user_existence
    return if User.where(email: email).exists?

    unconfirmed_invitation = UserCompanyInvitation.find_by(
      email: email,
      confirmed_at: nil
    )

    if unconfirmed_invitation.nil?
      self.password = (0...8).map { ('a'..'z').to_a[rand(26)] }.join
    else
      self.password = unconfirmed_invitation.password
    end

    return unless force_create_user

    user = create_user
    self.created_user_id = user.id
  end
end
