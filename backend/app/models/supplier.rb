# == Schema Information
#
# Table name: clients
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  surname            :string(255)
#  birthday           :datetime
#  country_id         :integer
#  city_id            :integer
#  company_id         :integer
#  user_id            :integer
#  address            :string(255)
#  email              :string(255)
#  work_phone         :string(255)
#  cell_phone         :string(255)
#  site               :string(255)
#  skype              :string(255)
#  facebook           :string(255)
#  viber              :string(255)
#  current_job        :string(255)
#  current_department :string(255)
#  current_position   :string(255)
#  current_is_head    :boolean
#  relations          :text(65535)
#  deleted_at         :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  status             :string(255)
#  manager_id         :integer
#  second_manager_id  :integer
#  visibility         :string(255)
#  legal_form         :string(255)
#  discount           :integer
#  notes              :text(65535)
#  legal_entity_name  :string(255)
#  legal_entity_email :string(255)
#  legal_entity_id    :integer
#  type               :string(255)      default("Client")
#  potential_amount   :decimal(18, 2)
#  source             :string(255)
#  recommender        :string(255)
#  latitude           :decimal(20, 14)
#  longitude          :decimal(20, 14)
#  work_phone_code_id :integer
#  cell_phone_code_id :integer
#  work_phone_full    :string(255)
#  cell_phone_full    :string(255)
#

class Supplier < Client
  def self.default_scope
    where(deleted_at: nil)
      .where(type: 'Supplier')
  end

  private

  def set_default_status
    self.status = :supplier_new
  end
end

