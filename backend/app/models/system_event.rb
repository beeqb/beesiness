# == Schema Information
#
# Table name: system_events
#
#  id                  :integer          not null, primary key
#  kind                :string(255)
#  user_id             :integer
#  company_id          :integer
#  subject_type        :string(255)
#  subject_id          :integer
#  general_description :text(65535)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  local_description   :text(65535)
#  meta_data           :json
#

class SystemEvent < ActiveRecord::Base
  validate :kind, :kind_is_supported
  after_initialize :set_company

  belongs_to :user
  belongs_to :subject, polymorphic: true
  belongs_to :company
  before_validation :set_subject_type

  private

  def kind_is_supported
    unless SystemEvent::Registry.include?(kind)
      errors.add(:kind, "Event #{kind} is not supported")
    end
  end

  def set_company
    scope, event = SystemEvent::Registry.parse_kind(kind)

    self.company_id = case scope
                      when :company then
                        subject.id
                      else
                        subject.try(:company_id)
                      end
  end

  # Фикс для лидов
  #
  # При использовании STI
  # и polymorhic associations в поле _type пишется
  # имя таблицы, а не класса.
  #
  # В нашем случае, в событиях лидов в поле
  # subject_type все равно будет писаться `Client`,
  # потому что и лиды, и клиенты хранятся в одной таблице.
  #
  # http://stackoverflow.com/questions/9628610/why-polymorphic-association-doesnt-work-for-sti-if-type-column-of-the-polymorph
  def set_subject_type
    self.subject_type = subject.class.name
  end
end
