class DealProductsReturn < ApplicationRecord
  belongs_to :deal
  belongs_to :company
  belongs_to :deal_product

  before_create :calculate_sum

  validate :count_is_not_excess

  private

  def calculate_sum
    self.sum = deal_product.product.price * count
  end

  def count_is_not_excess
    if count > deal_product.count_minus_returns
      errors.add(:count, 'Cannot be greater than deal products left count')
    end
  end
end
