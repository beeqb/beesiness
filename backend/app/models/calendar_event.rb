####################################################################
# Модель для событий на календаре, созданных пользователем вручную #
####################################################################
class CalendarEvent < Base::ResourceRecord
  belongs_to :user
  belongs_to :original_event,
             class_name: 'CalendarEvent',
             foreign_key: 'original_event_id'

  attr_accessor :kind

  # rubocop:disable Metrics/MethodLength
  def self.references
    {
      recurring_options: [
        {
          id: :daily,
          name: I18n.t('activerecord.calendar_event.recurring_options.daily'),
        },
        {
          id: :weekly,
          name: I18n.t('activerecord.calendar_event.recurring_options.weekly'),
        },
        {
          id: :monthly,
          name: I18n.t('activerecord.calendar_event.recurring_options.monthly'),
        },
        {
          id: :yearly,
          name: I18n.t('activerecord.calendar_event.recurring_options.yearly'),
        },
      ],
    }
  end
  # rubocop:enable Metrics/MethodLength
end
