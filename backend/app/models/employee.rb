# == Schema Information
#
# Table name: employees
#
#  id                     :integer          not null, primary key
#  pass_number            :string(255)
#  chief_id               :integer
#  salary                 :decimal(18, 2)
#  salary_period          :string(255)
#  activity_changed_at    :datetime
#  job_started_at         :datetime
#  participation_in_sales :string(255)      default("no")
#  deleted_at             :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  user_id                :integer
#  company_id             :integer
#  user_activity_id       :integer
#  identity               :string(255)
#  card_number            :string(255)
#  latitude               :decimal(20, 14)
#  longitude              :decimal(20, 14)
#  address                :string(255)
#  email                  :string(255)
#
#
# Модель для сущности "Сотрудник"
class Employee < Base::ResourceRecord
  belongs_to :user
  belongs_to :company
  belongs_to :user_activity
  has_many :employee_phones
  has_many :employees_departments
  has_many :departments, through: :employees_departments
  has_many :statistics,
           -> { order(created_at: :desc) },
           class_name: 'CompanyStatistic'
  has_and_belongs_to_many :forbidden_product_categories,
                          class_name: 'ProductCategory',
                          join_table: :employee_forbidden_product_categories

  delegate :name, to: :user
  delegate :surname, to: :user
  delegate :fullname, to: :user

  attr_accessor :selected_phones
  attr_accessor :selected_roles
  attr_accessor :selected_employees_departments

  after_save :save_roles, :save_employees_departments, :save_phones

  accepts_nested_attributes_for :user, update_only: true

  validates :email, email: true
  validates :email, uniqueness: { scope: :company_id }

  with_options if: :on_vacation? do |employee|
    employee.validates :vacation_start, :vacation_end, presence: true
    employee.validate :vacation_end_is_after_start
  end

  def dismiss
    update_attribute(:user_activity_id, UserActivity.dismissed.id)

    user.role_names_in_company(company.id).each do |role_name|
      user.remove_role(role_name, company)
    end

    user.add_role(:employee_dismissed, company)
  end

  def on_vacation?
    UserActivity.vacation?(user_activity_id)
  end

  def self.user_activities
    UserActivity.all
  end

  def self.references
    {
      country_phone_codes: ActiveModel::Serializer::CollectionSerializer.new(
        CountryPhoneCode.all
      ),
      sales_participation: [
        {
          id: 'no',
          name: I18n.t(
            'activerecord.employee.sales_participations.participate'
          ),
        },
        {
          id: 'yes',
          name: I18n.t(
            'activerecord.employee.sales_participations.not_participate'
          ),
        },
      ],

      salary_periods: [
        {
          id: 'hour',
          name: I18n.t('activerecord.employee.salary_periods.hour'),
        },
        {
          id: 'day',
          name: I18n.t('activerecord.employee.salary_periods.day'),
        },
        {
          id: 'week',
          name: I18n.t('activerecord.employee.salary_periods.week'),
        },
        {
          id: 'month',
          name: I18n.t('activerecord.employee.salary_periods.month'),
        },
        {
          id: 'year',
          name: I18n.t('activerecord.employee.salary_periods.year'),
        },
      ],
      user_activities: user_activities,
      phone_kinds: EmployeePhone.kinds,
    }
  end

  def self.labels
    {
      id: '#',
      name: I18n.t('activerecord.employee.name'),
      surname: I18n.t('activerecord.employee.surname'),
      birthday: I18n.t('activerecord.employee.birthday'),
      address: I18n.t('activerecord.employee.address'),
      email: I18n.t('activerecord.employee.email'),
      work_phone: I18n.t('activerecord.employee.work_phone'),
      work_cell_phone: I18n.t('activerecord.employee.work_cell_phone'),
      department: I18n.t('activerecord.employee.department'),
      position: I18n.t('activerecord.employee.position'),
      is_head: I18n.t('activerecord.employee.is_head'),
      salary: I18n.t('activerecord.employee.salary'),
      status: I18n.t('activerecord.employee.status'),
      pass_number: I18n.t('activerecord.employee.pass_number'),
      job_started_at: I18n.t('activerecord.employee.job_started_at'),
      vacation_start: I18n.t('activerecord.employee.vacation_start'),
      vacation_end: I18n.t('activerecord.employee.vacation_end'),
      access: I18n.t('activerecord.employee.access'),
      sales: I18n.t('activerecord.employee.sales'),
      cashbox_name: I18n.t('activerecord.employee.cashbox_name'),
      xedit_title: I18n.t('common.edit'),
    }
  end

  private

  def save_phones
    return if selected_phones.nil?

    max_int_32 = 2_147_483_647
    ids = selected_phones.pluck(:id).select { |id| id < max_int_32 }
    employee_phones.where.not(id: ids).destroy_all

    selected_phones.each do |phone|
      employee_phone = if phone[:id] >= max_int_32
                         EmployeePhone.new
                       else
                         EmployeePhone.find(phone[:id])
                       end

      phone.delete(:id) # no need id
      employee_phone.assign_attributes(phone)
      employee_phone.save!
    end
  end

  def save_roles
    return if selected_roles.nil?

    user.roles
        .where(
          resource_type: 'Company',
          resource_id: company.id
        )
        .where("LOCATE('employee_', name) > 0")
        .where.not(name: 'employee_creator')
        .delete_all

    director_id = nil
    selected_roles.each do |role_name|
      user.add_role(role_name, company)

      director_id = user.id if role_name == 'employee_director'
    end

    unless director_id.nil?
      company.director_id = director_id
      company.save!
    end
  end

  def save_employees_departments
    return if selected_employees_departments.nil?

    max_int_32 = 2_147_483_647 # @todo drop duplicate constant
    ids = selected_employees_departments.pluck(:id).select do |id|
      id < max_int_32
    end
    employees_departments.where.not(id: ids).destroy_all

    selected_employees_departments.each do |employee_department|
      if employee_department[:id] >= max_int_32
        employee_department_model = EmployeesDepartment.new
      else
        employee_department_model =
          EmployeesDepartment.find(employee_department[:id])
      end

      employee_department.delete(:id) # no need id
      employee_department_model.assign_attributes(employee_department)
      employee_department_model.save!
    end
  end

  def vacation_end_is_after_start
    return if vacation_end.nil? || vacation_start.nil?

    unless vacation_end > vacation_start
      errors.add(:vacation_end, 'Не может быть раньше даты начала')
    end
  end
end
