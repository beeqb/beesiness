# == Schema Information
#
# Table name: clients
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  surname            :string(255)
#  birthday           :datetime
#  country_id         :integer
#  city_id            :integer
#  company_id         :integer
#  user_id            :integer
#  address            :string(255)
#  email              :string(255)
#  work_phone         :string(255)
#  cell_phone         :string(255)
#  site               :string(255)
#  skype              :string(255)
#  facebook           :string(255)
#  viber              :string(255)
#  current_job        :string(255)
#  current_department :string(255)
#  current_position   :string(255)
#  current_is_head    :boolean
#  relations          :text(65535)
#  deleted_at         :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  status             :string(255)
#  manager_id         :integer
#  second_manager_id  :integer
#  visibility         :string(255)
#  legal_form         :string(255)
#  discount           :integer
#  notes              :text(65535)
#  legal_entity_name  :string(255)
#  legal_entity_email :string(255)
#  legal_entity_id    :integer
#  type               :string(255)      default("Client")
#  potential_amount   :decimal(18, 2)
#  source             :string(255)
#  recommender        :string(255)
#  latitude           :decimal(20, 14)
#  longitude          :decimal(20, 14)
#  work_phone_code_id :integer
#  cell_phone_code_id :integer
#  work_phone_full    :string(255)
#  cell_phone_full    :string(255)
#

###############################
# The model for client entity #
###############################
class Client < Base::ResourceRecord
  include CommonScopes

  def self.default_scope
    where(deleted_at: nil)
      .where(type: 'Client')
  end

  resourcify

  belongs_to :user
  belongs_to :company
  belongs_to :city
  belongs_to :country
  belongs_to :legal_entity,
             foreign_key: 'legal_entity_id',
             class_name: 'Company'
  belongs_to :manager,
             foreign_key: 'manager_id',
             class_name: 'Employee'
  belongs_to :second_manager,
             foreign_key: 'second_manager_id',
             class_name: 'Employee'
  belongs_to :work_phone_code,
             class_name: 'CountryPhoneCode',
             foreign_key: 'work_phone_code_id'
  belongs_to :cell_phone_code,
             class_name: 'CountryPhoneCode',
             foreign_key: 'cell_phone_code_id'
  belongs_to :status_info,
             class_name: 'ClientStatus',
             foreign_key: 'status'

  has_many :deposit_fees
  has_many :deals

  validates :email, email: true, allow_blank: true

  validates :cell_phone, :cell_phone_code_id, presence: true,
            if: [:individual?, :additional_fields_is_required?]

  with_options if: :individual? do |client|
    client.validates :name, presence: true

    client.validates_uniqueness_of :email, scope: [:company_id],
                                   allow_blank: true
    client.validates :birthday, not_in_future: true
  end

  validates :legal_entity_email, presence: true,
            if: [:legal_entity?, :additional_fields_is_required?]

  with_options if: :legal_entity? do |client|
    client.validates :legal_entity_name, presence: true
    client.validates_uniqueness_of :legal_entity_email, scope: [:company_id],
                                   allow_blank: true
  end

  validates :surname,
            :address,
            :work_phone_code_id,
            :work_phone,
            :email,
            presence: true, if: :additional_fields_is_required?

  before_create :invite_user, :set_default_status
  before_save :set_full_phones

  scope :visible_for, ->(person) {
    return unless person

    user_employee_ids = if person.is_a?(Employee)
                          person.id
                        else
                          person.employees.pluck(:id)
                        end

    where(visibility: [:all, nil])
      .or(
        where(visibility: :me, manager_id: user_employee_ids)
      )
      .or(
        where(second_manager_id: user_employee_ids)
      )
  }

  scope :managed_by, ->(person) {
    user_employee_ids = if person.is_a?(Employee)
                          person.id
                        else
                          person.employees.pluck(:id)
                        end
    where(manager_id: user_employee_ids)
      .or(
        where(second_manager_id: user_employee_ids)
      )
  }

  def individual?
    legal_form == 'individual'
  end

  def legal_entity?
    legal_form == 'legal_entity'
  end

  def additional_fields_is_required?
    false
  end

  def fullname
    return name unless surname

    name + ' ' + surname
  end

  def self.sources
    [
      {
        id: 'another_client',
        color: '#f05050',
        name: I18n.t('activerecord.lead.sources.another_client'),
      },
      {
        id: 'site',
        color: '#98a6ad',
        name: I18n.t('activerecord.lead.sources.site'),
      },
      {
        id: 'call',
        color: '#27c24c',
        name: I18n.t('activerecord.lead.sources.call'),
      },
      {
        id: 'social_media',
        color: '#7266ba',
        name: I18n.t('activerecord.lead.sources.social_media'),
      },
    ]
  end

  def self.references
    {
      sources: sources,
      visibilities: [
        {
          id: 'all',
          name: I18n.t('activerecord.client.visibilities.all'),
        },

        {
          id: 'me',
          name: I18n.t('activerecord.client.visibilities.me'),
        },
      ],
      legal_forms: [
        {
          id: 'legal_entity',
          name: I18n.t('activerecord.client.legal_forms.legal_entity'),
        },

        {
          id: 'individual',
          name: I18n.t('activerecord.client.legal_forms.individual'),
        },
      ],
      country_phone_codes: ActiveModel::Serializer::CollectionSerializer.new(
        CountryPhoneCode.all
      ),
      companies: ActiveModel::Serializer::CollectionSerializer.new(
        Company.all
      ),
      rfm_categories: rfm_categories,
    }
  end

  def self.rfm_categories
    [
      {
        id: :big_whale,
        name: I18n.t('activerecord.client.rfm.big_whale'),
      },
      {
        id: :whale,
        name: I18n.t('activerecord.client.rfm.whale'),
      },
      {
        id: :big_dolphin,
        name: I18n.t('activerecord.client.rfm.big_dolphin'),
      },
      {
        id: :dolphin,
        name: I18n.t('activerecord.client.rfm.dolphin'),
      },
      {
        id: :crucian,
        name: I18n.t('activerecord.client.rfm.crucian'),
      },
    ]
  end

  def self.labels
    {
      id: '#',
      name: I18n.t('activerecord.client.name'),
      surname: I18n.t('activerecord.client.surname'),
      birthday: I18n.t('activerecord.client.birthday'),
      country_id: I18n.t('activerecord.client.country_id'),
      city_id: I18n.t('activerecord.client.city_id'),
      company_id: I18n.t('activerecord.client.company_id'),
      address: I18n.t('activerecord.client.address'),
      email: I18n.t('activerecord.client.email'),
      work_phone: I18n.t('activerecord.client.work_phone'),
      cell_phone: I18n.t('activerecord.client.cell_phone'),
      site: I18n.t('activerecord.client.site'),
      skype: I18n.t('activerecord.client.skype'),
      facebook: I18n.t('activerecord.client.facebook'),
      viber: I18n.t('activerecord.client.viber'),
      current_job: I18n.t('activerecord.client.current_job'),
      current_department: I18n.t('activerecord.client.current_department'),
      current_position: I18n.t('activerecord.client.current_position'),
      current_is_head: I18n.t('activerecord.client.current_is_head'),
      relations: I18n.t('activerecord.client.relations'),
      created_at: I18n.t('activerecord.client.created_at'),
      status: I18n.t('activerecord.client.status'),
      manager_id: I18n.t('activerecord.client.manager_id'),
      second_manager_id: I18n.t('activerecord.client.second_manager_id'),
      visibility: I18n.t('activerecord.client.visibility'),
      legal_form: I18n.t('activerecord.client.legal_form'),
      discount: I18n.t('activerecord.client.discount'),
      notes: I18n.t('activerecord.client.notes'),
      legal_entity_name: I18n.t('activerecord.client.legal_entity_name'),
      legal_entity_email: I18n.t('activerecord.client.legal_entity_email'),
      source: I18n.t('activerecord.client.source'),
      deposit: I18n.t('activerecord.client.deposit'),
    }
  end

  def create_user_company_invitation
    if email
      invitation = UserCompanyInvitation.new
      invitation.email = email
      invitation.company_id = company_id
      invitation.role = :client_new

      invitation.force_create_user = true unless user_id
      invitation.save!

      self.user_id = invitation.created_user_id unless user_id
    end

    return if legal_entity_id || !legal_entity?

    client_company = Company.new
    client_company.name = legal_entity_name
    client_company.email = legal_entity_email
    client_company.address = address
    client_company.creator_id = user_id
    client_company.vat = 0
    client_company.currency = 'rub'
    client_company.inn = 0
    client_company.phone = work_phone
    client_company.phone_code_id = work_phone_code_id

    if client_company.valid?
      client_company.save!

      supplier = CompanyToSupplierMap.call(company)
      supplier.company_id = client_company.id
      supplier.save!
    end
  end

  private

  def set_default_status
    self.status = :client_new
  end

  def invite_user
    create_user_company_invitation if self.class != Lead
  end

  def set_full_phones
    self.work_phone_full = if work_phone_code && work_phone
                             work_phone_code.code + ' ' + work_phone
                           else
                             work_phone
                           end

    self.cell_phone_full = if cell_phone_code && cell_phone
                             cell_phone_code.code + ' ' + cell_phone
                           else
                             cell_phone
                           end
  end
end
