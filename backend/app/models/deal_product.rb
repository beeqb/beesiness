# == Schema Information
#
# Table name: deal_products
#
#  id          :integer          not null, primary key
#  deal_id     :integer
#  product_id  :integer
#  count       :integer
#  deleted_at  :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  returned_at :datetime
#  price       :decimal(18, 2)
#
# Модель, описывающая товар, участвующий в сделке
# Содержит в себе информацию о кол-ве товара, которое
# продается в сделке
class DealProduct < Base::ResourceRecord
  belongs_to :deal
  belongs_to :product

  has_many :returns, class_name: 'DealProductsReturn'

  validates :count, numericality: { only_integer: true }
  scope :not_returned, ->() { where(returned_at: nil) }

  def return(returned_count)
    products_return = returns.create!(
      company_id: deal.company_id,
      deal_id: deal_id,
      count: returned_count
    )

    update!(returned_at: Time.now) if count == returns.sum(:count)

    products_return
  end

  def count_minus_returns
    count - returns.sum(:count)
  end

  def was_returned?
    !returned_at.nil?
  end

  def self.references
    {}
  end

  def self.labels
    {
      product_count: I18n.t('activerecord.deal.product_count'),
      product_id: I18n.t('activerecord.deal.product_id'),
      product_category_id: I18n.t('activerecord.deal.product_category_id'),
    }
  end
end
