# == Schema Information
#
#
# Table name: departments
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  color      :string(255)
#  deleted_at :datetime
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  company_id :integer
#  editable   :boolean          default(TRUE)
#

class Department < Base::ResourceRecord
  resourcify

  belongs_to :company
  has_many :employees_departments
  has_many :employees, through: :employees_departments

  validates :name, :color, presence: true
  validates_length_of :color, minimum: 6, maximum: 6

  def after_destroy
    employees_departments.each do |employee_department|
      employee_department.update_attribute(:deleted_at, Time.now)
    end
  end

  def head
    employees_departments.find_by(is_head: true).try(:employee)
  end

  def has_employee(employee)
    0 < EmployeesDepartment
          .where(
            employee_id: employee.id,
            department_id: self.id,
          )
          .count
  end

  def add_employee(employee, position = nil)
    return if has_employee(employee)

    ed = EmployeesDepartment.new
    ed.employee_id = employee.id
    ed.department_id = self.id
    ed.position = position
    ed.is_head = false
    ed.save!
  end

  def remove_employee(employee)
    EmployeesDepartment.find(employee.id).destroy
  end

  def self.references
    {}
  end

  def self.labels
    {
      id: '#',
      name: I18n.t('activerecord.department.name'),
      color: I18n.t('activerecord.department.color'),
    }
  end

end
