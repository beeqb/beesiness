######################################################
# Вспомогательные методы для работы с датой/временем #
######################################################
module DateTimeHelper
  def current_month_range
    now.beginning_of_month..now.end_of_month
  end

  def prev_month_range
    month_ago = now - 1.month
    month_ago.beginning_of_month..month_ago.end_of_month
  end

  def slected_month_range_from_now(num)
    month_ago = now - num.to_i.month
    month_ago.beginning_of_month..month_ago.end_of_month
  end

  def current_year_range
    now.beginning_of_year..now.end_of_year
  end

  def year_range(year)
    date = Time.new(year, 1, 1)
    date.beginning_of_year..date.end_of_year
  end

  def current_quarter_range
    now.beginning_of_quarter..now.end_of_quarter
  end

  def quarter_months_range(quarter, year)
    date = Time.new(year, quarter * 3, 1)
    date.beginning_of_quarter..date.end_of_quarter
  end

  def quarter_months_number_range(quarter)
    (quarter * 3 - 2)..(quarter * 3)
  end

  def today_range
    now.beginning_of_day..now.end_of_day
  end

  def now
    Time.zone.now
  end

  def today
    Time.zone.today
  end

  # Возвращает массив, содержащий
  # только месяцы, входящие в интервал.
  # Месяц представлен объектом Date
  # вида Date(year, month, 1).
  def months_in_range(range)
    new_range = Range.new(
      range.begin.to_date.change(day: 1),
      range.end.to_date,
      range.exclude_end?
    )
    new_range.select { |date| date.day == 1 }
  end

  # Возвращает hash, ставящий в соответствие дате порядковый номер.
  # Ключами являются объекты Date, соответствующие первому дню месяца,
  # значениями - порядковый номер (отсчет начинается с единицы)
  #
  # Аргумент period - временной интервал в прошлом, используйте значения вида
  # 1.month, 2.years и т.д., т.е. объекты класса ActiveSupport::Duration
  #
  # Например, period = 2.years, сейчас январь 2017 года.
  # Метод вернет следующий hash:
  # {
  #   Date(2015, 1, 1) => 1,
  #   Date(2015, 2, 1) => 2,
  #   ...
  #   Date(2016, 1, 1) => 12,
  #   ...
  #   Date(2017, 1, 1) => 24,
  # }
  def index_months(period, direction = :backward)
    range = if direction == :backward
              (today - period).next_month...today
            else
              today...(today + period).prev_month
            end

    months_in_range(range)
      .each_with_index.map { |date, index| [date, index + 1] }.to_h
  end
end
