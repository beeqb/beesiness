#################################################
# Class for get all permissions of current_user #
#################################################

module Permissions
  def permissions
    permissions = {}

    rules.each do |rule|
      next unless rule.base_behavior

      expand_actions(rule.actions).each do |action|
        permissions[action] ||= []

        rule.subjects.each do |subject|
          permission = {
            name: subject.to_s,
          }

          unless rule.conditions.blank?
            permission[:conditions] = rule.conditions
          end

          if !rule.conditions.empty? && subject.respond_to?(:where)
            permission[:ids] = subject.where(rule.conditions).pluck(:id)
          end

          index = permissions[action].index { |p| p[:name] == permission[:name] }
          if index.nil?
            permissions[action].push(permission)
          else
            unless permission[:ids].nil?
              permissions[action][index][:ids] += permission[:ids]
              permissions[action][index][:ids].uniq!
            end
            unless permission[:conditions].nil?
              permission[:conditions].each do |key, value|
                if permissions[action][index][:conditions][key].blank?
                  permissions[action][index][:conditions][key] = []
                end
                permissions[action][index][:conditions][key] += Array.wrap(value)
                permissions[action][index][:conditions][key].uniq!
              end
            end
          end
        end
      end
    end

    permissions
  end
end
