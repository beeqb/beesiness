module CommonScopes
  extend ActiveSupport::Concern

  included do
    scope :created_after_hire, ->(company_id, person) do
      return unless person && company_id

      employee = if person.is_a?(Employee)
                   person
                 elsif person.is_a?(User)
                   person.employees.find_by(company_id: company_id)
                 end

      return unless employee

      where(company_id: company_id)
        .where('created_at >= ?', employee.job_started_at)
    end
  end
end
