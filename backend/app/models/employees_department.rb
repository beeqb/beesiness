# == Schema Information
#
# Table name: employees_departments
#
#  department_id :integer
#  employee_id   :integer
#  is_head       :boolean
#  position      :string(255)
#  id            :integer          not null, primary key
#  created_at    :datetime
#  updated_at    :datetime
#  deleted_at    :datetime
#

class EmployeesDepartment < Base::ResourceRecord
  belongs_to :employee
  belongs_to :department

  validates :employee_id, :department_id, presence: true
end
