module References
  def self.all
    {
      task: task,
      client: client,
    }
  end

  def self.client
    {
      sources: [
        {
          id: 'another_client',
          color: '#f05050',
          name: I18n.t('activerecord.lead.sources.another_client'),
        },
        {
          id: 'site',
          color: '#98a6ad',
          name: I18n.t('activerecord.lead.sources.site'),
        },
        {
          id: 'call',
          color: '#27c24c',
          name: I18n.t('activerecord.lead.sources.call'),
        },
        {
          id: 'social_media',
          color: '#7266ba',
          name: I18n.t('activerecord.lead.sources.social_media'),
        },
      ],
      visibilities: [
        {
          id: 'all',
          name: I18n.t('activerecord.client.visibilities.all'),
        },

        {
          id: 'me',
          name: I18n.t('activerecord.client.visibilities.me'),
        },
      ],
      legal_forms: [
        {
          id: 'legal_entity',
          name: I18n.t('activerecord.client.legal_forms.legal_entity'),
        },

        {
          id: 'individual',
          name: I18n.t('activerecord.client.legal_forms.individual'),
        },
      ],
      statuses: [
        {
          id: :client_new,
          name: I18n.t('roles.' + :client_new.to_s),
          color: '#f05050',
        },
        {
          id: :client_current,
          name: I18n.t('roles.' + :client_current.to_s),
          color: '#23b7e5',
        },
        {
          id: :client_permanent,
          name: I18n.t('roles.' + :client_permanent.to_s),
          color: '#7266ba',
        },
        {
          id: :client_special,
          name: I18n.t('roles.' + :client_special.to_s),
          color: '#27c24c',
        },
        {
          id: :client_no,
          name: I18n.t('roles.' + :client_no.to_s),
          color: '#58666e',
        },
      ],
    }
  end

  def self.task
    {
      statuses: [
        {
          id: :new,
          name: 'Новая',
        },
        {
          id: :suspended,
          name: 'Приостановлена',
        },
        {
          id: :in_progress,
          name: 'Выполняется',
        },
        {
          id: :done,
          name: 'Завершена',
        },
      ],
      importance: [
        { id: 0, name: 'Не важная' },
        { id: 1, name: 'Важная' },
      ],
      urgency: [
        { id: 0, name: 'Не срочная' },
        { id: 1, name: 'Срочная' },
      ],
    }
  end
end
