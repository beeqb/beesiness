# == Schema Information
#
# Table name: product_categories
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  color      :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  company_id :integer
#  deleted_at :datetime
#

class ProductCategory < Base::ResourceRecord
  belongs_to :company
  has_many :products, inverse_of: :category, foreign_key: 'category_id'

  validates :name, :color, presence: true
  validates_length_of :color, minimum: 6, maximum: 6

  def self.references
    {}
  end

  def self.labels
    {
      id: '#',
      name: I18n.t('activerecord.product_category.name'),
      has_part_in_average_price: \
        I18n.t('activerecord.product_category.has_part_in_average_price'),
      color: I18n.t('activerecord.product_category.color'),
      category_after_delete: \
        I18n.t('activerecord.product_category.category_after_delete'),
    }
  end

end
