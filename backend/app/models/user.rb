# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  provider               :string(255)      default("email"), not null
#  uid                    :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  confirmation_token     :string(255)
#  confirmed_at           :datetime
#  confirmation_sent_at   :datetime
#  unconfirmed_email      :string(255)
#  image                  :string(255)
#  name                   :string(255)
#  surname                :string(255)
#  email                  :string(255)
#  public_email           :string(255)
#  birthday               :date
#  skype                  :string(255)
#  whatsapp               :string(255)
#  viber                  :string(255)
#  facebook_link          :string(255)
#  cell_phone             :string(255)
#  lang                   :string(255)
#  status                 :string(255)
#  tokens                 :text(65535)
#  created_at             :datetime
#  updated_at             :datetime
#  appeal_id              :integer
#  sex                    :string(255)
#  city_id                :integer
#  balance                :decimal(10, )    default(0)
#  bonus                  :decimal(10, )    default(0)
#  facebook_identity      :string(255)
#  cell_phone_code_id     :integer
#  deleted_at             :datetime
#  last_seen              :datetime
#  facebook_image_url     :string(255)
#  latitude               :decimal(20, 14)
#  longitude              :decimal(20, 14)
#  address                :string(255)
#

class User < Base::ResourceRecord
  rolify

  has_many :employees

  belongs_to :city

  belongs_to :work_phone_code,
             class_name: 'CountryPhoneCode',
             foreign_key: 'work_phone_code_id'

  belongs_to :cell_phone_code,
             class_name: 'CountryPhoneCode',
             foreign_key: 'cell_phone_code_id'

  # Include default devise modules.
  devise :database_authenticatable,
         :registerable,
         :recoverable,
         :rememberable,
         :trackable,
         :validatable,
         :confirmable,
         :lastseenable

  include DeviseTokenAuth::Concerns::User

  mount_uploader :image, UserImageUploader

  attr_accessor :scenario
  attr_accessor :facebook_data

  validates :name, :email, presence: true

  validates :name, :surname, :cell_phone, :cell_phone_code_id,
            presence: true,
            # применять только при редактировании профиля
            if: proc { |user| user.scenario == 'profile' }

  validates :password, presence: true, on: :create
  validates :email, uniqueness: true
  validates :email, email: true
  validates :birthday, not_in_future: true
  validates_uniqueness_of :facebook_link, :facebook_identity, allow_blank: true

  before_create :parse_facebook_data
  after_create :add_default_role
  before_save :update_last_seen

  # Возвращает задачи, которые которые относятся к пользователю:
  # 1. он создал ее
  # 2. он ее исполнитель
  # 3. он один из ее наблюдателей
  # 4. задача выполняется совместно с ним
  def related_tasks
    Task
      .joins('INNER JOIN employees AS executor ON executor.id = executor_id')
      .joins('LEFT JOIN employees AS assistant ON assistant.id = assistant_id')
      .joins(
        'LEFT JOIN tasks_watchers ON task_id = tasks.id '\
        'LEFT JOIN employees as watchers ON watchers.id = employee_id'
      )
      .where(
        'creator_id = :user_id OR '\
        'executor.user_id = :user_id OR '\
        'assistant.user_id = :user_id OR '\
        'watchers.user_id = :user_id',
        user_id: id
      )
      .distinct
  end

  def update_last_seen
    self.last_seen = Time.current
  end

  def self.public_params
    [
      :address,
      :longitude,
      :latitude,
      :id,
      :image,
      :remove_image,
      :name,
      :surname,
      :email,
      :public_email,
      :birthday,
      :skype,
      :appeal_id,
      :whatsapp,
      :viber,
      :facebook_link,
      :facebook_identity,
      :facebook_image_url,
      :cell_phone,
      :lang,
      :status,
      :sex,
      :city_id,
      :created_at,
      :balance,
      :bonus,
      :cell_phone_code_id,
    ]
  end

  def online?
    return last_seen > 10.minutes.ago if last_seen

    false
  end

  def profile_fullness
    fields = %w(
      image
      appeal_id
      name
      surname
      birthday
      lang
      city_id
      sex
      cell_phone
      email
      facebook_identity
      skype
      whatsapp
      viber
    )

    value_of_division = 100.0 / fields.count

    result = value_of_division # the "password" field always exists

    fields.each do |field|
      next if send(field).blank?
      result += value_of_division
    end

    result
  end

  def role_names_in_company(company_id)
    Company
    .find_roles(nil, self)
    .where(resource_id: company_id)
    .where
    .not(name: 'user')
    .pluck(:name)
  end

  def allow_change_password
    !reset_password_token.nil?
  end

  def permissions
    ability_for(self).permissions
  end

  def fullname
    return name unless surname

    name + ' ' + surname
  end

  # Возвращает сотрудника(объект Employee),
  # которым является user
  # в компании с id == company_id
  def employee_in_company(company_id)
    employees.find_by(company_id: company_id)
  end

  def self.labels
    {
      id: '#',
      name: I18n.t('activerecord.user.name'),
      surname: I18n.t('activerecord.user.surname'),
      password: I18n.t('activerecord.user.password'),
      email: I18n.t('activerecord.user.email'),
      birthday: I18n.t('activerecord.user.birthday'),
      skype: I18n.t('activerecord.user.skype'),
      whatsapp: I18n.t('activerecord.user.whatsapp'),
      viber: I18n.t('activerecord.user.viber'),
      lang: I18n.t('activerecord.user.lang'),
      created_at: I18n.t('activerecord.user.created_at'),
      facebook: I18n.t('activerecord.user.facebook'),
      appeal: I18n.t('activerecord.user.appeal'),
      sex: I18n.t('activerecord.user.sex'),
      city_id: I18n.t('activerecord.user.city_id'),
      xedit_title: I18n.t('common.edit'),
      add_facebook_link: I18n.t('common.add_facebook_link'),
      cell_phone: I18n.t('activerecord.user.cell_phone'),
      address: I18n.t('activerecord.user.address'),
    }
  end

  def self.references
    {
      appeals: Appeal.all,
      country_phone_codes: ActiveModel::Serializer::CollectionSerializer.new(
        CountryPhoneCode.all
      ),
      genders: [
        {
          id: 'm',
          name: I18n.t('genders.male'),
        },
        {
          id: 'f',
          name: I18n.t('genders.female'),
        },
      ],
      languages: [
        {
          id: 'ru',
          name: I18n.t('languages.ru'),
        },
        {
          id: 'en',
          name: I18n.t('languages.en'),
        },
      ],
    }
  end

  private

  def parse_facebook_data
    return if facebook_data.nil?

    self.surname = facebook_data[:last_name]
    self.sex = facebook_data[:sex] == '2' ? 'm' : 'f'
    self.facebook_identity = facebook_data[:uid]
    self.facebook_link = facebook_data[:identity]
    self.birthday = facebook_data[:bdate]
    self.cell_phone = facebook_data[:phone]

    city = City.where(name: facebook_data[:city]).first
    unless city
      country = Country.where(name: facebook_data[:country]).first
      country = Country.create!(name: facebook_data[:country]) unless country
      city = City.create(
        name: facebook_data[:city],
        country_id: country.id
      )
    end
    self.city_id = city.id if city

    # замена http на https нужна, чтобы
    # carrierwave не ругался на redirect, который
    # осуществляет Facebook
    self.remote_image_url = facebook_data[:photo_big].sub('http:', 'https:')
  end

  def add_default_role
    add_role(:user)
  end
end
