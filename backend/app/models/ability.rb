class Ability
  include CanCan::Ability
  include Permissions

  def initialize(user)
    # @TODO add caching
    user ||= User.new

    can :resend_confirmation, User
    return if user.roles.blank?

    clients_ids = Client.managed_by(user).pluck(:id)
    can [:read, :update], Client, id: clients_ids

    leads_ids = Lead.managed_by(user).pluck(:id)
    can [:read, :update], Lead, id: leads_ids

    can :manage, Notification, user_id: [user.id]

    can [:read], Task, id: user.related_tasks.pluck(:id)
    can [:update], Task, id: Task.created_by(user).pluck(:id)

    can [:read], CalendarEvent

    roles_in_companies(user).each do |role, companies_ids|
      next if role == :employee_dismissed

      can [:read, :stream], Company, id: companies_ids
      can :read, [Department, ProductCategory], company_id: companies_ids
      can [:read, :activity], Employee, company_id: companies_ids
      can [:read, :recommended], Product, company_id: companies_ids

      companies_ids.each do |company_id|
        clients_ids = Client.visible_for(user)
                            .created_after_hire(company_id, user)
                            .pluck(:id)

        can [:read], Client, id: clients_ids

        leads_ids = Lead.visible_for(user)
                        .created_after_hire(company_id, user)
                        .pluck(:id)
        can [:read], Lead, id: leads_ids

        deals_ids = Deal.visible_for(user)
                        .created_after_hire(company_id, user)
                        .pluck(:id)
        can [:read], Deal, id: deals_ids

        products_ids = Product.created_after_hire(company_id, user)
                              .pluck(:id)
        can [:read], Product, id: products_ids
      end
    end

    #########
    # Admin #
    #########

    admin(user)

    #########
    # User #
    #########

    user(user)

    ############
    # Employee #
    ############

    employee_creator(user)
    employee_investor(user)
    employee_director(user)
    employee_admin(user)
    employee_hr(user)
    employee_accountant(user)
    employee_head_of_sales_department(user)
    employee_manager_of_sales_department(user)
    employee_head_of_marketing_department(user)
    employee_marketer(user)
    employee_purchasing_manager(user)
    employee_dismissed(user)
  end

  def admin(user)
    if user.has_role? :admin
      can :manage, :all # exception of rule
    end
  end

  def user(user)
    if user.has_role? :user
      can :search, User
      can :search, Company
      can :search_entities, Company
      can :search, Client

      forbidden_categories = 
         user
          .employees
          .includes(:forbidden_product_categories)
          .map(&:forbidden_product_category_ids)
          .flatten

      can :search, Product
      can :omni_search, Deal
      can [:read, :update, :validate, :destroy, :upload_image], User, id: user.id
      can [:create, :validate, :upload_logo], Company
      can [:create, :remove], CalendarEvent
    end
  end

  def get_deals_ids_of_companies(companies_ids)
    Deal.where(company_id: companies_ids).pluck(:id)
  end

  def get_leads_ids_of_companies(companies_ids)
    Lead.where(company_id: companies_ids).pluck(:id)
  end

  def employee_creator(user)
  end

  def employee_investor(user)
    companies = roles_in_companies(user)[:employee_investor]

    return if companies.blank?

    can :own, CashBookRecord, company_id: companies

    can(
      [:create, :read],
      CashBookRecord,
      company_id: companies,
      personal: false
    )

    can(
      [:create, :read],
      CashBookRecord,
      company_id: companies,
      personal: true,
      user_id: user.id
    )
  end

  def employee_director(user)
    companies = roles_in_companies(user)[:employee_director]

    return if companies.blank?

    can [
      :update,
      :destroy,
      :invite_user,
      :budget_statistics,
      :budget_statistics_by_days,
      :sales_report,
    ], Company, id: companies

    can [
      :validate,
      :update,
      :update_pass_number,
      :update_name,
      :update_surname,
      :update_status,
      :update_department,
      :update_role,
      :update_email,
      :update_work_phone,
      :update_cell_phone,
      :update_job_started_at,
      :update_user_activity_id,
      :update_salary,
      :update_work_address,
      :add_birthday,
      :dismiss,
      :destroy,
      :sales_funnel,
      :sales_forecast,
    ], Employee, company_id: companies

    can [
      :create,
      :validate,
      :update,
      :destroy,
    ], Department, company_id: companies

    can [
      :destroy,
      :set_recommended,
    ], Product, company_id: companies

    can [
      :read,
      :new,
      :create,
      :update,
      :validate,
      :set_status,
      :return,
      :receipt,
    ], Deal, company_id: companies

    deals = get_deals_ids_of_companies(companies)
    can [
      :read,
      :new,
      :create,
      :update,
      :validate,
      :return,
      :destroy,
    ], DealProduct, deal_id: deals

    can [
      :confirm,
      :discard,
    ], DealMonthlyPayment, deal_id: deals

    can [
      :read,
      :new,
      :create,
      :update,
      :validate,
    ], Client, company_id: companies

    can [
      :read,
      :new,
      :create,
      :update,
      :validate,
    ], Supplier, company_id: companies

    can [
      :read,
      :new,
      :create,
      :update,
      :validate,
      :validate_lead_as_client,
      :convert,
    ], Lead, company_id: companies

    can [
      :read,
      :create,
      :update,
      :validate,
      :delete_safely,
    ], LeadStatus, company_id: companies

    can [
      :read,
      :create,
      :update,
      :validate,
      :delete_safely,
    ], ClientStatus, company_id: companies

    can [
      :read,
      :create,
      :update,
      :validate,
      :delete_safely,
    ], DealStatus, company_id: companies

    can [
      :read,
      :create,
      :update,
      :validate,
      :delete_safely,
    ], SupplierStatus, company_id: companies

    leads = get_leads_ids_of_companies(companies)

    can [
      :read,
      :new,
      :create,
      :update,
      :validate,
    ], LeadProduct, lead_id: leads

    can :manage, [
      SalesPlan,
    ], company_id: companies

    can(
      [:create, :read, :update],
      CashBookRecord,
      company_id: companies,
      personal: false
    )

    can :manage, CashFlowBudgetCategory, company_id: companies

    can [
      :create,
      :read,
      :update,
      :new,
      :validate,
      :delete_safely,
    ], CashBookCategory, company_id: companies

    # can :create, Contractor
    # can :read, Contractor, company_id: companies
    # can :update, Contractor, company_id: companies
  end

  def employee_accountant(user)
    companies = roles_in_companies(user)[:employee_accountant]

    return if companies.blank?

    can [
      :create,
      :validate,
      :update,
      :destroy,
    ], Department, company_id: companies

    can :destroy, Product, company_id: companies

    can [
      :read,
      :new,
      :create,
      :update,
      :validate,
      :set_status,
    ], Deal, company_id: companies

    deals = get_deals_ids_of_companies(companies)
    can [
      :read,
      :new,
      :create,
      :update,
      :destroy,
      :validate,
    ], DealProduct, deal_id: deals

    can :manage, [
      SalesPlan,
    ], company_id: companies

    can [
      :create,
      :read,
      :update,
    ], [
      CashBookRecord,
    ], company_id: companies
  end

  def employee_head_of_sales_department(user)
    companies = roles_in_companies(user)[:employee_head_of_sales_department]

    return if companies.blank?

    can [
      :validate,
      :update,
      :update_participation_in_sales,
    ], Employee, company_id: companies

    can [
      :create,
      :validate,
      :update,
      :destroy,
    ], Department, company_id: companies

    can [
      :create,
      :update,
      :destroy,
      :validate,
      :delete_safely,
    ], ProductCategory, company_id: companies

    can [
      :create,
      :set_recommended,
      :upload_images,
      :remove_image,
      :validate,
      :update,
    ], Product, company_id: companies

    can [
      :read,
      :new,
      :create,
      :update,
      :validate,
      :set_status,
    ], Deal, company_id: companies

    deals = get_deals_ids_of_companies(companies)
    can [
      :read,
      :new,
      :create,
      :update,
      :destroy,
      :validate,
    ], DealProduct, deal_id: deals

    can [
      :read,
      :new,
      :create,
      :update,
      :validate,
    ], Client, company_id: companies

    can [
      :read,
      :new,
      :create,
      :update,
      :validate,
    ], Lead, company_id: companies

    leads = get_leads_ids_of_companies(companies)
    can [
      :read,
      :new,
      :create,
      :update,
      :validate,
    ], LeadProduct, lead_id: leads

    can :manage, [
      SalesPlan,
    ], company_id: companies

    # can :create, Contractor
    # can :read, Contractor, company_id: companies
    # can :update, Contractor, company_id: companies
  end

  def employee_manager_of_sales_department(user)
    companies = roles_in_companies(user)[:employee_manager_of_sales_department]

    return if companies.blank?

    can :read, [
      SalesPlan,
    ], company_id: companies

    can [
      :create,
      :validate,
      :update,
      :destroy,
    ], Department, company_id: companies

    can [
      :create,
      :update,
      :validate,
    ], ProductCategory, company_id: companies

    can [
      :create,
      :upload_images,
      :remove_image,
      :validate,
      :destroy,
    ], Product, company_id: companies

    can [
      :read,
      :new,
      :create,
      :update,
      :validate,
      :set_status,
    ], Deal, company_id: companies

    deals = get_deals_ids_of_companies(companies)
    can [
      :read,
      :new,
      :create,
      :update,
      :destroy,
      :validate,
    ], DealProduct, deal_id: deals

    can [
      :read,
      :new,
      :create,
      :update,
      :validate,
    ], Client, company_id: companies

    can [
      :read,
      :new,
      :create,
      :update,
      :validate,
      :convert,
    ], Lead, company_id: companies

    leads = get_leads_ids_of_companies(companies)
    can [
      :read,
      :new,
      :create,
      :update,
      :validate,
    ], LeadProduct, lead_id: leads

    # can :read, Employee, company_id: companies

    # can :create, Contractor
    # can :read, Contractor, company_id: companies
    # can :update, Contractor, company_id: companies
  end

  def employee_head_of_marketing_department(user)
    companies = roles_in_companies(user)[:employee_head_of_marketing_department]

    return if companies.blank?

    can [
      :create,
      :validate,
      :update,
      :destroy,
    ], Department, company_id: companies
  end

  def employee_marketer(user)
    companies = roles_in_companies(user)[:employee_marketer]

    return if companies.blank?

    can [
      :create,
      :validate,
      :update,
      :destroy,
    ], Department, company_id: companies

    can [
      :read,
      :new,
      :create,
      :update,
      :validate,
      :set_status,
    ], Deal, company_id: companies

    deals = get_deals_ids_of_companies(companies)
    can [
      :read,
      :new,
      :create,
      :update,
      :destroy,
      :validate,
    ], DealProduct, deal_id: deals
  end

  def employee_purchasing_manager(user)
    companies = roles_in_companies(user)[:employee_purchasing_manager]

    return if companies.blank?

    can [
      :create,
      :validate,
      :update,
      :destroy,
    ], Department, company_id: companies

    can [
      :create,
      :update,
      :destroy,
      :validate,
      :delete_safely,
    ], ProductCategory, company_id: companies

    can [
      :create,
      :upload_images,
      :remove_image,
      :validate,
      :update,
    ], Product, company_id: companies

    can [
      :read,
      :new,
      :create,
      :update,
      :validate,
      :set_status,
    ], Deal, company_id: companies

    deals = get_deals_ids_of_companies(companies)
    can [
      :read,
      :new,
      :create,
      :update,
      :destroy,
      :validate,
    ], DealProduct, deal_id: deals
  end

  def employee_hr(user)
    companies = roles_in_companies(user)[:employee_hr]

    return if companies.blank?

    can :invite_user, Company, id: companies

    can [
      :validate,
      :update,
      :update_name,
      :update_surname,
      :update_pass_number,
      :update_status,
      :update_email,
      :update_work_phone,
      :update_cell_phone,
      :update_department,
      :update_role,
      :add_birthday,
      :update_birthday,
      :update_work_address,
      :update_job_started_at,
      :update_user_activity_id,
      :update_salary,
      :dismiss,
      :destroy,
    ], Employee, company_id: companies

    can [
      :create,
      :validate,
      :update,
      :destroy,
    ], Department, company_id: companies
  end

  def employee_admin(user)
    companies = roles_in_companies(user)[:employee_admin]

    return if companies.blank?

    can [:update, :invite_user], Company, id: companies

    can [
      :validate,
      :update,
      :update_pass_number,
      :update_email,
      :update_name,
      :update_surname,
      :update_work_phone,
      :update_department,
      :update_cell_phone,
      :update_participation_in_sales,
      :update_work_address,
      :add_birthday,
      :destroy,
    ], Employee, company_id: companies

    can [
      :create,
      :validate,
      :update,
      :destroy,
    ], Department, company_id: companies

    can [
      :create,
      :update,
      :destroy,
      :validate,
      :delete_safely,
    ], ProductCategory, company_id: companies

    can [
      :create,
      :upload_images,
      :remove_image,
      :validate,
      :update,
      :set_recommended,
      :destroy,
      :order,
      :reserve,
      :discuss,
      :complain,
      :destroy,
    ], Product, company_id: companies

    can [
      :read,
      :new,
      :create,
      :update,
      :validate,
      :set_status,
      :return,
    ], Deal, company_id: companies

    deals = get_deals_ids_of_companies(companies)
    can [
      :read,
      :new,
      :create,
      :update,
      :validate,
      :destroy,
      :return,
    ], DealProduct, deal_id: deals

    can [
      :read,
      :new,
      :create,
      :update,
      :validate,
    ], Client, company_id: companies

    can [
      :read,
      :new,
      :create,
      :update,
      :validate,
    ], Lead, company_id: companies

    leads = get_leads_ids_of_companies(companies)
    can [
      :read,
      :new,
      :create,
      :update,
      :validate,
    ], LeadProduct, lead_id: leads

    # can :create, Contractor
    # can :read, Contractor, company_id: companies
    # can :update, Contractor, company_id: companies
  end

  def employee_dismissed(user)
  end

  def roles_in_companies(user)
    return @roles_in_companies if defined?(@roles_in_companies)

    db = ActiveRecord::Base.connection

    rows = db.exec_query(
      "SELECT c.id company_id, r.name role_name FROM companies c
      INNER JOIN roles r ON r.resource_type = 'Company' AND r.resource_id = c.id
      INNER JOIN users_roles ur ON ur.role_id = r.id
      WHERE ur.user_id = #{db.quote(user.id)}"
    )

    result = Hash.new { |hash, key| hash[key] = [] }

    @roles_in_companies = rows.each_with_object(result) do |row, result_hash|
      result_hash[row['role_name'].to_sym] |= [row['company_id']]
    end
  end
end
