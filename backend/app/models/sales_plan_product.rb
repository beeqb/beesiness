# == Schema Information
#
# Table name: sales_plan_products
#
#  id            :integer          not null, primary key
#  sales_plan_id :integer
#  product_id    :integer
#  quantity      :decimal(10, 2)   default(0.0)
#  sum           :decimal(10, 2)   default(0.0)
#  deleted_at    :datetime
#

class SalesPlanProduct < Base::ResourceRecord
  resourcify

  belongs_to :product
  belongs_to :sales_plan
end
