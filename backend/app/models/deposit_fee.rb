# Модель для хранения данных о взносах клиента на депозит
# Используется для подсчета общей прибыли от конкретного клиента
# Взнос может быть отрицательным - считаем это изъятием с депозита
class DepositFee < ApplicationRecord
  belongs_to :client

  validates :amount,
            numericality: true,
            presence: true
end
