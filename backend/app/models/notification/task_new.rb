class Notification
  ######################################
  # Уведомление о создании новых задач #
  ######################################
  class TaskNew < Notification
    def build_content
      self.content =
        "У вас появились новые задачи: #{tasks_count(user_id)} шт."
    end

    def self.subject
      Task
    end

    def self.notify
      @@tasks = subject.where(
        'DATE(created_at) = ? AND status = ?',
        Date.today,
        :new
      )

      user_ids = []

      @@tasks.each do |task|
        next if task.executor.nil?

        user_id = task.executor.user.id
        next if user_ids.include?(user_id)
        user_ids.push(user_id)

        self.for([user_id], task.id)
      end
    end

    private

    def tasks_count(user_id)
      return 0 unless defined? @@tasks

      employee_ids = Employee.where(user_id: user_id).pluck(:id)
      @@tasks.where(executor_id: employee_ids).count
    end
  end
end
