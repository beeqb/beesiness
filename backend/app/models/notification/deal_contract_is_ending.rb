class Notification
  ######################################################
  # Уведомление об истечении срока контракта по сделке #
  ######################################################
  class DealContractIsEnding < Notification
    def build_content
      self.content = 'Сделка: срок действия контракта истекает сегодня'
    end

    def self.subject
      Deal
    end

    def self.notify
      deals = subject.where('DATE(contract_ended_at) = CURDATE()')

      deals.each do |deal|
        next if deal.manager.nil?
        user_id = deal.manager.user.id
        self.for([user_id], deal.id)
      end
    end
  end
end
