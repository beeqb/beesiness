class Notification
  #####################################
  # Уведомление о неоплаченной сделке #
  #####################################
  class DealWaitsPayment < Notification
    def build_content
      self.content = 'Ожидается платеж по сделке'
    end

    def self.subject
      Deal
    end

    def self.notify
      deals = subject.where('
        DATE(deals.single_payment_date) = CURDATE() OR
        DATE(deals.monthly_payment_ended_at) = CURDATE() OR
        DATE(deals.single_payment_date) = DATE(DATE_ADD(NOW(), INTERVAL 7 DAY)) OR
        DATE(deals.monthly_payment_ended_at) = DATE(DATE_ADD(NOW(), INTERVAL 7 DAY))
      ')

      deals.each do |deal|
        next if deal.manager.nil?
        manager_id = deal.manager.user.id
        self.for([manager_id], deal.id)

        next if deal.client.nil?
        client_id = deal.client.user.id
        self.for([client_id], deal.id)
      end
    end
  end
end
