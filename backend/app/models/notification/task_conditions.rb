class Notification
  ####################################################
  # Уведомление об истечении срока реализации задачи #
  ####################################################
  class TaskConditions < Notification
    def build_content
      self.content =
        %(Срок реализации задачи "#{object.title}" истекает #{expires_at})
    end

    def self.subject
      Task
    end

    def self.notify
      current_date = Time.now.to_date
      tasks = subject.where('due_date = ? OR due_date = ?',
                            current_date,
                            current_date.tomorrow)

      tasks.each do |task|
        next if task.executor.nil?

        self.for([task.executor.user.id], task.id)
      end
    end

    private

    def expires_at
      current_date = Time.now.to_date
      return 'сегодня' if object.due_date == current_date
      return 'завтра' if object.due_date == current_date.tomorrow
      object.due_date.to_s
    end
  end
end
