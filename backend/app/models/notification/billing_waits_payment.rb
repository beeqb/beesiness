class Notification
  ######################################
  # Уведомление о необходимости оплаты #
  ######################################
  class BillingWaitsPayment < Notification
    def build_content
      self.content = 'billing: waits payment'
    end
  end
end
