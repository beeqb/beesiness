class Notification
  #############################################
  # Уведомление об истечении пробного периода #
  #############################################
  class BillingTrialIsEnding < Notification
    def build_content
      self.content = 'billing: trial is ending'
    end
  end
end
