class Notification
  ##################################
  # Уведомление о кассовом разрыве #
  ##################################
  class CashBookCashGap < Notification
    def build_content
      self.content = %(В компании "#{object.name}" произошел кассовый разрыв)
    end

    def self.subject
      Company
    end

    def self.for(user_ids, subject_id)
      super
      company = Company.find(subject_id)
      CashBookMailer.cash_gap(company).deliver_later
    end
  end
end
