class Notification
  class ProductPriceChanged < Notification
    def build_content
      url = "/companies/#{object.company_id}/products/#{object.id}"

      self.content = <<~MSG
        У товара <a href="#{url}">"#{object.name}"</a> изменилась цена
        c #{old_price} на #{new_price}
      MSG
    end

    def self.subject
      Product
    end

    def old_price
      @additional_data[:old_price]
    end

    def new_price
      @additional_data[:new_price]
    end
  end
end
