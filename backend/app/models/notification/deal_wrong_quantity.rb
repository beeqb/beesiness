class Notification
  ###############################################################
  # Уведомление о превышении допустимого кол-ва товара в сделке #
  ###############################################################
  class DealWrongQuantity < Notification
    def build_content
      self.content = 'Сделка: количество товара меньше проданных единиц товара'
    end

    def self.subject
      Deal
    end

    def self.notify
      deals = subject
        .joins(:deal_products)
        .joins(:products)
        .where('deal_products.count > products.quantity')

      deals.each do |deal|
        next if deal.manager.nil?
        user_id = deal.manager.user.id
        self.for([user_id], deal.id)
      end
    end
  end
end
