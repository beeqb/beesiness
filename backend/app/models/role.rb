# == Schema Information
#
# Table name: roles
#
#  id            :integer          not null, primary key
#  name          :string(255)
#  resource_type :string(255)
#  resource_id   :integer
#  created_at    :datetime
#  updated_at    :datetime
#

class Role < Base::ApplicationRecord
  has_and_belongs_to_many :users, join_table: :users_roles

  belongs_to :resource,
             polymorphic: true,
             optional: true

  validates :resource_type,
            inclusion: { in: Rolify.resource_types },
            allow_nil: true

  scopify

  attr_accessor :company

  def self.roles
    [
      :user,

      :admin,

      :employee,
      :employee_hr,
      :employee_investor,
      :employee_director,
      :employee_accountant,
      :employee_head_of_sales_department,
      :employee_manager_of_sales_department,
      :employee_head_of_marketing_department,
      :employee_marketer,
      :employee_purchasing_manager,
      :employee_admin,
      :employee_creator,
      :employee_dismissed,

      :client,
      :client_new,
      :client_current,
      :client_permanent,
      :client_special,
      :client_no,

      :lead,
      :lead_new,
      :lead_in_work,
      :lead_trash,

      :contractor,
    ]
  end
end
