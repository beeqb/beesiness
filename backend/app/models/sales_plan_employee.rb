# == Schema Information
#
# Table name: sales_plan_employees
#
#  id            :integer          not null, primary key
#  sales_plan_id :integer
#  employee_id   :integer
#  part          :integer
#  deleted_at    :datetime
#

class SalesPlanEmployee < Base::ResourceRecord
  resourcify
  belongs_to :employee
end
