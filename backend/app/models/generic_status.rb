class GenericStatus < Base::ResourceRecord
  before_save :set_id, unless: :system?
  before_save :add_hash_to_color

  scope :system, -> { where(system: true) }

  def self.labels
    {
      name: I18n.t('activerecord.generic_status.name'),
      color: I18n.t('activerecord.generic_status.color'),
      status_after_delete: I18n.t(
        'activerecord.generic_status.status_after_delete'
      ),
    }
  end

  def name
    return self[:name] unless system?

    I18n.t("activerecord.#{self.class.target_name}.statuses.#{id}")
  end

  def self.for_company(company_id)
    where(company_id: company_id).or(system)
  end

  def self.target
    name.gsub('Status', '').constantize
  end

  def self.target_name
    name.gsub('Status', '').underscore
  end

  def system?
    system
  end

  private
  
  def set_id
    self.id =
      self.class.target_name + '_' +
      Russian.translit(name).gsub(/[\s\W]/, '_').downcase
  end

  def add_hash_to_color
    return if color.starts_with?('#')

    self.color = '#' + color
  end
end
