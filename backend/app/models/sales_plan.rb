# == Schema Information
#
# Table name: sales_plans
#
#  id         :integer          not null, primary key
#  company_id :integer
#  kind       :integer
#  year       :integer
#  month      :integer
#  deleted_at :datetime
#  sum        :decimal(10, 2)   default(0.0)
#  quantity   :decimal(10, 2)   default(0.0)
#

#################################
# The class of sales plan model #
#################################
class SalesPlan < Base::ResourceRecord
  resourcify

  enum kind: [:by_quantity, :by_sum]
  validates :month,
            :year,
            :kind,
            presence: true

  validates :month, uniqueness: { scope: [:year, :company_id] }

  belongs_to :company
  has_many :sales_plan_products, dependent: :destroy
  has_many :sales_plan_employees
  has_many :sales_plan_product_categories, dependent: :destroy

  accepts_nested_attributes_for :sales_plan_products, allow_destroy: true
  accepts_nested_attributes_for :sales_plan_employees
  accepts_nested_attributes_for :sales_plan_product_categories, allow_destroy: true

  before_save :calculate_sum_and_quantity, if: :by_quantity?
  before_save :clear_products_plans, if: :by_sum?

  def editable
    return true if year.nil? && month.nil?

    now = Time.now
    return false if year < now.year

    if year == now.year
      return false if month < now.month
      return false if month == now.month && !id.nil?
    end
    true
  end

  def check_existence?(attr)
    return SalesPlanProduct.where(sales_plan_id: attr["sales_plan_id"], product_id: attr["product_id"]).count > 0
  end

  def self.current(company_id)
    now = Time.now
    find_by(year: now.year, month: now.month, company_id: company_id)
  end

  def self.labels
    {
      kind: I18n.t('activerecord.sales_plan.kind'),
      month: I18n.t('activerecord.sales_plan.month'),
    }
  end

  def self.references
    {
      kinds: kinds.map { |name, value| { id: value, name: name } },
      dates: dates,
    }
  end

  def self.dates
    dates = []
    (Time.now.year..(Time.now.year + 1)).each do |year|
      (1..12).each do |month|
        dates.push(
          id: year.to_s + '_' + month.to_s,
          year: year,
          month: month,
          name: I18n.t('months_by_number.m_' + month.to_s) + ' ' + year.to_s
        )
      end
    end
    dates
  end

  private

  def calculate_sum_and_quantity
    sum = 0
    quantity = 0

    by_categories = Hash.new { |h, k| h[k] = { sum: 0, quantity: 0} }

    sales_plan_products.each do |sales_plan_product|
      product_sum = sales_plan_product.product.price * sales_plan_product.quantity
      sum += product_sum
      quantity += sales_plan_product.quantity
      by_categories[sales_plan_product.product.category.id][:sum] += product_sum
      by_categories[sales_plan_product.product.category.id][:quantity] += sales_plan_product.quantity
      sales_plan_product_categories.where(product_category_id: sales_plan_product.product.category.id).destroy_all
    end

    by_categories.each do |category_id, values|
      object = sales_plan_product_categories.find_or_initialize_by(product_category_id: category_id)
      object.sum = values[:sum]
      object.quantity = values[:quantity]
      object.save
    end

    self.sum = sum
    self.quantity = quantity
  end

  def clear_products_plans
    #sales_plan_products.clear
    self.quantity = nil
  end
end
