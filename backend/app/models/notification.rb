# == Schema Information
#
# Table name: notifications
#
#  id            :integer          not null, primary key
#  type          :string(255)
#  user_id       :integer
#  content       :string(255)
#  subject_id    :integer
#  viewed_at     :datetime
#  email_sent_at :datetime
#  created_at    :datetime         not null
#  updated_at    :datetime         not null

class Notification < Base::ApplicationRecord
  validates :user_id, :content, presence: true

  before_validation :build_content, on: :create

  attr_accessor :additional_data

  def build_content
    raise 'method did not implement'
  end

  def self.notify
    raise 'method did not implement'
  end

  def self.subject
    raise 'method did not implement'
  end

  def object
    self.class.subject.find(subject_id)
  end

  def self.for(user_ids, subject_id, additional_data = nil)
    user_ids.each do |user_id|
      notification = new(user_id: user_id, subject_id: subject_id)
      notification.additional_data = additional_data
      notification.save!
    end
  end
end
