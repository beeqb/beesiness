# == Schema Information
#
# Table name: clients
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  surname            :string(255)
#  birthday           :datetime
#  country_id         :integer
#  city_id            :integer
#  company_id         :integer
#  user_id            :integer
#  address            :string(255)
#  email              :string(255)
#  work_phone         :string(255)
#  cell_phone         :string(255)
#  site               :string(255)
#  skype              :string(255)
#  facebook           :string(255)
#  viber              :string(255)
#  current_job        :string(255)
#  current_department :string(255)
#  current_position   :string(255)
#  current_is_head    :boolean
#  relations          :text(65535)
#  deleted_at         :datetime
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  status             :string(255)
#  manager_id         :integer
#  second_manager_id  :integer
#  visibility         :string(255)
#  legal_form         :string(255)
#  discount           :integer
#  notes              :text(65535)
#  legal_entity_name  :string(255)
#  legal_entity_email :string(255)
#  legal_entity_id    :integer
#  type               :string(255)      default("Client")
#  potential_amount   :decimal(18, 2)
#  source             :string(255)
#  recommender        :string(255)
#  latitude           :decimal(20, 14)
#  longitude          :decimal(20, 14)
#  work_phone_code_id :integer
#  cell_phone_code_id :integer
#  work_phone_full    :string(255)
#  cell_phone_full    :string(255)
#

class Lead < Client
  has_many :lead_products
  has_and_belongs_to_many :products, through: :lead_products
  belongs_to :status_info, class_name: 'LeadStatus', foreign_key: 'status'

  def self.default_scope
    where(deleted_at: nil).where(type: 'Lead')
  end

  def additional_fields_is_required?
    false
  end

  def set_default_status
    self.status = :lead_new
  end

  def convert!
    self.type = 'Client' # change type to 'Client'
    self.status = 'client_new'
    save!
  end

  def self.references
    {
      visibilities: [
        {
          id: 'all',
          name: I18n.t('activerecord.client.visibilities.all'),
        },

        {
          id: 'me',
          name: I18n.t('activerecord.client.visibilities.me'),
        },
      ],
      legal_forms: [
        {
          id: 'legal_entity',
          name: I18n.t('activerecord.client.legal_forms.legal_entity'),
        },

        {
          id: 'individual',
          name: I18n.t('activerecord.client.legal_forms.individual'),
        },
      ],
      sources: [
        {
          id: 'another_client',
          color: '#f05050',
          name: I18n.t('activerecord.lead.sources.another_client'),
        },
        {
          id: 'site',
          color: '#98a6ad',
          name: I18n.t('activerecord.lead.sources.site'),
        },
        {
          id: 'call',
          color: '#27c24c',
          name: I18n.t('activerecord.lead.sources.call'),
        },
        {
          id: 'social_media',
          color: '#7266ba',
          name: I18n.t('activerecord.lead.sources.social_media'),
        },
      ],
      country_phone_codes: ActiveModel::Serializer::CollectionSerializer.new(
        CountryPhoneCode.all
      ),
      companies: ActiveModel::Serializer::CollectionSerializer.new(
        Company.all
      ),
    }
  end

  def self.labels
    {
      id: '#',
      name: I18n.t('activerecord.lead.name'),
      surname: I18n.t('activerecord.lead.surname'),
      birthday: I18n.t('activerecord.lead.birthday'),
      country_id: I18n.t('activerecord.lead.country_id'),
      city_id: I18n.t('activerecord.lead.city_id'),
      company_id: I18n.t('activerecord.lead.company_id'),
      address: I18n.t('activerecord.lead.address'),
      email: I18n.t('activerecord.lead.email'),
      work_phone: I18n.t('activerecord.lead.work_phone'),
      cell_phone: I18n.t('activerecord.lead.cell_phone'),
      site: I18n.t('activerecord.lead.site'),
      skype: I18n.t('activerecord.lead.skype'),
      facebook: I18n.t('activerecord.lead.facebook'),
      viber: I18n.t('activerecord.lead.viber'),
      current_job: I18n.t('activerecord.lead.current_job'),
      current_department: I18n.t('activerecord.lead.current_department'),
      current_position: I18n.t('activerecord.lead.current_position'),
      current_is_head: I18n.t('activerecord.lead.current_is_head'),
      relations: I18n.t('activerecord.lead.relations'),
      created_at: I18n.t('activerecord.lead.created_at'),
      status: I18n.t('activerecord.lead.status'),
      manager_id: I18n.t('activerecord.lead.manager_id'),
      second_manager_id: I18n.t('activerecord.lead.second_manager_id'),
      visibility: I18n.t('activerecord.lead.visibility'),
      legal_form: I18n.t('activerecord.lead.legal_form'),
      discount: I18n.t('activerecord.lead.discount'),
      notes: I18n.t('activerecord.lead.notes'),
      legal_entity_name: I18n.t('activerecord.lead.legal_entity_name'),
      legal_entity_email: I18n.t('activerecord.lead.legal_entity_email'),
      source: I18n.t('activerecord.lead.source'),
      recommender: I18n.t('activerecord.lead.recommender'),
      potential_amount: I18n.t('activerecord.lead.potential_amount'),
    }
  end
end
