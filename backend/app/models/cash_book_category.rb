class CashBookCategory < Base::ResourceRecord
  scope :system, -> { where(system: true) }

  before_create :set_id, unless: :system?

  def system?
    system
  end

  def expense?
    expense
  end

  def self.labels
    {
      name: I18n.t('activerecord.cash_book_category.name'),
      expense: I18n.t('activerecord.cash_book_category.expense'),
      category_after_delete: \
        I18n.t('activerecord.cash_book_category.category_after_delete'),
    }
  end

  def self.for_company(company_id)
    where(company_id: company_id).or(system)
  end

  private

  def set_id
    self.id =
      Russian.translit(name).gsub(/[\s\W]/, '_').downcase +
      '_' + SecureRandom.hex(3)
  end
end
