module Base

  class ResourceRecord < ApplicationRecord
    self.abstract_class = true

    def self.default_scope
      where(deleted_at: nil)
    end

    def destroy
      self.deleted_at = Time.now
      self.save!

      self.after_destroy
    end

    def after_destroy
      true
    end

  end

end
