module Base

  class ApplicationRecord < ActiveRecord::Base
    self.abstract_class = true

    def self.pluck_hash(*keys)
      pluck(*keys).map { |pa| Hash[keys.zip(pa)] }
    end

    def ability_for(user)
      RequestStore.store[:"beeqb_ability_user_#{user.id}"] ||= ::Ability.new(user)
    end
  end

end
