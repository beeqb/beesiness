require 'ostruct'
require 'json'

class Receipt < ApplicationRecord
  enum kind: [:sell, :return]

  belongs_to :deal

  before_create :generate_content

  def generate_content
    deal_products_structs = deal_products.try(:map) do |deal_product|
      JSON.parse(deal_product.to_json, object_class: OpenStruct)
    end

    self.content = ReceiptContent.new(self, amount, deal_products_structs).html
  end
end
