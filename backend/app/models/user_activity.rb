# == Schema Information
#
# Table name: user_activities
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  color      :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class UserActivity < Base::ApplicationRecord
  validates :name, :color, presence: true
  validates_length_of :color, :minimum => 6, :maximum => 6

  def self.dismissed?(id)
    id == dismissed.id
  end

  def self.vacation?(id)
    id == vacation.id
  end

  def self.dismissed
    where(name: 'Уволен').first
  end

  def self.vacation
    where(name: 'В отпуске').first
  end
end
