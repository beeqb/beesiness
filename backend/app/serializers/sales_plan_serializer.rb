###############################################
# The class for serialize a sales plan object #
###############################################
class SalesPlanSerializer < ApplicationSerializer
  attributes :id,
             :company_id,
             :kind,
             :month,
             :year,
             :editable,
             :sum,
             :quantity

  ##################
  # CUSTOM INCLUDE #
  ##################

  attribute :sales_plan_products, if: include?(:sales_plan_products)
  attribute :sales_plan_product_categories, if: include?(:sales_plan_product_categories)
  attribute :sales_plan_employees, if: include?(:sales_plan_employees)

  def sales_plan_products
    options = {}
    if self.class.include?('sales_plan_products.product')
      options[:custom_include] = 'product'
    end
    ActiveModel::Serializer::CollectionSerializer.new(
      object.sales_plan_products,
      options
    )
  end

  def sales_plan_employees
    options = {}
    if self.class.include?('sales_plan_products.employee')
      options[:custom_include] = 'employee'
    end
    ActiveModel::Serializer::CollectionSerializer.new(
      object.sales_plan_employees,
      options
    )
  end
end
