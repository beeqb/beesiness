#######################################################
# The class for serialize a sales plan product object #
#######################################################
class SalesPlanProductSerializer < ApplicationSerializer
  attributes :id,
             :sales_plan_id,
             :product_id,
             :quantity,
             :sum

  ##################
  # CUSTOM INCLUDE #
  ##################

  attribute :product, if: include?(:product)

  def product
    options = {}
    if self.class.include?('sales_plan_employees.employee.employees_departments')
      options[:custom_include] = ['product_categories']
    end

    ProductSerializer.new(
      object.product,
      options
    )
  end
end
