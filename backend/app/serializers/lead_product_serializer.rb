#################################################
# The class for serialize a lead product object #
#################################################
class LeadProductSerializer < ApplicationSerializer
  attributes :id,
             :lead_id,
             :product_id,
             :count,
             :returned_at

  # TODO: do this attribute custom included
  attribute :product

  def product
    object.product
  end
end
