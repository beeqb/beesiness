#######################################################
# The class for serialize a sales plan product object #
#######################################################
class SalesPlanEmployeeSerializer < ApplicationSerializer
  attributes :id,
             :sales_plan_id,
             :employee_id,
             :product_category_id,
             :part

  ##################
  # CUSTOM INCLUDE #
  ##################

  attribute :employee, if: include?(:employee)

  def employee
    options = {
      custom_include: %w(full_name image online),
    }
    if self.class.include?('sales_plan_employees.employee.employees_departments')
      options[:custom_include].push('employees_departments')
    end

    EmployeeSerializer.new(
      object.employee,
      options
    )
  end
end
