class SearchResultSerializer < ApplicationSerializer
  attributes :clients, :leads, :deals, :products, :employees

  def employees
    return unless object.employees

    ActiveModel::Serializer::CollectionSerializer.new(
      object.employees,
      custom_include: %w(user employees_departments roles)
    )
  end

  def products
    return unless object.products

    ActiveModel::Serializer::CollectionSerializer.new(
      object.products,
      custom_include: %w(category)
    )
  end

  def deals
    return unless object.deals

    ActiveModel::Serializer::CollectionSerializer.new(
      object.deals,
      custom_include: %w(client)
    )
  end

end
