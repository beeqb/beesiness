class CashBookRecordSerializer < ApplicationSerializer
  attributes :id,
             :category,
             :company_id,
             :amount,
             :balance,
             :bill_kind,
             :created_at,
             :creator_id,
             :reporting_document,
             :cash_flow_budget_category_id,
             :comment,
             :currency,
             :deal_display_name,
             :personal,
             :deal_id,
             :source_type

  attribute :creator, if: include?(:creator)
  belongs_to :deal, if: include?(:deal)
  belongs_to :category_info
  belongs_to :source, polymorphic: true

  def balance
    if scope.has_role?(:employee_investor, object.company)
      object.personal_balance_for(scope.id)
    else
      object.balance
    end
  end

  def creator
    EmployeeSerializer.new(
      object.creator,
      custom_include: %w(user employees_departments),
    )
  end
end
