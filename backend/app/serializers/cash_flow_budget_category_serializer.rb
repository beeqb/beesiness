class CashFlowBudgetCategorySerializer < ApplicationSerializer
  attributes :id,
             :name,
             :kind,
             :company_id,
             :cash_book_category,
             :children,
             :parent_id

  def children
    instance_options[:children]
  end
end
