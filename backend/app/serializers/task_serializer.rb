# ActiveModelSerializer for Task entities
class TaskSerializer < ApplicationSerializer
  attributes :id,
             :assessment,
             :assistant,
             :assistant_id,
             :attachments,
             :category,
             :checklist,
             :client_id,
             :company_id,
             :created_at,
             :creator_id,
             :description,
             :due_date,
             :estimate,
             :executor,
             :executor_id,
             :important,
             :start_date,
             :status,
             :title,
             :urgent,
             :watcher_ids,
             :watchers,
             :overdue,
             :started_at,
             :done_at,
             :spent_time,
             :last_pause_at

  belongs_to :client
  belongs_to :creator
  has_many :watchers

  def executor
    return unless object.executor

    EmployeeSerializer.new(
      object.executor,
      custom_include: [
        'user',
        'employees_departments',
      ]
    )
  end


  def assistant
    return unless object.assistant

    EmployeeSerializer.new(
      object.assistant,
      custom_include: [
        'user',
        'employees_departments',
      ]
    )
  end

  def watchers
    ActiveModel::Serializer::CollectionSerializer.new(
      object.watchers,
      custom_include: [
        'user',
      ]
    )
  end

  def overdue
    object.overdue?
  end
end
