#################################################
# The class for serialize a system event object #
#################################################
class SystemEventSerializer < ApplicationSerializer
  attributes :id,
             :created_at,
             :general_description,
             :local_description,
             :kind,
             :meta_data
end
