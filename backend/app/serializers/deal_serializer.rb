#########################################
# The class for serialize a deal object #
#########################################
class DealSerializer < ApplicationSerializer
  attributes :id,
             :name,
             :number,
             :contract_number,
             :check,
             :invoice,
             :company_id,
             :address,
             :latitude,
             :longitude,
             :email,
             :phone,
             :phone_code_id,
             :started_at,
             :closed_at,
             :notes,
             :visibility,
             :discount,
             :total_sum,
             :check_sum,
             :client_id,
             :client_name,
             :discount_sum,
             :vat_sum,
             :created_at,
             :deleted_at,
             :status,
             :manager_id,
             :second_manager_id,
             :legal_form,
             :payment_period,
             :payment_method,
             :payment_option,
             :contract_started_at,
             :returned_at,
             :contract_ended_at,
             :contract_conclusion_at,
             :monthly_payment_started_at,
             :monthly_payment_ended_at,
             :single_payment_date,
             :interest_rate,
             :cash_flow_budget_category_id

  ##################
  # CUSTOM INCLUDE #
  ##################

  attribute :deal_products, if: include?(:deal_products)
  attribute :monthly_payments, if: include?(:monthly_payments)
  attribute :company, if: include?(:company)
  attribute :history_length, if: include?(:history_length)
  attribute :related_tasks_count, if: include?(:related_tasks_count)
  attribute :client, if: include?(:client)
  has_many :receipts, if: include?(:receipts)

  def client
    return nil unless object.client_id

    options = { custom_include: [] }

    if self.class.include?('client.rf_status')
      options[:custom_include] << 'rf_status'
    end

    if self.class.include?('client.rm_status')
      options[:custom_include] << 'rm_status'
    end

    ClientSerializer.new(object.client, options)
  end

  def monthly_payments
    object.deal_monthly_payments
  end

  def deal_products
    options = {}
    if self.class.include?('deal_products.product')
      options[:custom_include] = 'product'
    end

    ActiveModel::Serializer::CollectionSerializer.new(
      object.deal_products,
      options
    )
  end

  def client_name
    object.client.name if object.client
  end
end
