#########################################
# The class for serialize a role object #
#########################################
class RoleSerializer < ApplicationSerializer
  attributes :id,
             :name,
             :resource_type,
             :resource_id,
             :created_at,
             :updated_at

  attribute :human_name

  def human_name
    I18n.t('roles.' + object.name.to_s)
  end
end
