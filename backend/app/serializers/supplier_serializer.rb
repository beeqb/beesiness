#############################################
# The class for serialize a supplier object #
#############################################
class SupplierSerializer < ApplicationSerializer
  attributes :id,
             :name,
             :surname,
             :birthday,
             :country_id,
             :city_id,
             :company_id,
             :address,
             :latitude,
             :longitude,
             :email,
             :work_phone_code_id,
             :work_phone,
             :work_phone_full,
             :cell_phone_code_id,
             :cell_phone,
             :cell_phone_full,
             :site,
             :skype,
             :facebook,
             :viber,
             :current_job,
             :current_department,
             :current_position,
             :current_is_head,
             :relations,
             :created_at,
             :deleted_at,
             :status,
             :manager_id,
             :second_manager_id,
             :visibility,
             :legal_form,
             :discount,
             :notes,
             :legal_entity_email,
             :legal_entity_name,
             :user_id,
             :image,
             :source,
             :type
  def image
    if object.legal_form == 'individual'
      { url: object.user.try(:image).try(:url) || 'assets/img/default-avatar.png' }
    elsif object.legal_form == 'legal_entity'
      { url: object.legal_entity.try(:logo).try(:url) || 'assets/img/p0.jpg' }
    end
  end

  ##################
  # CUSTOM INCLUDE #
  ##################

  attribute :legal_entity, if: include?(:legal_entity)
  attribute :user, if: include?(:user)
  attribute :history_length, if: include?(:history_length)
  attribute :related_tasks_count, if: include?(:related_tasks_count)
  attribute :deal_events, if: include?(:deal_events)
  attribute :deals_stats, if: include?(:deals_stats)
  attribute :current_month_income, if: include?(:current_month_income)
  attribute :current_month_income_by_days,
            if: include?(:current_month_income_by_days)
  attribute :sales_dynamic, if: include?(:sales_dynamic)

  def related_tasks_count
    Task
    .where(
      client_id: object.id,
    )
    .count
  end

  def deals_stats 
    {
      first_purchase_date: indicators.first_purchase_date,
      last_purchase_date: indicators.last_purchase_date,
      total_deals: {
        count: indicators.total_deals_count,
        sum: indicators.total_deals_sum,
        average_sum: indicators.total_deals_average_sum,
      },
      returned_deals: {
        count: indicators.returned_deals_count,
        sum: indicators.returned_deals_sum,
      },
      potential_deals: {
        count: indicators.potential_deals_count,
        sum: indicators.potential_deals_sum,
      },
      won_deals: {
        count: indicators.won_deals_count,
        sum: indicators.won_deals_sum,
      },
      lost_deals: {
        count: indicators.lost_deals_count,
        sum: indicators.lost_deals_sum,
      },
    }
  end

  def deal_events
    deals_ids = object.deals.pluck(:id)

    data =
      SystemEvent
      .where(
        kind: 'deal:status_changed',
        subject_id: deals_ids,
      )
      .order(created_at: :desc)

    ActiveModel::Serializer::CollectionSerializer.new(data)
  end

  def current_month_income
    indicators.income
  end

  def current_month_income_by_days
    indicators.income_by_days
  end

  def sales_dynamic
    {
      sum: indicators.sales_dynamic(:sum),
      quantity: indicators.sales_dynamic(:quantity),
    }
  end

  private

  def indicators
    SupplierIndicators.new(object, instance_options[:current_company_id])
  end
end
