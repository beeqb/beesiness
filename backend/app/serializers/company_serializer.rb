############################################
# The class for serialize a company object #
############################################
class CompanySerializer < ApplicationSerializer
  attributes :id,
             :logo,
             :name,
             :description,
             :address,
             :latitude,
             :longitude,
             :site,
             :email,
             :phone,
             :currency,
             :vat,
             :director_id,
             :phone_code_id,
             :inn,
             :kpp

  ##################
  # CUSTOM INCLUDE #
  ##################

  attribute :director, if: include?(:director)
  attribute :creator, if: include?(:creator)
  attribute :product_categories, if: include?(:product_categories)
  attribute :categories, if: include?(:categories)
  attribute :last_clients, if: include?(:last_clients)
  attribute :last_leads, if: include?(:last_leads)
  attribute :last_deals, if: include?(:last_deals)
  attribute :clients_count, if: include?(:clients_count)
  attribute :leads_count, if: include?(:leads_count)
  attribute :products_count, if: include?(:products_count)
  attribute :deals_count, if: include?(:deals_count)
  attribute :created_clients_count_by_days,
            if: include?(:created_clients_count_by_days)

  attribute :clients_count_by_statuses, if: include?(:clients_count_by_statuses)
  attribute :suppliers_count_by_statuses,
            if: include?(:suppliers_count_by_statuses)
  attribute :deals_count_by_statuses, if: include?(:deals_count_by_statuses)
  attribute :leads_count_by_statuses, if: include?(:leads_count_by_statuses)
  attribute :indicators_dynamic, if: include?(:indicators_dynamic)
  attribute :sales_managers, if: include?(:sales_managers)
  attribute :plan_income, if: include?(:plan_income)
  attribute :actual_income, if: include?(:actual_income)
  attribute :actual_income_by_days, if: include?(:actual_income_by_days)
  attribute :income_from_deals_by_days, if: include?(:income_from_deals_by_days)

  attribute :lead_statuses, if: include?(:lead_statuses)
  attribute :client_statuses, if: include?(:client_statuses)
  attribute :deal_statuses, if: include?(:deal_statuses)
  attribute :supplier_statuses, if: include?(:supplier_statuses)
  attribute :receipt_template

  attribute :cash_book_categories, if: include?(:cash_book_categories)
  attribute :cash_flow_budget_categories,
            if: include?(:cash_flow_budget_categories)
  attribute :cash_book_records_counts_by_categories,
            if: include?(:cash_book_records_counts_by_categories)

  def sales_managers
    ActiveModel::Serializer::CollectionSerializer.new(
      object.sales_managers,
      custom_include: %w(full_name image)
    )
  end

  has_many :tasks, serializer: TaskSerializer, if: include?(:tasks)

  def clients_count_by_statuses
    object.clients_count_by_statuses(scope)
  end

  def deals_count_by_statuses
    object.deals_count_by_statuses(scope)
  end

  def leads_count_by_statuses
    object.leads_count_by_statuses(scope)
  end

  def suppliers_count_by_statuses
    object.suppliers_count_by_statuses(scope)
  end

  def indicators_dynamic
    scope_employee = scope.employees.find_by(company_id: object.id)

    yesterday_statistics = object.statistics
                                 .where(
                                   company_id: object.id,
                                   employee_id: scope_employee.id
                                 )
                                 .first

    { clients_count: clients_count,
      leads_count: leads_count,
      deals_count: deals_count,
      products_count: products_count, }.map do |key, value|
      [key, value <=> yesterday_statistics.try(key)]
    end.to_h
  end

  def clients_count
    indicators.clients_count
  end

  def leads_count
    indicators.leads_count
  end

  def products_count
    object.products_for(scope).count
  end

  def deals_count
    indicators.deals_count
  end

  def created_clients_count_by_days
    indicators.created_clients_count_by_days
  end

  def last_clients
    data = object.clients_for(scope)
                 .order(created_at: :desc)
                 .limit(5)
                 .all

    ActiveModel::Serializer::CollectionSerializer.new(
      data,
      custom_include: ['user']
    )
  end

  def last_leads
    data = object.leads_for(scope)
                 .order(created_at: :desc)
                 .limit(5)
                 .all

    ActiveModel::Serializer::CollectionSerializer.new(
      data,
      custom_include: ['user']
    )
  end

  def last_deals
    object.deals_for(scope)
          .order(created_at: :desc)
          .limit(5)
  end

  def actual_income
    indicators.current_month_income
  end

  def actual_income_by_days
    indicators.actual_income_by_days
  end

  def income_from_deals_by_days
    indicators.actual_income_by_days(only_deals: true)
  end

  def lead_statuses
    statuses =
      LeadStatus
      .where(company_id: object.id)
      .or(LeadStatus.where(system: true))
      .order(weight: :asc)

    ActiveModel::Serializer::CollectionSerializer.new(statuses)
  end

  def client_statuses
    statuses =
      ClientStatus
      .where(company_id: object.id)
      .or(ClientStatus.where(system: true))
      .order(weight: :asc)

    ActiveModel::Serializer::CollectionSerializer.new(statuses)
  end

  def deal_statuses
    statuses =
      DealStatus
      .where(company_id: object.id)
      .or(DealStatus.where(system: true))
      .order(weight: :asc)

    ActiveModel::Serializer::CollectionSerializer.new(statuses)
  end

  def supplier_statuses
    statuses =
      SupplierStatus
      .where(company_id: object.id)
      .or(SupplierStatus.where(system: true))
      .order(weight: :asc)

    ActiveModel::Serializer::CollectionSerializer.new(statuses)
  end

  def cash_book_categories
    categories = CashBookCategory.for_company(object.id)

    ActiveModel::Serializer::CollectionSerializer.new(categories)
  end

  def cash_flow_budget_categories
    CashFlowBudgetCategory.where(company_id: object.id)
  end

  def cash_book_records_counts_by_categories
    records = CashBookRecord.where(company_id: object.id)

    if scope.has_role?(:employee_investor, object)
      records = records.including_personal_for(scope.id)
    else
      records = records.not_personal
    end

    categories = CashBookCategory.for_company(object.id)

    categories.map do |category|
      [category.id, records.where(category: category.id).count]
    end.to_h
  end

  def receipt_template
    object.receipt_template || {
      header: ReceiptContent::HEADER,
      item: ReceiptContent::ITEM,
      footer: ReceiptContent::FOOTER,
    }
  end

  private

  def indicators
    CompanyIndicators.new(object, scope)
  end
end
