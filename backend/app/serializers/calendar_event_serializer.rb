class CalendarEventSerializer < ApplicationSerializer
  attributes :id,
             :title,
             :start,
             :kind,
             :end,
             :original_event_id
end
