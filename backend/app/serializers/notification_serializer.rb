#########################################
# The class for serialize a role object #
#########################################
class NotificationSerializer < ApplicationSerializer
  attributes :id,
             :user_id,
             :content,
             :subject_id,
             :viewed_at,
             :date

  def date
    date = object.created_at.to_date
    time = object.created_at.strftime("%H:%M")
    if date == Date.today
      time
    elsif date == 1.days.ago.to_date
      'вчера в ' + time
    else
      date
    end
  end
end
