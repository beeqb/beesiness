############################################
# The class for serialize a product object #
############################################
class ProductSerializer < ApplicationSerializer
  attributes :id,
             :images,
             :company_id,
             :name,
             :vendor_code,
             :barcode,
             :description,
             :price,
             :first_cost,
             :deleted_at,
             :activity,
             :deal_statistics,
             :delivery_time,
             :is_recommended,
             :preview_index,
             :quantity,
             :category_id

  ##################
  # CUSTOM INCLUDE #
  ##################

  attribute :deals_count, if: include?(:deals_count)
  has_one :company, if: include?(:company)
  belongs_to :category, if: include?(:category)
  attribute :history_length, if: include?(:history_length)
  attribute :current_month_income, if: include?(:current_month_income)
  attribute :current_month_income_by_days,
            if: include?(:current_month_income_by_days)
  attribute :current_month_plan, if: include?(:current_month_plan)
  attribute :popularity, if: include?(:popularity)
  attribute :clients_purchased_percent, if: include?(:clients_purchased_percent)
  attribute :sales_percent, if: include?(:sales_percent)
  attribute :income_percent, if: include?(:income_percent)

  delegate :sales_percent,
           :income_percent,
           :clients_purchased_percent,
           :popularity,
           :current_month_plan,
           to: :indicators

  delegate :income, :income_by_days, to: :indicators, prefix: :current_month

  def deals_count
    object.deals.count
  end

  def current_month_plan
      indicators.current_month_plan
  end

  def popularity
      indicators.popularity
  end

  # TODO: перенести всю эту байду в сервис ProductIndicators
  def deal_statistics
    statistics = {
      revenue_from_sales: 0,
      total_sold: 0,
      average_count_per_invoice: 0,
      total_invoices: 0,
      returns_count: 0,
      returns_sum: 0,
      created_at: object.created_at,
      last_sale_date: nil,
    }

    DealProduct
      .joins(:deal)
      .where(product_id: object.id)
      .where.not(deals: { payed_at: nil })
      .each do |dp|
        dp_count = dp.count.to_i
        dp_price = dp.product.price.to_f

        statistics[:total_invoices] += 1

        statistics[:revenue_from_sales] += dp_count * dp_price
        statistics[:total_sold] += dp_count

        if statistics[:last_sale_date].to_i < dp.deal.payed_at.to_i
          statistics[:last_sale_date] = dp.deal.payed_at
        end

        if dp.was_returned?
          statistics[:returns_count] += dp_count
          statistics[:returns_sum] += dp_count * dp_price
        else
          statistics[:returns_count] += dp.returns.sum(:count)
          statistics[:returns_sum] += dp.returns.sum(:sum)
        end
      end

    statistics[:average_count_per_invoice] =
      statistics[:total_sold] / statistics[:total_invoices] rescue 0

    statistics.as_json
  end

  private

  def indicators
    @indicators ||= ProductIndicators.new(object)
  end
end
