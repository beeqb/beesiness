#############################################
# The class for serialize a employee object #
#############################################
class EmployeeSerializer < ApplicationSerializer
  attributes :id,
             :user_id,
             :company_id,
             :user_activity_id,
             :chief_id,
             :salary,
             :salary_period,
             :activity_changed_at,
             :created_at,
             :deleted_at,
             :pass_number,
             :participation_in_sales,
             :job_started_at,
             :vacation_start,
             :vacation_end,
             :card_number,
             :identity,
             :latitude,
             :longitude,
             :address,
             :email,
             :dismissed,
             :cashbox_name,
             :name,
             :surname

  ##################
  # CUSTOM INCLUDE #
  ##################

  attribute :company, if: include?(:company)
  attribute :employee_phones
  attribute :user, if: include?(:user)
  attribute :full_name, if: include?(:full_name)
  attribute :image, if: include?(:image)
  attribute :online, if: include?(:online)
  attribute :tasks_statistic, if: include?(:tasks_statistic)
  attribute :income, if: include?(:income)
  attribute :income_by_days, if: include?(:income_by_days)

  has_many :forbidden_product_categories, if: include?(:forbidden_product_categories)
  has_many :employees_departments, if: include?(:employees_departments)

  def dismissed
    UserActivity.dismissed?(object.user_activity_id)
  end

  def online
    object.user.online?
  end

  def image
    object.user.image
  end

  def full_name
    object.user.name + ' ' + object.user.surname
  end

  def name
    object.user.name 
  end

  def surname
    object.user.surname
  end

  def employees_departments
    options = {}
    if self.class.include?('employees_departments.department')
      options[:custom_include] = 'department'
    end
    ActiveModel::Serializer::CollectionSerializer.new(
      object.employees_departments,
      options
    )
  end

  def tasks_statistic
    executor_scope = Task.assigned_to_employee(object)
    {
      total: executor_scope.count,
      this_month: executor_scope.done.in_this_month.count,
      done: executor_scope.done.count,
      in_progress: executor_scope.in_progress.count,
      overdue: executor_scope.overdue.count,
    }
  end

  def user
    UserSerializer.new(
      object.user,
      custom_include: [
        'roles_in_companies',
      ]
    )
  end

  def salary
    return nil if object.salary.nil?

    return object.salary.to_i if object.salary % 10 == 0

    object.salary
  end

  def employee_phones
    ActiveModel::Serializer::CollectionSerializer.new(
      object.employee_phones
    )
  end

  def company
    custom_include = %w(clients_count leads_count products_count deals_count)

    if self.class.include?('company.actual_income')
      custom_include.push('actual_income')
    end
    if self.class.include?('company.plan_income')
      custom_include.push('plan_income')
    end

    if self.class.include?('company.income_from_deals_by_days')
      custom_include.push('income_from_deals_by_days')
    end

    CompanySerializer.new(
      object.company,
      custom_include: custom_include,
      scope: scope
    )
  end

  def income
    EmployeeActivity.new(object).income
  end

  def income_by_days
    EmployeeActivity.new(object).income_by_days
  end
end
