###################################################
# The class for serialize a employee phone object #
###################################################
class EmployeePhoneSerializer < ApplicationSerializer
  attributes :id,
             :employee_id,
             :country_phone_code_id,
             :phone,
             :kind,
             :company_name

  # TODO: do this attribute custom included
  attribute :country_phone_code

  def country_phone_code
    object.country_phone_code
  end

  def company_name
    object.employee.company.name
  end
end
