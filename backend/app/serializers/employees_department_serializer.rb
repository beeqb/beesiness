########################################################
# The class for serialize a employee department object #
########################################################
class EmployeesDepartmentSerializer < ApplicationSerializer
  attributes :id,
             :employee_id,
             :department_id,
             :is_head,
             :position,
             :deleted_at

  def is_head
    return false if object.is_head.nil?
    object.is_head
  end

  ##################
  # CUSTOM INCLUDE #
  ##################

  attribute :department, if: include?(:department)
end
