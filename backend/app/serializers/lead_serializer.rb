#########################################
# The class for serialize a lead object #
#########################################
class LeadSerializer < ApplicationSerializer
  attributes :id,
             :name,
             :surname,
             :birthday,
             :country_id,
             :city_id,
             :company_id,
             :address,
             :latitude,
             :longitude,
             :email,
             :work_phone,
             :work_phone_code_id,
             :cell_phone,
             :cell_phone_code_id,
             :site,
             :skype,
             :facebook,
             :viber,
             :current_job,
             :current_department,
             :current_position,
             :current_is_head,
             :relations,
             :created_at,
             :deleted_at,
             :status,
             :manager_id,
             :second_manager_id,
             :visibility,
             :legal_form,
             :discount,
             :notes,
             :legal_entity_email,
             :legal_entity_name,
             :source,
             :recommender,
             :potential_amount,
             :image,
             :type

  def image
    if object.legal_form == 'individual'
      default_url = 'assets/img/default-avatar.png'
      { url: object.user.try(:image).try(:url) || default_url }
    elsif object.legal_form == 'legal_entity'
      default_url = 'assets/img/p0.jpg'
      { url: object.legal_entity.try(:logo).try(:url) || default_url }
    end
  end

  def potential_amount
    object.potential_amount || 0
  end

  ##################
  # CUSTOM INCLUDE #
  ##################

  attribute :legal_entity, if: include?(:legal_entity)
  attribute :history_length, if: include?(:history_length)
  attribute :related_tasks_count, if: include?(:related_tasks_count)
  attribute :lead_products, if: include?(:lead_products)

  def related_tasks_count
    Task
    .where(
      client_id: object.id,
    )
    .count
  end

  def lead_products
    options = {}
    if self.class.include?('lead_products.product')
      options[:custom_include] = 'product'
    end

    ActiveModel::Serializer::CollectionSerializer.new(
      object.lead_products,
      options
    )
  end
end
