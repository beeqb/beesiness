class GenericStatusSerializer < ApplicationSerializer
  attributes :id,
             :name,
             :color,
             :system,
             :company_id
end
