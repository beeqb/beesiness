###############################################
# The class for serialize a department object #
###############################################
class DepartmentSerializer < ApplicationSerializer
  attributes :id,
             :name,
             :color,
             :company_id,
             :editable,
             :deleted_at

  ##################
  # CUSTOM INCLUDE #
  ##################

  attribute :company, if: include?(:company)
  attribute :employees_count, if: include?(:employees_count)
  attribute :head, if: include?(:head)

  def head
    return nil unless object.head

    {
      id: object.head.id,
      name: object.head.user.name,
      surname: object.head.user.surname,
    }
  end

  def employees_count
    object.employees.count
  end
end
