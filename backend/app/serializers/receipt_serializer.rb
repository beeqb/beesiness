class ReceiptSerializer < ActiveModel::Serializer
  attributes :id,
             :content,
             :kind,
             :created_at
end
