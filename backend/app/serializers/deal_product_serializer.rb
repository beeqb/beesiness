#################################################
# The class for serialize a deal product object #
#################################################
class DealProductSerializer < ApplicationSerializer
  attributes :id,
             :deal_id,
             :product_id,
             :count,
             :price,
             :returned_at,
             :count_minus_returns

  # TODO: do this attribute custom included
  attribute :product

  def product
    object.product
  end
end
