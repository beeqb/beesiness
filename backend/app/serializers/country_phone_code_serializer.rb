#######################################################
# The class for serialize a country phone code object #
######################################################
class CountryPhoneCodeSerializer < ApplicationSerializer
  attributes :id,
             :country_name,
             :country_code,
             :code,
             :name

  def name
    "#{object.country_name} (#{object.code})"
  end
end
