class CashBookCategorySerializer < ApplicationSerializer
  attributes :id,
             :name,
             :system,
             :expense

  def name
    if object.system?
      I18n.t("activerecord.cash_book_record.categories.#{object.id}")
    else
      object.name
    end
  end
end
