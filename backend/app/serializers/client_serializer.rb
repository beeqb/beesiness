###########################################
# The class for serialize a client object #
###########################################
class ClientSerializer < ApplicationSerializer
  attributes :id,
             :name,
             :surname,
             :birthday,
             :country_id,
             :city_id,
             :company_id,
             :address,
             :latitude,
             :longitude,
             :email,
             :work_phone_code_id,
             :work_phone,
             :work_phone_full,
             :cell_phone_code_id,
             :cell_phone,
             :cell_phone_full,
             :site,
             :skype,
             :facebook,
             :viber,
             :current_job,
             :current_department,
             :current_position,
             :current_is_head,
             :relations,
             :created_at,
             :deleted_at,
             :status,
             :manager_id,
             :second_manager_id,
             :visibility,
             :legal_form,
             :discount,
             :notes,
             :legal_entity_email,
             :legal_entity_name,
             :user_id,
             :image,
             :source,
             :deposit,
             :type
  def image
    if object.legal_form == 'individual'
      {
        url: object.user.try(:image).try(:url) ||
          'assets/img/default-avatar.png',
      }
    elsif object.legal_form == 'legal_entity'
      { url: object.legal_entity.try(:logo).try(:url) || 'assets/img/p0.jpg' }
    end
  end

  ##################
  # CUSTOM INCLUDE #
  ##################

  attribute :legal_entity, if: include?(:legal_entity)
  attribute :user, if: include?(:user)
  attribute :history_length, if: include?(:history_length)
  attribute :related_tasks_count, if: include?(:related_tasks_count)
  has_many :deals, if: include?(:deals)
  attribute :rf_status, if: include?(:rf_status)
  attribute :rm_status, if: include?(:rm_status)
  attribute :income, if: include?(:income)
  attribute :income_by_days, if: include?(:income_by_days)
  attribute :deals_stats, if: include?(:deals_stats)
  attribute :products, if: include?(:products)
  attribute :sales_dynamic, if: include?(:sales_dynamic)

  def rf_status
    RFMAnalysis.new(object).rf_status
  end

  def rm_status
    RFMAnalysis.new(object).rm_status
  end

  def income
    indicators.income
  end

  def income_by_days
    indicators.income_by_days
  end

  def sales_dynamic
    {
      sum: indicators.sales_dynamic(:sum),
      quantity: indicators.sales_dynamic(:quantity),
    }
  end

  def deals_stats
    {
      first_purchase_date: indicators.first_purchase_date,
      last_purchase_date: indicators.last_purchase_date,
      total_deals: {
        count: object.deals.count,
        sum: object.deals.sum(:total_sum),
        average_sum: object.deals.average(:total_sum),
      },
      returned_deals: {
        count: object.deals.where(status: :returned).count,
        sum: object.deals.where(status: :returned).sum(:total_sum),
      },
      potential_deals: {
        count: indicators.potential_deals_count,
        sum: indicators.potential_deals_sum,
      },
      won_deals: {
        count: indicators.won_deals_count,
        sum: indicators.won_deals_sum,
      },
      lost_deals: {
        count: indicators.lost_deals_count,
        sum: indicators.lost_deals_sum,
      },
    }
  end

  def related_tasks_count
    Task.where(client_id: object.id).count
  end

  def deals
    object.deals.order(created_at: :desc)
  end

  def products
    deals_ids = object.deals.pluck(:id)
    products =
      Product
      .joins(:deals)
      .where(deals: { id: deals_ids })
      .where.not( deals: { payed_at: nil })
      .uniq

    ActiveModel::Serializer::CollectionSerializer.new(
      products,
      custom_include: ['category']
    )
  end

  private

  def indicators
    ClientIndicators.new(object)
  end
end
