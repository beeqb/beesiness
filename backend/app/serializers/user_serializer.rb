#########################################
# The class for serialize a user object #
#########################################
class UserSerializer < ApplicationSerializer
  attributes User.public_params
  attributes :online, :fullname

  def online
    scope.try(:id) == object.id || object.online?
  end

  def active_tasks_count
    Task.assigned_to(object).active.length
  end

  ##################
  # CUSTOM INCLUDE #
  ##################

  attribute :allow_change_password, if: include?(:allow_change_password)
  attribute :cell_phone_code, if: include?(:cell_phone_code)
  attribute :roles_in_companies, if: include?(:roles_in_companies)
  attribute :profile_fullness, if: include?(:profile_fullness)
  attribute :permissions, if: include?(:permissions)
  attribute :departments_in_companies, if: include?(:departments_in_companies)
  attribute :employees, if: include?(:employees)
  attribute :active_tasks_count, if: include?(:employees)
  attribute :companies_client_in, if: include?(:companies_client_in)

  def employees
    ActiveModel::Serializer::CollectionSerializer.new(
      object.employees,
      custom_include: %w(
        forbidden_product_categories
        company
        company.plan_income
        company.actual_income
        company.income_from_deals_by_days
        employees_departments
      ),
      scope: scope
    )
  end

  def departments_in_companies
    result = []

    object.employees.each do |employee|
      return if employee.company.nil?

      departments = employee.employees_departments

      result.push(
        company: CompanySerializer.new(employee.company, {}),
        departments: ActiveModel::Serializer::CollectionSerializer.new(departments),
        dismissed: UserActivity.dismissed?(employee.user_activity_id),
      )
    end

    result
  end

  def companies_client_in
    companies_ids = Client.where(user_id: object.id).pluck(:company_id).uniq

    Company.find(companies_ids)
  end

  def roles_in_companies
    sql = '
      select distinct
        roles.name as role_name,
        roles.resource_id,
        companies.id as company_id,
        companies.name as company_name,
        companies.logo as company_logo
      from users
      join employees on employees.user_id = users.id
      join users_roles on users_roles.user_id = users.id
      join roles on roles.id = role_id
      join companies on companies.id = resource_id
      where
        users.id = ' + object.id.to_s + ' and
        LOCATE("employee_", roles.name) > 0 and
        roles.resource_type = \'Company\'
      order by companies.id asc
    '
    records = ActiveRecord::Base.connection.execute(sql)

    result = []
    records.each do |record|
      company = Company.new(
        id: record[2],
        name: record[3],
        logo: record[4]
      )
      index = result.index { |item| item[:company_id] == record[2] }
      if index.nil?
        result.push(
          company_id: record[2],
          company: CompanySerializer.new(company, {}),
          roles: [{
            name: record[0],
            human_name: I18n.t('roles.' + record[0].to_s),
            resource_id: record[1],
          },]
        )
        next
      end

      result[index][:roles].push(
        name: record[0],
        human_name: I18n.t('roles.' + record[0].to_s),
        resource_id: record[1]
      )
      next
    end
    result
  end
end
