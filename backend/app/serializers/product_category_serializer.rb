#####################################################
# The class for serialize a product category object #
#####################################################
class ProductCategorySerializer < ApplicationSerializer
  attributes :id,
             :name,
             :color,
             :company_id,
             :has_part_in_average_price,
             :deleted_at

  ##################
  # CUSTOM INCLUDE #
  ##################

  attribute :products_count, if: include?(:products_count)
  has_one :company, if: include?(:company)

  def products_count
    object.products.count
  end
end
