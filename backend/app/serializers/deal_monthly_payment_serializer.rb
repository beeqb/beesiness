class DealMonthlyPaymentSerializer < ApplicationSerializer
  attributes :id,
             :amount,
             :date,
             :deal_id,
             :paid_at

  attribute :deal_status

  def deal_status
    object.deal.status
  end
end
