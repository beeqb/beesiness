#################################
# This class is base serializer #
#################################
class ApplicationSerializer < ActiveModel::Serializer
  def self.include?(name)
    -> { instance_options[:custom_include].include?(name.to_s) }
  end

  def history_length
    SystemEvent
    .where(
      subject_type: object.class.name,
      subject_id: object.id
    )
    .count
  end

  def related_tasks_count
    return nil unless object.respond_to?(:client_id)
    return 0 unless object.client_id

    Task.where(client_id: object.client_id).count
  end

  protected

  def instance_options
    options = super
    options[:custom_include] ||= []
    options
  end
end
