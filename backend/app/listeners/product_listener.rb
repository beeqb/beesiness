############################################
# Обработчик событий, связанных с товарами #
############################################
class ProductListener
  def product_attribute_changed(event)
    return unless event.dig(:actors, :attribute).to_sym == :price

    product = event[:subject]
    
    UpdateDealsWithProductSums.call(product)

    managers =
      product.company.employees_with_roles(
        :employee_manager_of_sales_department
      )

    Notification::ProductPriceChanged.for(
      managers.pluck(:user_id),
      product.id,
      old_price: product.previous_changes[:price].first,
      new_price: product.previous_changes[:price].last,
    )
  end
end
