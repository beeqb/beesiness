class ClientListener
  def client_updated(event)
    @client = event[:subject]

    deposit_changes = @client.previous_changes['deposit']

    return if deposit_changes.blank?

    category = 'income'
    deposit_increase = deposit_changes[1] - (deposit_changes[0] || 0)

    @client.deposit_fees.create(amount: deposit_increase)

    if deposit_increase < 0
      category = 'outcome'

      deposit_increase *= -1
    end

    company_id = @client.company_id

    cash_book_record = CashBookRecord.new(
      company_id: company_id,
      creator_id: event[:user].employee_in_company(company_id).id,
      amount: deposit_increase,
      currency: @client.company.currency,
      category: category
    )

    cash_book_record.save!
  end
end
