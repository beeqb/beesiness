#############################################
# Обработчик событий, связанных со сделками #
#############################################
class DealListener
  def deal_created(event)
    @deal = event[:subject]

    create_supplier if should_create_supplier?

    CreateCashBookRecordsFromDeal.call(@deal)
    AdjustProductsCountsInDeal.call(@deal, :decrease)
    CreateReceipt.call(@deal) if @deal.status.in? CashBook.paid_deal_statuses
  end

  def deal_status_changed(event)
    deal = event[:subject]
    employee = event[:user].employee_in_company(deal.company_id)

    CreateCashBookRecordsFromDeal.call(deal) do |record|
      record.creator_id = employee.id
    end 

    CreateReceipt.call(deal) if deal.status.in? CashBook.paid_deal_statuses
  end

  def deal_returned(event)
    deal = event[:subject]
    employee = event[:user].employee_in_company(deal.company_id)

    CreateCashBookRecordsFromDeal.call(deal) do |record|
      record.creator_id = employee.id
    end

    AdjustProductsCountsInDeal.call(deal, :increase)
    CreateReceipt.call(deal, :return)
  end

  def deal_partially_returned(event)
    deal = event[:subject]
    products_return = event[:actors][:return]

    employee = event[:user].employee_in_company(deal.company_id)

    cash_book_record = DealToCashBookMap.call(deal)
    cash_book_record.creator_id = employee.id
    cash_book_record.amount = products_return.sum
    cash_book_record.category = 'outcome'
    cash_book_record.cash_flow_budget_category = nil

    cash_book_record.save!

    product = products_return.deal_product.product
    AdjustProductQuantity.call(product, :increase, products_return.count)

    receipt_deal_product = products_return.deal_product.dup
    receipt_deal_product.count = products_return.count
    CreateReceipt.call(deal, :return, [receipt_deal_product])
  end

  def deal_product_created(event)
    deal_product = event[:subject]

    product = deal_product.product
    AdjustProductQuantity.call(product, :decrease, deal_product.count)
  end

  def deal_product_updated(event)
    deal_product = event[:subject]

    return unless deal_product.previous_changes[:count]

    product = deal_product.product
    AdjustProductQuantity.call(
      product,
      :decrease,
      deal_product.previous_changes[:count].last -
        deal_product.previous_changes[:count].first
    )
  end

  private

  def create_supplier
    supplier = CompanyToSupplierMap.call(@deal.company)
    supplier.company_id = @deal.client.legal_entity_id
    supplier.save!
  end

  def should_create_supplier?
    return false unless client

    client.legal_entity? && !client.legal_entity.has_supplier?(@deal.company)
  end

  def client
    @deal.client
  end
end
