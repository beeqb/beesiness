class SystemEventListener
  def respond_to?(name)
    true
  end

  def method_missing(name, *args)
    kind = name.to_s

    return unless SystemEvent::Registry.include?(kind)

    args_hash = args.first

    event = SystemEvent.new(
      kind: kind,
      user: args_hash[:user],
      subject: args_hash[:subject]
    )

    event_description = event_class(kind).constantize rescue nil

    return if event_description.nil?

    description_object = event_description.new(event, args_hash[:actors])

    event.general_description = description_object.general_description
    event.local_description = description_object.local_description
    event.meta_data = description_object.meta_data

    event.save!
  end

  private

  def event_class(kind)
    scope, event = SystemEvent::Registry.parse_kind(kind).map(&:to_s)
    "SystemEvent::Events::#{scope.classify}::#{event.classify}"
  end
end
