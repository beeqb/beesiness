class CashBookListener
  def on_cash_gap(company)
    user_ids = company
               .employees_with_roles(
                 :employee_accountant,
                 :employee_director
               )
               .pluck(:user_id)

    Notification::CashBookCashGap.for(user_ids, company.id)
  end
end
