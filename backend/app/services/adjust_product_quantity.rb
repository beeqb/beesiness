class AdjustProductQuantity
  def initialize(product)
    @product = product
  end

  def self.call(product, operation, count)
    return if product.quantity.zero?

    new(product).perform(operation, count)
  end

  def perform(operation, count)
    message = case operation.to_sym
              when :increase then :+
              when :decrease then :-
              else
                raise ArgumentError 'Invalid operation `#{operation}` provided'
              end

    @product.update(quantity: @product.quantity.send(message, count))
  end
end
