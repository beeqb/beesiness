class SalesReport
  include DateTimeHelper

  def initialize(company, user, quarter, year = Time.now.year)
    @company = company
    @user = user
    @quarter = quarter.to_i
    @year = year
  end

  def build
    {
      sales_plan_monthly: sales_plan_monthly,
      sales_plan_summary: sales_plan_summary,

      actual_sales_monthly: actual_sales_monthly,
      actual_sales_summary: actual_sales_summary,

      sales_plan_by_categories_monthly: sales_plan_by_categories_monthly,
      sales_plan_by_categories_summary: sales_plan_by_categories_summary,

      actual_sales_by_categories_monthly: actual_sales_by_categories_monthly,
      actual_sales_by_categories_summary: actual_sales_by_categories_summary,

      actual_sales_year: actual_sales_year,
    }
  end

  def sales_plan_monthly
    return @sales_plan_monthly if defined? @sales_plan_monthly

    plans = @company.sales_plans.where(
        month: quarter_months_number_range(@quarter),
        year: @year
    )

    @sales_plan_monthly = plans.each_with_object({}) do |plan, result|
      result[plan.month] = { money: plan.sum, quantity: plan.quantity }
    end
  end

  def actual_sales_year
    now = Time.now

    sales_money = cash_book_records.income.where(
      created_at: now.beginning_of_year..now.end_of_year
    ).order(created_at: :asc)

    result = Hash.new { |h, k| h[k] = { money: 0 } }
    sales_money.each do |income|
      month = income.created_at.month
      result[month][:money] += income.amount
    end

    result
  end

  def actual_sales_monthly
    return @actual_sales_monthly if defined? @actual_sales_monthly

    result = Hash.new { |h, k| h[k] = { money: 0 } }

    paid_deals_in_quarter.each do |income|
      month = income.payed_at.month
      plan = sales_plan_monthly.dig(month, :money) || 0
      result[month][:money] += income.total_sum
      result[month][:money_up] = result[month][:money] >= plan
    end

    @actual_sales_monthly =
      actual_sales_in_quantity.each_with_object(result) do |sale, result|
        month = sale.month
        plan = sales_plan_monthly.dig(month, :quantity) || 0

        data = { quantity: sale.count, quantity_up: sale.count >= plan }

        if result[month]
          result[month].merge!(data)
        else
          result[month] = data
        end
      end
  end

  def sales_plan_summary
    return @sales_plan_summary if defined? @sales_plan_summary

    year_plans = @company.sales_plans.where(year: @year)

    return unless year_plans.exists?

    @sales_plan_summary = {
        money: year_plans.sum(:sum),
        quantity: year_plans.sum(:quantity),
    }
  end

  def actual_sales_summary
    deals = paid_deals_in_year

    return unless deals.exists?

    result = {
        money: deals.sum(:total_sum),
        quantity: DealProduct.where(deal_id: deals.pluck(:id)).sum(:count),
    }

    result[:money_up] = result[:money] >= sales_plan_summary[:money]
    result[:quantity_up] = result[:quantity] >= sales_plan_summary[:quantity]

    result
  end

  def sales_plan_by_categories_monthly
    if defined? @sales_plan_by_categories_monthly
      return @sales_plan_by_categories_monthly
    end

    categories_plans = SalesPlanProductCategory.by_date(
        quarter_months_number_range(@quarter),
        @year
    )

    result = {}

    categories_plans.each do |plan|
      category_id = plan.product_category_id
      month = plan.sales_plan.month

      result[month] ||= Hash.new { |h, k| h[k] = { money: 0, quantity: 0 } }

      result[month][category_id][:money] += plan.sum
      result[month][category_id][:quantity] += plan.quantity || 0
    end

    result.each do |month, by_categories|
      by_categories.each do |category_id, values|
        by_categories[category_id][:average_price] =
          values[:money] / values[:quantity]
      end
    end

    @sales_plan_by_categories_monthly = result
  end

  def sales_plan_by_categories_summary
    if defined? @sales_plan_by_categories_summary
      return @sales_plan_by_categories_summary
    end

    categories_plans = SalesPlanProductCategory.by_year(@year)

    result = {}

    categories_plans.each do |plan|
      category_id = plan.product_category_id

      result[category_id] ||= Hash.new(0)

      result[category_id][:money] += plan.sum
      result[category_id][:quantity] += (plan.quantity || 0)
    end

    result.each do |category, values|
      result[category][:average_price] = values[:money] / values[:quantity]
    end

    @sales_plan_by_categories_summary = result
  end

  def actual_sales_by_categories_monthly
    if defined? @actual_sales_by_categories_monthly
      return @actual_sales_by_categories_monthly
    end

    deals_ids = paid_deals_in_quarter.pluck(:id)

    return if deals_ids.empty?

    rows = db.exec_query(
      "SELECT p.category_id as category_id, d.total_sum as sum,
              SUM(dp.count) as quantity, MONTH(d.payed_at) as month
      FROM deals d
      JOIN deal_products dp ON d.id = dp.deal_id
      JOIN products p on p.id = dp.product_id
      WHERE d.id IN (#{deals_ids.join(',')})
      GROUP BY p.category_id, MONTH(d.payed_at)"
    )

    @actual_sales_by_categories_monthly =
      rows.each_with_object({}) do |row, result|
        category_id, month = row.values_at('category_id', 'month')
        plan = sales_plan_by_categories_monthly
        plan_money = plan.dig(month, category_id, :money) || 0
        plan_quantity = plan.dig(month, category_id, :quantity) || 0
        plan_average_price = plan.dig(month, category_id, :average_price) || 0
        result[month] ||= {}

        data = {
          money: row['sum'],
          money_up: row['sum'] >= plan_money,
          quantity: row['quantity'],
          quantity_up: row['quantity'] >= plan_quantity,
        }
        data[:money_deviation] = deviation(row['sum'], plan_money)
        data[:quantity_deviation] =
          deviation_percent(row['quantity'], plan_quantity)
        data[:average_price] = row['sum'] / row['quantity']
        data[:average_price_up] = data[:average_price] >= plan_average_price
        data[:average_price_deviation] =
          deviation(data[:average_price], plan_average_price)
        data[:average_price_deviation_percent] =
          deviation_percent(data[:average_price], plan_average_price)

        result[month][category_id]  = data
      end
  end

  def actual_sales_by_categories_summary
    deals_ids = paid_deals_in_year.pluck(:id)

    return if deals_ids.blank?

    rows = db.exec_query(
        "SELECT p.category_id as category_id, d.total_sum as sum,
              SUM(dp.count) as quantity
      FROM deals d
      JOIN deal_products dp ON d.id = dp.deal_id
      JOIN products p on p.id = dp.product_id
      WHERE d.id IN (#{deals_ids.join(',')})
      GROUP BY p.category_id"
    )

    rows.each_with_object({}) do |row, result|
      category_id = row['category_id']
      plan = sales_plan_by_categories_summary
      plan_money = plan.dig(category_id, :money) || 0
      plan_quantity = plan.dig(category_id, :quantity) || 0
      plan_average_price = plan.dig(category_id, :average_price) || 0

      data = {
          money: row['sum'],
          money_up: row['sum'] >= plan_money,
          quantity: row['quantity'],
          quantity_up: row['quantity'] >= plan_quantity,
      }
      data[:money_deviation] = deviation(row['sum'], plan_money)
      data[:quantity_deviation] =
          deviation_percent(row['quantity'], plan_quantity)
      data[:average_price] = row['sum'] / row['quantity']
      data[:average_price_up] = data[:average_price] >= plan_average_price
      data[:average_price_deviation] =
          deviation(data[:average_price], plan_average_price)
      data[:average_price_deviation_percent] =
          deviation_percent(data[:average_price], plan_average_price)

      result[category_id] = data
    end
  end

  private

  def cash_book_records
    CashBookRecord.where(company_id: @company.id)
  end

  def deviation(actual, planned)
    return if planned.zero?

    (actual - planned).abs
  end

  def deviation_percent(actual, planned)
    return if planned.zero?

    numerator = (actual - planned).abs

    ((numerator / planned) * 100).round(2)
  end

  def paid_deals_in_quarter
    @company.deals_for(@user).paid_in(quarter_months_range(@quarter, @year))
  end

  def paid_deals_in_year
    @company.deals_for(@user).paid_in(year_range(@year))
  end

  def planned_category_ids
    @planned_category_ids ||=
      SalesPlanProductCategory
        .where.not(quantity: nil)
        .pluck(:product_category_id)
        .uniq
  end

  def actual_sales_in_quantity
    deals_ids = paid_deals_in_quarter.pluck(:id)

    DealProduct
      .select('MONTH(deal_products.created_at) as month, SUM(count) as count')
      .joins(:product)
        .where(deal_id: deals_ids)
        .where(products: { category_id: planned_category_ids })
        .group('MONTH(deal_products.created_at)')
  end

  def db
    ActiveRecord::Base.connection
  end
end
