######################################################
# Сервис для преобразования пользователей и компаний #
# в лиды (физ. и юр. лица соответственно)            #
######################################################
class UserOrCompanyToLeadMap
  def self.call(user_or_company)
    return convert_user(user_or_company) if user_or_company.is_a?(User)
    return convert_company(user_or_company) if user_or_company.is_a?(Company)
  end

  private_class_method

  def self.convert_user(user)
    employee_department = if user.employees.present?
                            user.employees.first.employees_departments.first
                          else
                            nil
                          end

    Lead.new(
      legal_form: 'individual',
      name: user.name,
      email: user.email,
      surname: user.surname,
      cell_phone: user.cell_phone,
      cell_phone_code_id: user.cell_phone_code_id,
      work_phone: user.cell_phone,
      work_phone_code_id: user.cell_phone_code_id,
      user: user,
      manager_id: super_admin_employee.id,
      birthday: user.birthday,
      city: user.city,
      company: super_company,
      address: user.address,
      skype: user.skype,
      facebook: user.facebook_link,
      viber: user.viber,
      latitude: user.latitude,
      longitude: user.longitude,
      current_job: user.employees.first.try(:company).try(:name),
      current_position: employee_department.try(:position),
      current_department: employee_department.try(:department).try(:name),
      current_is_head: employee_department.try(:is_head)
    )
  end

  def self.convert_company(company)
    director = company.director

    Lead.new(
      legal_form: 'legal_entity',
      legal_entity: company,
      legal_entity_name: company.name,
      legal_entity_email: company.email,
      work_phone: company.phone,
      work_phone_code_id: company.phone_code_id,
      address: company.address,
      company: super_company,
      site: company.site,
      name: director.name,
      surname: director.surname,
      email: director.email,
    )
  end

  def self.super_company
    Company.find_by_name('BeeQB')
  end

  def self.super_admin_employee
    user = User.with_role(:admin).find do |user|
      user.employee_in_company(super_company)
    end

    user.employee_in_company(super_company)
  end
end
