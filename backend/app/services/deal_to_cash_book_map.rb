##############################################################
# Сервис для преобразования сделки в запись в кассовой книге #
##############################################################
class DealToCashBookMap
  def self.call(deal)
    return nil if deal.total_sum.zero?

    cash_book_record = CashBookRecord.new(
      company: deal.company,
      creator_id: deal.manager_id,
      amount: deal.total_sum,
      currency: deal.company.currency,
      cash_flow_budget_category: deal.cash_flow_budget_category,
      category: 'income',
      bill_kind: deal_payment_option_to_bill_kind(deal.payment_option),
      deal_id: deal.id
    )

    cash_book_record
  end

  private_class_method

  def self.deal_payment_option_to_bill_kind(payment_option)
    return 'cash' if payment_option == 'cash'
    'cashless'
  end
end
