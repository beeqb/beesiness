module Calendar
  ###########################################################
  # Сервис для генерации повторяющихся событий на календаре #
  ###########################################################
  class RecurringEventsGenerator
    def initialize(start_date, end_date, original_event)
      @start_date = DateTime.parse(start_date)
      @end_date = DateTime.parse(end_date)
      @original_event = original_event

      check_original_event
    end

    def events
      send("generate_#{recurring_mode}")
    end

    private

    def generate_daily
      (@start_date..@end_date).each_with_object([]) do |date, result|
        result << build_event_on_date(date)
      end
    end

    def generate_weekly
      week_day = @original_event.start.wday
      week_days = (@start_date..@end_date).select do |date|
        date.wday == week_day
      end

      week_days.each_with_object([]) do |date, result|
        new_event = build_event_on_date(date)
        next unless in_range?(new_event.start, new_event.end)

        result << new_event
      end
    end

    def generate_monthly
      day = @original_event.start.day
      months = time_interval.select { |date| date.day == 1 }

      months.each_with_object([]) do |date, result|
        if date.month == @original_event.start.month
          result << @original_event
          next
        end

        new_event = build_event_on_date(date.change(day: day))
        next unless in_range?(new_event.start, new_event.end)

        result << new_event
      end
    end

    def generate_yearly
      years = time_interval.map(&:year).uniq

      years.each_with_object([]) do |year, result|
        if year == @original_event.start.year
          result << @original_event
          next
        end

        new_event = build_event_on_date(
          @original_event.start.change(year: year)
        )
        next unless in_range?(new_event.start, new_event.end)

        result << new_event
      end
    end

    def in_range?(start_date, end_date)
      overlaps?(start_date, end_date) && (
        @original_event.recurring_end_at.nil? ||
        start_date < @original_event.recurring_end_at
      )
    end

    def overlaps?(start_date, end_date)
      start_date < @end_date && (end_date.nil? || end_date > @start_date)
    end

    def check_original_event
      raise ArgumentError, 'Event is not recurring' unless recurring_mode

      unless recurring_mode.in?([:daily, :weekly, :monthly, :yearly])
        raise ArgumentError,
              "Event's recurring mode `#{recurring_mode}` is not valid"
      end
    end

    def recurring_mode
      @original_event.recurring.try(:to_sym)
    end

    def time_interval
      @start_date.change(day: 1)..@end_date
    end

    def event_span
      return nil unless @original_event.end

      (@original_event.end.to_date - @original_event.start.to_date).to_i
    end

    def calculate_new_event_end(new_start)
      event_span ? new_start + event_span : nil
    end

    def build_event_on_date(date)
      new_event_start = date.change(
        hour: @original_event.start.hour,
        min: @original_event.start.min,
      )
      new_event_end = calculate_new_event_end(new_event_start)

      new_event = @original_event.dup
      new_event.original_event = @original_event
      new_event.start = new_event_start
      new_event.end = new_event_end

      new_event
    end
  end
end
