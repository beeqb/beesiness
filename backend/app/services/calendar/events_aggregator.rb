module Calendar
  class EventsAggregator
    def initialize(start_date, end_date)
      @start_date = start_date
      @end_date = end_date
    end

    def events_for(user)
      @user = user
      
      tasks_events + vacations + created_manually_events + recurring_events
    end

    private

    def tasks_events
      Task
        .assigned_to(@user)
        .where(due_date: time_interval)
        .map do |task|
          CalendarEvent.new(
            title: task.title,
            start: task.due_date,
            kind: :task
          )
        end
    end

    def vacations
      companies_ids = user_employees.uniq.pluck(:company_id)

      # Для проверки условия на даты
      #
      # vs - vacation_start, ve - vacation_end, s - start_date, e - end_date
      #
      # vs - ve - s - e: не пересекаются и не будут выбраны(потому что ve < s)
      # vs - s - ve - e: пересекаются и будут выбраны
      # s - vs - e - ve: пересекаются и будут выбраны
      # s - e - vs - ve: не пересекаются и не будут выбраны(потому что vs > e)
      # s - vs - ve - e: пересекаются и будут выбраны
      # vs - s - e - ve: пересекаются и будут выбраны
      #
      # Источник:
      # http://stackoverflow.com/questions/2545947/mysql-range-date-overlap-check
      Employee
        .where(company_id: companies_ids)
        .where('vacation_start < ?', @end_date)
        .where('vacation_end > ?', @start_date)
        .map do |employee|
          CalendarEvent.new(
            title: vacation_event_title(employee),
            start: employee.vacation_start,
            end: employee.vacation_end,
            kind: :vacation
          )
        end
    end

    def vacation_event_title(employee)
      if employee.user_id == @user.id
        "Отпуск в компании #{employee.company.name}"
      else
        "Отпуск #{employee.user.fullname} в компании #{employee.company.name}"
      end
    end

    def created_manually_events
      query = user_events
        .where('start < ?', @end_date)
        .where('end IS NULL OR end > ?', @start_date)
        .where(recurring: nil)
    end

    def recurring_events
      events = user_events
               .where('start < ?', @end_date)
               .where.not(recurring: nil)

      events.each_with_object([]) do |event, result|
        generator =
          Calendar::RecurringEventsGenerator.new(@start_date, @end_date, event)

        result.push(*generator.events)
      end
    end

    def user_employees
      @user.employees
    end

    def user_events
      CalendarEvent.where(user: @user)
    end

    def time_interval
      @start_date..@end_date
    end
  end
end
