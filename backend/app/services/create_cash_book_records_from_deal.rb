####################################################################
# Сервис для создания записей в кассовой книге на основании сделки #
####################################################################
class CreateCashBookRecordsFromDeal
  def self.call(deal)
    new(deal).work
  end

  def initialize(deal)
    @deal = deal
  end

  def work
    return unless @deal.status.in?(paid_statuses)

    cash_book_record = DealToCashBookMap.call(@deal)

    return unless cash_book_record

    yield cash_book_record if block_given?
    cash_book_record.category = category
    setup_client_source(cash_book_record)

    cash_book_record.save!

    create_cash_book_record_for_client if client && client.legal_entity?
  end

  private

  def setup_client_source(cash_book_record)
    if client
      cash_book_record.source = client
    else
      cash_book_record.source_type = 'Client'
    end
  end

  def setup_supplier_source(cash_book_record)
    return unless client && client.legal_entity?

    cash_book_record.source = Supplier.find_by(
      company_id: client.legal_entity_id,
      legal_entity_id: @deal.company_id
    )
  end

  def category
    @deal.status.in?(outcome_statuses) ? 'outcome' : 'income'
  end

  def create_cash_book_record_for_client
    cash_book_record = DealToCashBookMap.call(@deal)
    cash_book_record.category = CashBook.invert_category(category)
    cash_book_record.company = client.legal_entity
    setup_supplier_source(cash_book_record)
    cash_book_record.save!
  end

  def paid_statuses
    CashBook.paid_deal_statuses
  end

  def outcome_statuses
    CashBook.outcome_deal_statuses
  end

  def client
    @deal.client
  end
end
