# Сервис для построения воронки продаж отдельного сотрудника
class SalesFunnel
  include DateTimeHelper

  attr_writer :deals_scopes
  attr_writer :start_date
  attr_writer :end_date

  def initialize(employee)
    @employee = employee
  end

  def data
    result = deal_statuses.map do |status|
      [
        status,
        {
          in_status: deals_in_status(status).count,
          to_payment: deal_transitions_count(status, :payed),
          to_refusal: deal_transitions_count(status, :lost),
        },
      ]
    end.to_h

    result[:deals_total_count] = deals.count
    result
  end

  def average_time_until_statuses
    (deal_statuses + [:payed]).map do |status|
      [status, average_time_until_status(status)]
    end.to_h
  end

  def average_time_until_status(status)
    timestamp_column = "#{status}_at"
    deals
      .where.not(timestamp_column => nil)
      .pluck("AVG(timestampdiff(hour, created_at, #{timestamp_column}))")
      .first
      .try(:round)
  end

  def employee_activity
    paid_deals = deals.paid_in(start_date..end_date)
    {
      average_deals_sum: paid_deals.average(:total_sum),
      deals_count: paid_deals.count,
      deals_sum: deals_sum,
      percentage_of_the_plan: percentage_of_the_plan,
    }
  end

  private

  def deals
    deals_scopes = if @deals_scopes.present?
                     @deals_scopes.dup
                   else
                     [:not_closed_between, :by_closed_at]
                   end

    query =
      Deal
      .by_manager(@employee.id)
      .send(deals_scopes.shift, start_date, end_date)

    deals_scopes.each do |scope|
      query = query.or(
        Deal.by_manager(@employee.id).send(scope, start_date, end_date)
      )
    end

    query
  end

  def percentage_of_the_plan
    index = Hash.new { |h, k| h[k] = 0 }

    (start_date..end_date).each do |date|
      index[[date.month, date.year]] += 1
    end

    planned_sum = index.map do |(month, year), days_count|
      plan = SalesPlan.find_by(
        company_id: @employee.company_id,
        month: month,
        year: year
      )

      next unless plan

      part =
        plan
        .sales_plan_employees
        .find_by(employee_id: @employee.id)
        .try(:part) || 0

      day_plan = plan.sum / Time.days_in_month(month, year)
      day_plan * days_count * part.fdiv(100)
    end.sum

    (deals_sum / planned_sum) * 100
  end

  def deals_sum
    deals.paid_in(start_date..end_date).sum(:total_sum)
  end

  def deal_transitions_count(from, to)
    transitions_count =
        SystemEvent
      .joins('INNER JOIN deals d ON d.id = subject_id')
      .where(created_at: start_date..end_date, kind: 'deal:status_changed')
      .where("JSON_EXTRACT(meta_data, '$.old_status') = ?", from)
            .where("JSON_EXTRACT(meta_data, '$.new_status') = ?", to)
      .where('d.manager_id = ?', @employee.id)
      .count

    transitions_count += instant_transitions(to) if from == :in_progress

    transitions_count
  end

  def instant_transitions(to_status)
    Deal
        .where("created_at = #{to_status}_at")
        .where(
            created_at: start_date..end_date,
            manager: @employee,
            status: to_status
        )
        .count
  end

  def deals_in_status(status)
    query = deals.where("#{status}_at <= ?", end_date)
    (all_deal_statuses - [status]).each do |other_status|
      query = query.where(
        "#{other_status}_at <= #{status}_at OR #{other_status}_at IS NULL"
      )
    end

    query
  end

  def all_deal_statuses
    DealStatus.system.pluck(:id).map(&:to_sym)
  end

  def deal_statuses
    [:in_progress, :contract_conclusion, :contract_signed, :won]
  end

  def start_date
    if @start_date
      Date.parse(@start_date)
    else
      Date.today.beginning_of_month
    end
  end

  def end_date
    if @end_date
      Date.parse(@end_date)
    else
      Date.today.end_of_month
    end
  end
end
