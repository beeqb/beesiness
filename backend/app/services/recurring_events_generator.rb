module Calendar
  class RecurringEventsGenerator
    def initialize(start_date, end_date, original_event)
      @start_date = start_date
      @end_date = end_date
      @original_event = original_event

      check_original_event
    end

    def events
      send("generate_#{recurring_mode}")
    end

    def generate_monthly
      day = @original_event.start.day
      months = time_interval.select { |date| date.day == 1 }

      months.map do |date|
        new_event_start = date.change(day: day)
        new_event_end = calculate_new_event_end(new_event_start)

        next unless new_event_start < @end_date && new_event_end > @start_date

        new_event = original_event.dup
        new_event.start = new_event_start
        new_event.end = new_event_end

        new_event
      end
    end

    private

    def check_original_event
      unless recurring_mode
        raise ArgumentError, 'Event is not recurring'
      end

      unless recurring_mode.in?(:daily, :weekly, :monthly, :early)
        raise ArgumentError,
              "Event's recurring mode `#{recurring_mode}` is not valid"
      end
    end

    def recurring_mode
      @original_event.recurring.try(:to_sym)
    end

    def time_interval
      @start_date..@end_date
    end

    def event_span
      return nil unless @original_event.end

      @original_event.end - @original_event.start
    end

    def calculate_new_event_end(new_start)
      event_span ? new_start + event_span : nil
    end
  end
end
