# сервис для расчета показателей по поставщикам
class SupplierIndicators
  include DateTimeHelper

  def initialize(supplier, client_company_id)
    @supplier = supplier
    @client_company_id = client_company_id
  end

  # Возвращает суммарный доход от продаж товара за текущий месяц
  def income
    current_month_paid_deals.sum(:total_sum)
  end

  # Возвращает доход от продаж товара за текущий месяц,
  # разбитый по дням месяца
  def income_by_days
    days_number = current_month_range.end.day

    result = Array.new(days_number, 0)

    current_month_paid_deals
      .select('DAY(payed_at) day, SUM(total_sum) sum')
      .group('DAY(payed_at)')
      .each_with_object(result) do |row, result_array|
        result_array[row.day.to_i - 1] = row.sum
      end
  end

  def potential_deals_count
    deals.in_progress.count
  end

  def potential_deals_sum
    deals.in_progress.sum(:total_sum)
  end

  def won_deals_count
    deals.won.count
  end

  def won_deals_sum
    deals.won.sum(:total_sum)
  end

  def lost_deals_count
    deals.lost.count
  end

  def lost_deals_sum
    deals.lost.sum(:total_sum)
  end

  def total_deals_count
    deals.count
  end

  def total_deals_sum
    deals.sum(:total_sum)
  end

  def total_deals_average_sum
    deals.average(:total_sum)
  end

  def returned_deals_count
    deals.where(status: :returned).count
  end

  def returned_deals_sum
    deals.where(status: :returned).sum(:total_sum)
  end

  # Возвращает дату первой покупки -
  # дату первой оплаты сделки
  def first_purchase_date
    deals
      .where.not(payed_at: nil)
      .order(payed_at: :asc)
      .first
      .try(:payed_at)
  end

  # Возвращает дату последней покупки -
  # дату последней оплаты сделки
  def last_purchase_date
    deals
      .where.not(payed_at: nil)
      .order(payed_at: :desc)
      .first
      .try(:payed_at)
  end

  def sales_dynamic(field)
    unless [:sum, :quantity].include?(field.to_sym)
      raise ArgumentError,
            "`field` argument should be :sum or :quantity, got #{field}"
    end

    months_index = index_months(24.months)
    result = Array.new(24) { |index| [index + 1, 0] }.to_h

    deals
      .joins(:deal_products)
      .where('payed_at >= ?', now - 2.years)
      .where.not(status: :returned)
      .select(
        'MONTH(payed_at) month, YEAR(payed_at) year, '\
        'SUM(total_sum) sum, SUM(count) quantity'
      )
      .group('MONTH(payed_at)')
      .each_with_object(result) do |row, result_hash|
        row_date = Date.new(row.year, row.month, 1)
        result_hash[months_index[row_date]] = row.send(field)
      end
      .to_a
  end

  private

  def deals
    Deal
      .joins(:client)
      .where(
        company_id: @supplier.legal_entity_id,
        clients: { legal_entity_id: @client_company_id }
      )
  end

  def current_month_paid_deals
    deals.paid_in_current_month
  end
end
