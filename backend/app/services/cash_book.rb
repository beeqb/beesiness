# Сервис для различных операций над кассовой книгой
class CashBook
  include Wisper::Publisher

  def initialize(company_id)
    @company_id = company_id
    @company = Company.find(@company_id)
  end

  def recalculate_balance
    all_records =
      CashBookRecord
      .where(company_id: @company_id)
      .order(created_at: :asc)

    investors_ids = User.with_role(:employee_investor, @company).pluck(:id)

    running_total = calculate_running_total(all_records.not_personal)
    CashBookRecord.update(running_total.keys, running_total.values)

    investors_ids.each do |investor_id|
      running_total = calculate_running_total(
         all_records.including_personal_for(investor_id)
      )

      running_total.each do |record_id, values|
        personal_balance = PersonalCashBookBalance.find_or_initialize_by(
          user_id: investor_id,
          record_id: record_id
        )
        personal_balance.balance = values[:balance]
        personal_balance.save
      end
    end

    return
  end

  def self.invert_category(category)
    category = category.to_sym

    return :outcome if category == :income
    return :income if category == :outcome
  end

  def self.paid_deal_statuses
    %w(payed returned)
  end

  def self.outcome_deal_statuses
    %w(returned)
  end

  private

  def calculate_running_total(records, balance_attribute = :balance)
    tuples = records.pluck_hash(:id, :amount, balance_attribute)

    tuples.each_with_index do |record, index|
      if index == 0
        record[balance_attribute] = record[:amount]
      else
        record[balance_attribute] =
          record[:amount] + tuples[index - 1][balance_attribute]
      end

      broadcast(:cash_gap, @company) if record[balance_attribute] < 0

      record.delete(:amount)
    end

    indexed_tuples = tuples.index_by { |record| record.delete(:id) }
  end
end
