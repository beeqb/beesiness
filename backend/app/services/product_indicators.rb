# сервис для расчета показателей по товарам
class ProductIndicators
  include DateTimeHelper

  def initialize(product)
    @product = product
    @company_id = product.company_id
  end

  # Возвращает суммарный доход от продаж товара за текущий месяц
  def income
    paid_deal_products.pluck('SUM(price * count)').first || 0
  end

  # Возвращает доход от продаж товара за текущий месяц,
  # разбитый по дням месяца
  def income_by_days
    days_number = current_month_range.end.day

    result = Array.new(days_number, 0)

    paid_deal_products
      .select('DAY(deal_products.created_at) day, SUM(price * count) sum')
      .group('DAY(deal_products.created_at)')
      .each_with_object(result) do |row, result_array|
        result_array[row.day.to_i - 1] = row.sum
      end
  end

  def current_month_plan
    plan =
        SalesPlanProduct
            .joins(:sales_plan)
            .find_by(
                product_id: @product.id,
                sales_plans: { month: now.month, year: now.year }
            )

    return nil unless plan

    plan_sum = plan.quantity * @product.price
    sold = paid_deal_products.pluck('SUM(price * count)').first
    implementation = (sold || 0) / plan_sum * 100

    {
        sum: plan_sum,
        quantity: plan.quantity,
        implementation: implementation,
    }
  end

  def popularity
    deals_with_product_count = deals_with_product.uniq.count
    all_deals_paid_count = paid_deals.count

    return nil if all_deals_paid_count.zero?

    deals_with_product_count.fdiv(all_deals_paid_count) * 100
  end

  def clients_purchased_percent
    clients_count = deals_with_product.select(:client_id).uniq.count
    #Rails.logger.debug "[clients_purchased_percent] clients_count:" + clients_count.to_yaml()
    res = clients_count.fdiv(Client.where(company_id: @company_id).count) * 100
    #Rails.logger.debug "[clients_purchased_percent] res:" + res.to_yaml()
    if res.nan? then
      res = 0
    end
    res
  end

  def sales_percent
    all_sales = paid_deals.sum(:total_sum)
    product_sales = paid_deal_products.pluck('SUM(price * count)').first || 0

    return nil if all_sales.zero?

    product_sales.fdiv(all_sales) * 100
  end

  def income_percent
    all_sales = paid_deals.sum(:total_sum)
    product_sales =
      paid_deal_products
      .joins(:product)
      .pluck('SUM((deal_products.price - products.first_cost) * count)')
      .first || 0

    return nil if all_sales.zero?

    product_sales.fdiv(all_sales) * 100
  end

  private

  def paid_deals
    Deal.paid_in_current_month.where(company_id: @company_id)
  end

  def deals_with_product
    Deal
      .paid_in_current_month
      .joins(:deal_products)
      .where(deal_products: { product: @product })
  end

  def paid_deal_products
    DealProduct
      .joins(:deal)
      .merge(Deal.paid_in_current_month)
      .where(product_id: @product.id)
  end
end
