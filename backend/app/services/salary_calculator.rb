class SalaryCalculator
  def initialize(company, user)
    @company = company
    @user = user
  end

  def annual_salary_expense
    @company.employees.reduce(0) do |result, employee|
      result += employee_annual_salary(employee)
    end
  end

  def employee_annual_salary(employee)
    coefficient = case employee.salary_period
                  when 'hour'
                    weeks_count_in_year * 40
                  when 'day'
                    days_count_in_year
                  when 'week'
                    weeks_count_in_year
                  when 'month'
                    12
                  when 'year'
                    1
                  else
                    0
                  end

    coefficient * (employee.salary || 0)
  end

  def days_count_in_year
    Date.new(Date.today.year, 12, 31).yday
  end

  def weeks_count_in_year
    year = Date.today.year
    # all years starting with Thursday, and leap years starting with Wednesday have 53 weeks
    # http://en.wikipedia.org/wiki/ISO_week_date#Last_week
    date = Date.new(year, 1, 1)
    if date.wday == 4 || date.leap? and date.wday == 3
      53
    else
      52
    end
  end
end
