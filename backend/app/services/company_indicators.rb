#####################################################
# Сервис для расчета различных показателей компании #
#####################################################
class CompanyIndicators
  include DateTimeHelper

  def initialize(company, user)
    @company = company
    @user = user
  end

  concerning :CRM do
    def deals_count
      deals.count
    end

    def paid_deals_count
      deals.in_status(:payed).count
    end

    def clients_count
      @company.clients_for(@user).count
    end

    def leads_count
      @company.leads_for(@user).count
    end

    # Возвращает массив, длина которого - кол-во дней в текущем месяце,
    # а каждый элемент - количество клиентов, созданных в соответствующий
    # индексу день
    #
    # Пример:
    # Пусть сейчас январь 2017 года. В месяце 31 день.
    # Пусть 5-го было создано 7 клиентов, 10-го - 11, 15-го - 3
    #
    # Тогда метод вернет
    # [0, 0, 0, 0, 7, 0, 0, 0, 0, 11, 0, 0, 0, 0, 3, 0, 0, ... , 0]
    def created_clients_count_by_days
      days_number = current_month_range.end.day

      result = Array.new(days_number, 0)

      @company
        .clients_for(@user)
        .where(created_at: current_month_range)
        .select('DAY(created_at) day, COUNT(*) count')
        .group('DAY(created_at)')
        .each_with_object(result) do |row, result_array|
          result_array[row.day.to_i - 1] = row.count
        end
    end

    # Возвращает массив, длина которого - кол-во дней в текущем месяце,
    # а каждый элемент - доход компании в соответствующий индексу день.
    # Доходом считается сумма записей c типом "Доход" из кассовой книги.
    #
    # Пример:
    # Пусть сейчас январь 2017 года. В месяце 31 день.
    # Пусть 3-го был доход 1000, 5-го - 1150, 10-го - 3690
    #
    # Тогда метод вернет
    # [0, 0, 1000, 0, 1150, 0, 0, 0, 0, 3690, 0, 0, ... , 0]
    def actual_income_by_days(only_deals: false)
      days_number = current_month_range.end.day

      result = Array.new(days_number, 0)

      data_source = current_month_cash_book_records.income
      data_source = data_source.where.not(deal_id: nil) if only_deals

      data_source
        .select('DAY(created_at) day, SUM(amount) sum')
        .group('DAY(created_at)')
        .each_with_object(result) do |row, result_array|
          result_array[row.day.to_i - 1] = row.sum
        end
    end

    def actual_budget_stats_by_days(min_date, max_date)
      max_date ||= Time.zone.now.to_s
      min_date ||= Time.parse('1970-01-01 00:00:00').to_s
      min = Time.parse min_date
      max = Time.parse max_date

      records = cash_book_records_for_user_in_company.where('created_at BETWEEN ? AND ?', min.to_s, max.to_s)

      if !records.first.nil? then
        min = records.first.created_at
      end

      if max.to_i > Time.zone.now.to_i
        max = Time.zone.now
      end

      days_number = (max.to_i - min.to_i) / 1.day

      res = Array.new(days_number, nil)
      res.each_with_index {|item, index|
        res[index] = {
            income: 0,
            outcome: 0,
            revenue: 0,
            created_at: (min + index.days).to_s
        }
      }

      records
        .select("created_at, DAY(created_at) day, SUM(IF(category = 'income', amount, 0)) as income_sum, SUM(IF(category = 'outcome', amount, 0)) as outcome_sum")
        .group("DAY(created_at)")
        .each do |row|
          item = res[(row.created_at.to_i - min.to_i) / 1.day]
          item[:income] = row.income_sum.abs
          item[:outcome] = row.outcome_sum.abs
          item[:revenue] = row.income_sum.abs - row.outcome_sum.abs
          item[:created_at] = row.created_at.to_s
        end

      res
    end
  end

  concerning :BreakEvenAnalysis do
    def fixed_expenses
      salary_calculator = SalaryCalculator.new(@company, @user)
      salary_calculator.annual_salary_expense / 12
    end

    def product_average_price
      @company
        .products
        .joins(:category)
        .where(product_categories: { has_part_in_average_price: true })
        .distinct
        .average(:price)
    end

    def products_increase
      @company
        .products
        .where(created_at: current_month_range)
        .count
    end

    def break_even_point
      (fixed_expenses / product_average_price).ceil
    end

    def cost_per_customer
      previous_month = 1.month.ago.beginning_of_month..1.month.ago.end_of_month

      deals_ids =
        @company
        .deals_for(@user)
        .where(created_at: previous_month)
        .pluck(:id)

      products_sold = DealProduct.where(deal_id: deals_ids).sum(:count)

      fixed_expenses / products_sold
    end
  end

  concerning :ActualBudget do
    def plan_percent_month
      plan = sales_plans.find_by(month: now.month, year: now.year).try(:sum)

      return 0 unless plan

      ((current_month_income / plan) * 100).floor
    end

    def plan_percent_year
      plan = annual_plan

      return 0 if plan.zero?

      year_income = deals.paid_in(current_month_range).sum(:total_sum)

      ((year_income / plan) * 100).floor
    end

    def plan_percent_quarter
      plan =
        sales_plans
        .where(
          year: now.year,
          month: now.beginning_of_quarter.month..now.end_of_quarter.month
        )
        .sum(:sum)

      return 0 if plan.zero?

      quarter_income = deals.paid_in(current_quarter_range).sum(:total_sum)

      ((quarter_income / plan) * 100).floor
    end

    def plan_percent_today
      month_plan =
        sales_plans
        .find_by(year: now.year, month: now.month)
        .try(:sum)

      return 0 if month_plan.nil? || month_plan.zero?

      day_plan = month_plan.fdiv(now.end_of_month.day)

      today_income = deals.paid_in(today_range).sum(:total_sum)

      result = ((today_income / day_plan) * 100).floor

      result > 100 ? 100 : result
    end
  end

  def monthly_budget
    records =
      cash_book_records
      .select('MONTH(created_at) as month, category, SUM(amount) as sum')
      .where(created_at: current_year_range)
      .group('MONTH(created_at), category')

    result = records.each_with_object({}) do |record, result|
      result[record.month] ||= {}
      result[record.month][record.category.to_sym] = record.sum.abs
    end

    result.each_key do |month|
      data = result[month]

      result[month][:total_income] =
        data.fetch(:income, 0) + data.fetch(:loan, 0)
      result[month][:total_outcome] =
        data.fetch(:outcome, 0) + data.fetch(:loan_return, 0)
      result[month][:actual_income] =
        data[:total_income] - data[:total_outcome]
    end

    result
  end

  concerning :CashBook do
    def avg_month_income
      current_month_cash_book_records.income.average(:amount)
    end

    def avg_month_outcome
      current_month_cash_book_records.outcome.average(:amount).try(:abs)
    end

    def avg_monthly_income
      ordered_by_creation_date = cash_book_records_for_user_in_company.order('created_at asc')

      return ordered_by_creation_date.income.sum(:amount) /
          months_in_range(ordered_by_creation_date.first.created_at..ordered_by_creation_date.last.created_at).length
    end

    def avg_monthly_outcome
      ordered_by_creation_date = cash_book_records_for_user_in_company.order('created_at asc')

      return ordered_by_creation_date.outcome.sum(:amount) /
          months_in_range(ordered_by_creation_date.first.created_at..ordered_by_creation_date.last.created_at).length
    end
  end

  def total_income
    cash_book_records_for_user_in_company.income.sum(:amount)
  end

  def total_outcome
    cash_book_records_for_user_in_company.outcome.sum(:amount)
  end

  def current_month_income
    @company.deals_for(@user).paid_in_current_month.sum(:total_sum)
  end

  def current_month_outcome
    current_month_cash_book_records.outcome.sum(:amount).abs
  end

  def current_month_loans
    current_month_cash_book_records.loan.sum(:amount)
  end

  def current_month_loan_returns
    current_month_cash_book_records.loan_return.sum(:amount).abs
  end

  def annual_plan_implementation
    categories_amounts = Hash.new(0)
    plan = annual_plan

    cash_book_records.income.find_each(batch_size: 100) do |record|
      categories_amounts[record.cash_flow_budget_category_id] += record.amount
    end

    {
      categories_amounts: categories_amounts,
      residue: plan - categories_amounts.values.sum,
    }
  end

  def annual_plan
    sales_plans.where(year: now.year).sum(:sum)
  end

  def to_hash(*wanted_parameters)
    wanted_parameters.each_with_object({}) do |param, obj|
      obj[param] = send(param)
    end
  end

  private

  def deals
    @company.deals_for(@user)
  end

  def cash_book_records
    CashBookRecord.where(company_id: @company.id)
  end

  def sales_plans
    SalesPlan.where(company_id: @company.id)
  end

  def cash_book_records_for_user_in_company
    if @user.has_role?(:employee_investor, @company)
      cash_book_records.including_personal_for(@user.id)
    else
      cash_book_records.not_personal
    end
  end

  def current_month_cash_book_records
    cash_book_records_for_user_in_company
      .where(created_at: current_month_range)
  end

  def selected_month_from_now_cash_book_records(month_num_from_now)
    cash_book_records_for_user_in_company.where(created_at: slected_month_range_from_now(month_num_from_now))
  end
end
