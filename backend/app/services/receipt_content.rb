class ReceiptContent
  delegate :name, :inn, :address, :vat, to: :@company, prefix: :company
  delegate :fullname, to: :@manager, prefix: :manager
  delegate :cashbox_name, to: :@manager
  delegate :discount_sum, :vat_sum, to: :@deal

  HEADER = <<~TEXT
    <div style="text-align:center">%{company_name}</div>
    <div>Оператор <span style="float: right">%{manager_fullname}</span></div>
    <div>ИНН <span style="float: right">%{company_inn}</span></div>
    <div>%{payed_at} <span style="float: right">Касса %{cashbox_name}</span></div>
  TEXT

  ITEM = <<~TEXT
    <div>%{product_name} <span style="float: right">%{product_quantity} x %{product_price}</span></div>
  TEXT

  FOOTER = <<~TEXT
    <div><strong>ИТОГ:</strong> <span style="float: right">%{total_sum}</span></div>
    <div style="text-align:center">Всего доброго!</div>
  TEXT

  def initialize(receipt, total_sum = nil, deal_products = nil)
    @receipt = receipt
    @deal = receipt.deal
    @company = @deal.company
    @manager = @deal.manager
    @deal_products = deal_products
    @total_sum = total_sum
  end

  def html(templates = {})
    templates =
      ActiveSupport::HashWithIndifferentAccess.new(
        header: HEADER,
        item: ITEM,
        footer: FOOTER
    ).merge(@company.receipt_template || {}).merge(templates)

    result = '<div style="margin: 0 auto; width: 5cm; ' \
              'font-size: 10pt; overflow: hidden">'
    result += substitute_placeholders(templates[:header])

    deal_products.each do |deal_product|
      result += substitute_placeholders(templates[:item], deal_product)
    end

    result += substitute_placeholders(templates[:footer])
    result += '</div>'

    result
  end

  private

  def substitute_placeholders(template, *params)
    template.gsub(/%\{(\w+)\}/) { |match| send($1, *params) }
  end

  def receipt_kind
    return unless @receipt.kind

    { sell: 'Приход', return: 'Возврат прихода' }[@receipt.kind.to_sym]
  end

  def total_sum
    @total_sum || @deal.total_sum
  end

  def deal_products
    if @deal_products.nil?
      @deal.deal_products
    else
      @deal_products
    end
  end

  def product_name(deal_product)
    deal_product.product.name
  end

  def product_price(deal_product)
    deal_product.price
  end

  def product_quantity(deal_product)
    deal_product.count
  end

  def paid_amount
    if @deal.payment_option == 'cash'
      @deal.paid_cash_amount
    else
      @deal.total_sum
    end
  end

  def change
    if @deal.payment_option == 'cash'
      @deal.paid_cash_amount - @deal.total_sum
    else
      0
    end
  end

  def payed_at
    # Time.now используется для превью шаблона чека
    (@receipt.created_at || Time.now).strftime('%d.%m.%Y %H:%M')
  end

  def payment_option
    options = Deal.references[:payment_options]
    option = options.detect { |option| option[:id] == @deal.payment_option }

    option[:name] if option
  end
end
