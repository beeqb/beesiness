#############################################
# Класс для проведения RFM-анализа клиентов #
#############################################
class RFMAnalysis
  def initialize(client)
    @client = client
  end

  def rf_status
    recency_weight = category_weights[recency] 
    frequency_weight = category_weights[frequency] 

    average_weight = (recency_weight + frequency_weight) / 2

    category_weights.key(average_weight.round)
  end

  def rm_status
    recency_weight = category_weights[recency] 
    monetary_weight = category_weights[monetary] 

    average_weight = (recency_weight + monetary_weight) / 2

    category_weights.key(average_weight.round)
  end

  def recency
    return @recency if defined? @recency

    last_deal = @client.deals.order(created_at: :desc).last
    recency = if last_deal
                (Date.today - last_deal.created_at.to_date).to_i
              else
                0
              end

    @recency = classify(recency, all_clients_recency, true)
  end

  def frequency
    classify(@client.deals.count, all_clients_frequency)
  end

  def monetary
    classify(@client.deals.sum(:total_sum), all_clients_frequency)
  end

  private

  def db
    ActiveRecord::Base.connection
  end

  def percentile(values, percentile)
    return 0 if values.blank? || values.size < 2

    k = (percentile * (values.length - 1) + 1).floor - 1
    f = (percentile * (values.length - 1) + 1).modulo(1)

    values[k] + (f * (values[k + 1] - values[k]))
  end

  def classify(value, data, reverse = false)
    percentiles = [25, 50, 75, 95].each_with_object({}) do |percent, result|
      result[percent] = percentile(data, percent.fdiv(100))
    end

    if reverse
      return case value.to_f
        when 0..percentiles[25] then :big_whale
        when percentiles[25].to_f.next_float..percentiles[50] then :whale
        when percentiles[50].to_f.next_float..percentiles[75] then :big_dolphin
        when percentiles[75].to_f.next_float...percentiles[95] then :dolphin
        when percentiles[95]..Float::INFINITY then :crucian
      end
    end

    case value.to_f
      when 0..percentiles[25] then :crucian
      when percentiles[25].next_float..percentiles[50] then :dolphin
      when percentiles[50].next_float..percentiles[75] then :big_dolphin
      when percentiles[75].next_float...percentiles[95] then :whale
      when percentiles[95]..Float::INFINITY then :big_whale
    end
  end

  def category_weights
    {
      big_whale: 5,
      whale: 4,
      big_dolphin: 3,
      dolphin: 2,
      crucian: 1,
    }
  end

  def all_clients_recency
    db.execute(
      'SELECT DATEDIFF(NOW(), d.created_at) recency
      FROM clients c join (
        SELECT MAX(id) max_id,created_at, client_id
        FROM deals
        WHERE deleted_at IS NULL
        GROUP BY client_id
      ) d on d.client_id = c.id
      ORDER BY recency ASC'
    ).to_a.flatten
  end

  def all_clients_frequency
    db.execute(
      'SELECT COUNT(*) frequency FROM deals
      WHERE deleted_at IS null
      GROUP BY client_id
      ORDER BY frequency ASC'
    ).to_a.flatten
  end

  def all_clients_monetary
    db.execute(
      'SELECT SUM(total_sum) monetary FROM deals
      WHERE deleted_at IS null
      GROUP BY client_id
      ORDER BY monetary ASC'
    ).to_a.flatten
  end
end
