module Search
  class Result < ActiveModelSerializers::Model
    attr_accessor :clients, :leads, :products, :deals, :employees, :suppliers

    def serializer_class
      SearchResultSerializer
    end
  end
end
