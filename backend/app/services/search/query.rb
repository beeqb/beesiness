module Search
  # Service object for search action
  class Query
    def initialize(token, company_id, sections = nil)
      @token = token.strip
      @company_id = company_id

      @sections = if sections
                    Array.wrap(sections)
                  else
                    [:clients, :leads, :products, :employees, :deals]
                  end
    end

    def perform
      Result.new(@sections.each_with_object({}) do |section, results|
        results[section] = send('search_' + section.to_s)
      end)
    end

    def references
      company_serializer = CompanySerializer.new(Company.find(@company_id))

      {
        client_statuses: company_serializer.client_statuses,
        country_phone_codes: Company.country_phone_codes,
        sources: Client.sources,
        lead_statuses: company_serializer.lead_statuses,
        user_activities: Employee.user_activities,
        departments: Department.all,
        product_activities: Product.activities,
        deal_statuses: company_serializer.deal_statuses,
      }
    end

    private

    def search_clients
      Client
        .where(
          'name LIKE :token OR ' \
          'surname LIKE :token OR ' \
          'legal_entity_name LIKE :token',
          db_query_params
        )
        .where(company_id: @company_id)
    end

    def search_leads
      Lead
        .where(
          'name LIKE :token OR ' \
          'surname LIKE :token OR ' \
          'legal_entity_name LIKE :token',
          db_query_params
        )
        .where(company_id: @company_id)
    end

    def search_suppliers
      Supplier
        .where(
          'name LIKE :token OR ' \
          'surname LIKE :token OR ' \
          'legal_entity_name LIKE :token',
          db_query_params
        )
        .where(company_id: @company_id)
    end

    def search_products
      Product
        .where(
          'name LIKE :token',
          db_query_params
        )
        .where(company_id: @company_id)
    end

    def search_employees
      Employee
        .joins(:user)
        .where(
          'users.name LIKE :token OR users.surname LIKE :token',
          db_query_params
        )
        .where(company_id: @company_id)
    end

    def search_deals
      Deal
        .where(
          'name LIKE :token',
          db_query_params
        )
        .where(company_id: @company_id)
    end

    def db_query_params
      { token: "%#{@token.downcase}%" }
    end
  end
end
