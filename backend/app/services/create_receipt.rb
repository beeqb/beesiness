class CreateReceipt
  def self.call(deal, kind = nil, deal_products = nil, amount = nil)
    new(deal, kind, deal_products, amount).perform
  end

  def initialize(deal, kind, deal_products, amount)
    @amount = amount
    @deal = deal
    @kind = kind

    @deal_products = deal_products.try(:map) do |deal_product|
      {
        count: deal_product.count,
        product: { name: deal_product.product.name },
        price: deal_product.price
      }
    end
    @receipt = Receipt.new(deal: deal)
  end

  def perform
    @receipt.kind = kind
    @receipt.amount = amount
    @receipt.deal_products = @deal_products

    @receipt.save!
  end

  private

  def amount
    return @amount if @amount

    if @deal_products.present?
      @deal_products.inject(0) { |acc, dp| acc + dp[:price] * dp[:count] }
    else
      @deal.total_sum
    end
  end

  def kind
    return @kind if @kind

    if @deal.status == 'returned'
      :return
    else
      :sell
    end
  end
end
