require 'matrix'

class Regression
  def initialize(x, y, degree)
    @x = x
    @y = y
    @degree = degree
  end

  def regress
    x_data = @x.map { |xi| (0..@degree).map { |pow| (xi**pow).to_f } }

    mx = Matrix[*x_data]
    my = Matrix.column_vector(@y)

    @polynom_coefficients = ((mx.t * mx).inv * mx.t * my).transpose.to_a[0]
  end

  def determination_coeff
    y_approximated = approximation

    numerator = @y.each_with_index.inject(0) do |memo, (value, index)|
      memo + (value - y_approximated[index]) ** 2
    end

    avg = y.inject(&:+).to_f / y.size

    denom = y.inject(0) { |memo, value| memo + (value - avg) ** 2 }

    1 - numerator / denom
  end

  def approximation
    @x.map do |value|
      @polynom_coefficients.each_with_index.inject(0) do |memo, (coeff, index)|
        memo + coeff * value ** (@degree - index)
      end
    end
  end
end
