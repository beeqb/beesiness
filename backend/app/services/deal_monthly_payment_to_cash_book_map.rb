####################################################################################
# Сервис для преобразования отдельного платежа по сделке в запись в кассовой книге #
####################################################################################
class DealMonthlyPaymentToCashBookMap
  # payment [DealMonthlyPayment]
  def self.call(payment, category = 'income')
    return nil if payment.amount == 0

    deal = payment.deal

    cash_book_record = CashBookRecord.new(
      company: deal.company,
      creator_id: deal.manager_id,
      amount: payment.amount,
      currency: deal.company.currency,
      category: category,
      cash_flow_budget_category: deal.cash_flow_budget_category,
      bill_kind: deal_payment_option_to_bill_kind(deal.payment_option),
      deal_id: deal.id
    )

    cash_book_record
  end

  private_class_method

  def self.deal_payment_option_to_bill_kind(payment_option)
    return 'cash' if payment_option == 'cash'
    'cashless'
  end
end
