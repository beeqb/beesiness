class EmployeeActivity
  include DateTimeHelper

  def initialize(employee)
    @employee = employee
  end

  def data
    deals_sum = month_deals.sum(:total_sum)
    {
      leads_count: leads_count,
      clients_count: clients_count,
      deals_count: month_deals.count,
      deals_sum: deals_sum,
      deals_dynamics: deals_dynamics(deals_sum),
      average_deals_sum: deals.average(:total_sum),
      month_average_deals_sum: month_deals.average(:total_sum) || 0,
      last_deal_date: deals.maximum(:created_at),
      percentage_of_the_plan: percentage_of_the_plan(deals_sum),
    }
  end

  # Возвращает доход, принесенный сотрудником за текущий месяц
  def income
    deals.paid_in_current_month.sum(:total_sum)
  end

  # Возвращает доход, принесенный сотрудником за текущий месяц,
  # разбитый по дням.
  #
  # Формат: [[день 1, сумма], [день 2, сумма], ... ]
  def income_by_days
    days_number = current_month_range.end.day

    result = Array.new(days_number, 0)

    deals
      .paid_in_current_month
      .select('DAY(payed_at) day, SUM(total_sum) sum')
      .group('DAY(payed_at)')
      .each_with_object(result) do |row, result_array|
        result_array[row.day.to_i - 1] = row.sum
      end
  end

  private

  def id
    @employee.id
  end

  def deals
    Deal.where(manager_id: id)
  end

  def leads_count
    Lead.where(manager_id: id, created_at: current_month_range).count
  end

  def clients_count
    Client.where(manager_id: id, created_at: current_month_range).count
  end

  def month_deals
    deals.where(created_at: current_month_range)
  end

  def prev_month_deals
    deals.where(created_at: prev_month_range)
  end

  def deals_dynamics(deals_sum)
    prev_deals_sum = prev_month_deals.sum(:total_sum)

    if prev_deals_sum > 0
      (((deals_sum / prev_deals_sum) * 100) - 100).round(2)
    else
      0
    end
  end

  def month_plan
    SalesPlan
      .select('sales_plans.sum * sales_plan_employees.part / 100 AS plan')
      .joins(:sales_plan_employees)
      .find_by(
        month: now.month,
        year: now.year,
        sales_plan_employees: { employee_id: id }
      )
      .try(:plan)
  end

  def percentage_of_the_plan(deals_sum)
    if month_plan
      (deals_sum / month_plan * 100).round(2)
    else
      0
    end
  end
end
