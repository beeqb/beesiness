################################################################
# сервис для расчета прогноза продаж для отдельного сотрудника #
################################################################
class SalesForecast
  include DateTimeHelper

  def initialize(employee)
    @employee = employee
  end

  def data
    {
      forecast: forecast,
      income: income,
      average_receipt: average_receipt,
      deals_count: deals_count,
    }
  end

  def forecast
    months_indexes = index_months(24.months, :forward)

    period
      .each_with_object([]) do |date, result|
        result << [
          months_indexes[date],
          (last_year_average_sales[date.month] || 0) +
          (monthly_payments[date] || 0),
        ]
      end
  end

  def income
    last_year_data
      .find_by('MONTH(payed_at) = ?', today.month)
      .total_sum || 0
  end

  def average_receipt
    last_year_data
      .find_by('MONTH(payed_at) = ?', today.month)
      .avg_sum || 0
  end

  def deals_count
    last_year_data
      .find_by('MONTH(payed_at) = ?', today.month)
      .deals_count || 0
  end

  private

  def monthly_payments
    return @monthly_payments if defined? @monthly_payments

    @monthly_payments =
      deals
      .joins(:deal_monthly_payments)
      .select('date, amount')
      .where('date > ?', today.change(day: 1))
      .group(:date)
      .map { |row| [row.date.change(day: 1), row.amount] }
      .to_h
  end

  def deals
    Deal.where(manager: @employee)
  end

  def period
    months_in_range(now..(now + 24.months))
  end

  def last_year_data
    return @last_year_data if defined? @last_year_data

    @last_year_data =
      deals
      .where('YEAR(payed_at) = ?', now.year - 1)
      .select([
                'SUM(total_sum) total_sum',
                'AVG(total_sum) avg_sum',
                'MONTH(payed_at) month',
                'COUNT(*) deals_count',
              ])
  end

  def last_year_average_sales
    return @last_year_average_sales if defined? @last_year_average_sales

    @last_year_average_sales =
      last_year_data
      .group('MONTH(payed_at)')
      .map { |row| [row.month, row.avg_sum] }
      .to_h
  end
end
