#########################################
# Сервис для уменьшения кол-ва товаров, #
# участвовавших в сделке                #
#########################################
class AdjustProductsCountsInDeal
  def initialize(deal)
    @deal_products = deal.deal_products
  end

  def self.call(deal, operation)
    new(deal).perform(operation)
  end

  def perform(operation)
    message = case operation.to_sym
              when :increase then :+
              when :decrease then :-
              else
                raise ArgumentError 'Invalid operation `#{operation}` provided'
              end

    @deal_products.each do |deal_product|
      product = deal_product.product
      next if product.quantity.zero?

      product.update(
        quantity: product.quantity.send(message, deal_product.count)
      )
    end
  end
end
