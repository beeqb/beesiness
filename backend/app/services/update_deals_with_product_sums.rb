################################################
# Сервис для перерасчета стоимостей сделок при #
# изменении цены на товар                      #
################################################
class UpdateDealsWithProductSums
  def self.call(product)
    new(product).perform
  end

  def initialize(product)
    @product = product
  end

  def perform
    Deal.transaction do
      update_deal_products
      update_deals
    end
  end

  def update_deal_products
    DealProduct
      .joins(:deal)
      .where(product: @product)
      .where
      .not(deals: { status: ignored_deal_statuses })
      .update(price: @product.price)
  end

  def update_deals
    Deal
      .includes(:deal_products)
      .where(deal_products: { product: @product })
      .where
      .not(status: ignored_deal_statuses)
      .each(&:update_sums!)
  end

  private

  def ignored_deal_statuses
    %w(won payed lost)
  end
end
