# Сервис для создания поставщика из компании
class CompanyToSupplierMap
  # Возвращает инициализированного поставщика,
  # но не сохраненного в БД
  #
  # params:
  #   company: Company
  # returns Supplier
  def self.call(company)
    supplier = Supplier.new(
      legal_entity_id: company.id,
      legal_entity_email: company.email,
      legal_entity_name: company.name,
      legal_form: 'legal_entity',
      address: company.address,
      latitude: company.latitude,
      longitude: company.longitude,
      site: company.site,
      work_phone: company.phone,
      work_phone_code_id: company.phone_code_id
    )

    contact = company.director || company.creator
    supplier.assign_attributes(
      name: contact.name,
      surname: contact.surname,
      email: contact.email
    )

    supplier
  end
end
