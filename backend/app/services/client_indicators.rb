####################################################
# Сервис для расчета различных показателей клиента #
####################################################
class ClientIndicators
  include DateTimeHelper

  def initialize(client)
    @client = client
  end

  # Возвращает доход от клиента за текущий месяц.
  #
  # Доход рассчитывается, как сумма поля :total_sum всех оплaченных
  # сделок с клиентом + сумма :amount всех взносов на депозит
  def income
    paid_deals.sum(:total_sum) + current_month_deposit_fees.sum(:amount)
  end

  def income_by_days
    days_number = current_month_range.end.day

    result = Array.new(days_number, 0)

    result =
      paid_deals
      .select('DAY(payed_at) day, SUM(total_sum) sum')
      .group('DAY(payed_at)')
      .each_with_object(result) do |row, result_array|
        result_array[row.day.to_i - 1] = row.sum
      end

    current_month_deposit_fees.each_with_object(result) do |fee, result_array|
      result_array[fee.created_at.day - 1] += fee.amount
    end
  end

  # Возвращает массив с данными об объемах продаж
  # за последние два года.
  #
  # Параметр field задает интересующий показатель:
  # :sum - объем продаж в деньгах
  # :quantity - объем продаж в количестве товара
  #
  # Результирующий массив выглядит так:
  # [[1, 100], [2, 300], ..., [24, 422]]
  # Первый элемент каждого вложенного массива - это номер месяца
  # Второй - объем продаж
  def sales_dynamic(field)
    unless [:sum, :quantity].include?(field.to_sym)
      raise ArgumentError,
            "`field` argument should be :sum or :quantity, got #{field}"
    end

    months_index = index_months(24.months)
    result = Array.new(24) { |index| [index + 1, 0] }.to_h

    @client
      .deals
      .joins(:deal_products)
      .where('payed_at >= ?', now - 2.years)
      .where.not(status: :returned)
      .select(
        'MONTH(payed_at) month, YEAR(payed_at) year, '\
        'SUM(total_sum) sum, SUM(count) quantity'
      )
      .group('MONTH(payed_at)')
      .each_with_object(result) do |row, result_hash|
        row_date = Date.new(row.year, row.month, 1)
        result_hash[months_index[row_date]] = row.send(field)
      end
      .to_a
  end

  def potential_deals_count
    @client.deals.in_progress.count
  end

  def potential_deals_sum
    @client.deals.in_progress.sum(:total_sum)
  end

  def won_deals_count
    @client.deals.won.count
  end

  def won_deals_sum
    @client.deals.won.sum(:total_sum)
  end

  def lost_deals_count
    @client.deals.lost.count
  end

  def lost_deals_sum
    @client.deals.lost.sum(:total_sum)
  end

  # Возвращает дату первой покупки -
  # дату первой оплаты сделки
  def first_purchase_date
    @client
      .deals
      .where.not(payed_at: nil)
      .order(payed_at: :asc)
      .first
      .try(:payed_at)
  end

  # Возвращает дату последней покупки -
  # дату последней оплаты сделки
  def last_purchase_date
    @client
      .deals
      .where.not(payed_at: nil)
      .order(payed_at: :desc)
      .first
      .try(:payed_at)
  end

  private

  def paid_deals
    @client.deals.paid_in_current_month
  end

  def current_month_deposit_fees
    @client.deposit_fees.where(created_at: current_month_range)
  end
end
