require 'rails_helper'

RSpec.describe ClientIndicators, type: :service do
  let(:client) { create(:client) }
  subject { described_class.new(client) }

  describe '#income' do
    it 'returns sum of all deals in this month' do
      deal_sums = [1500, 4000, 2600]
      deal_sums.each do |deal_sum|
        create(
          :deal,
          :payed,
          payed_at: Time.now,
          client: client,
          total_sum: deal_sum,
        )
      end

      expect(subject.income).to eql(deal_sums.sum)
    end
  end

  describe '#income_by_days' do
    it 'returns time series of income by days of month' do
      deal_sums = [1500, 4000, 2600]
      dates = dates_in_range(deal_sums.size, :current_month)

      expected = Array.new(Time.now.end_of_month.day, 0)
      dates.each_with_index do |date, index|
        expected[date.day - 1] = deal_sums[index]
      end

      deal_sums.each_with_index do |deal_sum, index|
        deal = create(:deal, :payed, client: client, total_sum: deal_sum)
        deal.update(payed_at: dates[index])
      end

      expect(subject.income_by_days).to eql(expected)
    end
  end
end
