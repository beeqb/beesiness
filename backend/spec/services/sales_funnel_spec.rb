require 'rails_helper'

RSpec.describe SalesFunnel, type: :service do
  let(:employee) { create(:employee) }
  subject { funnel = described_class.new(employee) }

  def create_events(subject_id, old_status, new_statuses_counts)
    new_statuses_counts.each do |new_status, count|
      create_list(
        :system_event,
        count,
        subject_type: 'Deal',
        subject_id: subject_id,
        kind: 'deal:status_changed',
        meta_data: { old_status: old_status, new_status: new_status }
      )
    end
  end

  describe '#data' do
    it 'returns aggregated data' do
      deals_ids = [
        create_list(:deal, 5, :in_progress, manager: employee),
        create_list(:deal, 2, :contract_conclusion, manager: employee),
        create(:deal, :contract_signed, manager: employee),
        create_list(:deal, 2, :won, manager: employee),
      ].flatten.pluck(:id)

      create_events(deals_ids.sample, :in_progress, payed: 3, lost: 1)
      create_events(deals_ids.sample, :contract_conclusion, payed: 1, lost: 1)
      create_events(deals_ids.sample, :contract_signed, payed: 1, lost: 2)
      create_events(deals_ids.sample, :won, payed: 3, lost: 2)

      expected = {
        in_progress: {
          to_payment: 3,
          in_status: 5,
          to_refusal: 1,
        },
        contract_conclusion: {
          to_payment: 1,
          in_status: 2,
          to_refusal: 1,
        },
        contract_signed: {
          to_payment: 1,
          in_status: 1,
          to_refusal: 2,
        },
        won: {
          to_payment: 3,
          in_status: 2,
          to_refusal: 2,
        },
        deals_total_count: 10,
      }

      expect(subject.data).to eq(expected)
    end
  end

  describe '#average_time_until_status' do
    it 'returns average until deal status in hours' do
      now = Time.now
      hours_since_now = [3 * 24, 5 * 24, 100]
      expected = hours_since_now.sum.fdiv(hours_since_now.size).round
      status = :contract_conclusion

      hours_since_now.each do |hours|
        deal = create(
          :deal,
          status,
          created_at: now,
          manager: employee
        )
        deal.update(contract_conclusion_at: now.advance(hours: hours))
      end

      expect(subject.average_time_until_status(status)).to eql(expected)
    end
  end

  describe '#average_time_until_statuses' do
    it 'aggregates average_time_until_status results for all statuses' do
      now = Time.now
      deal1 = create(
        :deal,
        :contract_conclusion,
        created_at: now,
        manager: employee,
      )
      deal1.update(contract_conclusion_at: now.advance(hours: 3 * 24))

      deal2 = create(
        :deal,
        :contract_signed,
        created_at: now,
        manager: employee,
      )
      deal2.update(contract_signed_at: now.advance(hours: 6 * 24))

      deal3 = create(
        :deal,
        :won,
        created_at: now,
        manager: employee,
      )
      deal3.update(won_at: now.advance(hours: 12 * 24))

      deal4 = create(
        :deal,
        :payed,
        created_at: now,
        manager: employee,
      )
      deal4.update(payed_at: now.advance(hours: 24 * 24))

      expected = {
        contract_conclusion: 3 * 24,
        contract_signed: 6 * 24,
        won: 12 * 24,
        payed: 24 * 24,
        in_progress: nil,
      }
      
      expect(subject.average_time_until_statuses).to eql(expected)
    end
  end
end
