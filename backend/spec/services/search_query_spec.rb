require 'rails_helper'

RSpec.describe Search::Query do
  let(:company) { create(:company) }
  subject { Search::Query.new('foo', company.id) }
  let(:search_results) { subject.perform }

  it 'searches clients' do
    allow_any_instance_of(Client).to receive(:invite_user)
    good_clients = [
      create(:client, name: 'Foorer', company: company),
      create(:client, surname: 'Gofoor', company: company),
    ]
    bad_client = create(
      :client,
      name: 'John',
      surname: 'Doe',
      company: company,
    )

    expect(search_results.clients).to match_array good_clients
  end

  it 'searches leads' do
    allow_any_instance_of(Lead).to receive(:invite_user)
    good_leads = [
      create(:lead, name: 'Foorer', company: company),
      create(:lead, surname: 'Gofoor', company: company),
    ]
    bad_lead = create(
      :lead,
      name: 'John',
      surname: 'Doe',
      company: company
    )

    expect(search_results.leads).to match_array good_leads
  end

  it 'searches products' do
    good_products = [
      create(:product, name: 'Foorer', company: company),
      create(:product, name: 'Gofoor', company: company),
    ]
    bad_product = create(
      :product,
      name: 'Shampoo',
      company: company
    )

    expect(search_results.products).to match_array good_products
  end

  it 'searches employees' do
    good_employees = [
      create(:user, name: 'Foorer'),
      create(:user, surname: 'Gofoor'),
    ].map { |user| create(:employee, user: user, company: company) }

    bad_employee = create(:employee, company: company)

    expect(search_results.employees).to match_array good_employees
  end

  it 'searches deals' do
    good_deals = [
      create(:deal, name: 'Foorer', company: company),
      create(:deal, name: 'Gofoor', company: company),
    ]
    bad_deal = create(:deal, name: 'Shampoo', company: company)

    expect(search_results.deals).to match_array good_deals
  end
end
