require 'rails_helper'

RSpec.describe CompanyIndicators::CRM, type: :service do
  let(:user) { create(:user) }
  let(:company) { create(:company, creator: user) }

  subject { CompanyIndicators.new(company, user) }

  describe '#created_clients_count_by_days' do
    it 'returns time series of created clients count by days of month' do
      expected = Array.new(Time.now.end_of_month.day, 0)
      expected[4] = 3
      expected[6] = 2

      create_list(
        :client,
        3,
        company: company,
        created_at: day_in_current_month(5),
      )
      create_list(
        :client,
        2,
        company: company,
        created_at: day_in_current_month(7),
      )

      expect(subject.created_clients_count_by_days).to eql(expected)
    end
  end
end
