require 'rails_helper'

RSpec.describe SalaryCalculator do
  let(:user) { build(:user) }
  let(:company) { build(:company) }
  subject { described_class.new(company, user) }

  describe '#annual_salary_expense' do
    let(:employees_count) { 3 }
    before :each do
      employees = build_list(:employee, employees_count)
      allow(company).to receive(:employees).and_return(employees)
    end

    it 'calls #employee_annual_salary for each employee in company' do
      expect(subject).to \
        receive(:employee_annual_salary).exactly(employees_count).times
        .and_return(100)

      subject.annual_salary_expense
    end

    it 'returns the sum of all employees annual salaries' do
      salary_stub = 100
      allow(subject).to receive(:employee_annual_salary).and_return(salary_stub)

      expect(subject.annual_salary_expense).to \
        eql(employees_count * salary_stub)
    end
  end
end
