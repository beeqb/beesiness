require 'rails_helper'

RSpec.describe SalesReport, type: :service do
  before(:all) do
    @company = create(:company)
    @user = @company.director
    @subject = described_class.new(@company, @user, 1)

    seed_for_sales_report
  end

  describe '#actual_sales_by_categories_monthly', skip: true do
    it 'works' do
      actual = @subject.actual_sales_by_categories_monthly

      expect(actual).to \
        eql(expected_actual_sales_by_categories_monthly)
    end 
  end
end
