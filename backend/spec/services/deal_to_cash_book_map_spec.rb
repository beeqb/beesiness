require 'rails_helper'

RSpec.describe DealToCashBookMap do
  let(:deal) { create(:deal) }
  before(:each) { @cash_book_record = described_class.call(deal) }

  it 'maps total_sum to amount' do
    expect(@cash_book_record.amount).to eql(deal.total_sum)
  end

  it 'maps company to company' do
    expect(@cash_book_record.company).to eql(deal.company)
  end

  it 'maps manager to creator' do
    expect(@cash_book_record.creator_id).to eql(deal.manager_id)
  end

  it 'maps company\'s currency to currency' do
    expect(@cash_book_record.currency).to eql(deal.company.currency)
  end

  it 'sets category to "income"' do
    expect(@cash_book_record.category).to eql('income')
  end

  it 'binds to deal' do
    expect(@cash_book_record.deal_id).to eql(deal.id)
  end

  context 'when payment option is "cash"' do
    let(:deal) { create(:deal, payment_option: 'cash') }
    it 'sets bill_kind to "cash"' do
      expect(@cash_book_record.bill_kind).to eql('cash')
    end
  end

  context 'when payment option is "card"' do
    let(:deal) { create(:deal, payment_option: 'card') }

    it 'sets bill_kind to "cashless"' do
      expect(@cash_book_record.bill_kind).to eql('cashless')
    end
  end

  context 'when payment option is "account_charge"' do
    let(:deal) do
      create(:deal, payment_option: 'account_charge')
    end

    it 'sets bill_kind to "cashless"' do
      expect(@cash_book_record.bill_kind).to eql('cashless')
    end
  end
end
