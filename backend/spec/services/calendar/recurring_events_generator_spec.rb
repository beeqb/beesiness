require 'rails_helper'

describe Calendar::RecurringEventsGenerator, type: :service do
  subject { described_class.new(start_date, end_date, event) }

  describe 'monthly' do
    let(:start_date) { '2017-01-01' }
    let(:end_date) { '2017-03-13' }
    let(:event) do
      build(
        :calendar_event,
        :recurring_monthly,
        start: start_date,
        end: end_date,
      )
    end

    it 'generates one event per month' do
      p subject.events.pluck(:start)
      expect(subject.events.size).to eq(3)
    end
    
  end
end
