require 'rails_helper'

RSpec.describe CompanyIndicators, type: :service do
  let(:user) { create(:user) }
  let(:company) { create(:company) }
  subject { described_class.new(company, user) }

  describe '#monthly_budget' do
    it 'returns proper hash' do
      create_list(
        :cash_book_record,
        3,
        :income,
        amount: 200,
        company: company,
        created_at: '2017-11-10',
      )
      create_list(
        :cash_book_record,
        4,
        :income,
        amount: 100,
        company: company,
        created_at: '2017-12-10',
      )
      create_list(
        :cash_book_record,
        2,
        :outcome,
        amount: 200,
        company: company,
        created_at: '2017-11-10',
      )
      create_list(
        :cash_book_record,
        3,
        :outcome,
        amount: 100,
        company: company,
        created_at: '2017-12-10',
      )

      expected = {
        11 => {
          income: BigDecimal.new(600).round(18),
          outcome: BigDecimal.new(400).round(18),
          total_income: BigDecimal.new(600).round(18),
          total_outcome: BigDecimal.new(400).round(18),
          actual_income: BigDecimal.new(200).round(18),
        },
        12 => {
          income: BigDecimal.new(400).round(18),
          outcome: BigDecimal.new(300).round(18),
          total_income: BigDecimal.new(400).round(18),
          total_outcome: BigDecimal.new(300).round(18),
          actual_income: BigDecimal.new(100).round(18),
        }
      }

      expect(subject.monthly_budget).to eql(expected)
    end
  end

  describe '#product_average_price' do
    it 'works' do
      c1 = create(
        :product_category,
        has_part_in_average_price: true,
        company: company,
      ) 
      c2 = create(
        :product_category,
        has_part_in_average_price: false,
        company: company,
      ) 

      p1 = create(
        :product,
        category: c1,
        company: company,
        price: 300
      )
      p2 = create(
        :product,
        category: c2,
        company: company,
        price: 400
      )
      p3 = create(
        :product,
        category: c1,
        company: company,
        price: 500
      )

      expect(subject.product_average_price).to eql(BigDecimal.new(400, 18))
    end  
  end

  describe '#products_increase' do
    it 'returns count of products, created in current month' do
      create_list(:product, 2, company: company, created_at: 2.months.ago)
      create_list(:product, 3, company: company)

      expect(subject.products_increase).to eql(3)
    end
  end

  describe '#break_even_point' do
    it 'returns expenses to product average price ratio' do
      expect(subject).to receive(:fixed_expenses).and_return(50_000.to_d)
      expect(subject).to receive(:product_average_price).and_return(30_000.to_d)

      expect(subject.break_even_point).to eql(2)
    end
  end

  describe '#actual_income_by_days', broken: true do
    it 'works' do
      allow(Time).to receive(:now).and_return(Time.new(2017, 2, 20, 10))

      deal = create(:deal)
      dates = [
        Time.new('2017-02-4'),
        Time.new('2017-02-7'),
        Time.new('2017-02-10'),
        Time.new('2017-02-15'),
        Time.new('2017-02-18'),
        Time.new('2017-02-20'),
      ]
      dates[0..2].each do |date|
        allow(Time).to receive(:now).and_return(date)
        create(:cash_book_record, :income, amount: 100)
      end

      # create(:cash_book_record, :outcome, created_at: dates[3], amount: 200)

      dates[4..-1].each do |date|
        allow(Time).to receive(:now).and_return(date)
        create(:cash_book_record, :income, amount: 130, deal_id: deal.id)
      end

      expected = Array.new(28, 0)
      expected[0] = 100
      expected[6] = 100
      expected[9] = 100
      expected[17] = 130
      expected[19] = 130

      puts 'TEST' * 100
      p CashBookRecord.all.pluck(:created_at)
      expect(subject.actual_income_by_days).to eql expected
    end
  end
end
