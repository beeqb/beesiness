require 'rails_helper'

RSpec.describe Product, type: :model do
  subject { build(:product) }

  it { should belong_to(:category).class_name('ProductCategory') }

  describe 'Validations' do
    it 'is valid with valid attributes' do
      expect(subject).to be_valid
    end

    it { should validate_presence_of  :activity }
    it { should validate_presence_of :name }
    it { should validate_presence_of :category }

    it { should validate_numericality_of(:barcode).only_integer }
    it { should validate_length_of(:barcode).is_at_least(9) }

    it { should validate_numericality_of(:quantity) }
  end

  describe 'Scopes' do
    describe '.recommended' do
      it 'returns recommended products' do
        create_list(:product, 3)
        recommended_products = create_list(:product, 2, :recommended)

        expect(described_class.recommended).to match_array(recommended_products)
      end
    end
  end # end of Scopes
end
