require 'rails_helper'

RSpec.describe SystemEvent::Registry, type: :model do
  let(:mock_registry) do
    {
      client: {
        created: {
          general: '%{user_name} создал клиента %{client_name_link}',
          local: 'создан пользователем %{user_name}',
        },
        updated: '%{user_name} изменил информацию о сделке %{deal_number_link}',
      },
    }
  end

  before :each do
    allow(described_class).to receive(:all).and_return(mock_registry)
  end

  describe '.include?' do
    it 'returns true if there is such event' do
      expect(described_class.include?('client:created')).to be true
    end

    it 'returns false if there is no such event' do
      expect(described_class.include?('foo:bar')).to be false
    end
  end

  describe '.local_message_for' do
    context 'when local message exists' do
      it 'returns local message' do
        actual = described_class.local_message_for('client:created')
        expect(actual).to eql(mock_registry[:client][:created][:local])
      end
    end

    context 'when local message doesn\'t exists' do
      it 'returns existing message' do
        actual = described_class.local_message_for('client:updated')
        expect(actual).to eql(mock_registry[:client][:updated])
      end
    end
  end

  describe '.general_message_for' do
    context 'when general message exists' do
      it 'returns general message' do
        actual = described_class.general_message_for('client:created')
        expect(actual).to eql(mock_registry[:client][:created][:general])
      end
    end

    context 'when general message doesn\'t exists' do
      it 'returns existing message' do
        actual = described_class.general_message_for('client:updated')
        expect(actual).to eql(mock_registry[:client][:updated])
      end
    end
  end

  describe '.parse_kind' do
    it 'returns parsed event kind (as array)' do
      actual = described_class.parse_kind('client:updated')
      expect(actual).to eql([:client, :updated])
    end

    it 'doesn\'t care, whether event in registry or not' do
      actual = described_class.parse_kind('foo:bar')
      expect(actual).to eql([:foo, :bar])
    end
  end
end
