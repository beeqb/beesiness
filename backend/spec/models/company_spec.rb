require 'rails_helper'

RSpec.describe Company, type: :model do
  subject { create(:company) }

  describe 'Validations' do
    it 'is valid with valid attributes' do
      expect(subject).to be_valid
    end

    expect_fields_required(
      :creator,
      :name,
      :email,
      :vat,
      :currency,
      :phone,
      :phone_code,
      :address
    )

    it 'requires correct email' do
      subject.email = 'invalid_email'
      expect(subject).not_to be_valid

      subject.email = 'invalid_email@'
      expect(subject).not_to be_valid

      subject.email = 'invalid_email@invalid'
      expect(subject).not_to be_valid
    end

    it 'requires unique email' do
      exists_email = 'beeqb@beeqb.com' # it exists in seeds
      subject.email = exists_email
      expect(subject).not_to be_valid
    end

    it 'requires alphanumeric(spaces are allowed) name' do
      subject.name = 's@me_string!'
      expect(subject).not_to be_valid
    end

    it 'does not allow "beeqb" in the name' do
      allow(subject).to receive(:new_record?).and_return(true)
      subject.name = 'beeqb'
      expect(subject).not_to be_valid

      subject.name = 'BeeQb'
      expect(subject).not_to be_valid
    end

    it 'allows name to be 256 characters maximum' do
      subject.name = 'a' * 257
      expect(subject).not_to be_valid
    end

    it 'allows description to be 256 characters maximum' do
      subject.description = 'b' * 257
      expect(subject).not_to be_valid
    end

    it 'requires site to be a valid url' do
      subject.site = 'invalid.url'
      expect(subject).not_to be_valid

      subject.site = 'http://example.com'
      expect(subject).to be_valid
    end
  end

  describe '#strip_name' do
    it 'strips name' do
      subject.name = ' Name with spaces  '
      subject.strip_name
      expect(subject.name).to be_eql('Name with spaces')
    end
  end

  describe '#after_destroy' do
    it 'marks all employees as deleted' do
      employee = create(:employee, company: subject)

      subject.after_destroy

      employee.reload
      expect(employee.deleted_at).not_to be_nil
    end
  end

  describe '#sales_managers' do
    it 'returns managers and heads of sales dep, participating in sales' do
      sales_managers =
        create_list(:manager_of_sales, 2, :in_sales, company: subject)

      sales_heads = create(:head_of_sales, company: subject)
      other = create(:employee, company: subject)

      expect(subject.sales_managers).to match_array(sales_managers)
    end
  end

  describe '#employees_with_roles' do
    before(:each) do
      hr_user = create(:user)
      accountant_user = create(:user)
      @investor_user = create(:user)

      subject.add_user(hr_user, :employee_hr)
      subject.add_user(accountant_user, :employee_accountant)
      subject.add_user(@investor_user, :employee_investor)
    end

    it 'returns the correct number of employees' do
      employees =
        subject.employees_with_roles(:employee_hr, :employee_accountant)

      expect(employees.size).to be_eql(2)
    end

    it 'returns Employee instances' do
      employees = subject.employees_with_roles(:employee_hr)

      expect(employees.first).to be_instance_of(Employee)
    end

    it 'returns correctly linked to users instances' do
      employees = subject.employees_with_roles(:employee_investor)

      expect(employees.first.user_id).to be_eql(@investor_user.id)
    end
  end

  describe '#add_user' do
    context 'New employee' do
      let(:user) { create(:user) }

      it 'adds a role to user' do
        subject.add_user(user, :employee_accountant)

        expect(user).to have_role(:employee_accountant, subject)
      end

      it 'creates an employee' do
        expect {
          subject.add_user(user, :employee_accountant)
        }.to change{subject.employees.count}.from(1).to(2)
      end

      it 'links new employee with user' do
        subject.add_user(user, :employee_accountant)

        expect(subject.employees.where(user_id: user.id).any?).to be true
      end
    end

    context 'Existing employee' do
      let(:employee) { create(:employee, company: subject) }

      it 'adds a role to user' do
        subject.add_user(employee.user, :employee_accountant)

        expect(employee.user).to have_role(:employee_accountant, subject)
      end

      it 'adds employee to department' do
        department = create(:department)

        subject.add_user(employee.user, :employee_accountant, department)

        expect(department.has_employee(employee)).to be true
      end
    end
  end

  describe '#director' do
    it 'returns company director' do
      expect(subject.director).to have_role(:employee_director, subject)
    end
  end

  describe '*_for methods' do
    let(:user) { create(:user) }
    before(:all) { Client.skip_callback(:create, :before, :invite_user) }
    after(:all) { Client.set_callback(:create, :before, :invite_user) }

    before(:each) { subject.add_user(user, :employee_director) }

    describe '#leads_for' do
      let(:leads) { create_list(:lead, 3, company: subject) }

      it 'return leads, visible for user' do
        expect(subject.leads_for(user)).to match_array(leads)
      end

      # not working for unknown reason
      it 'return leads, visible to employee' do
        employee = subject.employees.find_by(user_id: user.id)
        expect(subject.leads_for(employee)).to match_array(leads)
      end
    end

    describe '#deals_for' do
      let(:deals) { create_list(:deal, 3, company: subject) }

      it 'return deals, visible for user' do
        expect(subject.deals_for(user)).to match_array(deals)
      end

      it 'return deals, visible to employee' do
        employee = subject.employees.find_by(user_id: user.id)
        expect(subject.deals_for(employee)).to match_array(deals)
      end
    end

    describe '#clients_for' do
      let(:clients) { create_list(:client, 3, company: subject) }

      it 'return clients, visible for user' do
        expect(subject.clients_for(user)).to match_array(clients)
      end

      it 'return clients, visible to employee' do
        employee = subject.employees.find_by(user_id: user.id)
        expect(subject.clients_for(employee)).to match_array(clients)
      end
    end

    describe '#products_for' do
      let(:products) { create_list(:product, 3, company: subject) }

      it 'return products, visible for user' do
        expect(subject.products_for(user)).to match_array(products)
      end

      # not working for unknown reason
      it 'return products, visible to employee' do
        employee = subject.employees.find_by(user_id: user.id)
        expect(subject.products_for(employee)).to match_array(products)
      end
    end
  end

  describe '*_count_by_statuses' do
    let(:user) { create(:user) }
    before(:all) do
      Client.skip_callback(:create, :before, :invite_user)
      Client.skip_callback(:create, :before, :set_default_status)
      Deal.skip_callback(:create, :before, :set_initial_status)
    end
    after(:all) do
      Client.set_callback(:create, :before, :invite_user)
      Client.set_callback(:create, :before, :set_default_status)
      Deal.set_callback(:create, :before, :set_initial_status)
    end

    before(:each) do
      subject.add_user(user, :employee_director)
    end

    describe '#leads_count_by_statuses' do
      it 'returns hash with leads counts, groupped by status' do
        expected = {lead_new: 5, lead_in_work: 4, lead_trash: 6}
        expected.each do |status, count|
          create_list(:lead, count, status, company: subject)
        end

        leads_count = subject.leads_count_by_statuses(user)

        expect(leads_count).to eql(expected)
      end
    end

    describe '#clients_count_by_statuses' do
      it 'returns hash with clients counts, groupped by status' do
        expected = {
          client_new: 5,
          client_current: 4,
          client_permanent: 6,
          client_special: 2,
          client_no: 3,
        }

        expected.each do |status, count|
          create_list(:client, count, status, company: subject)
        end

        clients_count = subject.clients_count_by_statuses(user)

        expect(clients_count).to eql(expected)
      end
    end

    describe '#deals_count_by_statuses' do
      it 'returns hash with deals counts, groupped by status' do
        expected = {
          in_progress: 5,
          contract_conclusion: 4,
          contract_signed: 6,
          won: 2,
          payed: 3,
          lost: 5,
          returned: 1,
        }

        expected.each do |status, count|
          create_list(:deal, count, status, company: subject)
        end

        deals_count = subject.deals_count_by_statuses(user)

        expect(deals_count).to eql(expected)
      end
    end

    describe '#suppliers_count_by_statuses' do
      it 'returns hash with suppliers counts, groupped by status' do
        expected = {
          supplier_new: 5,
          supplier_current: 4,
          supplier_permanent: 6,
          supplier_special: 2,
          supplier_no: 3,
        }

        expected.each do |status, count|
          create_list(:supplier, count, status, company: subject)
        end

        suppliers_count = subject.suppliers_count_by_statuses(user)

        expect(suppliers_count).to eql(expected)
      end
    end
  end

  describe '#plan_income' do
    it 'returns sales plans sum for current month' do
      SalesPlan.skip_callback(:save, :before, :calculate_sum_and_quantity)
      create(:sales_plan, company: subject, sum: 100)

      expect(subject.plan_income).to eql(100)

      SalesPlan.set_callback(:save, :before, :calculate_sum_and_quantity)
    end
  end
end
