require 'rails_helper'

RSpec.describe CashBookRecord, type: :model do
  subject { build_stubbed(:cash_book_record) }
  let(:company) { create(:company) }

  describe 'Validations' do
    it 'is valid with valid attributes' do
      expect(subject).to be_valid
    end

    it { should validate_presence_of  :currency }
    it { should validate_presence_of :category }
    it { should validate_presence_of :creator }
    it { should validate_presence_of :amount }

    it { should validate_numericality_of :amount }

    it 'should validate that loan return is not greater than loan itself' do
      allow(CashBookRecord).to receive_message_chain(:loan, :sum) { 1000 }
      allow(CashBookRecord).to receive_message_chain(:loan_return, :sum) { 800 }
      subject.category = 'loan_return'
      subject.amount = 300

      expect(subject).to be_invalid
    end
  end

  describe 'Callbacks' do
    describe '#set_amount_sign' do
      context 'when creating new record' do
        it 'negates amount if outcome' do
          # чтобы не было кассового разрыва
          create(:cash_book_record, amount: 200, category: :income, company: company)
          record = create(:cash_book_record, amount: 100, category: :outcome, company: company)
          expect(record.amount).to eql(-100)
        end

        it 'negates amount if loan return' do
          # чтобы не было кассового разрыва
          create(:cash_book_record, amount: 200, category: :loan, company: company)
          record = create(
            :cash_book_record,
            amount: 100,
            category: :loan_return,
            company: company
          )
          expect(record.amount).to eql(-100)
        end
      end

      context 'when updating record' do
        it 'negates amount if outcome' do
          record = create(:cash_book_record, amount: 100, category: :income)

          record.update(category: :outcome)
          expect(record.amount).to eql(-100)
        end

        it 'negates amount if loan return' do
          # чтобы не было кассового разрыва
          create(:cash_book_record, amount: 200, category: :loan, company: company)
          record = create(:cash_book_record, amount: 100, category: :income, company: company)

          record.update(category: :loan_return)
          expect(record.amount).to eql(-100)
        end
      end
    end
  end
end
