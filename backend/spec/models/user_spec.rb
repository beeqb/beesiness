require 'rails_helper'

RSpec.describe User, type: :model do
  subject { build(:user) }

  describe 'Validation' do
    subject { create(:user) }

    it { is_expected.to be_valid }

    it 'requires name' do
      subject.name = nil
      expect(subject).to be_invalid
    end

    it 'requires email' do
      subject.email = nil
      expect(subject).to be_invalid
    end

    it 'can\'t have birthday in a future' do
      subject.birthday = 10.days.from_now
      expect(subject).to be_invalid
    end

    context 'Profile scenario' do
      before(:each) { subject.scenario = 'profile' }

      it 'requires name' do
        subject.name = nil
        expect(subject).to be_invalid
      end

      it 'requires surname' do
        subject.surname = nil
        expect(subject).to be_invalid
      end

      it 'requires cell_phone' do
        subject.cell_phone = nil
        expect(subject).to be_invalid
      end

      it 'requires cell_phone_code_id' do
        subject.cell_phone_code_id = nil
        expect(subject).to be_invalid
      end
    end

    it 'requires password on create' do
      subject.password = nil
        expect(subject).to be_invalid(:create)
    end

    it 'requires email to be unique' do
      existing_email = 'some@foo.com'
      create(:user, email: existing_email)

      subject.email = existing_email
      expect(subject).to be_invalid
    end

    it 'requires valid email' do
      subject.email = 'not_email@'
      expect(subject).to be_invalid
    end

    it 'requires unique facebook_link' do
      existing_facebook_link = 'some string'
      create(:user, facebook_link: existing_facebook_link)

      subject.facebook_link = existing_facebook_link
      expect(subject).to be_invalid
    end

    it 'requires unique facebook_identity' do
      existing_facebook_identity = 'some string'
      create(:user, facebook_identity: existing_facebook_identity)

      subject.facebook_identity = existing_facebook_identity
      expect(subject).to be_invalid
    end

    it 'allows blank facebook_identity' do
      subject.facebook_identity = nil
      expect(subject).to be_valid
    end

    it 'allows blank facebook_link' do
      subject.facebook_link = nil
      expect(subject).to be_valid
    end
  end # end of Validations

  describe 'Callbacks' do
    subject { build(:user) }

    it 'triggers #parse_facebook_data before create' do
      expect(subject).to receive(:parse_facebook_data)
      subject.save
    end

    it 'triggers #add_default_role after create' do
      expect(subject).to receive(:add_default_role)
      subject.save
    end

    it 'triggers #update_last_seen before save' do
      expect(subject).to receive(:update_last_seen)
      subject.save
    end

    describe '#parse_facebook_data' do
      facebook_data = {
        last_name: { attr: :surname, value: 'Last Name'},
        sex: {attr: :sex, value: '2', expected: 'm'},
        uid: {attr: :facebook_identity, value: 'identity'},
        identity: {attr: :facebook_link, value: 'link'},
        bdate: {
          attr: :birthday,
          value: '1990-11-15',
          expected: Date.parse('1990-11-15'),
        },
        phone: {attr: :cell_phone, value: '32443534'},
        photo_big: {
          attr: :remote_image_url,
          value: 'http://photo.url',
          expected: 'https://photo.url',
        },
      }

      before(:each) do
        allow_any_instance_of(UserImageUploader).to receive(:download!)
        subject.facebook_data =
          facebook_data.map { |k, v| [k, v[:value]] }.to_h
      end

      facebook_data.each do |key, object|
        it "assigns #{object[:attr]}" do
          subject.save

          actual = subject.send(object[:attr])
          expected = if object.has_key?(:expected)
                       object[:expected]
                     else
                       object[:value]
                     end

          expect(actual).to eql(expected)
        end
      end

      context 'City is present in database' do
        it 'assigns city id from database' do
          city_name = 'Москва'
          city_id = City.find_by_name(city_name).id

          subject.facebook_data[:city] = city_name
          subject.save
          expect(subject.city_id).to eql(city_id)
        end
      end

      context 'City is not present in database' do
        it 'creates city' do
          expect{subject.save}.to change{City.count}.by(1)
        end

        it 'assigns city name properly' do
          subject.save
          expect(City.last.name).to eql(subject.facebook_data[:city])
        end

        it 'assigns city_id to newly created city' do
          subject.save
          expect(subject.city_id).to eql(City.last.id)
        end

        context 'Country is present in database' do
          let(:country_name) { 'Россия' }
          let(:country_id) { Country.find_by_name(country_name).id }

          before(:each) do
            subject.facebook_data[:city] = 'Челябинск'
            subject.facebook_data[:country] = country_name
          end

          it 'binds new city to the existing country' do
            subject.save
            expect(City.last.country_id).to eql(country_id)
          end

          it 'does not create country' do
            expect{subject.save}.to_not change{Country.count}
          end
        end

        context 'Country is not present in database' do
          before(:each) do
            subject.facebook_data[:city] = 'Астана'
            subject.facebook_data[:country] = 'Казахстан'
          end

          it 'creates country' do
            expect{subject.save}.to change{Country.count}.by(1)
          end

          it 'assigns country name properly' do
            subject.save
            expect(Country.last.name).to eql(subject.facebook_data[:country])
          end

          it 'binds new city to the new country' do
            subject.save
            expect(City.last.country_id).to eql(Country.last.id)
          end
        end
      end
    end # end of #parse_facebook_data

    describe '#add_default_role' do
      it 'adds role "user"' do
        subject.save
        expect(subject).to have_role(:user)
      end
    end

  end # end of Callbacks

  describe '#related_tasks', :slow do
    subject { create(:user) }

    it 'returns tasks, created by user' do
      tasks = create_list(:task, 3, creator: subject)
      expect(subject.related_tasks).to match_array(tasks)
    end

    it 'returns tasks, which user is executing' do
      employee = create(:employee, user: subject)
      tasks = create_list(:task, 3, executor: employee)
      expect(subject.related_tasks).to match_array(tasks)
    end

    it 'returns tasks, which user is watching' do
      employee = create(:employee, user: subject)
      tasks = create_list(:task, 3)
      tasks.each { |task| task.watchers << employee }

      expect(subject.related_tasks).to match_array(tasks)
    end
  end

  describe '#update_last_seen' do
    it 'updates last_seen' do
      expect{subject.update_last_seen}.to change{subject.last_seen}
    end
  end

  describe '#online?' do
    it 'returns true, if last_seen < 10 minutes ago' do
      subject.last_seen = 9.minutes.ago
      expect(subject.online?).to be true
    end

    it 'returns false otherwise' do
      subject.last_seen = 11.minutes.ago
      expect(subject.online?).to be false
    end

    it 'returns false, if last_seen is falsy' do
      subject.last_seen = nil
      expect(subject.online?).to be false
    end
  end

  describe '#profile_fullness' do
    it 'returns number of percents, basing on how much fields are not blank' do
      value_of_division = 100.0 / 14 # from implementation

      # avoid factory to have determined number of not blank fields
      user = User.new
      user.name = 'Name'
      user.surname = 'Surname'

      # password is always present, so we've got 3 here
      expect(user.profile_fullness).to eql(3 * value_of_division)
    end
  end

  describe '#role_names_in_company' do
    it 'returns user role names in company with given id' do
      company = create(:company)
      expected_roles = ['employee_hr', 'employee_accountant']

      expected_roles.each { |role| subject.add_role(role, company) }
      subject.save

      actual_roles = subject.role_names_in_company(company.id)
      expect(actual_roles).to match_array(expected_roles)
    end
  end

  describe '#allow_change_password' do
    it 'returns true, if reset_password_token is set' do
      subject.reset_password_token = 'sometoken'
      expect(subject.allow_change_password).to be true
    end

    it 'returns false otherwise' do
      subject.reset_password_token = nil
      expect(subject.allow_change_password).to be false
    end
  end

  describe '#permissions' do
    it 'returns ability permissions' do
      permissions = Ability.new(subject).permissions
      expect(subject.permissions).to eql(permissions)
    end
  end

  describe '#fullname' do
    it 'returns concatenated name and surname' do
      subject.name = 'John'
      subject.surname = 'Doe'
      expect(subject.fullname).to eql('John Doe')
    end

    it 'returns only name, if surname is empty' do
      subject.name = 'John'
      subject.surname = nil
      expect(subject.fullname).to eql('John')
    end
  end

  describe '#employee_in_company' do
    let(:company) { create(:company) }

    it 'returns employee for user in given company' do
      expected_employee = create(:employee, user: subject, company: company)
      actual_employee = subject.employee_in_company(company.id)

      expect(actual_employee).to eql(expected_employee)
    end
  end
end
