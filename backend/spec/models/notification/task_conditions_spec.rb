require 'rails_helper'

RSpec.describe Notification::TaskConditions, type: :model do 
  it_behaves_like 'notification'

  describe '.subject' do
    it 'returns Task class' do
      expect(described_class.subject).to eql(Task)
    end
  end

  describe '.notify' do
    let(:user) { create(:user) }
    let(:employee) { create(:employee, user: user) }

    it 'creates notification for tasks, due today' do
      task = create(:task, executor: employee, due_date: Date.today)

      expect(described_class).to receive(:for).with([user.id], task.id)
      described_class.notify
    end

    it 'creates notification for tasks, due tommorow' do
      task = create(:task, executor: employee, due_date: Date.tomorrow)

      expect(described_class).to receive(:for).with([user.id], task.id)
      described_class.notify
    end

  end

  describe '#build_content' do
    it 'assigns content attribute' do
      task = create(:task, title: 'Test task', due_date: Date.today)
      notification = described_class.new(subject_id: task.id) 

      notification.build_content

      expect(notification.content).to eql(
        %(Срок реализации задачи "#{task.title}" истекает сегодня)
      )
    end
  end
end
