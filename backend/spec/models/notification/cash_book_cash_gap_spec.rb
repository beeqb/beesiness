require 'rails_helper'

RSpec.describe Notification::CashBookCashGap, type: :model do
  let(:company) { create(:company) }
  subject { described_class.new(subject_id: company.id) }

  before(:each) do
    allow(CashBookMailer).to receive_message_chain(:cash_gap, :deliver_later)
  end

  it_behaves_like 'notification'

  describe '.subject' do
    it 'returns Company class' do
      expect(described_class.subject).to eql(Company)
    end
  end

  describe '#build_content' do
    it 'assigns content attribute' do
      subject.build_content

      expect(subject.content).to eql(
        %(В компании "#{company.name}" произошел кассовый разрыв)
      )
    end
  end
end
