require 'rails_helper'

RSpec.describe Notification::DealWaitsPayment, type: :model do
  it_behaves_like 'notification'

  describe '.subject' do
    it 'returns Deal class' do
      expect(described_class.subject).to eql(Deal)
    end
  end

  describe '.notify' do
    let(:user) { create(:user) }
    let(:employee) { create(:employee, user: user) }
    let(:client) { create(:client, user: user) }

    context 'when deals have single payment' do
      it 'creates notification when payment is today' do
        deal = create(
          :deal,
          manager: employee,
          single_payment_date: Time.now,
        )

        expect(described_class).to receive(:for).with([user.id], deal.id)
        described_class.notify
      end

      it 'creates notification when payment is in a week' do
        deal = create(
          :deal,
          manager: employee,
          single_payment_date: 7.days.from_now,
        )

        expect(described_class).to receive(:for).with([user.id], deal.id)
        described_class.notify
      end
    end

    context 'when deals have monthly payments' do
      it 'creates notification when payment ends today' do
        deal = create(
          :deal,
          manager: employee,
          monthly_payment_ended_at: Time.now,
        )

        expect(described_class).to receive(:for).with([user.id], deal.id) 
        described_class.notify
      end

      it 'creates notification when payment ends in a week' do
        deal = create(
          :deal,
          manager: employee,
          monthly_payment_ended_at: 7.days.from_now,
        )

        expect(described_class).to receive(:for).with([user.id], deal.id) 
        described_class.notify
      end
    end

    it 'creates notification for client' do
      deal = create(
        :deal,
        manager: employee,
        client: client,
        monthly_payment_ended_at: Time.now,
      )
      
      expect(described_class).to receive(:for).with([user.id], deal.id).twice
      described_class.notify
    end
  end

  describe '#build_content' do
    it 'assigns content attribute' do
      subject.build_content

      expect(subject.content).to eql('Ожидается платеж по сделке')
    end
  end
end
