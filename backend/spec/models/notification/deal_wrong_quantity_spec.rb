require 'rails_helper'

RSpec.describe Notification::DealWrongQuantity, type: :model do
  it_behaves_like 'notification'

  describe '.subject' do
    it 'returns Deal class' do
      expect(described_class.subject).to eql(Deal)
    end
  end

  describe '.notify' do
    let(:user) { create(:user) }
    let(:product) { create(:product, quantity: 100) }
    let(:employee) { create(:employee, user: user) }

    it 'creates notification if sold more products than exist' do
      deal = create(:deal, manager: employee)
      create(:deal_product, deal: deal, product: product, count: 105)

      expect(described_class).to receive(:for).with([user.id], deal.id)
      described_class.notify
    end

  end

  describe '#build_content' do
    it 'assigns content attribute' do
      subject.build_content

      expect(subject.content).to eql(
        'Сделка: количество товара меньше проданных единиц товара'
      )
    end
  end
end
