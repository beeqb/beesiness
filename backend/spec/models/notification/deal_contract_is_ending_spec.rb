require 'rails_helper'

RSpec.describe Notification::DealContractIsEnding, type: :model do
  it_behaves_like 'notification'

  describe '#build_content' do
    it 'assigns content attribute' do
      subject.build_content

      expect(subject.content).to eql('Сделка: срок действия контракта истекает сегодня')
    end
  end
end
