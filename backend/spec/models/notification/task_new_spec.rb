require 'rails_helper'

RSpec.describe Notification::TaskNew, type: :model do 
  it_behaves_like 'notification'

  let(:user) { create(:user) }
  let(:employee) { create(:employee, user: user) }

  describe '.subject' do
    it 'returns Task class' do
      expect(described_class.subject).to eql(Task)
    end
  end

  describe '.notify' do
    it 'creates notification for tasks, created today' do
      task = create(:task, executor: employee, due_date: Date.today)

      expect(described_class).to receive(:for).with([user.id], task.id)
      described_class.notify
    end

    it 'creates notification for tasks, created today' do
      task = create(:task, executor: employee, due_date: Date.today)

      expect(described_class).to receive(:for).with([user.id], task.id)
      described_class.notify
    end

  end

  describe '#build_content' do
    it 'assigns content attribute' do 
      task = create(
        :task,
        title: 'Test task',
        due_date: Date.today,
        executor: employee,
      )

      notification = described_class.new(subject_id: task.id, user_id: user.id) 

      # чтобы инициализировать @@tasks 
      described_class.notify

      notification.build_content

      expect(notification.content).to eql('У вас появились новые задачи: 1 шт.')
    end
  end
end
