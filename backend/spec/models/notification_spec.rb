require 'rails_helper'

RSpec.describe Notification, type: :model do
  describe '#build_content' do
    it 'is not implemented' do
      expect { subject.build_content }.to \
        raise_error(RuntimeError, 'method did not implement')
    end
  end

  describe '.notify' do
    it 'is not implemented' do
      expect { described_class.notify }.to \
        raise_error(RuntimeError, 'method did not implement')
    end
  end

  describe '.subject' do
    it 'is not implemented' do
      expect { described_class.subject }.to \
        raise_error(RuntimeError, 'method did not implement')
    end
  end 
end
