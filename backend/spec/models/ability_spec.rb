require 'rails_helper'

RSpec.describe Ability, ability: :true do 
  let(:user) { build_stubbed(:user) }
  let(:company) { build_stubbed(:company) }

  subject(:ability) { Ability.new(user) }

  describe 'Check permissions', broken: true do
    it 'only resend_confirmation permission allow for guest' do
      permissions = ability.permissions
      expect(permissions).to eq(
        resend_confirmation: [{ name: 'User' }]
      )
    end
  end

  describe 'Companies' do
    assert_ability :read, except: 'dismissed'
    assert_ability :search, '*'
    assert_ability :stream, except: 'dismissed'
    assert_ability :create, '*'
    assert_ability :validate, '*'
    assert_ability :upload_logo, '*'
    assert_ability :update, %w(director admin)
    assert_ability :destroy, 'director'
    assert_ability :invite_user, %w(director hr admin)
  end

  describe 'Departments' do
    let(:department) { build_stubbed(:department, company: company) }

    roles = %w(
        accountant
        admin
        director
        head_of_marketing_department
        head_of_sales_department
        hr
        manager_of_sales_department
        marketer
        purchasing_manager
    )

    assert_ability :read, except: 'dismissed'
    assert_ability :create, roles
    assert_ability :validate, roles
    assert_ability :update, roles
    assert_ability :destroy, roles
  end

  describe 'Employees' do
    let(:employee) { build(:employee, company: company) }

    roles = %w(
        admin
        director
        hr
    )

    assert_ability :read, except: 'dismissed'
    assert_ability :activity, except: 'dismissed'
    assert_ability :validate, roles + ['head_of_sales_department']
    assert_ability :update, roles + ['head_of_sales_department']
    assert_ability(
      :update_participation_in_sales,
      %w(head_of_sales_department admin)
    )
    assert_ability :update_name, roles
    assert_ability :update_surname, roles
    assert_ability :update_work_address, roles
    assert_ability :update_email, roles
    assert_ability :update_work_phone, roles
    assert_ability :update_cell_phone, roles
    assert_ability :update_salary, %w(director hr)
    assert_ability :update_status, %w(director hr)
    assert_ability :update_pass_number, roles
    assert_ability :update_job_started_at, %w(director hr)
    assert_ability :update_role, %w(director hr)
    assert_ability :add_birthday, roles
    assert_ability :update_birthday, 'hr'
    assert_ability :update_department, roles
    assert_ability :dismiss, %w(director hr)
    assert_ability :destroy, roles
  end

  describe 'Clients' do
    let(:client) { build(:client, company: company) }

    roles = %w(
        admin
        director
        head_of_sales_department
        manager_of_sales_department
    )

    assert_ability :read, roles
    assert_ability :search, '*'
    assert_ability :new, roles
    assert_ability :validate, roles
    assert_ability :create, roles
    assert_ability :update, roles
  end

  describe 'Product Categories' do
    let(:product_category) { build(:product_category, company: company) }

    roles = %w(
        admin
        head_of_sales_department
        manager_of_sales_department
        purchasing_manager
    )

    assert_ability :read, except: 'dismissed'
    assert_ability :create, roles
    assert_ability :validate, roles
    assert_ability :update, roles
    assert_ability :destroy, roles - ['manager_of_sales_department']
    assert_ability :delete_safely, roles - ['manager_of_sales_department']
  end

  describe 'Tasks' do
    let(:user) { create(:user) }
    let(:company) { create(:company) }
    let(:employee) { create(:employee, company: company, user: user) }

    it 'executor can read' do
      assigned_task = create(:task, executor: employee)

      expect(subject).to be_able_to(:read, assigned_task)
    end

    it 'assistant can read' do
      assistant_task = create(:task, assistant: employee)

      expect(subject).to be_able_to(:read, assistant_task)
    end

    it 'creator can read' do
      created_task = create(:task, creator: user)

      expect(subject).to be_able_to(:read, created_task)
      expect(subject).to be_able_to(:update, created_task)
    end

    it 'creator can update' do
      created_task = create(:task, creator: user)

      expect(subject).to be_able_to(:update, created_task)
    end

    it 'watcher can read' do
      watched_task = create(:task)
      watched_task.watchers << employee

      expect(subject).to be_able_to(:read, watched_task)
    end
  end

  describe 'Sales plans' do
    let(:sales_plan) { build(:sales_plan, company: company) }

    assert_ability :manage, %w(
        director
        head_of_sales_department
        accountant
    )
  end

  describe 'Deals' do
    let(:deal) { build(:deal, company: company) }
    roles = %w(
        accountant
        admin
        director
        head_of_sales_department
        manager_of_sales_department
        marketer
        purchasing_manager
    )

    assert_ability :create, roles
    assert_ability :read, roles
    assert_ability :new, roles
    assert_ability :validate, roles
    assert_ability :set_status, roles
    assert_ability :update, roles
    assert_ability :return, %w(
        admin
        director
    )
    # no one can destroy a deal
    assert_ability :destroy, []
  end

  describe 'Products' do
    let(:product) { build(:product, company: company) }

    creators = %w(
        admin
        head_of_sales_department
        manager_of_sales_department
        purchasing_manager
    )

    assert_ability :read, except: 'dismissed'
    assert_ability :search, '*'
    assert_ability :recommended, except: 'dismissed'
    assert_ability :set_recommended, %w(
        admin
        director
        head_of_sales_department
    )
    assert_ability :create, creators
    assert_ability :validate, creators
    assert_ability :upload_images, creators
    assert_ability :remove_image, creators
    assert_ability :update, %w(
        admin
        head_of_sales_department
        purchasing_manager
    )
    assert_ability :reserve, 'admin'
    assert_ability :order, 'admin'
    assert_ability :discuss, 'admin'
    assert_ability :complain, 'admin'
    assert_ability :destroy, %w(
        accountant
        admin
        director
        manager_of_sales_department
    )
  end

  describe 'Leads' do
    let(:lead) { build(:lead_individual, company: company) }

    roles = %w(
        admin
        director
        head_of_sales_department
        manager_of_sales_department
    )

    assert_ability :read, roles
    assert_ability :new, roles
    assert_ability :create, roles
    assert_ability :validate, roles
    assert_ability :update, roles
    assert_ability :convert, %w(
        director
        manager_of_sales_department
    )
  end
end
