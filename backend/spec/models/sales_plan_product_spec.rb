require 'rails_helper'

RSpec.describe SalesPlanProduct, type: :model do
  it { should belong_to :product }
  it { should belong_to :sales_plan }
end
