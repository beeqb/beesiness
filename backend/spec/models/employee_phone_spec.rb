require 'rails_helper'

RSpec.describe EmployeePhone, type: :model do
  it { should belong_to :country_phone_code }
  it { should belong_to :employee }
  it { should validate_presence_of :phone }
  it { should validate_presence_of :kind }
end
