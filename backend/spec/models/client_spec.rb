require 'rails_helper'

RSpec.describe Client, type: :model do
  subject { build(:client) }

  describe 'Validation' do

    it { is_expected.to be_valid }

    context 'Individual' do
      subject { build(:individual_client) }

      it { should validate_presence_of(:name) }
      it do
        should validate_uniqueness_of(:email).scoped_to(:company_id).allow_blank
      end

      it 'can\'t have birthday in a future' do
        subject.birthday = 10.days.from_now
        expect(subject).to be_invalid
      end
    end

    context 'Legal entity' do
      subject { build(:legal_client) }

      it { should validate_presence_of(:legal_entity_name) }
      it do
        should \
          validate_uniqueness_of(:legal_entity_email)
          .scoped_to(:company_id)
          .allow_blank
      end
    end

    it 'requires valid email' do
      subject.email = 'not_email@'
      expect(subject).to be_invalid
    end
  end # end of Validation

  describe 'Callbacks' do
    describe '#set_default_status' do
      it 'sets a #client_new" status before create' do
        allow(subject).to receive(:invite_user)
        subject.save
        expect(subject.status).to eql('client_new')
      end
    end

    describe '#invite_user' do
      before :each do
        allow_any_instance_of(UserCompanyInvitation).to\
          receive(:send_invitation)
      end

      it 'creates new invitation' do
        expect { subject.save }.to change{UserCompanyInvitation.count}.by(1)
      end

      it 'sets invitation email to client email' do
        subject.save
        expect(UserCompanyInvitation.last.email).to eql(subject.email)
      end

      it 'sets invitation company to client company' do
        subject.company = create(:company)
        subject.save
        expect(UserCompanyInvitation.last.company_id).to eql(subject.company_id)
      end

      context 'Client\'s user is not present' do
        it 'creates new user' do
          subject.user_id = nil
          subject.save
          expect(subject.user_id).not_to be_nil
        end
      end

      it 'creates company for legal entity if there is none' do
        subject = create(:legal_client, legal_entity_id: nil)
        new_company = Company.find_by_name(subject.legal_entity_name)
        expect(new_company).not_to be_nil
      end
    end
  end # end of Callbacks

  describe 'Scopes' do
    before :each do
      allow_any_instance_of(UserCompanyInvitation).to\
        receive(:send_invitation)
    end

    describe 'visible_for' do
      let(:employee) { create(:employee) }

      it 'returns clients with undefined visibility' do
        clients = create_list(:client, 3, visibility: nil)
        expect(Client.visible_for(employee)).to match_array(clients)
      end

      include_examples 'visible_for'
    end

    describe 'managed_by' do
      let(:employee) { create(:employee) }

      context 'Employee given' do
        it 'returns clients, that this employee manage' do
          clients = create_list(:client, 3, manager: employee)
          expect(Client.visible_for(employee)).to match_array(clients)
        end

        it 'returns clients where this employee is second manager' do
          clients = create_list(:client, 3, second_manager: employee)
          expect(Client.visible_for(employee)).to match_array(clients)
        end
      end

      context 'User given' do
        let(:user) { create(:user) }
        let(:employee) { create(:employee, user: user) }

        it 'returns clients, that this user manage' do
          clients = create_list(:client, 3, manager: employee)
          expect(Client.visible_for(user)).to match_array(clients)
        end

        it 'returns clients where this user is second manager' do
          clients = create_list(:client, 3, second_manager: employee)
          expect(Client.visible_for(user)).to match_array(clients)
        end
      end

    end
  end # end of Scopes
end
