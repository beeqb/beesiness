require 'rails_helper'

RSpec.describe Employee, type: :model do
  it { should belong_to :user }
  it { should belong_to :company }
  it { should belong_to :user_activity }
  it { should have_many :employee_phones }
  it { should have_many :employees_departments }
  it { should have_many(:departments).through(:employees_departments) }
  it do
    should have_many(:statistics)
           .class_name('CompanyStatistic')
           .order(created_at: :desc)
  end

  it { should validate_uniqueness_of(:email).scoped_to(:company_id) }

  describe '#dismiss' do
    let(:employee) { build(:employee, user_activity_id: 1) }

    before(:each) do
      employee.company.add_user(employee.user, :employee_director)
      employee.dismiss
    end

    it 'sets user activity to dismissed' do
      expect(employee.user_activity.name).to eql('Уволен')
    end

    it 'replace all user roles with dismissed' do
      user_roles = employee.user.role_names_in_company(employee.company.id)
      expect(user_roles).to contain_exactly('employee_dismissed')
    end
  end
end
