require 'rails_helper'

RSpec.describe Role, type: :model do
  it { should have_and_belong_to_many :users }
  it { should belong_to :resource }

  it do
    roles = Rolify.resource_types
    should validate_inclusion_of(:resource_type)
           .in_array(roles)
           .allow_nil
  end
end
