require 'rails_helper'

RSpec.describe Deal, type: :model do
  subject { build(:deal) }
  describe 'Validations' do
    it 'is valid with valid attributes' do
      expect(subject).to be_valid
    end

    expect_fields_required(
      :legal_form,
      :started_at,
      :manager_id,
      :visibility,
    )

    it 'requires discount to be integer' do
      subject.discount = 10.1
      expect(subject).not_to be_valid
    end

    it 'allows discount to be nil' do
      subject.discount = nil
      expect(subject).to be_valid
    end

    it 'is not valid if started_at is in the future' do
      subject.started_at = Date.tomorrow
      expect(subject).not_to be_valid
    end

    it 'is not valid without a manager_id' do
      subject.manager_id = nil
      expect(subject).not_to be_valid
    end
  end

  describe 'Scopes' do
    before :each do
      allow_any_instance_of(Deal).to receive(:set_number)
    end

    describe '.visible_for' do
      include_examples 'visible_for'
    end

    describe 'by_client_name' do
      it 'returns deals with clients, which name matches giving string' do
        john = create(:client, name: 'John Doe')
        johnson = create(:client, name: 'Eric Johnson')
        jack = create(:client, name: 'Jack Weiss')

        john_deal = create(:deal, client: john)
        johnson_deal = create(:deal, client: johnson)
        jack_deal = create(:deal, client: jack)

        expect(described_class.by_client_name('john')).to \
          match_array([john_deal, johnson_deal])
        expect(described_class.by_client_name('jack')).to \
          match_array([jack_deal])
      end
    end
  end

  describe '#return' do
    before(:each) { allow(subject).to receive(:set_number) }

    context 'when already persisted' do
      it 'changes status to "returned"' do
        subject.save!
        subject.return
        expect(subject.status).to eql('returned')
      end
    end

    context 'anyway' do
      it 'persists deal' do
        expect(subject).to receive(:save!)
        subject.return
      end

      it 'mark all products as returned' do
        create_list(:deal_product, 3, deal: subject)
        subject.return

        subject.deal_products.each do |deal_product|
          expect(deal_product.returned_at).not_to be_nil
        end
      end

      it 'persists all products' do
        create_list(:deal_product, 3, deal: subject)

        subject.deal_products.each do |deal_product|
          expect(deal_product).to receive(:save!)
        end

        subject.return
      end
    end
  end # end of #return

  describe 'Callbacks' do
    describe '#set_number' do
      it 'sets number to existed deals count + 1' do
        deals_count = 5
        subject.company = create(:company_with_deals, deals_count: deals_count)
        subject.save!

        expect(subject.number).to eql(deals_count + 1)
      end
    end

    describe '#set_initial_status' do
      before(:each) { allow(subject).to receive(:set_number) }

      it 'sets status to "in_progress"' do
        subject.save!
        expect(subject.status).to eql('in_progress')
      end

      it 'sets in_progress_at' do
        subject.save!
        expect(subject.in_progress_at).not_to be_nil
      end
    end

    describe '#set_status_change_time' do
      before :each do
        allow(subject).to receive(:set_number)
        subject.save!
      end

      Company.references[:deal_statuses].as_json.each do |status|
        it "sets #{status[:id]}_at" do
          subject.status = status[:id]
          subject.save!

          expect(subject[status[:id].to_s + '_at']).not_to be_nil
        end
      end
    end

    describe '#convert_if_client_is_lead' do
      before(:each) do
        allow(subject).to receive(:set_number)
        allow_any_instance_of(Lead).to receive(:invite_user)
        allow_any_instance_of(Client).to receive(:invite_user)
      end

      it 'converts client, if it is Lead in fact' do
        subject.client = create(:lead)

        expect(subject.client).to receive(:convert!)
        subject.save!
      end
    end

    describe '#save_products' do
      let(:selected_products_count) { 2 }

      before(:each) do
        allow(subject).to receive(:set_number)
        create_list(:deal_product, selected_products_count + 2, deal: subject)

        subject.selected_products =
          Array.new(selected_products_count) { attributes_for(:deal_product) }
      end

      it 'deletes all deal_products' do
        expect(subject.deal_products).to receive(:delete_all)
        subject.save!
      end

      it 'saves selected products' do
        subject.save!
        subject.reload

        expect(subject.deal_products.size).to eql(selected_products_count)
      end
    end
  end # end of Callbacks
end
