require 'rails_helper'

RSpec.describe Lead, type: :model do
  subject { build(:lead) }

  describe 'Callbacks' do
    describe '#set_default_status' do
      it 'sets a #lead_new" status before create' do 
        allow(subject).to receive(:invite_user)
        subject.save  
        expect(subject.status).to eql('lead_new')
      end
    end
  end

  describe 'Scopes' do
    describe '.default_scope' do
      it 'returns not deleted leads' do
        allow_any_instance_of(Lead).to receive(:invite_user) 
        allow_any_instance_of(Client).to receive(:invite_user) 

        leads = create_list(:lead, 5) 
        deleted_leads = create_list(:lead, 2, :deleted) 
        not_leads = create_list(:client, 3) 

        expect(Lead.all).to match_array(leads)
      end
    end 
  end

  describe '#convert!' do
    before(:each) { allow(subject).to receive(:invite_user) }

    context 'when already persisted' do
      it 'changes status to "client_new"' do
        subject.save!
        subject.convert!
        expect(subject.status).to eql('client_new')
      end
    end

    context 'anyway' do
      before(:each) { subject.convert! }

      it 'changes type to "Client"' do
        expect(subject.type).to eql('Client')
      end

      it { is_expected.to be_persisted }
    end
  end
end
