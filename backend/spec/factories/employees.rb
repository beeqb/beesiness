FactoryGirl.define do
  factory :employee do
    association :user
    association :company

    sequence(:email) { |n| "employee#{n}@beeqb.com" }
    user_activity_id 1

    trait :in_sales do
      participation_in_sales 'yes'
    end

    factory :manager_of_sales do
      after(:create) do |employee|
        employee
        .user
        .add_role(
          :employee_manager_of_sales_department,
          employee.company,
        )
      end
    end

    factory :head_of_sales do
      after(:create) do |employee|
        employee
        .user
        .add_role(
          :employee_head_of_sales_department,
          employee.company,
        )
      end
    end
  end
end
