FactoryGirl.define do
  factory :lead do
    association :company

    name 'Lead'
    surname 'Leadson'
    address 'Some lead address'
    sequence(:email) { |n| "lead#{n}@beeqb.com" }
    work_phone '32343253'
    work_phone_code_id { CountryPhoneCode.first.id }

    factory :lead_individual do
      cell_phone { '34235434546' }
      cell_phone_code_id { CountryPhoneCode.first.id }
    end

    trait :deleted do
      deleted_at { 7.days.ago }
    end

    trait :lead_new do
      status :lead_new
    end

    trait :lead_in_work do
      status :lead_in_work
    end

    trait :lead_trash do
      status :lead_trash
    end
  end
end
