FactoryGirl.define do
  factory :company do
    association :creator, factory: :user
    name { Faker::Company.name.gsub(/[^a-zA-Zа-яА-Я0-9\s]/, '') }
    address 'Some company address'
    sequence(:email) { |n| "company#{n}@beeqb.com" }
    vat { Faker::Number.decimal(1) }
    inn { Faker::Number.number(8) }
    currency { [:rub, :usd, :eur].sample }
    phone '32343253'
    phone_code_id { CountryPhoneCode.first.id }

    factory :company_with_deals do
      transient do
        deals_count 3
      end

      after(:create) do |company, evaluator|
        create_list(
          :deal,
          evaluator.deals_count,
          company: company,
        )
      end
    end
  end
end
