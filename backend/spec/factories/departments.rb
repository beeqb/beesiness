FactoryGirl.define do
  factory :department do
    company
    name 'Department'
    color 'cdcdcd'
  end
end
