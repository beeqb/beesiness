FactoryGirl.define do
  factory :supplier do
    association :company

    name 'Supplier'
    surname 'Supplierson'
    address 'Some supplier address'
    sequence(:email) { |n| "supplier#{n}@beeqb.com" }
    work_phone '32343253'
    work_phone_code_id { CountryPhoneCode.first.id }

    trait :supplier_new do
      status :supplier_new
    end

    trait :supplier_current do
      status :supplier_current
    end

    trait :supplier_permanent do
      status :supplier_permanent
    end

    trait :supplier_special do
      status :supplier_special
    end

    trait :supplier_no do
      status :supplier_no
    end
  end
end
