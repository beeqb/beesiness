# Фабрика задач (незакончена)
FactoryGirl.define do
  factory :task do
    association :executor, factory: :employee
    association :assistant, factory: :employee
    association :creator, factory: :user

    title { Faker::Lorem.sentence }
    start_date { Faker::Date.backward(7) }

    status :new
  end
end
