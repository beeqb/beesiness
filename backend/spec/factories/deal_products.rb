FactoryGirl.define do
  factory :deal_product do
    association :deal
    association :product

    count 1
  end
end
