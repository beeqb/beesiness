FactoryGirl.define do
  factory :product_category do
    company
    name 'Category'
    color 'fcfcfc'
  end
end
