FactoryGirl.define do
  factory :sales_plan do
    association :company

    month { Time.now.month }
    year { Time.now.year }
    kind { [0, 1].sample }
    sum 1000

    trait :by_sum do
      kind 'by_sum'
    end

    trait :by_quantity do
      kind 'by_quantity'
    end
  end
end
