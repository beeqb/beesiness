FactoryGirl.define do
  factory :cash_book_record do
    company    
    association :creator, factory: :employee

    bill_kind 'cash'

    category 'income'

    currency 'rub'
    amount 2500
    balance 10000

    trait :income do
      category 'income'
    end

    trait :outcome do
      category 'outcome'
    end

    trait :loan do
      category 'loan'
    end

    trait :loan_return do
      category 'loan_return'
    end
  end
end
