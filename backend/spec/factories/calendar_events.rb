FactoryGirl.define do
  factory :calendar_event do
    title 'Some important event'
    
    trait :recurring_monthly do
      recurring :monthly
    end
  end
end
