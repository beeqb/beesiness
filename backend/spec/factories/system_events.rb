FactoryGirl.define do
  factory :system_event do
    company
    user

    general_description 'GENERAL DESCRIPTION'
    local_description 'LOCAL DESCRIPTION'
    kind 'client:created'

    initialize_with { new(kind: kind) }
  end
end
