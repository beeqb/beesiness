FactoryGirl.define do
  factory :client do
    association :company
    association :manager, factory: :employee
    association :second_manager, factory: :employee

    name 'Client'
    surname 'Clientson'
    address 'Some client address'
    sequence(:email) { |n| "client#{n}@beeqb.com" }
    work_phone '32343253'
    work_phone_code_id { CountryPhoneCode.first.id }

    factory :individual_client do
      legal_form 'individual'
      cell_phone '32343253'
      cell_phone_code_id { CountryPhoneCode.first.id }
    end

    factory :legal_client do
      legal_form 'legal_entity'
      legal_entity_name 'Some Company'
      legal_entity_email 'some@some.co'
      address 'Some legal client address'
    end

    trait :client_new do
      status :client_new
    end

    trait :client_current do
      status :client_current
    end

    trait :client_permanent do
      status :client_permanent
    end

    trait :client_special do
      status :client_special
    end

    trait :client_no do
      status :client_no
    end
  end
end
