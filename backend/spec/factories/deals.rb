FactoryGirl.define do
  factory :deal do
    association :company
    association :manager, factory: :employee

    started_at { Time.now }
    visibility { [:all, :me].sample }
    legal_form { [:legal_entity, :individual].sample }
    total_sum 100

    factory :deal_with_products do
      transient do
        products_count 3
      end

      after(:create) do |deal, evaluator|
        create_list(
          :deal_product,
          evaluator.products_count,
          deal: deal,
        )
      end
    end

    trait :in_progress do
      status :in_progress
    end

    trait :contract_conclusion do
      status :contract_conclusion
    end

    trait :contract_signed do
      status :contract_signed
    end

    trait :won do
      status :won
    end

    trait :payed do
      status :payed
      payed_at { 1.day.ago }
    end

    trait :lost do
      status :lost
    end

    trait :returned do
      status :returned
    end
  end
end
