FactoryGirl.define do
  factory :product do
    name { Faker::Lorem.word }
    activity 'yes'
    barcode '34135677892'
    category { ProductCategory.first }

    trait :recommended do
      is_recommended true
    end
  end
end
