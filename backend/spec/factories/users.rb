FactoryGirl.define do
  factory :user do
    name { Faker::Name.first_name }
    surname { Faker::Name.last_name }
    sequence(:email) { |n| "user#{n}@beeqb.com" }
    password 'longsecret'
  end
end
