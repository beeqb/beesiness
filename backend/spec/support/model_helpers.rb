module ModelHelpers
  def expect_fields_required(*fields)
    fields.each do |field|
      it "requires #{field}" do
        association = subject.class.reflect_on_association(field)

        if not association or association.macro == :belongs_to
          subject.send("#{field}=", nil)
        else
          subject.send("#{field}=", [])
        end

        expect(subject).to be_invalid
      end  
    end
  end
end
