module AbilityHelpers
  def roles
    Role
      .all
      .distinct
      .pluck(:name)
      .select { |name| name =~ /^employee_.+$/ }
      .map { |name| name.sub('employee_', '') }
  end

  # Generates test cases for checking abilities on
  # particular roles. It helps avoid a lot of
  # repititive code in specs. It assumes exclusive ability, i.e.
  # if role1, role2 should be able to :do_stuff, then all other
  # roles should not be able to do that.
  #
  # Here's conventions you must follow:
  #
  # 1. Use it inside describe/context block with `let` declaration:
  #
  #   context 'Some Things' do
  #     let(:some_thing) {...}
  #     ...
  #
  #     assert_ability :do_stuff, %w(director hr ...)
  #   end
  #
  # Note correspondence between context name and `let` parameter
  # 
  # 2. let(:company) and let(:user) must be declared somewhere in access :)
  #
  # 3. Omit 'employee_' prefix in role names
  #
  #   :employee_director -> :director
  #
  def assert_ability(action, roles_options, focus = false)
    allowed_roles = process_allowed_roles(roles_options)
    humanized_action = action.to_s.humanize(capitalize: false)

    # Takes entity name from surrounding describe/context call.
    # This name must correspond to `let` declaration, so
    # entity for ability checking can be retrieved
    entity_name = description.downcase.singularize.gsub(' ', '_')

    metadata = if focus then {focus: true} else {} end

    describe action, metadata do

      allowed_roles.each do |role_name|
        it "#{role_name.humanize} can #{humanized_action}" do
          entity = send(entity_name)

          user.add_role(:user)
          allow_any_instance_of(Ability).to(
            receive(:roles_in_companies)
            .and_return({:"employee_#{role_name}" => [company.id]})
          )
          ability = Ability.new(user)

          expect(ability).to be_able_to(action, entity)
        end
      end

      (roles - allowed_roles).each do |role_name|
        it "#{role_name.humanize} cannot #{humanized_action}" do

          entity = send(entity_name)

          allow_any_instance_of(Ability).to(
            receive(:roles_in_companies)
            .and_return({:"employee_#{role_name}" => [company.id]})
          )
          user.add_role(:user)
          ability = Ability.new(user)

          expect(ability).not_to be_able_to(action, entity)
        end
      end

    end
  end

  private

  def process_allowed_roles(allowed_roles)
    if allowed_roles.is_a?(Hash) and allowed_roles[:except]
      roles - Array.wrap(allowed_roles[:except])
    elsif allowed_roles == '*'
      roles
    else
      Array.wrap(allowed_roles)
    end
  end
end
