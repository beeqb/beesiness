RSpec.shared_examples 'visible_for' do |parameter|
  klass = described_class.to_s.underscore

  let(:employee) { create(:employee) }

  it "returns #{klass.pluralize} visible for all" do
    entities = create_list(klass, 3, visibility: :all)
    expect(described_class.visible_for(employee)).to match_array(entities)
  end

  context 'Employee given' do
    it "returns #{klass.pluralize} visible for manager only, that this employee manage" do
      entities = create_list(klass, 3, visibility: :me, manager: employee)

      expect(described_class.visible_for(employee)).to match_array(entities)
    end

    it "returns #{klass.pluralize} visible for manager only "\
      'where this employee is second manager' do
      entities = create_list(
        klass,
        3,
        visibility: :me,
        second_manager: employee
      )

      expect(described_class.visible_for(employee)).to match_array(entities)
    end
  end

  context 'User given' do
    let(:user) { create(:user) }
    let(:employee) { create(:employee, user: user) }

    it "returns #{klass.pluralize}, that this user manage" do
      entities = create_list(klass, 3, visibility: :me, manager: employee)

      expect(described_class.visible_for(user)).to match_array(entities)
    end

    it "returns #{klass.pluralize} where this user is second manager" do
      entities = create_list(
        klass,
        3,
        visibility: :me,
        second_manager: employee
      )

      expect(described_class.visible_for(user)).to match_array(entities)
    end
  end
end
