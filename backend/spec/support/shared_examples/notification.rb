RSpec.shared_examples 'notification' do |parameter|
  describe '.for' do
    it 'creates instances for given users' do
      users = create_list(:user, 2)
      user_ids = users.pluck(:id)
      
      begin
        subject_class = described_class.subject 
        notification_subject = create(subject_class.to_s.underscore.to_sym)
        subject_id = notification_subject.id
      rescue
        subject_id = 3
      end

      described_class.for(user_ids, subject_id)

      expect(described_class.count).to eql(user_ids.size)

      user_ids.each do |user_id|
        existing = described_class.exists?(
          user_id: user_id,
          subject_id: subject_id
        )
        expect(existing).to be true
      end
    end
  end
end
