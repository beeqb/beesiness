module ServiceHelpers
  def day_in_current_month(day)
    month = Date.today.month
    year = Date.today.year

    Time.new(year, month, day, 10)
  end

  def dates_in_range(count, range_or_keyword)
    if range_or_keyword.is_a? Range
      range = range_or_keyword
    else
      range = case range_or_keyword
              when :current_month
                now = Time.now
                now.beginning_of_month..now.end_of_month
              end
    end

    count.times.map do
      Time.at((range.end.to_f - range.begin.to_f) * rand + range.begin.to_f)
    end
  end

  def seed_for_sales_report
    @categories = create_categories
    @products = create_products
    create_deals
  end

  def expected_actual_sales_by_categories_monthly
    {
      1 => {
        @categories[0].id => {
          money: 1000.to_d.round(18),
          quantity: 2,
          money_up: true,
          quantity_up: true,
          money_deviation: nil,
          quantity_deviation: nil,
          average_price: 1000.to_d / 2,
          average_price_deviation_percent: nil,
          average_price_deviation: nil,
          average_price_up: true,
        },
        @categories[1].id => {
          money: 1500.to_d.round(18),
          quantity: 1,
          money_up: true,
          quantity_up: true,
          money_deviation: nil,
          quantity_deviation: nil,
          average_price: 1500.to_d.round(36),
          average_price_deviation_percent: nil,
          average_price_deviation: nil,
          average_price_up: true,
        },
        @categories[2].id => {
          money: 1000.to_d.round(18),
          quantity: 4,
          money_up: true,
          quantity_up: true,
          money_deviation: nil,
          quantity_deviation: nil,
          average_price: 1000.to_d / 4,
          average_price_deviation_percent: nil,
          average_price_deviation: nil,
          average_price_up: true,
        },
      },
      2 => {
        @categories[1].id => {
          money: 1790.to_d.round(18),
          quantity: 1,
          money_up: true,
          quantity_up: true,
          money_deviation: nil,
          quantity_deviation: nil,
          average_price: 1790.to_d.round(36),
          average_price_deviation_percent: nil,
          average_price_deviation: nil,
          average_price_up: true,
        },
        @categories[2].id => {
          money: 1790.to_d.round(18),
          quantity: 6,
          money_up: true,
          quantity_up: true,
          money_deviation: nil,
          quantity_deviation: nil,
          average_price: 1790.to_d / 6,
          average_price_deviation_percent: nil,
          average_price_deviation: nil,
          average_price_up: true,
        },
      },
      3 => {
        @categories[0].id => {
          money: 2460.to_d.round(18),
          quantity: 1,
          money_up: true,
          quantity_up: true,
          money_deviation: nil,
          quantity_deviation: nil,
          average_price: 2460.to_d.round(36),
          average_price_deviation_percent: nil,
          average_price_deviation: nil,
          average_price_up: true,
        },
        @categories[1].id => {
          money: 2460.to_d.round(18),
          quantity: 1,
          money_up: true,
          quantity_up: true,
          money_deviation: nil,
          quantity_deviation: nil,
          average_price: 2460.to_d.round(36),
          average_price_deviation_percent: nil,
          average_price_deviation: nil,
          average_price_up: true,
        },
      },
    }
  end

  private

  def create_categories
    [
      create(:product_category, company: @company, name: 'Books'),
      create(:product_category, company: @company, name: 'Electronics'),
      create(:product_category, company: @company, name: 'Tableware'),
    ]
  end

  def create_products
    books, electronics, tableware = @categories
    common_attributes = {company: @company}
    books_attributes = common_attributes.merge(categories: [books.id])
    electronics_attributes =
      common_attributes.merge(categories: [electronics.id])
    tableware_attributes = common_attributes.merge(categories: [tableware.id])

    {
      books: [
        create(:product, books_attributes.merge(name: 'Moby Dick')),
        create(:product, books_attributes.merge(name: 'Sherlock Holmes')),
        create(:product, books_attributes.merge(name: 'LOTR')),
      ],

      electronics: [
        create(:product, electronics_attributes.merge(name: 'Keyboard')),
        create(:product, electronics_attributes.merge(name: 'Mouse')),
        create(
          :product,
          company: @company,
          categories: [electronics.id, books.id],
          name: 'Kindle'
        ),
      ],

      tableware: [
        create(:product, tableware_attributes.merge(name: 'Plate')),
        create(:product, tableware_attributes.merge(name: 'Knife')),
        create(:product, tableware_attributes.merge(name: 'Spoon')),
        create(:product, tableware_attributes.merge(name: 'Glass')),
      ],
    }
  end

  def create_deals
    attributes = { company: @company, visibility: :all }  
    moby, sherlock, lotr = @products[:books]
    keyboard, mouse, kindle = @products[:electronics]
    plate, knife, spoon, glass = @products[:tableware]
    date = Time.new(Date.today.year, 1, 1).utc
    dates = [date, date + 1, date.next_month, date.end_of_quarter]

    deal = create(
      :deal,
      attributes.merge(created_at: dates[0], total_sum: 1000)
    )
    create(:deal_product, deal: deal, product: moby)
    create(:deal_product, deal: deal, product: plate, count: 4)
    create(:deal_product, deal: deal, product: lotr)

    deal = create(
      :deal,
      attributes.merge(created_at: dates[1], total_sum: 1500)
    )
    create(:deal_product, deal: deal, product: keyboard)

    deal = create(
      :deal,
      attributes.merge(created_at: dates[2], total_sum: 1790)
    )
    create(:deal_product, deal: deal, product: mouse)
    create(:deal_product, deal: deal, product: glass, count: 6)

    deal = create(
      :deal,
      attributes.merge(created_at: dates[3], total_sum: 2460)
    )
    create(:deal_product, deal: deal, product: kindle)
  end
end
