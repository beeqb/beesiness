require 'rails_helper'

RSpec.describe EmployeesController, :type => :controller do
  describe '#dismiss' do
    let(:company) { create(:company) }
    let(:employee) { create(:employee, company: company) }
    let(:user) { create(:user) }

    before(:each) do
      company.add_user(user, :employee_director)
      sign_in(user)
    end

    it 'transfers tasks' do
      create(:task, executor: employee, company: company)
      create(:task, executor: employee, company: company, status: :done)

      post(:dismiss, params: {company_id: company.id, id: employee.id})

      employee.reload

      # finished tasks don't transfer
      expect(Task.assigned_to_employee(employee).size).to eql(1)
      expect(Task.assigned_to(user).size).to eql(1)
    end

    it 'dismisses employee' do
      post(:dismiss, params: {company_id: company.id, id: employee.id})
      employee.reload

      expect(employee.user_activity.name).to eql('Уволен')
      expect(employee.user).to have_role :employee_dismissed, employee.company
    end
  end
end
