require 'rails_helper'

RSpec.describe CompaniesController, type: :controller do
  describe '#stream' do
    let(:company) { create(:company) }
    let(:user) { create(:user) }

    before(:each) do
      sign_in(user)
      company.add_user(user, :employee_director)
    end

    it 'returns empty if there is none events' do
      get(:stream, params: {id: company.id})

      body = JSON.parse(response.body)

      expect(body['entities']).to be_empty
      expect(body['total_pages']).to eq(0)
    end

    it 'returns paginated output' do
      allow_any_instance_of(SystemEvent).to receive(:set_company)
      create_list(:system_event, 25, company: company)

      get(:stream, params: {id: company.id, page: 3})

      body = JSON.parse(response.body)

      expect(body['entities'].size).to eq(5)
      expect(body['total_pages']).to eq(3)
    end
  end

end
