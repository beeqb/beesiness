# BeeQB
=======

Многопользовательская CRM

Указания по развёртыванию:
==========================

  1. Склонировать данный репозиторий

  2. Выполнить команду `make up`

  3. Выполнить `docker exec backend.beeqb.local rails db:seed`


Использование:
=============

  1. Проект доступен по адресу `http://localhost`

  2. Почтовый сервер доступен по адресу `http://localhost:1080`


Кодирование:
=============

  1. На проекте используется `Rubocop`. Стиль кодирования определяется им. 
  
  2. Отдельный контейнер `rubocop.backend.beeqb.local` отслеживает изменения в 
     файлах проекта (`/backend/app`) и запускает `rubocop` без автокоррекции.
     В логах контейнера можно читать результат проверки редактируемого файла.
     
  3. Можно использовать автокоррекцию исходного кода, запустив 
     `docker exec rubocop.backend.beeqb.local rubocop -a $file_path$`
     или использовать горячие клавиши для быстрого запуска этой команды для 
     редактируемого файла.

Тестирование:
=============

* Сперва нужно наполнить тестовую БД данными из `seeds.rb`:

    `docker exec backend.beeqb.local bin/rake db:seed RAILS_ENV=test`

* Для запуска юнит-тестов используйте команду

    `docker exec backend.beeqb.local rspec`

    Также для запуска тестов настроен [Guard](http://guardgem.org/).
    При изменении файлов в `backend/spec`, `backend/app/models/` и `backend/app/controllers` соответствующие тесты запустятся автоматически.
    
    Вывод можно посмотреть в логах контейнера `test.beeqb.local`:

    `docker logs --follow test.beeqb.local`

    или

    `docker attach test.beeqb.local` (эта команда не будет показывать старые логи, рекомендуется ознакомиться с документацией)

* Для запуска приемочных тестов (Capybara) используйте

    `docker exec capybara.beeqb.local xvfb-run -a rspec --require ./spec_helper.rb spec/features`

    Также для запуска интеграционных тестов настроен [Guard](http://guardgem.org/), 
    при изменении файлов в `test/spec` измененный тест будет запущен автоматически.      
    
    Вывод можно посмотреть в логах контейнера `capybara.beeqb.local`:

    `docker logs --follow capybara.beeqb.local`

    или

    `docker attach capybara.beeqb.local` (эта команда не будет показывать старые логи, рекомендуется ознакомиться с документацией)