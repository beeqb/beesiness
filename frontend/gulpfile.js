process.on('SIGTERM', function () {
    process.exit();
});

var fs = require('fs');
var gulp = require('gulp');
var changed = require('gulp-changed');
var util = require('gulp-util');
var debug = require('gulp-debug');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var merge = require('gulp-merge');
var _ = require('underscore');
var runSequence = require('run-sequence');
var ngAnnotate = require('gulp-ng-annotate');
var rootPath = '/app'
//var rootPath = '~/Douments/Workspace/beeqb-webapp/frontend'

// Release part

function getReleaseFileName(prefix, postfix) {
    var parts = [prefix, process.env.VERSION, postfix];
    var result = [];
    for (var partIndex in parts) {
        if (!parts.hasOwnProperty(partIndex))
            continue;
        var part = parts[partIndex];
        if (part) {
            result.push(part);
        }
    }

    return result.join('.');
}

function createCompileViews(isPretty) {
    var slim = require('gulp-slim');

    return gulp
        .src(['views/**/*.slim', '!views/index.slim'])
        .pipe(changed('build/views', {extension: '.html'}))
        .pipe(slim({
            pretty: isPretty,
            require: rootPath + '/config/slim.rb'
        }))
        .on('error', function (message) {
            util.log(util.colors.red(message));
            this.emit('end');
        })
        .pipe(debug({title: 'render-views:'}))
        .pipe(gulp.dest('build/views'));
}

function getAssets() {
    var assetsContents = fs.readFileSync('config/assets.json', 'utf8');
    var assets = JSON.parse(assetsContents);

    assets.js = expandGlobs(assets.js);
    assets.css = expandGlobs(assets.css);

    return assets;
}

function preserveFolders(files) {
    return _.collect(files, function (file) {
        return '{_,' + file + '}';
    })
}

function createCopyCssStream(release) {
    var css = _.map(getAssets().css, function (file) {
        return 'build/' + file
    });

    var task = gulp
        .src(preserveFolders(css))
        .pipe(sourcemaps.init());

    if (!release) {
        task = task.pipe(changed('build'))
    }

    task = task.pipe(debug({title: 'copy-css:'}));

    return task;
}

function createCopyJsStream(release) {
    var js = _.map(getAssets().js, function (file) {
        return 'build/' + file
    });

    var task = gulp
        .src(preserveFolders(js))
        .pipe(sourcemaps.init());

    if (!release) {
        task = task.pipe(changed('build', {extension: '.js'}))
    }

    task = task.pipe(debug({title: 'copy-js:'}));

    return task;
}

function expandGlobs(files) {
    var result = [];

    for (var fileIndex in files) {
        if (!files.hasOwnProperty(fileIndex))
            continue;
        var file = files[fileIndex];
        if (file.indexOf('*') === -1) {
            result.push(file);
            continue;
        }

        var list = require('glob').sync(file);
        list.sort();

        for (var prop in list) {
            if (!list.hasOwnProperty(prop))
                continue;
            if (list[prop].search(/\.\w+$/) === -1) {
                continue;
            }

            var target = list[prop].replace(/\.\w+$/, '.js');

            if (result.indexOf(target) === -1) {
                result.push(target);
            }
        }
    }

    return result;
}

//index

gulp.task('render-index', function () {
    var slim = require('gulp-slim');
    var template = require('gulp-template');
    var contents = fs.readFileSync('config/assets.json', 'utf8');
    var assets = JSON.parse(contents);

    assets.css = expandGlobs(assets.css);
    assets.js = expandGlobs(assets.js);

    return gulp
        .src(['views/index.slim', 'views/plain.slim'])
        .pipe(template({assets: assets}))
        .pipe(slim({
            pretty: true,
            require: rootPath + '/config/slim.rb'
        }))
        .on('error', function (message) {
            util.log(util.colors.red(message));
            this.emit('end');
        })
        .pipe(debug({title: 'render-index:'}))
        .pipe(gulp.dest('build/views'));
});

gulp.task('release-index', function () {
    var slim = require('gulp-slim');
    var template = require('gulp-template');
    var contents = fs.readFileSync('config/assets.json', 'utf8');
    var assets = JSON.parse(contents);

    return gulp
        .src('views/index.slim')
        .pipe(template({
            assets: {
                js: [getReleaseFileName('js/app', 'js')],
                css: [getReleaseFileName('css/app', 'css')]
            }
        }))
        .on('error', function (message) {
            util.log(util.colors.red(message));
            this.emit('end');
        })
        .pipe(slim({
            pretty: true,
            require: rootPath + '/config/slim.rb'
        }))
        .pipe(debug({title: 'release-index:'}))
        .pipe(gulp.dest('build/views'));
});

// views

gulp.task('render-views', function () {
    return createCompileViews(true);
});

// css

gulp.task('compile-css', function () {
    var autoprefixer = require('gulp-autoprefixer');
    var sass = require('gulp-sass');

    return gulp
        .src(['css/**/*'])
        .pipe(changed('build/css', {extension: '.css'}))
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(debug({title: 'compile-css:'}))
        .pipe(gulp.dest('build/css'));
});

// img

gulp.task('copy-img', function () {
    return gulp
        .src(['img/**/*'])
        .pipe(changed('build/img'))
        .pipe(debug({title: 'copy-img:'}))
        .pipe(gulp.dest('build/img'));
});

// theme css

gulp.task('copy-vendor-css', function () {
    return gulp
        .src([
            'theme/src/css/app.css',
            'theme/src/css/animate.css',
            'theme/src/css/simple-line-icons.css',

            'node_modules/bootstrap/dist/css/bootstrap.css',
            'node_modules/font-awesome/css/font-awesome.css',
            'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-csp.css',
            'node_modules/angular-xeditable/dist/css/xeditable.css',
            'node_modules/ion-rangeslider/css/ion.rangeSlider.css',
            'node_modules/ion-rangeslider/css/ion.rangeSlider.skinHTML5.css',
            'node_modules/angucomplete-alt/angucomplete-alt.css',
            'node_modules/ui-select/dist/select.css',
            'node_modules/angular-ui-tree/dist/angular-ui-tree.css',

            'js/vendor/angular-slider/angular-slider.css',

            'node_modules/bootstrap-daterangepicker/daterangepicker.css',

            'node_modules/angularjs-toaster/toaster.css',

            'node_modules/textangular/dist/textAngular.css',
            'node_modules/c3/c3.css',

        ])
        .pipe(changed('build/css/vendor/'))
        .pipe(debug({title: 'copy-css:'}))
        .pipe(gulp.dest('build/css'));
});

// theme js

gulp.task('copy-theme-js', function () {
    return gulp
        .src([
            './theme/src/js/directives/*.js',
            './theme/src/js/services/ui-load.js',
            './theme/src/js/main.js',
            './theme/html/js/ui-jp.config.js',
            './theme/html/js/ui-jp.js',
            './theme/libs/jquery/jquery.easy-pie-chart/dist/jquery.easypiechart.fill.js',
        ])
        .pipe(changed('build/js/theme'))
        .pipe(debug({title: 'copy-theme-js:'}))
        .pipe(gulp.dest('build/js/vendor/theme'));
});

gulp.task('copy-theme-css', function () {
    return gulp
        .src([
            './theme/libs/jquery/fullcalendar/dist/fullcalendar.css',
            './theme/libs/jquery/fullcalendar/dist/fullcalendar.theme.css',
        ])
        .pipe(changed('build/css'))
        .pipe(debug({title: 'copy-theme-js:'}))
        .pipe(gulp.dest('build/css'));
});

// fonts

gulp.task('copy-fonts', function () {
    return gulp
        .src(['fonts/**/*'])
        .pipe(changed('build/fonts'))
        .pipe(debug({title: 'copy-fonts:'}))
        .pipe(gulp.dest('build/fonts'));
});

gulp.task('copy-vendor-fonts', function () {
    return gulp
        .src([
            'theme/src/fonts/Simple-Line-Icons.{svg,eot,ttf,woff}',
            'node_modules/bootstrap/dist/fonts/*',
            'node_modules/font-awesome/fonts/*',
            'node_modules/source-sans-pro/{EOT,OTF,TTF,WOFF}/**/*',
            'node_modules/source-sans-pro/source-sans-pro.css'
        ])
        .pipe(changed('build/fonts'))
        .pipe(debug({title: 'copy-vendor:'}))
        .pipe(gulp.dest('build/fonts'));
});

// js

gulp.task('compile-coffee', function () {
    var coffee = require('gulp-coffee');
    var sourcemaps = require('gulp-sourcemaps');

    return gulp
        .src(['js/**/*.coffee'])
        .pipe(sourcemaps.init())
        .pipe(changed('build/js', {extension: '.js'}))
        .pipe(coffee())
        .on('error', function (message) {
            util.log(message);
            this.end();
        })
        .pipe(sourcemaps.write())
        .pipe(debug({title: 'compile-coffee:'}))
        .pipe(gulp.dest('build/js'));
});

gulp.task('copy-vendor-js', function () {
    return gulp
        .src([
            'node_modules/angular/angular.js',
            'node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js',
            'node_modules/angular-route/angular-route.js',
            'node_modules/angular-resource/angular-resource.js',
            'node_modules/angular-cookies/angular-cookies.js',
            'node_modules/angular-cookie/angular-cookie.js',
            'node_modules/angular-modal-service/dst/angular-modal-service.js',
            'node_modules/angular-class-coffee/build/angular-class.js',
            'node_modules/angular-sanitize/angular-sanitize.js',
            'node_modules/angular-google-maps/dist/angular-google-maps.js',
            'node_modules/angular-simple-logger/dist/angular-simple-logger.js',
            'node_modules/angular-local-storage/dist/angular-local-storage.js',
            'node_modules/angular-touch/angular-touch.js',
            'node_modules/underscore/underscore.js',
            'node_modules/jquery/dist/jquery.js',
            'node_modules/{moment,_}/moment.js',
            'node_modules/{moment,_}/locale/ru.js',
            'node_modules/bootstrap/dist/js/bootstrap.js',
            'node_modules/bootstrap-filestyle/src/bootstrap-filestyle.js',
            'node_modules/oclazyload/dist/ocLazyLoad.js',
            'node_modules/ng-token-auth/dist/ng-token-auth.js',
            'node_modules/screenfull/dist/screenfull.js',
            'node_modules/angular-translate/dist/angular-translate.js',
            'node_modules/easy-pie-chart/dist/angular.easypiechart.js',
            'node_modules/ng-file-upload/dist/ng-file-upload-shim.js',
            'node_modules/ng-file-upload/dist/ng-file-upload.js',
            'node_modules/jquery-sparkline/jquery.sparkline.js',
            'node_modules/lodash/lodash.js',
            'node_modules/ion-rangeslider/js/ion.rangeSlider.js',

            'node_modules/jquery-flot/jquery.flot.js',
            'node_modules/jquery-flot/jquery.flot.pie.js',
            'node_modules/jquery-flot/jquery.flot.resize.js',
            'node_modules/jquery.flot.tooltip/js/jquery.flot.tooltip.js',
            'node_modules/angular-xeditable/dist/js/xeditable.js',
            'node_modules/angular-ui-mask/dist/mask.js',
            'node_modules/angular-ui-scroll/dist/ui-scroll.js',
            'node_modules/angularjs-geolocation/src/geolocation.js',
            'node_modules/angucomplete-alt/dist/angucomplete-alt.min.js',

            'node_modules/accounting/accounting.min.js',

            'node_modules/ng-infinite-scroll/build/ng-infinite-scroll.js',

            'node_modules/ui-select/dist/select.js',

            'js/vendor/ulogin.js',
            // Этот файл изменен, обновлять и удалять
            // с крайней осторожностью!
            // Подробности тут:
            // https://idfly-team.atlassian.net/browse/BQB-55?focusedCommentId=11111&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel#comment-11111
            'js/vendor/angular-slider/angular-slider.js',

            'node_modules/angular-ui-calendar/src/calendar.js',
            'node_modules/fullcalendar/dist/fullcalendar.min.js',

            'node_modules/angular-ui-tree/dist/angular-ui-tree.js',

            'node_modules/bootstrap-daterangepicker/daterangepicker.js',

            'node_modules/angularjs-toaster/toaster.js',

            'node_modules/textangular/dist/textAngular-rangy.min.js',
            'node_modules/textangular/dist/textAngular-sanitize.js',
            'node_modules/textangular/dist/textAngular.min.js',
            'node_modules/c3/c3.js',
            'node_modules/d3/d3.js',
            "js/app/facebook_pixel.js",
            "js/app/google_analytics_counter.js",
            "js/app/yandex_metrica_counter.js",
        ])
        .pipe(changed('build/js/vendor'))
        .pipe(debug({title: 'copy-vendor:'}))
        .pipe(gulp.dest('build/js/vendor'));
});


gulp.task('release-css', function () {
    var gzip = require('gulp-gzip');

    return merge(createCopyCssStream(true))
        .pipe(concat(getReleaseFileName('app', 'css')))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('build/css'))
        .pipe(gzip())
        .pipe(gulp.dest('build/css'));
});

gulp.task('release-js', function () {
    var uglify = require('gulp-uglify');
    var gzip = require('gulp-gzip');
    var templateCache = require('gulp-angular-templatecache');

    var releaseViews = createCompileViews(false)
        .pipe(templateCache({
            module: 'app',
            transformUrl: function (url) {
                return 'assets/views/' + url
            }
        }));

    return merge(
        createCopyJsStream(true),
        releaseViews
    )
        .pipe(ngAnnotate())
        .pipe(uglify({mangle: false}))
        .pipe(concat(getReleaseFileName('app', 'js')))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('build/js/'))
        .pipe(gzip())
        .pipe(gulp.dest('build/js/'));
});


gulp.task('release', function () {
    runSequence(
        [
            'compile-coffee',
            'compile-css',
            'copy-theme-js',
            'copy-vendor-js',
            'copy-vendor-fonts',
            'copy-vendor-css',
            'copy-img',
            'copy-fonts'
        ],
        [
            'release-css',
            'release-js',
            'release-index'
        ]
    )
});


// default

gulp.task('default', [
    'compile-coffee',
    'compile-css',

    'render-views',
    'render-index',

    'copy-theme-js',
    'copy-theme-css',
    'copy-vendor-js',
    'copy-vendor-fonts',
    'copy-vendor-css',

    'copy-img',
    'copy-fonts'
]);

gulp.task('compile-and-render', [
	'compile-coffee',
	'compile-css',

	'render-views',
	'render-index'
]);

gulp.task('_watch', function () {
    gulp.watch('js/**/*', ['compile-coffee', 'render-index']);
    gulp.watch('css/**/*', ['compile-css', 'render-index']);
    gulp.watch('fonts/**/*', ['copy-fonts']);
    gulp.watch('img/**/*', ['copy-img']);

    gulp.watch(
        ['views/**/*', '!views/index.slim'],
        ['render-views']
    );

    gulp.watch(
        ['views/index.slim', 'config/assets.json'],
        ['render-index']
    );
});

gulp.task('watch', ['default', '_watch']);
