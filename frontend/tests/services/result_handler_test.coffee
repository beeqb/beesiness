describe 'result handler', () ->

  beforeEach ->
    instance = new App.Util.ResultHandler()
    @scope = {}
    @message = 'MESSAGE'
    @callback = sinon.stub()
    @handler = instance.handler(@scope, @message, @callback)

  describe 'handler', ->

    it 'sets data to scope', ->
      @handler(data: 'RESULT')
      expect(@scope.result).toEqual('RESULT')

    it 'sets error if status greater than 300', ->
      @handler(status: 400)
      expect(@scope.result.error).not.toEqual(undefined)

    it 'sets error if success is false', ->
      @handler(data: {success: false})
      expect(@scope.result.error).not.toEqual(undefined)

    it 'not sets result if status is 200', ->
      @handler(status: 200)
      expect(@scope.result).toEqual(undefined)

    it 'not sets error if success is true', ->
      @handler(data: {success: true})
      expect(@scope.result.error).toEqual(undefined)

    it 'calls callback', ->
      @handler(data: 'DATA')
      expect(@callback.getCall(0).args).toEqual(['DATA', {data: 'DATA'}])