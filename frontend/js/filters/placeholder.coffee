angular.module('app').filter('placeholder', () ->
  (input, placeholder) ->
    return placeholder unless input
    input
)
