angular.module('app').filter('filterRange', () ->
  return (items, field, range) ->
    _.filter(items, (item) ->
      return item[field] >= range.min && item[field] <= range.max
    )
)
