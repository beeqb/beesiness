angular.module('app').filter('treePath', () ->
  getNodePath = (input, tree) ->
    for node in tree
      if node.id == input
        return [node.name]

      path = getNodePath(input, node.children)

      if path
        path.unshift(node.name)
        return path
    
    return null

  (input, tree) ->
    path = getNodePath(input, tree)
    path.join(' > ')
)
