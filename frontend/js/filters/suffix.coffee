angular.module('app').filter('suffix', () ->
  (input, suffix) ->
    return if !input && input != 0

    input + suffix
)
