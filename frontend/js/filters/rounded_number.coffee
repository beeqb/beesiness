app.filter('roundedNumber', () ->
  (input, decimalCount = 1) ->
    return unless input

    number = parseFloat(input)

    return input if _.isNaN(number)

    return number if number % 1 is 0 # число целое

    number.toFixed(decimalCount).replace(/\.0+\b/, '')
)
