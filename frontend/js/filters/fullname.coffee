angular.module('app').filter('fullname', () ->
  (input) ->
    return '' unless input

    return input.name unless input.surname

    input.name + ' ' + input.surname
)
