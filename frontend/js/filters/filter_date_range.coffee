angular.module('app').filter('filterDateRange', () ->
  return (items, field, range) ->
    _.filter(items, (item) ->

      return true if range.min == null and range.max == null

      date = Number(item[field])
      min = Number(range.min)
      max = Number(range.max)

      return (min == 0 || date >= min) && (date <= max || max == 0)
    )
)
