angular.module('app').filter('unsafe', ($sce) ->
  (input) ->
    $sce.trustAsHtml(input)
)
