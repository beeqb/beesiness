angular.module('app').filter('statusColor', () ->
  (input, statuses) ->
    id = if angular.isObject(input) then input?.status else input

    return _.findWhere(statuses, {id: id})?.color
)
