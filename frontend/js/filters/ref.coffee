# Takes input as a key value to find a value in dictionary
# It will lookup dictionary by `index` and return value
# of the `field`
#
# Useful for displaying human-readable values of static references
app.filter('ref', () ->
  (input, dictionary, field = 'name', index = 'id') ->
    return unless input

    if not _.isArray(input)
      return _.find(
        dictionary,
        (value) -> String(value[index]) == String(input)
      )?[field]

    predicate = (value) -> value[index] in input
    values = _.select(dictionary, predicate)

    return _.pluck(values, field).join(', ')
)
