angular.module('app').filter('productActivity', ($filter) ->
  (product, activities) ->
    return product.delivery_time if product.activity == 'yes'

    $filter('ref')(product.activity, activities)
)
