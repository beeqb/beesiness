angular.module('app').filter('formatMoney', () ->
  (
    input,
    currency,
    precision = 2,
    thousandSeparator = '.',
    decimalSeparator = ','
  ) ->
    return if !input && input != 0

    formatted = accounting.formatMoney(
      input,
      currency,
      precision,
      thousandSeparator,
      decimalSeparator
    )
    # Если целое число на входе, то убираем десятичные нули
    # 100.00 $ -> 100 $
    trailingZerosRegex = new RegExp(decimalSeparator + '0+\b')

    return formatted.replace(trailingZerosRegex, '')
)
