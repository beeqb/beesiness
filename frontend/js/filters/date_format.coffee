# Форматирует дату-время с условиями:
#  1. Если дата сегодняшняя, отдается
#     только время в формате timeFormat
#  2. Если год совпадает с текущим, то
#     отдается дата в формате dateFormat
#     (года там быть не должно)
#  3. Иначе дата отдается в формате
#     dateFormat + ' YYYY'
#
# Использовать форматы дат moment.js

angular.module('app').filter('dateFormat', () ->
  return ((date, timeFormat = 'HH:mm', dateFormat = 'ddd, D MMM HH:mm') ->
    if !date
      return null

    today = moment()
    date = moment(date)

    if date.isSame(today, 'day')
      return date.format(timeFormat)

    if date.year() == today.year()
      return date.format(dateFormat)

    return date.format(dateFormat + ' YYYY')
  )
)
