angular.module('app').filter('hoursToDays', () ->
  (input) ->
    return unless input

    hours = parseInt(input)

    if _.isNaN(hours)
      console.warn('NaN given as a hours number')
      return

    if hours < 0
      console.warn('negative given as a hours number')
      return

    days = Math.floor(hours / 24)
    remainder = hours % 24

    result = "#{days}д"

    result += " #{remainder}ч" if remainder

    result
)
