angular.module('app').component('salesPlanChart', {
  bindings: {
    salesPlans: '<',
    onChartInit: '&',
    company: '<',
    theme: '@?'
  },
  templateUrl: '/assets/views/directives/sales_plan_chart.html',
  controller: () ->
    salesPlansLoaded = false
    salesPlans = []

    @salesPlanChartData = {
      x: 'x',
      xFormat: '%Y-%m-%d',
      columns: [
        ['x'].concat(_.times(12, _.iteratee((index) => moment().month(index).format("YYYY-MM-DD")))),
        ['1'].concat(_.times(12, _.constant(0)))
      ],
      groups: [[]],
      names: {
        1: 'План продаж в деньгах'
      },
      type: 'line'
    }

    getChartTicksCount = () =>
      return 0 unless salesPlans.length > 0
      lastYear = salesPlans[salesPlans.length - 1].year
      firstYear =  salesPlans[0].year
      return (lastYear - firstYear + 1) * 12

    @salesPlanChart = c3.generate({
      bindto: '#salesPlanChart',
      data: @salesPlanChartData,
      axis: {
        x: {
          type: 'timeseries',
          tick: {
            format: '%Y-%m',
            fit: true,
            culling: false
          }
        }
      },
      zoom: {
        enabled: true,
      }
    })

    this.$onInit = (() =>
      if @onChartInit?
        @onChartInit()
      processSalesPlansData()
    )

    this.$doCheck = (() =>
      return if angular.equals(salesPlans, @salesPlans)
      return unless @salesPlans

      salesPlans = angular.copy(@salesPlans)
      salesPlans = _.sortBy(salesPlans, 'year')
      processSalesPlansData()
    )

    this.$onChanges = ((changes) =>
      if !salesPlansLoaded && !_.isEmpty(changes.salesPlans?.currentValue)
        # избегаем обновления данных, график должен показывать только
        # текущий год/квартал
        salesPlans = angular.copy(@salesPlans)
        salesPlans = _.sortBy(salesPlans, 'year')

        processSalesPlansData()
        salesPlansLoaded = true
    )

    processSalesPlansData = (() =>
      return unless salesPlans && salesPlans.length > 0

      tmpTicksCount = getChartTicksCount()
      timeRange = _.times(tmpTicksCount, _.iteratee((index) =>
        moment([salesPlans[0].year, salesPlans[0].month - 1]).add(index, "month")
      ))

      salesPlanIndex = 0
      timeRangeForSalesPlan = _.times(tmpTicksCount, _.iteratee((index) =>
        res = {
          sum: 0,
          quantity: 0,
          categories: []
        }
        if salesPlanIndex < salesPlans.length && salesPlans[salesPlanIndex].year == timeRange[index].year() && salesPlans[salesPlanIndex].month - 1 == timeRange[index].month()
          if salesPlans[salesPlanIndex].sum?
            res.sum = salesPlans[salesPlanIndex].sum
          if salesPlans[salesPlanIndex].quantity?
            res.quantity = salesPlans[salesPlanIndex].quantity
          if  salesPlans[salesPlanIndex].sales_plan_product_categories?
            res.categories = salesPlans[salesPlanIndex].sales_plan_product_categories
          salesPlanIndex += 1
        return res
      ))
      @salesPlanChartData.columns = []
      @salesPlanChartData.columns[0] = ['x'].concat(_.map(timeRange, (value, index) => value.format("YYYY-MM-DD")))
      @salesPlanChartData.columns[1] = ['1'].concat(_.map(timeRangeForSalesPlan, (value, index) => value.sum))

      @salesPlanChartData.types = {}
      @salesPlanChartData.colors = {}
      @salesPlanChartData.groups = [[]]

      columnIndex = @salesPlanChartData.columns.length
      if @company.product_categories?
        for cat in @company.product_categories
          @salesPlanChartData.columns[columnIndex] = [cat.name].concat(_.map(timeRangeForSalesPlan, ((value, index) =>
            return _.reduce(_.where(value.categories, {product_category_id: cat.id}), ((memo, value) =>
              return memo + value.sum), 0))))
          @salesPlanChartData.types[cat.name] = "bar"
          @salesPlanChartData.colors[cat.name] = cat.color
          @salesPlanChartData.groups[0].push(cat.name)
          columnIndex += 1
      @salesPlanChart.load(@salesPlanChartData)
    )

    return
  }
)

