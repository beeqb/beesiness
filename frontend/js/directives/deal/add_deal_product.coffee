angular.module('app').component('newDealProduct', {
  templateUrl: "/assets/views/directives/deal/add_deal_product.html",
  bindings: {
    selectedDealProduct: '=?',
    submitFunc: '&',
    companyId: '=',
    companyCurrency: '=',
    product: '=?',
  },

  controller: (($timeout, ResourceLoader, toaster) ->
    @errors = {}

    _resetSelectedDealProduct = (() =>
      @selectedDealProduct = {
        product: @product || null,
        returned_at: null,
        count: if @product then 1 else undefined,
      }
    )

    @submit = (() =>
      product = @selectedDealProduct.product
      @selectedDealProduct.price = product.price

      productQuantity = Number(product.quantity)

      if productQuantity && @selectedDealProduct.count > productQuantity
        @selectedDealProduct.count = productQuantity
        toaster.pop({
          type: 'warning',
          body: "Товара «#{product.name}» осталось только #{product.quantity} штук",
        })
        return false

      if @submitFunc({dealProduct: @selectedDealProduct}) != false
        _resetSelectedDealProduct()
    )

    @searchedProducts = []

    _resetSelectedDealProduct()

    @search = {
      product: '',
    }

    @validateCount = (() =>
      unless @selectedDealProduct.count
        @errors.count = ['Укажите количество']
        return

      value = '' + @selectedDealProduct.count
      match = value.match(/^\d+(\.\d{1,3})?$/)

      if !match
        @errors.count = ['Используйте не более трех знаков после запятой']
      else
        @errors.count = null
    )

    @searchProduct = ((search) =>
      return unless search

      resource = new ResourceLoader(@companyId)

      resource
      .include('category')
      .params({available: true})
      .post('products/search', {
        or: {
          vendor_code: search,
          name: search,
          barcode: search,
        }
      })
      .then((response) =>
        @searchedProducts = response.data
        _.setAll(@searchedProducts, 'quantity', (product) ->
          parseFloat(product.quantity)
        )
      )
    )

    return
  ),
})

