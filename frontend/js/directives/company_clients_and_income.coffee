angular.module('app').component('companyClientsAndIncome', {
  bindings: {
    createdClientsCountByDays: '<',
    clientsCount: '<',
    actualIncomeByDays: '<',
    actualIncome: '<',
    currency: '<',
  },
  templateUrl: 'assets/views/directives/company_clients_and_income.html',

  controller: () ->
    return
})
