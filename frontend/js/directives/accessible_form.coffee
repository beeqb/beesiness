# Директива для форм
# Реагирует на событие 'aform:errors', при данном событии
# скроллит страницу на первое поле в форме с ошибкой
#
# Из контроллера событие создавать с помошью `$broadcast`:
#   scope.$broadcast('aform:errors')
#
# Скорее всего, понадобится это делать в обработчике ng-submit после
# получения ответа сервера
angular.module('app').directive 'accessibleForm', ($timeout) ->
  return {
    restrict: 'A',
    link: (scope, elem) ->
      # поправка на высоту фиксированной шапки
      headerAdjust = 70

      scope.$on('aform:errors', () ->
        $timeout((() ->
          firstInvalid = elem.find('.has-error')

          if firstInvalid
            $('body').animate(
              {
                scrollTop: firstInvalid.offset().top - headerAdjust,
              },
              'fast'
            )
        ), 0)
      )
  }
