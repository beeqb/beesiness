angular.module('app').directive('rangeSlider', ->
  return {
    restrict: 'AE',
    scope: {
      ngModel: '='
      rangeSliderOptions: '='
    },
    link: (scope, element) ->
      slider = element.ionRangeSlider({
        min: scope.rangeSliderOptions.min,
        max: scope.rangeSliderOptions.max,
        type: scope.rangeSliderOptions.type,
        grid: scope.rangeSliderOptions.grid,
        grid_num: scope.rangeSliderOptions.grid_num,
        step: scope.rangeSliderOptions.step,
        hide_min_max: scope.rangeSliderOptions.hide_min_max,
        hide_from_to: scope.rangeSliderOptions.hide_from_to,
        postfix: scope.rangeSliderOptions.postfix,
        onFinish: () ->
          scope.$applyAsync(() ->
            scope.ngModel = element.val()
          )
      }).data('ionRangeSlider')

      scope.$watchGroup(
        ['ngModel'],
        (() =>
          slider.update({
            from: scope.ngModel
          })
        )
      )
  }
)