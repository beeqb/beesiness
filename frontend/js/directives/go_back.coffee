angular.module('app').directive('goBack', ($window) ->
  return {
    restrict: 'A'

    link: (scope, element) =>
      element.on('click', () =>
        $window.history.back()
      )
  }
)

