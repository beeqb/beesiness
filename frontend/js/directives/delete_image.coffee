angular.module('app').directive('deleteImage', ->
  return {
    templateUrl: "/assets/views/directives/delete_image.html"
    restrict: 'EA',
    scope: {
      model: '=ngModel',
      default: '=?default',
    },
    link: (scope) ->
      scope.removeImage = () ->
        if scope.default
          scope.model.url = scope.default
  }
)

