app.directive('fieldWrap', () ->
  return {
    restrict: 'E',
    templateUrl: 'assets/views/directives/field_wrap.html',
    scope: {
      errors: '=',
    },
    transclude: true,
    link: (scope, element, attrs, ctrl, transclude) ->
      scope.hasErrors = () ->
        return !_.isEmpty(scope.errors)
  }
)
