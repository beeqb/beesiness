angular.module('app').directive('datetimepicker', ->
  return {
    scope: {
      model: "=datetimepickerModel"
    },
    restrict: 'A',
    link: (scope, element) ->
      element.datetimepicker({
          widgetPositioning: {
            horizontal: 'right'
          },
          format: 'DD.MM.YYYY',
          useCurrent: false,
          debug: false,
        })
        .on('dp.change', (e)->
          scope.$applyAsync(->
            scope.model = e.date.format('YYYY-MM-DD')
          )
        )

      obj = element.data("DateTimePicker")
      scope.$watch(
        'model',
        (() =>
          if !scope.model
            return
          date = moment(scope.model).format('DD.MM.YYYY')
          obj.date(date)
        ),
        true,
      )
  }
)

