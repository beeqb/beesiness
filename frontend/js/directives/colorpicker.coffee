angular.module('app').directive('colorpicker', ->
  return {
    templateUrl: "/assets/views/directives/colorpicker.html",
    scope: {
      colorpickerModel: '=',
      colorpickerOptions: '=',
    },
    restrict: 'AE',
    link: (scope, element) ->
      scope.selectColor = (color) ->
        scope.colorpickerModel = color

      scope.addHash = (color) ->
        return color if color[0] == '#'
        '#' + color
   
      scope.$watch(
        'colorpickerModel',
        () ->
          if !scope.colorpickerModel
            scope.colorpickerModel = scope.colorpickerOptions[0]
      )
   }
)

