angular.module('app').component('cashBookRecordSource', {
  bindings: {
    record: '<',
  },
  templateUrl: '/assets/views/directives/cash_book_record_source.html',
  controller: () ->
    if @record.source
      @source = @record.source[@record.source.type]
      @source_type = @record.source.type
    else
      @source_type = @record.source_type.toLowerCase()

    @sourceTypeLabels = {
      client: 'Клиент',
      supplier: 'Поставщик',
      employee: 'Сотрудник',
    }

    @url = (() =>
      path = "/companies/#{@record.company_id}/"
      path += "#{@source_type}s/#{@source.id}"
      path
    )

    return
})
