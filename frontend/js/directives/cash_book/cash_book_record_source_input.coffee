angular.module('app').component('cashBookRecordSourceInput', {
  bindings: {
    record: '=',
    companyId: '<',
  },
  templateUrl: '/assets/views/directives/cash_book/source_input.html',
  controller: (ResourceLoader, $filter) ->
    companyResource = new ResourceLoader(@companyId)

    types = {
      Client: 'Клиенты',
      Employee: 'Сотрудники',
      Supplier: 'Поставщики',
    }

    typeKeyToClassName = ((type) ->
      # slice удалит последнюю s,
      # clients -> Client
      # employees -> Employee
      type.capitalize().slice(0, -1)
    )

    @setSourceOfRecord = (() =>
      @record.source_id = @source?.id
      @record.source_type = @source?.type
    )

    @getType = ((entity) =>
      types[entity.type]
    )

    @searchSources = ((term) =>
      companyResource
        .query('search/cash_book_sources', {token: term})
        .then((response) =>
          @searchedSources = []

          for type, entities of response.data
            _.setAll(entities, 'type', typeKeyToClassName(type))
            @searchedSources.push(entities...)
        )
    )
    return
})
