angular.module('app').component('salesForecast', {
  bindings: {
    employee: '<',
  },
  templateUrl: '/assets/views/directives/employee/sales_forecast.html',
  controller: (ResourceLoader) ->
    dataLoaded = false

    this.$onChanges = ((changes) =>
      return unless changes.employee?.currentValue

      loadForecastData() unless dataLoaded

      @currency = @employee.company.currency
    )

    loadForecastData = (() =>
      dataLoaded = true
      companyResource = new ResourceLoader(@employee.company_id)

      companyResource
      .query("employees/#{@employee.id}/sales_forecast")
      .then((response) =>
        @data = response.data
      )
    )

    return
})
