angular.module('app').component('salesFunnel', {
  bindings: {
    employee: '<',
  },
  templateUrl: '/assets/views/directives/employee/sales_funnel.html',
  controller: (ResourceLoader) ->
    funnelLoaded = false
    dealsScopes = []
    @startDate = moment().startOf('month')
    @endDate = moment().endOf('month')

    @indicators = {}

    @foo = ((start, end) =>
      @startDate = start
      @endDate = end

      loadFunnelData()
    )

    this.$onChanges = ((changes) =>
      return unless changes.employee?.currentValue

      loadFunnelData() unless funnelLoaded

      @indicators = @employee.activity
      @currency = @employee.company.currency
    )

    @toggleDealsScope = ((scopeName) =>

      if @dealsScopeIsSet(scopeName)
        dealsScopes = _.without(dealsScopes, scopeName)
      else
        dealsScopes.push(scopeName)
        dealsScopes = _.uniq(dealsScopes)

      loadFunnelData()
    )

    @dealsScopeIsSet = ((scopeName) =>
      scopeName in dealsScopes
    )

    loadFunnelData = (() =>
      funnelLoaded = true

      companyResource = new ResourceLoader(@employee.company_id)

      params = {}

      unless _.isEmpty(dealsScopes)
        params['deals_scopes[]'] = dealsScopes

      if @startDate && @endDate
        params.start_date = @startDate.format('YYYY-MM-DD')
        params.end_date = @endDate.format('YYYY-MM-DD')

      companyResource
      .query("employees/#{@employee.id}/sales_funnel", params)
      .then((response) =>
        funnel = _.omit(response.data.funnel_data, 'deals_total_count')
        @dealsTotalCount = response.data.funnel_data.deals_total_count

        @averageTimeUntilStatuses = response.data.average_time_until_statuses
        _(@indicators).extend(response.data.employee_activity)

        funnel.summary = {
          active: _(funnel).sumProperty('in_status'),
          paid: _(funnel).sumProperty('to_payment'),
          refused: _(funnel).sumProperty('to_refusal'),
        }

        funnel.total =
          funnel.summary.active + funnel.summary.paid + funnel.summary.refused
        
        @funnel = funnel
      )
    )

    @offsetBefore = ((category, transition) ->
      return unless @funnel

      num = switch category
        when 'in_progress'
          switch transition
            when 'to_payment'
              0
            when 'in_status'
              0
            when 'to_refusal'
              @funnel.summary.refused - @funnel.in_progress.to_refusal
        when 'contract_conclusion'
          switch transition
            when 'to_payment'
              @funnel.in_progress.to_payment
            when 'in_status'
              @funnel.in_progress.in_status
            when 'to_refusal'
              @funnel.summary.refused -
                @funnel.in_progress.to_refusal -
                @funnel[category][transition]
        when 'contract_signed'
          switch transition
            when 'to_payment'
              @funnel.in_progress.to_payment + @funnel.contract_conclusion.to_payment
            when 'in_status'
              @funnel.in_progress.in_status + @funnel.contract_conclusion.in_status
            when 'to_refusal'
              @funnel.summary.refused -
                @funnel.in_progress.to_refusal -
                @funnel.contract_conclusion.to_refusal -
                @funnel[category][transition]
        when 'won'
          switch transition
            when 'to_payment'
              @funnel.summary.paid - @funnel[category][transition]
            when 'in_status'
              @funnel.summary.active - @funnel[category][transition]
            when 'to_refusal'
              0

      @barWidthInPercents(num)
    )

    @offsetAfter = ((category, transition) =>
      return unless @funnel

      num = if transition == 'to_payment'
        @funnel.summary.paid -
          @offsetBefore(category, transition) -
          @funnel[category][transition]
      else if transition == 'in_status'
        @funnel.summary.active -
          @offsetBefore(category, transition) -
          @funnel[category][transition]

      @barWidthInPercents(num)
    )

    @barWidthInPercents = ((num) =>
      return 0 unless @funnel && @funnel.total

      (num / @funnel.total) * 100
    )

    return
})
