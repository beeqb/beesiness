angular.module('app').directive('switcher', ($rootScope) ->
  return {
    templateUrl: "/assets/views/directives/switcher.html"
    restrict: 'AE'
    scope: {
      xeditable: '=',
      confirmExit: '=?',
    }

    link: (scope) ->
      if !scope.xeditable
        return

      scope.$watch('xeditable.$visible', () ->
        scope.checked = scope.xeditable.$visible

        return unless scope.confirmExit

        if scope.checked
          onLocationChangeStart()
        else
          offLocationChangeStart && offLocationChangeStart()
      )

      offLocationChangeStart = null

      onLocationChangeStart = (() ->
        offLocationChangeStart && offLocationChangeStart()

        offLocationChangeStart = $rootScope.$on('$locationChangeStart', (event) ->
          if !confirm('Вы уверены, что хотите покинуть страницу? Все несохраненные изменения будут удалены.')
            event.preventDefault()
            return
          offLocationChangeStart()
        )
      )

      scope.switch = () =>
        if scope.xeditable.$visible
          scope.xeditable.$submit()
        else
          scope.xeditable.$show()

        scope.checked = scope.xeditable.$visible
  }
)

