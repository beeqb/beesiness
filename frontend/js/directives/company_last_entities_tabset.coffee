angular.module('app').component('companyLastEntitiesTabset', {
  bindings: {
    company: '<',
    references: '<',
    user: '<',
  },
  templateUrl: '/assets/views/directives/company_last_entities_tabset.html',
})
