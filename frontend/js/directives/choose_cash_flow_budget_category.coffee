angular.module('app').component('chooseCashFlowBudgetCategory', {
  bindings: {
    cashFlowBudgetCategories: '<',
    onChoose: '&',
    model: '=',
    branch: '=',
  },
  templateUrl: '/assets/views/directives/choose_cash_flow_budget_category.html',
  controller: (ModalService) ->
    this.$onChanges = ((changes) =>
      return unless changes.cashFlowBudgetCategories?.currentValue

      @cashFlowBudgetCategories.all =
        @cashFlowBudgetCategories.income.concat(
          @cashFlowBudgetCategories.outcome
        )
    )

    @chooseCategory = () =>
      ModalService.showModal({
        templateUrl: '/assets/views/companies/cash_flow_budget_categories/choose.html',
        controller: 'CompaniesCashFlowBudgetCategoriesChooseCtrl',
        inputs: {
          categories: JSON.stringify(@cashFlowBudgetCategories),
          selectedId: @model,
          branch: @branch,
        },
      })
      .then((modal) =>
        modal.element.modal()
        modal.close.then((result) =>
          @model = result.category.id || null
          @onChoose({value: @model})
        )
      )
    return
})
