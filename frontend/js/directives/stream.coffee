class App.Directives.Stream extends IdFly.AngularClass

  @_import = [
    '$scope',
    '$http',
  ]

  @scope: {
    'companyId': '=',
    'subjectType': '@?',
    'subjectId': '=?',
    'level': '@?',
    'onLoad': '&',
  }

  @restrict: 'E'
  @templateUrl: '/assets/views/directives/stream.html'

  app.directive('stream', @directive())

  link: () =>
    @_scope.events = []
    @_scope.loaded = false

    @page = 1

    @_load(@page)

    @page += 1

    @_scope.loadMore = @_loadMore
    @_scope.isThereMore = @_isThereMore

    if @_scope.level in ['general', 'local']
      @_scope.descriptionKey = "#{@_scope.level}_description"
    else
      @_scope.descriptionKey = 'general_description'

  _loadMore: () =>
    @_load(@page)
    @page += 1

  _load: (page = 1) =>
    params = {
      page: page,
      subject_type: @_scope.subjectType,
      subject_id: @_scope.subjectId,
    }

    @_http
    .get("/api/companies/#{@_scope.companyId}/stream", {params: params})
    .then((response) =>
      @_scope.events = @_scope.events.concat(response.data.entities)
      @totalPages = response.data.total_pages

      @_scope.onLoad({data: @_scope.events})
      @_scope.loaded = true
    )

  _isThereMore: () =>
    @page <= @totalPages

