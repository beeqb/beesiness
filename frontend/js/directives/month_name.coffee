angular.module('app').component('monthName', {
  bindings: {
    month: '<',
    year: '<',
  },
  templateUrl: '/assets/views/directives/month_name.html',
  controller: () ->
    @monthName = moment.monthsShort()[@month - 1]

    @yearIsCurrent = (() =>
      Number(@year) == moment().year()
    )

    return
})
