class App.Directives.Field extends IdFly.AngularClass

  @_import = [
    '$scope',
  ]

  @scope: {
    'model': '=',
    'field': '=',
    'type': '=',
    'options': '=',
    'placeholder': '=',
    'attrClass': '=',
    'attrName': '='
    'attrId': '='
    'label': '=',
    'template': '=?',
    'minLength': '@',
    'required': '@',
    'multiple': '=?',
    'readonly': '=?',
    'datepickerOptions': '=?',
  }

  @restrict: 'E'
  @templateUrl: '/assets/views/directives/field.html'

  app.directive('field', @directive())

  link: (element) ->
    @_scope.uniqueId = Math.random().toString(36).substring(6)

    element.bind 'change', () ->
      if element.attr('type') == "'file'"
        files = event.target.files
        fileName = files[0].name
        element[0].querySelector('input[type=text]').setAttribute('value', fileName)

    if @_scope.template == undefined
        @_scope.template = 'group,input,label,error'

    @_scope.parts = @_scope.template.split(',')

    if @_scope.required == undefined
      @_scope.required = false

    # customizimg ui-mask
    @_scope.maskOptions = {
      maskDefinitions: {
        'x': /([0-9]|\-)/
      },
      clearOnBlur: false,
      allowInvalidValue: true
    }

    # init by date
    @_scope.$watch('model.'+ @_scope.field,(() =>

      switch @_scope.type
        when 'date'
          return unless @_scope.model[@_scope.field]

          @_scope.model[@_scope.field + '_uib'] =
            moment(@_scope.model[@_scope.field]).toDate()
        when 'file'
          unless @_scope.model[@_scope.field]
            $('input[type=text]', element).val('')
    ))

    # set date to origin model
    @_scope.changed = (() =>
      datepickerValue = moment(@_scope.model[@_scope.field + '_uib'])

      if datepickerValue.isValid()
        modelValue = datepickerValue.format('YYYY-MM-DD')
      else
        modelValue = null

      @_scope.model[@_scope.field] = modelValue

      if !@_scope.model.errors
        @_scope.model.errors = []
      @_scope.model.errors[@_scope.field] = null
    )
