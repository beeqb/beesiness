angular.module('app').component('dealProduct', {
  bindings: {
    dealProduct: '<',
    editable: '<',
    currency: '<',
    mode: '@',
  },
  templateUrl: 'assets/views/directives/deal_product.html',
  controller: () ->
    @product = @dealProduct.product

    @count = if @mode == 'returns'
               @dealProduct.count - @dealProduct.count_minus_returns
             else
               @dealProduct.count
    return
})
