angular.module('app').directive('map', ($rootScope, $timeout, $log,
  geolocation,
  LOCALE_RU,
  uiGmapGoogleMapApi,
  uiGmapIsReady) ->
  return {
    templateUrl: "/assets/views/directives/map.html",
    restrict: 'E',
    scope: {
      model: '='
      address: '=',
      latitude: '=',
      longitude: '=',
      errors: '=',
      map: '=',
      detect: '=?',
      onChange: '&',
    },

    link: (scope) ->
      scope.map.events = {
        click: (mapObject, eventName, originalEventArgs) =>
          lat = originalEventArgs[0].latLng.lat()
          lng = originalEventArgs[0].latLng.lng()
          @_setMarkerPosition(scope, lat, lng)
          @_updateAddressByCoords(scope, lat, lng)
          scope.model.dirty = true if scope.model
      }

      scope.$watch('address', () =>
        return unless scope.address

        if !scope.map.isManualChange
          scope.map.isManualChange = true
          return

        @_findAddressOnMap(scope, scope.address)
      )

      scope.$watchGroup(['latitude', 'longitude'], () =>
        if !scope.latitude || !scope.longitude || !scope.address
          return

        @_findAddressOnMap(scope, scope.address)
      )

      $rootScope.$on('googleMaps:initialized', () =>
        return if scope.address

        geolocation.getLocation().then(
          ((data) =>
            @_setMarkerPosition(scope, data.coords.latitude, data.coords.longitude, true)
            @_updateAddressByCoords(scope, data.coords.latitude, data.coords.longitude)
          ),
          ((error) ->
            if $rootScope.user.lang == 'ru'
              error = LOCALE_RU.geolocation[error]
            scope.errors['address'] = [error]
          )
        )
      )

      if scope.model and scope.model.loaded == false
        scope.$watch('model.loaded', (newValue, oldValue) =>
          @_googleMapInit(scope) if newValue is true
        )
      else
        @_googleMapInit(scope)

    _googleMapInit: (scope) ->
      uiGmapGoogleMapApi.then((map) =>
        if scope.detect
          $rootScope.$emit('googleMaps:initialized')

        scope.gmap = map
        scope.geocoder = new scope.gmap.Geocoder()

        @_setMarkerPosition(
          scope,
          scope.latitude,
          scope.longitude,
          true
        )
      )
      uiGmapIsReady
      .promise()
      .then(() =>
        @_setMapCenterPosition(
          scope,
          scope.latitude,
          scope.longitude
        )
      )

    _setMarkerPosition: (scope, latitude, longitude, init = false) ->
      scope.map.marker.coords = {
        latitude: latitude || scope.map.defaultCoords.latitude,
        longitude: longitude || scope.map.defaultCoords.longitude
      }

      if init
        scope.map.center = {
          latitude: latitude || scope.map.defaultCoords.latitude,
          longitude: longitude || scope.map.defaultCoords.longitude
        }
        scope.map.marker.coords = {
          latitude: latitude || scope.map.defaultCoords.latitude,
          longitude: longitude || scope.map.defaultCoords.longitude
        }

      scope.$applyAsync(()->
        scope.latitude = latitude
        scope.longitude = longitude
      )

    _updateAddressByCoords: (scope, latitude, longitude) ->
      latlng = new google.maps.LatLng(latitude, longitude)
      scope.geocoder.geocode(
        {'latLng': latlng},
        (results, status) =>
          switch
            when status == 'OK' then (
              scope.map.isManualChange = false

              scope.onChange({
                countryCode: @_getCountryCode(results[0].address_components),
              })

              scope.$applyAsync(() =>
                scope.address = results[0].formatted_address
              )
            )
            when status == 'ZERO_RESULTS' then return
            else
              $log.error 'Geocode error: ' + status
      )

    _setMapCenterPosition: (scope, latitude, longitude) ->
      if latitude || latitude
        latlng = new google.maps.LatLng(latitude, longitude)
        scope.map.center = latlng
      else
        scope.map.center = {
          latitude: scope.map.defaultCoords.latitude,
          longitude: scope.map.defaultCoords.longitude
        }

    _findAddressOnMapPromise: null

    _findAddressOnMap: (scope, address) ->
      if _findAddressOnMapPromise
        $timeout.cancel(_findAdressOnMapPromise)
      _findAddressOnMapPromise = $timeout(
        (() =>
          scope.geocoder.geocode(
            {'address': address},
            (results, status) =>
              switch
                when status == 'OK' then (
                  addressLat = results[0].geometry.location.lat()
                  addressLng = results[0].geometry.location.lng()
                  @_setMarkerPosition(scope, addressLat, addressLng, true)

                  scope.onChange({
                    countryCode: @_getCountryCode(results[0].address_components),
                  })
                )
                when status == 'ZERO_RESULTS' then (
                  scope.errors.address = ['Объект не найден']
                )
                else
                  $log.error 'Geocode error: ' + status
          )
        ),
        1000
      )

    _getCountryCode: (addressComponents) ->
      _.find(addressComponents, (component) ->
        'country' in component.types
      )?.short_name
  }
)

