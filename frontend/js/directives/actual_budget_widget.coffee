angular.module('app').component('actualBudgetWidget', {
  bindings: {
    company: '<',
  },
  templateUrl: '/assets/views/companies/actual_budget_widget.html',
  controller: (ResourceLoader) ->
    this.$onChanges = ((changes) =>
      return unless changes.company.currentValue

      indicators = [
        'current_month_income',
        'paid_deals_count',
        'leads_count',
        'clients_count',
        'plan_percent_today',
        'plan_percent_month',
        'plan_percent_quarter',
        'plan_percent_year',
      ]

      companyResource = new ResourceLoader(@company.id)

      companyResource
      .query('budget_statistics', {indicators: indicators.join(',')})
      .then((response) =>
        @data = response.data.data
      )
    )

    return
})
