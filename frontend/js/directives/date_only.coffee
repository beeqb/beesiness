angular.module('app').directive('dateOnly', ($parse) ->
  {
    restrict: 'A',
    require: 'ngModel',
    link: (scope, element, attr, ngModelController) ->
      discardTimezoneOffset = ((date) ->
        return undefined unless date

        date.setMinutes(date.getMinutes() - date.getTimezoneOffset())
        date
      )

      ngModelController.$parsers.push(discardTimezoneOffset)
      ngModelController.$formatters.push(discardTimezoneOffset)
  }
)
