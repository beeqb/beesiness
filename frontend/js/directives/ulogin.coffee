class App.Directives.ULogin extends IdFly.AngularClass

  @_import = [
    '$scope',
  ]

  @scope: {
    fields: '=',
    optional: '=',
    callback: '=',
  }

  @restrict: 'E'
  @templateUrl: '/assets/views/directives/ulogin.html'

  app.directive('ulogin', @directive())

  link: () ->
    @_scope.$applyAsync(() =>
      window.uLogin && window.uLogin.init()
    )
