angular.module('app').directive('pagination', () ->
  {
    templateUrl: 'assets/views/directives/pagination.html',
    replace: true,
    restrict: 'E',
    scope: {
      total: '=',
      per: '=',
      current: '=',
    },
    link: (scope) =>
      scope.$watchGroup(
        ['total', 'per'],
        (() =>
          return if !scope.total || !scope.per

          pagesCount = Math.ceil(scope.total / scope.per)
          scope.pages = []
          i = 0
          while i < pagesCount
            scope.pages.push(i + 1)
            i++
        ),
        true
      )
  }
)
