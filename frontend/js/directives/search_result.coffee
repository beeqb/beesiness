angular.module('app').component('searchResult', {
  bindings: {
    entity: '<',
    references: '<',
  },
  templateUrl: ($element, $attrs) ->
    "/assets/views/companies/#{$attrs.type}s/search_result.html"
})
