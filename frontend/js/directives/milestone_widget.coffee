angular.module('app').component('milestoneWidget', {
  templateUrl: '/assets/views/directives/milestone_widget.html',
  controller: (ResourceLoader, $rootScope, getCompanyIdFromCurrentRoute) ->
    resource = new ResourceLoader()
    currentCompanyId = null

    _loadData = (companyId) =>
      resource
      .params({indicators: 'plan_percent_today,plan_percent_month'})
      .query("companies/#{companyId}/budget_statistics")
      .then((response) =>
        @data = response.data.data
      )

    currentCompanyId = getCompanyIdFromCurrentRoute()
    _loadData(currentCompanyId) if currentCompanyId

    $rootScope.$on('$routeChangeSuccess', (event, current, previous) =>
      newCompanyId = getCompanyIdFromCurrentRoute()
      return if newCompanyId == currentCompanyId

      if !newCompanyId
        @data = null
        currentCompanyId = null
      else
        currentCompanyId = newCompanyId
        _loadData(currentCompanyId)
    )

    $rootScope.$on('milestone:reload', () ->
      _loadData(currentCompanyId) if currentCompanyId
    )

    return
})
