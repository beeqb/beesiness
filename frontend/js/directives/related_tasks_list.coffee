angular.module('app').component('relatedTasksList', {
  bindings: {
    tasks: '<',
    showClient: '<',
  },
  templateUrl: '/assets/views/directives/related_tasks_list.html',
  controller: (References) ->
    @references = References.all()

    unless angular.isDefined(@showClient)
      @showClient = true

    return
})
