app.config ($routeProvider) ->
  resolve = {
    auth: ($auth) ->
      return $auth.validateUser()
  }

  resolveChangePassword = {
    allowChangePassword: (($rootScope, $q) ->
      defer = $q.defer()
      $rootScope.$watch('user.allow_change_password', () =>
        if $rootScope.user.allow_change_password
          defer.resolve(true)
      )
      return defer.promise
    )
    auth: (($auth, $rootScope) ->
      return $auth.validateUser() && $rootScope.user.allow_change_password
    ),
  }

  $routeProvider
  .when('/', {
    templateUrl: '/assets/views/index/index.html',
    controller: 'IndexCtrl',
    resolve: resolve,
  })
  .when('/profile', {
    templateUrl: '/assets/views/users/xedit.html',
    controller: 'UsersXEditCtrl',
    resolve: resolve,
  })

# users
  .when('/users/sign_in', {
    templateUrl: '/assets/views/users/sign_in.html',
    controller: 'UsersSignInCtrl',
  })
  .when('/users/sign_up', {
    templateUrl: '/assets/views/users/sign_up.html',
    controller: 'UsersSignUpCtrl',
  })
  .when('/users/sign_up_success', {
    templateUrl: '/assets/views/users/sign_up_success.html',
    controller: 'UsersSignUpCtrl',
  })
  .when('/users/restore_password', {
    templateUrl: '/assets/views/users/restore_password.html',
    controller: 'UsersRestorePasswordCtrl',
  })
  .when('/users/restore_password_success', {
    templateUrl: '/assets/views/users/restore_password_success.html',
  })
  .when('/users/sign_out', {
    template: '',
    controller: 'UsersSignOutCtrl',
    resolve: resolve,
  })
  .when('/users/change_password', {
    templateUrl: '/assets/views/users/change_password.html',
    controller: 'UsersChangePasswordCtrl',
    resolve: resolveChangePassword,
  })
  .when('/users/:id', {
    templateUrl: '/assets/views/users/xedit.html',
    controller: 'UsersXEditCtrl',
    resolve: resolve,
  })

  # companies

  .when('/companies/new', {
    templateUrl: '/assets/views/companies/new.html',
    controller: 'CompaniesNewCtrl',
    resolve: resolve,
  })
  .when('/companies/:id/edit', {
    templateUrl: '/assets/views/companies/edit.html',
    controller: 'CompaniesEditCtrl',
    resolve: resolve,
  })
  .when('/companies/:id', {
    templateUrl: '/assets/views/companies/view.html',
    controller: 'CompaniesViewCtrl',
    resolve: resolve,
  })
  .when('/companies/:company_id/search-results', {
    templateUrl: '/assets/views/companies/search_results.html',
    controller: 'CompaniesSearchResultsCtrl',
    resolve: resolve,
  })

  # company products

  .when('/companies/:company_id/products', {
    templateUrl: '/assets/views/companies/products/index.html',
    controller: 'CompaniesProductsIndexCtrl',
    resolve: resolve,
  })
  .when('/companies/:company_id/products/new', {
    templateUrl: '/assets/views/companies/products/new.html',
    controller: 'CompaniesProductsNewCtrl',
    resolve: resolve,
  })
  .when('/companies/:company_id/products/:product_id', {
    templateUrl: '/assets/views/companies/products/show.html',
    controller: 'CompaniesProductsXEditCtrl',
    resolve: resolve,
  })
  .when('/companies/:company_id/products/new', {
    templateUrl: '/assets/views/companies/products/new.html',
    controller: 'CompaniesProductsNewCtrl',
    resolve: resolve,
  })
  .when('/companies/:company_id/products/:id', {
    templateUrl: '/assets/views/companies/products/edit.html',
    controller: 'CompaniesProductsEditCtrl',
    resolve: resolve,
  })

  # company clients

  .when('/companies/:company_id/clients', {
    templateUrl: '/assets/views/companies/clients/index.html',
    controller: 'CompaniesClientsIndexCtrl',
    resolve: resolve,
  })
  .when('/companies/:company_id/clients/new', {
    templateUrl: '/assets/views/companies/clients/new.html',
    controller: 'CompaniesClientsNewCtrl',
    resolve: resolve,
  })
  .when('/companies/:company_id/clients/:client_id', {
    templateUrl: '/assets/views/companies/clients/show.html',
    controller: 'CompaniesClientsXEditCtrl',
    resolve: resolve,
  })
  .when('/companies/:company_id/clients/:id', {
    templateUrl: '/assets/views/companies/clients/edit.html',
    controller: 'CompaniesClientsEditCtrl',
    resolve: resolve,
  })

  # company suppliers

  .when('/companies/:company_id/suppliers', {
    templateUrl: '/assets/views/companies/suppliers/index.html',
    controller: 'CompaniesSuppliersIndexCtrl',
    resolve: resolve,
  })
  .when('/companies/:company_id/suppliers/new', {
    templateUrl: '/assets/views/companies/suppliers/new.html',
    controller: 'CompaniesSuppliersNewCtrl',
    resolve: resolve,
  })
  .when('/companies/:company_id/suppliers/:supplier_id', {
    templateUrl: '/assets/views/companies/suppliers/show.html',
    controller: 'CompaniesSuppliersXEditCtrl',
    resolve: resolve,
  })
  .when('/companies/:company_id/suppliers/:id', {
    templateUrl: '/assets/views/companies/suppliers/edit.html',
    controller: 'CompaniesSuppliersEditCtrl',
    resolve: resolve,
  })

  # company leads

  .when('/companies/:company_id/leads', {
    templateUrl: '/assets/views/companies/leads/index.html',
    controller: 'CompaniesLeadsIndexCtrl',
    resolve: resolve,
  })
  .when('/companies/:company_id/leads/new', {
    templateUrl: '/assets/views/companies/leads/new.html',
    controller: 'CompaniesLeadsNewCtrl',
    resolve: resolve,
  })
  .when('/companies/:company_id/leads/:lead_id', {
    templateUrl: '/assets/views/companies/leads/show.html',
    controller: 'CompaniesLeadsXEditCtrl',
    resolve: resolve,
  })
  .when('/companies/:company_id/leads/:id/convert', {
    templateUrl: '/assets/views/companies/leads/convert.html',
    controller: 'CompaniesLeadsConvertCtrl',
    resolve: resolve,
  })
  .when('/companies/:company_id/leads/:id', {
    templateUrl: '/assets/views/companies/leads/edit.html',
    controller: 'CompaniesLeadEditCtrl',
    resolve: resolve,
  })
  # company employees

  .when('/companies/:company_id/employees', {
    templateUrl: '/assets/views/companies/employees/index.html',
    controller: 'CompaniesEmployeesIndexCtrl',
    resolve: resolve,
  })

  .when('/companies/:company_id/employees/:employee_id', {
    templateUrl: '/assets/views/companies/employees/show.html',
    controller: 'CompaniesEmployeesXEditCtrl',
    resolve: resolve,
  })

  # company deals

  .when('/companies/:company_id/deals', {
    templateUrl: '/assets/views/companies/deals/index.html',
    controller: 'CompaniesDealsIndexCtrl',
    resolve: resolve,
  })

  .when('/companies/:company_id/deals/new', {
    templateUrl: '/assets/views/companies/deals/new.html',
    controller: 'CompaniesDealsNewCtrl',
    resolve: resolve,
  })

  .when('/companies/:company_id/deals/:deal_id', {
    templateUrl: '/assets/views/companies/deals/show.html',
    controller: 'CompaniesDealsXEditCtrl',
    resolve: resolve,
  })

  .when('/companies/:company_id/deals/:deal_id/receipt', {
    templateUrl: '/assets/views/companies/deals/receipt.html',
    controller: 'CompaniesDealsReceiptCtrl',
    resolve: resolve,
  })

  # company pos

  .when('/companies/:company_id/pos', {
    templateUrl: '/assets/views/companies/pos/index.html',
    controller: 'CompaniesPosIndexCtrl',
    resolve: resolve,
  })

  # company BA&BI

  .when('/companies/:company_id/sales_plan', {
    templateUrl: '/assets/views/companies/bi/sales_plans/index.html',
    controller: 'CompaniesBiSalesPlansIndexCtrl',
    resolve: resolve,
  })

  .when('/companies/:company_id/sales_plan/manage', {
    templateUrl: '/assets/views/companies/bi/sales_plans/manage.html',
    controller: 'CompaniesBiSalesPlansManageCtrl',
    resolve: resolve,
  })

  .when('/companies/:company_id/budget', {
    templateUrl: '/assets/views/companies/bi/budget/index.html',
    controller: 'CompaniesBiBudgetIndexCtrl',
    resolve: resolve,
  })

  .when('/companies/:company_id/break-even-analysis', {
    templateUrl: '/assets/views/companies/bi/break_even_analysis.html',
    controller: 'CompaniesBiBreakEvenAnalysisCtrl',
    resolve: resolve,
  })

  .when('/companies/:company_id/sales-report', {
    templateUrl: '/assets/views/companies/bi/sales_report.html',
    controller: 'CompaniesBiSalesReportCtrl',
    resolve: resolve,
  })

  .when('/companies/:company_id/cash_book', {
    templateUrl: '/assets/views/companies/bi/cash_book/index.html',
    controller: 'CompaniesBiCashBookIndexCtrl',
    resolve: resolve,
  })

  # company crm

  .when('/companies/:company_id/crm', {
    templateUrl: '/assets/views/companies/crm.html',
    controller: 'CompaniesCRMCtrl',
    resolve: resolve,
  })

  # calendar

  .when('/calendar', {
    templateUrl: '/assets/views/calendar/index.html',
    controller: 'CalendarCtrl',
    resolve: resolve,
  })

  # tasks

  .when('/companies/:company_id/tasks/new', {
    templateUrl: '/assets/views/tasks/new.html',
    controller: 'TasksNewCtrl',
    resolve: resolve,
  })

  .when('/tasks', {
    templateUrl: '/assets/views/tasks/index.html',
    controller: 'TasksIndexCtrl',
    resolve: resolve,
  })

  .when('/policy', {
    templateUrl: '/assets/views/policy/index.html',
    resolve: resolve,
  })

  .when('/translations', {
    templateUrl: '/assets/views/translations/index.html',
    controller: 'TranslationsCtrl',
    resolve: resolve,
  })

  .when('/desktop', {
    templateUrl: '/assets/views/desktop/index.html',
    resolve: resolve,
  })

  .when('/cash_book', {
    templateUrl: '/assets/views/cash_book/index.html',
    resolve: resolve,
  })

  .when('/budget', {
    templateUrl: '/assets/views/budget/index.html',
    resolve: resolve,
  })

  .when('/analysis', {
    templateUrl: '/assets/views/analysis/index.html',
    resolve: resolve,
  })

  .otherwise({
    redirectTo: '/'
  })

  # temp static pages

  .when('/tmp/deals', {
    templateUrl: '/assets/views/companies/deals.html',
  })

  .when('/tmp/clients', {
    templateUrl: '/assets/views/companies/clients.html',
  })

  .when('/tmp/leads', {
    templateUrl: '/assets/views/companies/leads.html',
  })
