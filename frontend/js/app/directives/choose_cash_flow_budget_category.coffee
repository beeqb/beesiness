angular.module('app').component('chooseCashFlowBudgetCategory', {
  bindings: {
    cashFlowBudgetCategories: '<',
    onChoose: '&',
    model: '=',
  },
  templateUrl: '/assets/views/directives/choose_cash_flow_budget_category.html',
  controller: (ModalService) ->
    this.$onChanges = ((changes) =>
      debugger
      return unless changes.cashFlowBudgetCategories?.currentValue

      @cashFlowBudgetCategories.all =
        @cashFlowBudgetCategories.income.concat(
          @cashFlowBudgetCategories.outcome
        )
      console.log @cashFlowBudgetCategories
    )

    @chooseCategory = () =>
      ModalService.showModal({
        templateUrl: '/assets/views/companies/cash_flow_budget_categories/choose.html',
        controller: 'CompaniesCashFlowBudgetCategoriesChooseCtrl',
        inputs: {
          categories: JSON.stringify(@cashFlowBudgetCategories),
        },
      })
      .then((modal) =>
        modal.element.modal()
        modal.close.then((result) =>
          @model = result.category.id
          @onChoose()
        )
      )
    return
})
