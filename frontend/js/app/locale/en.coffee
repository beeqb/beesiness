app.constant('LOCALE_EN', {
    header: {
      navbar: {
        NEW: 'Create',
        PROJECT: 'Project',
        CLIENT: 'Client',
        DEAL: 'Deal',
        LEAD: 'Lead',
        EMAIL: 'Email',
        TASK: 'Task'
      },
      userMenu: {
        SETTINGS: 'Settings',
        PROFILE: 'Profile',
        HELP: 'Help',
        NEW: 'New',
        LOGOUT: 'Logout'
      },
      notifications: {
        YOU_HAVE: 'You have',
        NOTIFICATIONS: 'notifications',
        HOURS_AGO: 'hours ago',
        MINUTES_AGO: 'minutes ago',
        SEE_ALL_NOTIFICATIONS: 'View all notifications'
      }
    },
    aside: {
      nav: {
        COMPANY: 'My companies',
        CALENDAR: 'Calendar',
        COMPONENTS: 'Components',
        CONTACTS: 'Contacts',
        TASKS: 'Tasks',
        NOTES: 'Notes',
        YOUR_STUFF: 'Your stuff',
        PROFILE: 'Profile',
        DOCUMENTS: 'Documents',
        PRODUCTS: 'Products',
        CLIENTS: 'Clients',
        EMPLOYEES: 'Employees',
        SUPPLIERS: 'Suppliers',
      }
    }
    filter: {
      clients: {
        TOOLTIP: 'Clients filter',
        NAME: 'Client name'
      }
      deals: {
        CLIENT: 'Client name'
        START_RANGE: 'Deal create date from-to',
        CONCLUSION_RANGE: 'Deal conclusion date from-to',
        CLOSE_RANGE: 'Deal close date from-to',
        NAME: 'Deal name',
        TOOLTIP: 'Deals filter'
      }
      leads: {
        TOOLTIP: 'Leads filter',
        NAME: 'Lead name'
      }
      STATUS: 'Status'
      RANGE: 'Deal sum - range'
      MANAGER: 'Manager'
      CLEAR: 'Remove filter'
    }
    form: {
      sign_in: {
        SIGN_IN_TO_GET_IN_TOUCH: 'Sign in to get in touch',
        RESEND_CONFIRMATION_EMAIL: 'Resend confirmation email',
        CONFIRMATION_EMAIL_WAS_RESEND: 'Confirmation email was resend',
        SIGN_IN: 'Sign in',
        FORGOT_PASSWORD: 'Forgot password?',
        DO_NOT_HAVE_ACCOUNT: 'Do not have account?',
        CREATE_ACCOUNT: 'Create account',
      },
      sign_up: {
        SIGN_UP_TO_FIND_INTERESTING_THING: 'Sign up to find interesting thing',
        SIGN_UP: 'Sign up',
        ALREADY_HAVE_ACCOUNT: 'Already have account?',
        AGREE_TERMS: 'Agree terms'
      },
      restore_password: {
        TITLE: 'Restore password',
        EMAIL_WAS_SENT: 'Email with restore password instructions was sent'
        SUBMIT: 'Send',
      },
      change_password: {
        TITLE: 'Change password',
        DESCRIPTION: 'Password must be at least 8 characters',
        PASSWORD: 'New password',
        PASSWORD_CONFIRMATION: 'Confirm new password',
        SAVE: 'Save',
        CANCEL: 'Cancel'
      },
      profile: {
        REGISTRATION_DATE: 'Registration date',
        BALANCE: 'Balance',
        FILL_UP: 'Fill up',
        BONUS: 'Bonus',
        MY_COMPANIES: 'My companies:',
        I_AM_CLIENT_OF: 'I am client of:'
        WORK_PHONES: 'Work phones:',
        WORK_CELL_PHONES: 'Work cell phones:'
      },
      company: {
        NEW_COMPANY_PAGE_TITLE: 'New company',
        NEW_COMPANY_SUB_TITLE: 'You create your company, be careful',
        WARNING: 'All fields must be filled',
        EDIT_COMPANY_SUB_TITLE: 'You edit your company, be careful',
      },
      deal: {
        NEW_DEAL_PAGE_TITLE: 'New deal',
        NEW_DEAL_SUB_TITLE: 'You create new deal, be careful',
        WARNING: 'All fields checked by asterisk must be filled',
      }
      product: {
        CREATE_CATEGORY: 'Create category',
        NEW_PRODUCT_PAGE_TITLE: 'New product',
        NEW_PRODUCT_SUB_TITLE:  'You create new product, be careful',
        WARNING: 'All fields must be filled',
        PRODUCT_CARD_TITLE: 'Product card',
        DESCRIPTION_TITLE: 'Description'
      },
      lead: {
        NEW_LEAD_PAGE_TITLE: 'New lead',
        NEW_LEAD_SUB_TITLE: 'You create new lead',
        WARNING: 'All fields must be filled',
        INDIVIDUAL: 'Individual',
        LEGAL_ENTITY: 'Legal entity',
        COMPANY: 'Company',
        CONTACT: 'Contact'
      },
      client: {
        NEW_CLIENT_PAGE_TITLE: 'New client',
        NEW_CLIENT_SUB_TITLE: 'You create new client',
        WARNING: 'All fields must be filled',
        INDIVIDUAL: 'Individual',
        LEGAL_ENTITY: 'Legal entity',
        COMPANY: 'Company',
        CONTACT: 'Contact'
      },
      sales_plan: {
        NEW_SALES_PLAN_PAGE_TITLE: 'New sales plan',
        NEW_SALES_PLAN_SUB_TITLE: 'You are creating a new sales plan, be attentive'
        KINDS: {
          BY_QUANTITY: 'By quantity',
          BY_SUM: 'By sum',
        }
      },
      NAME: 'Name',
      EMAIL: 'E-mail',
      PASSWORD: 'Password',
      CHANGE_PASSWORD: 'Change password',
      CHANGE_PASSWORD_INSTRUCTIONS_SENT: 'Instructions to change password was sent',
    },
    model: {
      product: {
        PAGE_TITLE: 'Company products',
        SUB_TITLE: 'All, that you may to sell',
        INVITE_EMPLOYEE: 'Invite employee',
        ALREADY_EXISTS_TITLE: 'Product already exists',
        DELETE_EXISTS: 'Delete exists product',
        CREATE_PRODUCT: 'Create product',
      },
      client: {
        PAGE_TITLE: 'Clients',
        SUB_TITLE: '',
        CREATE: 'Create client',
      },
      product_category: {
        CREATE_TITLE: 'Create product category',
        EDIT_TITLE: 'Edit product category'
      },
      department: {
        CREATE_TITLE: 'Create department',
        EDIT_TITLE: 'Edit department',
      },
      lead: {
        PAGE_TITLE: 'Leads',
        SUB_TITLE: '',
        CREATE: 'Create lead',
      },
      user: {
        PROFILE_TITLE: 'My profile',
        EDIT_PROFILE_TITLE: 'Edit profile',
      },
      company: {
        PAGE_TITLE: 'My companies',
        SUB_TITLE: 'Companies in which you are an employee',
        CREATE_COMPANY: 'Add company',
        SALES_FACT: 'Sales fact',
        FACT: 'Fact',
        PLAN: 'Plan',
        DEALS_COUNT: 'Deals',
        LEADS_COUNT: 'Leads',
        CLIENTS_COUNT: 'Clients',
        PRODUCTS_COUNT: 'Products',
      },
      deal: {
        PAGE_TITLE: 'Deals',
        SUB_TITLE: 'Result of your job',
        CREATE: 'Create deal',
        ADD_PRODUCT: 'Add product'
      },
      deal_product: {
        TITLE: 'Deal products',
        NAME: 'Name',
        COUNT: 'Count',
        SUM: 'Sum'
      },
      employee: {
        PAGE_TITLE: 'Employees',
        INVITE: 'Invite employee',
        ADD_TO_DEPARTMENT: 'Add to department'
      },
      sales_plan: {
        MANAGE: 'Manage',
      },
    },
    common: {
      CLOSE: 'Close',
      SAVE: 'Save',
      SEND: 'Send',
      DELETE: 'Delete',
      CANCEL: 'Cancel',
      CONTINUE_EDITING: 'Continue editing',
      SAVING: 'Saving...',
      PROCESSING_PHOTOS: 'Processing photos',
      ADD: 'Add',
      BUSINESS_CARD: 'Business card',
      SERVICE_INFORMATION: 'Service information'
    },
    months: {
      1: 'Jan',
      2: 'Feb',
      3: 'Mar',
      4: 'Apr',
      5: 'May',
      6: 'Jun',
      7: 'Jul',
      8: 'Aug',
      9: 'Sep',
      10: 'Okt',
      11: 'Nov',
      12: 'Dec',
    }
  }
)
