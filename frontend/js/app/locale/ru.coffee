app.constant('LOCALE_RU', {
    geolocation: {
      'Browser does not support location services': 'Браузер не поддерживает функцию геолокации',
      'You have rejected access to your location': 'Функция геолокации отключена',
      'Unable to determine your location': 'Не удалось определить местоположение',
      'Service timeout has been reached': 'Невозможно подключиться к сервису',
    },
    header: {
      navbar: {
        NEW: 'Создать',
        DEAL: 'Сделку',
        CLIENT: 'Клиента',
        LEAD: 'Лид',
        EMAIL: 'Email',
        TASK: 'Задачу'
      },
      userMenu: {
        SETTINGS: 'Настройки',
        PROFILE: 'Профиль',
        HELP: 'Помощь',
        NEW: 'Новое',
        LOGOUT: 'Выйти'
      },
      notifications: {
        YOU_HAVE: 'У вас',
        NOTIFICATIONS: 'уведомления',
        HOURS_AGO: 'часов назад',
        MINUTES_AGO: 'минут назад',
        SEE_ALL_NOTIFICATIONS: 'Посмотреть все уведомления',
        MARK_ALL_AS_READ: 'Отметить все как прочитанное',
      }
    },
    aside: {
      nav: {
        COMPANY: 'Мои компании',
        CALENDAR: 'Календарь',
        COMPONENTS: 'Компоненты',
        CONTACTS: 'Контакты',
        TASKS: 'Задачи',
        NOTES: 'Записи',
        YOUR_STUFF: 'Ваши результаты',
        PROFILE: 'Профиль',
        DOCUMENTS: 'Документы',
        PRODUCTS: 'Товары',
        EMPLOYEES: 'Сотрудники',
        CLIENTS: 'Клиенты'
        SUPPLIERS: 'Поставщики',
      }
    }
    filter: {
      clients: {
        TOOLTIP: 'Фильтр клиентов',
        NAME: 'Название клиента'
      }
      deals: {
        CLIENT: 'Название клиента',
        STATUS: 'Заключение-активность контракта',
        START_RANGE: 'Дата создания сделки от-до',
        CONCLUSION_RANGE: 'Дата заключения контракта от-до',
        CLOSE_RANGE: 'Дата закрытия сделки от-до',
        NAME: 'Название сделки',
        TOOLTIP: 'Фильтр сделок'
      }
      leads: {
        TOOLTIP: 'Фильтр лидов',
        NAME: 'Название лида'
      }
      STATUS: 'Статус'
      RANGE: 'Сумма сделки - рейнж'
      MANAGER: 'Менеджер'
      CLEAR: 'Удалить фильтр'
    }
    form: {
      sign_in: {
        SIGN_IN_TO_GET_IN_TOUCH: 'Войдите, чтобы быть на связи',
        RESEND_CONFIRMATION_EMAIL: 'Отправить письмо повторно',
        CONFIRMATION_EMAIL_WAS_RESEND: 'Письмо с инструкциями отправлено',
        SIGN_IN: 'Войти',
        FORGOT_PASSWORD: 'Забыли пароль?',
        DO_NOT_HAVE_ACCOUNT: 'Нет аккаунта?',
        CREATE_ACCOUNT: 'Создать аккаунт',
      },
      sign_up: {
        SIGN_UP_TO_FIND_INTERESTING_THING: 'Зарегистрируйтесь, чтобы найти интересную вещь',
        SIGN_UP: 'Зарегистрироваться',
        ALREADY_HAVE_ACCOUNT: 'Уже есть аккаунт?',
        AGREE_TERMS: 'Согласен с условиями и политикой'
      },
      restore_password: {
        TITLE: 'Восстановление пароля',
        EMAIL_WAS_SENT: 'Письмо с инструкциями по восстановлению пароля выслано',
        SUBMIT: 'Отправить',
      },
      change_password: {
        TITLE: 'Смена пароля',
        DESCRIPTION: 'Пароль должен содержать не меньше 8 символов',
        PASSWORD: 'Новый пароль',
        PASSWORD_CONFIRMATION: 'Подтверждение нового пароля',
        SAVE: 'Сохранить',
        CANCEL: 'Отмена'
      },
      profile: {
        REGISTRATION_DATE: 'Дата регистрации',
        BALANCE: 'Баланс',
        FILL_UP: 'Пополнить',
        BONUS: 'Ваш бонус',
        MY_COMPANIES: 'Мои компании:',
        I_AM_CLIENT_OF: 'Я клиент в:',
        WORK_PHONES: 'Телефоны раб.:',
        WORK_CELL_PHONES: 'Телефоны раб. моб.:'
      },
      company: {
        NEW_COMPANY_PAGE_TITLE: 'Новая компания',
        NEW_COMPANY_SUB_TITLE: 'Вы создаете свою компанию, будьте внимательны',
        WARNING: 'Все поля являются обязательными к заполнению',
        EDIT_COMPANY_SUB_TITLE: 'Вы редактируете свою компанию, будьте внимательны',
      },
      deal: {
        NEW_DEAL_PAGE_TITLE: 'Новая сделка',
        NEW_DEAL_SUB_TITLE: 'Вы создаете сделку, будьте внимательны',
        WARNING: 'Все поля, отмеченные *, являются обязательными к заполнению',
      },
      deal_product: {
        TITLE: 'Товары сделки',
        NAME: 'Название',
        COUNT: 'Количество',
        SUM: 'Сумма'
      },
      product: {
        CREATE_CATEGORY: 'Создать категорию'
        NEW_PRODUCT_PAGE_TITLE: 'Новый товар',
        NEW_PRODUCT_SUB_TITLE: 'Вы создаете новый товар, будьте внимательны',
        WARNING: 'Поля, отмеченные *, обязательны к заполнению',
        PRODUCT_CARD_TITLE: 'Карточка товара',
        DESCRIPTION_TITLE: 'Описание'
      },
      lead: {
        NEW_LEAD_PAGE_TITLE: 'Новый лид',
        NEW_LEAD_SUB_TITLE: 'Вы создаете нового лида',
        NEW_LEAD_ACTION: 'Создать нового лида',
        WARNING: 'Поля, отмеченные *, обязательны к заполнению',
        INDIVIDUAL: 'Физическое лицо',
        LEGAL_ENTITY: 'Юридическое лицо',
        COMPANY: 'Компания',
        CONTACT: 'Контактное лицо'
      },
      client: {
        NEW_CLIENT_PAGE_TITLE: 'Новый клиент',
        NEW_CLIENT_SUB_TITLE: 'Вы создаете нового клиента',
        NEW_CLIENT_ACTION: 'Создать нового клиента',
        WARNING: 'Поля, отмеченные *, обязательны к заполнению',
        INDIVIDUAL: 'Физическое лицо',
        LEGAL_ENTITY: 'Юридическое лицо',
        COMPANY: 'Компания',
        CONTACT: 'Контактное лицо'
      },
      supplier: {
        NEW_SUPPLIER_PAGE_TITLE: 'Новый поставщик',
        NEW_SUPPLIER_SUB_TITLE: 'Вы создаете нового поставщика',
        NEW_SUPPLIER_ACTION: 'Создать нового поставщика',
        WARNING: 'Поля, отмеченные *, обязательны к заполнению',
        INDIVIDUAL: 'Физическое лицо',
        LEGAL_ENTITY: 'Юридическое лицо',
        COMPANY: 'Компания',
        CONTACT: 'Контактное лицо'
      },
      sales_plan: {
        NEW_SALES_PLAN_PAGE_TITLE: 'Новый план продаж',
        NEW_SALES_PLAN_SUB_TITLE: 'Вы создаете новый план продаж, будьте внимательны',
        KINDS: {
          BY_QUANTITY: 'В количестве',
          BY_SUM: 'В деньгах',
        }
      },
      task: {
        NEW_TASK_PAGE_TITLE: 'Новая задача',
        NEW_TASK_SUB_TITLE: 'Вы создаете новую задачу',
        WARNING: 'Все поля, отмеченные *, являются обязательными к заполнению',
      },
      NAME: 'Имя',
      EMAIL: 'E-mail',
      PASSWORD: 'Пароль',
      CHANGE_PASSWORD: 'Сменить пароль',
      CHANGE_PASSWORD_INSTRUCTIONS_SENT: 'Инструкции по смене пароля высланы на ваш e-mail',
    },
    model: {
      product: {
        PAGE_TITLE: 'Товары компании',
        SUB_TITLE: 'Все то, что можно продавать',
        INVITE_EMPLOYEE: 'Пригласить сотрудника',
        CREATE_PRODUCT: 'Добавить товар',
        ALREADY_EXISTS_TITLE: 'Товар уже существует',
        DELETE_EXISTS: 'Удалить существующий товар',
      },
      client: {
        PAGE_TITLE: 'Клиенты',
        SUB_TITLE: 'Ваша гордость и достижение',
        CREATE: 'Добавить клиента',
        SEARCH_RESULTS: 'Найдено {{count}} пользователей с именем {{name}}',
        COMPANY_SEARCH_RESULTS: 'По запросу {{search}} найдено {{count}} компаний',
      },
      supplier: {
        PAGE_TITLE: 'Поставщики',
        SUB_TITLE: 'Компаньоны вашего успеха',
        CREATE: 'Добавить поставщика',
      },
      product_category: {
        CREATE_TITLE: 'Создание категории товаров',
        EDIT_TITLE: 'Изменение категории товаров',
      },
      department: {
        CREATE_TITLE: 'Создание департамента',
        EDIT_TITLE: 'Изменение департамента',
      },
      generic_status: {
        CREATE_TITLE: 'Создание статуса',
        EDIT_TITLE: 'Изменение статуса',
      },
      lead: {
        PAGE_TITLE: 'Лиды',
        SUB_TITLE: 'Все еще не клиенты вашей компании',
        CREATE: 'Создать лид',
      },
      deal_products: {
        CREATE_TITLE: 'Добавление товара',
      },
      user: {
        PROFILE_TITLE: 'Мой профиль',
        EDIT_PROFILE_TITLE: 'Редактирование профиля'
      },
      company: {
        PAGE_TITLE: 'Компании',
        SUB_TITLE: 'Компании, в которых вы - сотрудник',
        CREATE_COMPANY: 'Добавить',
        SALES_FACT: 'Факт продаж',
        FACT: 'Факт',
        PLAN: 'План',
        DEALS_COUNT: 'Сделок',
        LEADS_COUNT: 'Лидов',
        CLIENTS_COUNT: 'Клиентов',
        PRODUCTS_COUNT: 'Товаров',
      },
      deal: {
        PAGE_TITLE: 'Сделки',
        SUB_TITLE: 'Результаты вашей работы',
        CREATE: 'Создать сделку',
        ADD_PRODUCT: 'Добавить товар'
      },
      employee: {
        PAGE_TITLE: 'Сотрудники компании',
        SUB_TITLE: 'Все те, кто работает в вашей компании',
        INVITE: 'Пригласить сотрудника',
        ADD_TO_DEPARTMENT: 'Добавить в департамент'
      },
      sales_plan: {
        MANAGE: 'Управление планом',
      },
      cash_book_category: {
        CREATE_TITLE: 'Создание категории',
        EDIT_TITLE: 'Изменение категории',
      }
    },
    common: {
      CLOSE: 'Закрыть',
      SAVE: 'Сохранить',
      SEND: 'Отправить',
      DELETE: 'Удалить',
      ADD: 'Добавить',
      CANCEL: 'Отмена',
      SAVING: 'Сохранение...',
      CONTINUE_EDITING: 'Продолжить редактирование',
      PROCESSING_PHOTOS: 'Обрабатываем фото',
      CREATE: 'Создать',
      BUSINESS_CARD: 'Визитка',
      SERVICE_INFORMATION: 'Служебная информация',
      CHOOSE: 'Выбрать',
    },
    months: {
      1: 'Янв',
      2: 'Фев',
      3: 'Мар',
      4: 'Апр',
      5: 'Май',
      6: 'Июн',
      7: 'Июл',
      8: 'Авг',
      9: 'Сен',
      10: 'Окт',
      11: 'Ноя',
      12: 'Дек',
    }
  }
)
