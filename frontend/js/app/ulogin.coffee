((window) ->
  getULoginData = (token, callback) =>
    url = "//ulogin.ru/token.php?host=" +
      encodeURIComponent(window.location.toString()) +
      "&token=" + token +
      "&callback=?"
    $.getJSON(url, (data) =>
      data = JSON.parse(data.toString())
      callback && callback(data)
    )

  window.authWithFacebook = (token) =>
    getULoginData(token, (data) =>
      if (data.error)
        console.log(data.error)
        return
      formScope = angular.element('.sing-in-form').scope()
      if formScope
        formScope.signFacebook(data)
    )

  window.signUpWithFacebook = (token) =>
    getULoginData(token, (data) =>
      if (data.error)
        console.log(data.error)
        return
      formScope = angular.element('.sing-up-form').scope()
      if formScope
        formScope.signFacebook(data)
    )

  window.linkFacebookProfile = (token) =>
    getULoginData(token, (data) =>
      if (data.error)
        console.log(data.error)
        return

      input = angular.element('input[name="facebook_link"]')
      if !input
        console.log('facebook_link input not found')
        return

      input.val(data.identity)

      scope = input.scope().$parent
      if scope
        scope.user.facebook_identity = data.uid
        scope.user.facebook_link = data.identity
    ))(window)