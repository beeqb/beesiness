app.constant('JQ_CONFIG',
    {
      easyPieChart:   [ 'assets/js/vendor/theme/jquery.easypiechart.fill.js'],
      sparkline:      [ 'assets/js/vendor/jquery.sparkline.js'],
      plot:           [ 'assets/js/vendor/jquery.flot.js',
                        'assets/js/vendor/jquery.flot.pie.js',
                        'assets/js/vendor/jquery.flot.resize.js',
                        'assets/js/vendor/jquery.flot.tooltip.js'
                        # '../libs/jquery/flot.orderbars/js/jquery.flot.orderBars.js',
                        # '../libs/jquery/flot-spline/js/jquery.flot.spline.min.js'
                      ],
      moment:         [   '../libs/jquery/moment/moment.js'],
      screenfull:     [   'assets/js/vendor/screenfull.js'],
      slimScroll:     [   '../libs/jquery/slimscroll/jquery.slimscroll.min.js'],
      sortable:       [   '../libs/jquery/html5sortable/jquery.sortable.js'],
      nestable:       [   '../libs/jquery/nestable/jquery.nestable.js',
                        '../libs/jquery/nestable/jquery.nestable.css'],
      filestyle:      [ 'assets/js/vendor/bootstrap-filestyle.js'],
      slider:         [   '../libs/jquery/bootstrap-slider/bootstrap-slider.js',
                        '../libs/jquery/bootstrap-slider/bootstrap-slider.css'],
      chosen:         [   '../libs/jquery/chosen/chosen.jquery.min.js',
                        '../libs/jquery/chosen/bootstrap-chosen.css'],
      TouchSpin:      [   '../libs/jquery/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js',
                        '../libs/jquery/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css'],
      wysiwyg:        [   '../libs/jquery/bootstrap-wysiwyg/bootstrap-wysiwyg.js',
                        '../libs/jquery/bootstrap-wysiwyg/external/jquery.hotkeys.js'],
      dataTable:      [   '../libs/jquery/datatables/media/js/jquery.dataTables.min.js',
                        '../libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.js',
                        '../libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.css'],
      vectorMap:      [   '../libs/jquery/bower-jvectormap/jquery-jvectormap-1.2.2.min.js',
                        '../libs/jquery/bower-jvectormap/jquery-jvectormap-world-mill-en.js',
                        '../libs/jquery/bower-jvectormap/jquery-jvectormap-us-aea-en.js',
                        '../libs/jquery/bower-jvectormap/jquery-jvectormap.css'],
      footable:       [   '../libs/jquery/footable/v3/js/footable.min.js',
                        '../libs/jquery/footable/v3/css/footable.bootstrap.min.css'],
      fullcalendar:   [ 'assets/js/vendor/fullcalendar.min.js',
                        'assets/css/fullcalendar.css',
                        'assets/css/fullcalendar.theme.css'],
      daterangepicker:[ 'assets/js/vendor/daterangepicker.js',
                        'assets/css/daterangepicker.css',
                      ],
      tagsinput:      [   '../libs/jquery/bootstrap-tagsinput/dist/bootstrap-tagsinput.js',
                        '../libs/jquery/bootstrap-tagsinput/dist/bootstrap-tagsinput.css']
                    
      }
    )
    .constant('MODULE_CONFIG',
      [
        {
          name: 'ngGrid',
          files: [
              '../libs/angular/ng-grid/build/ng-grid.min.js',
              '../libs/angular/ng-grid/ng-grid.min.css',
              '../libs/angular/ng-grid/ng-grid.bootstrap.css'
          ]
        },
        {
          name: 'ui.grid',
          files: [
              '../libs/angular/angular-ui-grid/ui-grid.min.js',
              '../libs/angular/angular-ui-grid/ui-grid.min.css',
              '../libs/angular/angular-ui-grid/ui-grid.bootstrap.css'
          ]
        },
        {
          name: 'ui.select',
          files: [
              '../libs/angular/angular-ui-select/dist/select.min.js',
              '../libs/angular/angular-ui-select/dist/select.min.css'
          ]
        },
        {
          name:'angularFileUpload',
          files: [
            '../libs/angular/angular-file-upload/angular-file-upload.js'
          ]
        },
        {
          name:'ui.calendar',
          files: ['/assets/js/vendor/calendar.js']
        },
        {
          name: 'ngImgCrop',
          files: [
              '../libs/angular/ngImgCrop/compile/minified/ng-img-crop.js',
              '../libs/angular/ngImgCrop/compile/minified/ng-img-crop.css'
          ]
        },
        {
          name: 'angularBootstrapNavTree',
          files: [
              '../libs/angular/angular-bootstrap-nav-tree/dist/abn_tree_directive.js',
              '../libs/angular/angular-bootstrap-nav-tree/dist/abn_tree.css'
          ]
        },
        {
          name: 'toaster',
          files: [
              '../libs/angular/angularjs-toaster/toaster.js',
              '../libs/angular/angularjs-toaster/toaster.css'
          ]
        },
        {
          name: 'vr.directives.slider',
          files: [
              '../libs/angular/venturocket-angular-slider/build/angular-slider.js',
              '../libs/angular/venturocket-angular-slider/build/angular-slider.css'
          ]
        },
        {
          name: 'com.2fdevs.videogular',
          files: [
              '../libs/angular/videogular/videogular.min.js'
          ]
        },
        {
          name: 'com.2fdevs.videogular.plugins.controls',
          files: [
              '../libs/angular/videogular-controls/controls.min.js'
          ]
        },
        {
          name: 'com.2fdevs.videogular.plugins.buffering',
          files: [
              '../libs/angular/videogular-buffering/buffering.min.js'
          ]
        },
        {
          name: 'com.2fdevs.videogular.plugins.overlayplay',
          files: [
              '../libs/angular/videogular-overlay-play/overlay-play.min.js'
          ]
        },
        {
          name: 'com.2fdevs.videogular.plugins.poster',
          files: [
              '../libs/angular/videogular-poster/poster.min.js'
          ]
        },
        {
          name: 'com.2fdevs.videogular.plugins.imaads',
          files: [
              '../libs/angular/videogular-ima-ads/ima-ads.min.js'
          ]
        },
        {
          name: 'xeditable',
          files: [
              '../libs/angular/angular-xeditable/dist/js/xeditable.min.js',
              '../libs/angular/angular-xeditable/dist/css/xeditable.css'
          ]
        },
        {
          name: 'smart-table',
          files: [
              '../libs/angular/angular-smart-table/dist/smart-table.min.js'
          ]
        },
        {
          name: 'angular-skycons',
          files: [
              '../libs/angular/angular-skycons/angular-skycons.js'
          ]
        }
      ]
    )
    .config(['$ocLazyLoadProvider', 'MODULE_CONFIG', ($ocLazyLoadProvider, MODULE_CONFIG) ->
      $ocLazyLoadProvider.config(
        {
          debug:  false,
          events: true,
          modules: MODULE_CONFIG
        }
      )
    ])
