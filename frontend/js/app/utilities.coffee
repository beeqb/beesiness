App.createFactory = (classObject) ->
  importValues = (classObject._import || [])

  prepareImportName = (importName) ->
    if importName.substr(0, 1) == '$'
      importName = importName.slice(1)

    importName = importName.substr(0, 1).toLowerCase() + importName.slice(1)
    importName = '_' + importName

    return importName

  factory = () ->
    args = Array.prototype.concat.apply([], arguments)

    result = new classObject()

    for importName, index in importValues
      result[prepareImportName(importName)] = args[index]

    if result.init != undefined
      result.init()

    return result

  return importValues.concat(factory)
