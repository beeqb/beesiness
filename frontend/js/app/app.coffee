@App = {}

String.prototype.toCamelCase = () ->
  @replace(/_([a-z])/g, (match) -> match[1].toUpperCase())

String.prototype.toSnakeCase = () ->
  @replace(/([A-Z])/g, (match) -> '_' + match[1].toLowerCase())

String.prototype.capitalize = () ->
  @charAt(0).toUpperCase() + @slice(1)

@app = angular.module('app', [
  'ngRoute',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngTouch',
  'ipCookie',
  'ng-token-auth',
  'ui.load',
  'ui.jq',
  'ui.bootstrap',
  'ui.mask',
  'ui.scroll',
  'ui.select',
  'ui.tree',
  'oc.lazyLoad',
  'pascalprecht.translate',
  'xeditable',
  'easypiechart',
  'ngFileUpload',
  'nemLogging',
  'uiGmapgoogle-maps',
  'geolocation',
  'angularModalService',
  'angucomplete-alt',
  'LocalStorageModule',
  'vr.directives.slider',
  'infinite-scroll',
  'toaster',
  'textAngular',
])

accounting.settings.currency.format = '%v %s' # 'сумма валюта', например 100,00 rub

app.run(($http,
  $rootScope,
  $location,
  $route,
  $auth,
  editableOptions,
  $translate,
  $cookies,
  $filter,
  ResourceLoader) ->
  window.filter = $filter('filter')
  $http.defaults.headers.common['Content-Type'] = 'application/json'
  $http.defaults.headers.common['Accept'] = 'application/json'

  editableOptions.theme = 'bs3'

  $rootScope.defaultAvatar = 'assets/img/default-avatar.png'


  $rootScope.overlayMessage = null
  $rootScope.showOverlay = ((message) ->
    $rootScope.overlayMessage = message
  )
  $rootScope.hideOverlay = (() ->
    $rootScope.overlayMessage = null
  )

  $rootScope.range = ((number) ->
    return [1..number]
  )

  # Toggle sidebar
  $rootScope.toggleSidebar = (() ->
    $rootScope.viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)

    if $rootScope.viewportWidth <= 992
      $rootScope.app.settings.asideFolded = true
    else
      $rootScope.app.settings.asideFolded = false

    $rootScope.$apply()
  )

  $rootScope.app = {
    name: 'Angulr',
    version: '2.2.0',
    color: {
      primary: '#7266ba',
      info: '#23b7e5',
      success: '#27c24c',
      warning: '#fad733',
      danger: '#f05050',
      light: '#e8eff0',
      dark: '#3a3f51',
      black: '#1c2b36'
    },
    settings: {
      themeID: 1,
      navbarHeaderColor: 'bg-black',
      navbarCollapseColor: 'bg-white-only',
      asideColor: 'bg-black',
      headerFixed: true,
      asideFixed: false,
      asideFolded: false,
      asideDock: false,
      container: false
    }
  }

  # Toggle Sidebar for tablets
  $rootScope.toggleSidebar()

  window.onresize = (e) ->
    if e.target.window != undefined
      $rootScope.toggleSidebar()

  $rootScope.$on('auth:login-success', () ->
    $location.path('/')
  )

  $rootScope.$on('user:changed', () =>
    $rootScope.user.synced = false
    resource = new ResourceLoader()

    resource
    .include('employees', 'permissions', 'roles_in_companies')
    .get('users', $rootScope.user.id)
    .then((response) =>
      _.extend($rootScope.user, response.data.entity)
      $rootScope.user.synced = true
    )
  )

  $rootScope.$on('user:reloadPermissions', () =>
    $rootScope.user.synced = false

    $http.get("/api/users/#{$rootScope.user.id}?include=permissions")
    .then((response) =>
      $rootScope.user.permissions = response.data.entity.permissions
      $rootScope.user.synced = true
    )
  )

  $rootScope.isFirstPageLoading = true
  $rootScope.$watch('user.id', () =>
    return unless $rootScope.user && $rootScope.user.id

    $rootScope.user.employeeInCompany = ((companyId) ->
      _.findWhere(@employees, {company_id: Number(companyId)})
    )

    $rootScope.user.hasRole = ((roleName, companyId) ->
      roles =
        _.findWhere(
          @roles_in_companies,
          {company_id: Number(companyId)},
        )?.roles

      return false unless roles

      _.any(roles, (role) -> role.name == roleName)
    )

    $rootScope.user.can = ((checkedAction, checkedModel, condition = null) ->
      for action, models of $rootScope.user.permissions
        if action != checkedAction && action != 'manage'
          continue

        for model in models
          if model.name != 'all' && model.name != checkedModel
            continue

          if (typeof condition) == 'object'
            can = true

            for key, value of condition
              modelCondition = model.conditions?[key]

              unless modelCondition?
                can = false
                break

              if _.isArray(modelCondition)
                can = value in modelCondition
              else
                can = value == modelCondition

              break unless can

            if can
              return true


          if (typeof condition) == 'number'
            # check class-wide permission
            if model.ids == undefined && condition == null
              return true

            # check instance-wide permissions
            if model.ids == undefined || model.ids.indexOf(condition) != -1
              return true

      return false
    )

    $rootScope.$emit('user:changed')
  )

  $auth.validateUser()
  .then(
    (()->),
    (()->
      public_urls = [
        '/users/sign_in',
        '/users/sign_up',
        '/users/sign_up_success',
        '/users/restore_password',
        '/users/restore_password_success',
        '/policy'
      ]
      if _.contains(public_urls, $location.path())
        return
      $location.path('/users/sign_in')
    ),
  )

  $rootScope.$watch('user.lang', () =>
    if $rootScope.user.lang
      $translate.use($rootScope.user.lang)
      $cookies.put('lang', '"' + $rootScope.user.lang + '"')
  )

  # Хак для исправной работы jQuery-плагинов, которые используют
  # $.ajax для AJAX-запросов (например, fullcalendar)
  # Проблема в том, что при использовании $.ajax запрос не обрабатывается
  # библиотекой ng-token-auth, и токен аутентификации становится
  # невалидным.
  #
  # Источник: https://github.com/lynndylanhurley/ng-token-auth/issues/163
  $(document).ajaxComplete((ev, xhr, settings) ->
    # set header for each key in `tokenFormat` config
    newHeaders = {}

    # set flag to ensure that we don't accidentally nuke the headers
    # if the response tokens aren't sent back from the API
    blankHeaders = true

    # set header key + val for each key in `tokenFormat` config
    for key in $auth.getConfig().tokenFormat
      newHeaders[key] = xhr.getResponseHeader(key)

      if newHeaders[key]
        blankHeaders = false

    if !blankHeaders
      $auth.setAuthHeaders(newHeaders)
  )

  _.mixin({
    setAll: ((collection, attr, value) ->
      return unless collection

      for item in collection
        if _.isFunction(value)
          item[attr] = value(item)
        else
          item[attr] = value

      return
    ),
    sumProperty: ((collection, property) ->
      _(collection)
        .reduce(((memo, item) -> memo + 1 * item[property]), 0)
    ),
  })
)
app.config((localStorageServiceProvider) ->
  localStorageServiceProvider.setPrefix('beeqb')
)

app.config(($locationProvider) ->
  $locationProvider.html5Mode(true)
)

app.config(($authProvider) ->
  $authProvider.configure({
    confirmationSuccessUrl: 'profile?edit=true',
    passwordResetSuccessUrl: 'users/change_password',
    validateOnPageLoad: false,
  })
)

app.config((uiGmapGoogleMapApiProvider) ->
  getCookie = (name) ->
    nameEQ = name + "="
    ca = document.cookie.split("")
    i = 0

    while i < ca.length
      c = ca[i]
      c = c.substring(1, c.length)  while c.charAt(0) is " "
      return c.substring(nameEQ.length, c.length)  if c.indexOf(nameEQ) is 0
      i++
    null

  if getCookie('lang')
    lang = getCookie('lang').replace(/%22/g, '')
  else
    lang = navigator.browserLanguage || navigator.language || navigator.userLanguage

  uiGmapGoogleMapApiProvider.configure({
    v: '3',
    libraries: 'weather,geometry,visualization',
    sensors: false,
    key: 'AIzaSyB6cOoTJ6bIw4GKXcB9GXEmu0aGmx50svw',
    language: lang
  })
)

app.config(($httpProvider) ->
  convertDateStringsToDateObjects = ((input) ->
    return input unless angular.isObject(input)

    result = angular.copy(input)

    for key, value of result
      # Check for string properties which look like dates.
      if /^\d{4}-\d{2}-\d{2}/.test(value)
        result[key] = moment(value).toDate()
      else if angular.isObject(value)
        result[key] = convertDateStringsToDateObjects(value)

    result
  )

  $httpProvider.defaults.transformResponse.push((responseData) ->
    convertDateStringsToDateObjects(responseData)
  )
)

app.config(($provide) ->
  $provide.decorator('taOptions', (taRegisterTool, $delegate) ->
    insertTextAtCursor = (text) ->
      if window.getSelection
        sel = window.getSelection()

        if sel.getRangeAt && sel.rangeCount
          range = sel.getRangeAt(0)
          range.deleteContents()
          range.insertNode(document.createTextNode(text))

      else if document.selection && document.selection.createRange
        document.selection.createRange().text = text

    moveCaret = (charCount) ->
      if window.getSelection
        sel = window.getSelection()

        if sel.rangeCount > 0
            textNode = sel.focusNode
            sel.collapse(textNode.nextSibling, charCount)

      else if sel = window.document.selection
        if sel.type != 'Control'
            range = sel.createRange()
            range.move('character', charCount)
            range.select()

    surroundSelection = (element) ->
      if window.getSelection
        sel = window.getSelection()
        if sel.rangeCount
          range = sel.getRangeAt(0).cloneRange()
          range.surroundContents(element)
          sel.removeAllRanges()
          sel.addRange(range)

    taRegisterTool('floatLeft', {
      buttontext: 'fl',
      action: () ->
        element = document.createElement('span')
        element.style.float = 'left'
        surroundSelection(element)
    })

    taRegisterTool('floatRight', {
      buttontext: 'fr',
      action: () ->
        element = document.createElement('span')
        element.style.float = 'right'
        surroundSelection(element)
    })

    placeholders = {
      company_name: 'Имя компании',
      company_address: 'Адрес компании',
      company_vat: 'НДС',
      manager_fullname: 'Менеджер',
      company_inn: 'ИНН Компании',
      payed_at: 'Дата оплаты',
      payment_option: 'Форма оплаты',
      paid_amount: 'Оплачено',
      change: 'Сдача',
      cashbox_name: 'Касса',
      receipt_kind: 'Расчетный признак',
      product_name: 'Наименование товара',
      product_quantity: 'Кол-во товара',
      product_price: 'Цена товара',
      discount_sum: 'Сумма скидки',
      vat_sum: 'Сумма НДС',
      total_sum: 'Итого',
    }

    _(placeholders).each((toolLabel, toolName) ->
      taRegisterTool(toolName.toCamelCase(), {
        buttontext: toolLabel,
        action: () -> insertTextAtCursor("%{#{toolName}}")
      })
    )

    $delegate
  )
)

app.constant('mapConfig', {
  isManualChange: true,
  center: {
    latitude: 55,
    longitude: 37
  },
  options: {
    draggableCursor: 'default'
  },
  marker: {
    id: 0,
    coords: {
      latitude: 55,
      longitude: 37
    },
    options: {
      draggable: false
    },
    events: {}
  }
  defaultCoords: {
    latitude: 55,
    longitude: 37
  }
})
