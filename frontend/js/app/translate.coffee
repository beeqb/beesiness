app.config ($translateProvider, LOCALE_EN, LOCALE_RU) ->
  $translateProvider.translations('ru', LOCALE_RU)
  $translateProvider.translations('en', LOCALE_EN)
  $translateProvider.useSanitizeValueStrategy('escape')

app.run ($translate, $window, $cookies, $rootScope) ->
  lang = $cookies.getObject('lang')
  unless lang
    lang = $window.navigator.language || $window.navigator.userLanguage
    switch
      when lang in ['en', 'en-US', 'en-us'] then (
        lang = 'en'
      )
      when lang in ['ru', 'ru-RU', 'ru-ru'] then (
        lang = 'ru'
      )
      else
        lang = 'en'

  $rootScope.user.lang = lang
  $translate.use(lang)
