angular.module('app').service('dealCalculator', ($http) ->
  return {
    calculate: ((deal, dealProducts, vat, discount) ->
      deal.check_sum = 0
      for dealProduct in dealProducts
        continue if dealProduct.returned_at

        deal.check_sum +=
          dealProduct.price *
          (dealProduct.count_minus_returns || dealProduct.count)

      deal.discount_sum = deal.check_sum * ((discount || 0) / 100)
      deal.total_sum = deal.check_sum - deal.discount_sum

      deal.vat_sum = deal.total_sum * ((vat || 0) / 100)
    ),

    saveCalculation: ((deal) ->
      url = '/api/companies/' + deal.company_id +
        '/deals/' + deal.id

      $http.put(url, {deal: deal})
    )
  }
)

