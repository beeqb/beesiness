angular.module('app').factory('Department', ($resource) ->
  return $resource(
    '/api/companies/:company_id/departments/:id',
    {
      company_id: '@company_id',
    },
    {
      query: {
        method: 'GET',
        isArray: true,
        transformResponse: ((data) ->
          data = angular.fromJson(data)
          return data.entities
        )
      }
    }
  )
)
