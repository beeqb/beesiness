angular.module('app').factory('getCompanyIdFromCurrentRoute', ($location, $routeParams) ->
  return () ->
    path = $location.path()

    return null unless /companies/.test(path) && !/new/.test(path)

    $routeParams.company_id || $routeParams.id
)
