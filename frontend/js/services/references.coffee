angular.module('app').factory('References', ($http) ->
  all = {}

  {
    sync: (() ->
      $http.get('/api/references').then((response) =>
        all = response.data
      )
    ),
    all: (() ->
      all
    ),
  }
)
