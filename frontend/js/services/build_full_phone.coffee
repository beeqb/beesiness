angular.module('app').factory('buildFullPhone', () ->
  (model, phoneType, countryPhoneCodes) ->
    codeId = model["#{phoneType}_code_id"]
    phone = model[phoneType]

    code = _.findWhere(countryPhoneCodes, {id: codeId})?.code

    return phone unless code

    code + ' ' + phone
)
