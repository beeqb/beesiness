angular.module('app').factory('Client', ($resource) ->
  return $resource(
    '/api/companies/:company_id/clients/:id',
    {
      company_id: '@company_id',
    },
    {
      query: {
        method: 'GET',
        isArray: true,
        transformResponse: ((data) ->
          data = angular.fromJson(data)
          return data.entities
        )
      },
      save: {
        method: 'POST',
        transformRequest: ((data) ->
          data.work_phone_full =
            data.work_phone_code + ' ' + data.work_phone

          data.cell_phone_full =
            data.cell_phone_code + ' ' + data.cell_phone

          return angular.toJson(data)
        )
      }
    }
  )
)
