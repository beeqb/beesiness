angular.module('app').factory('Page', () ->
  prefix = 'beeqb'
  title = 'beeqb'

  sectionTitles = {
    leads: 'Лиды',
    clients: 'Клиенты',
    deals: 'Сделки',
    employees: 'Сотрудники',
    products: 'Товары',
    suppliers: 'Поставщики',
    tasks: 'Задачи',
    user_profile: 'Профиль',
    home: 'Главная страница',
    dashboard: 'Dashboard',
    crm: 'aCRM',
    pos: 'POS',
    companies: 'Компании',
    sales_plans: 'План продаж',
    cash_book: 'Кассовая книга',
    budget: 'Фактический бюджет',
    sales_report: 'Отчет по продажам',
    break_even_analysis: 'Анализ безубыточности',
  }

  return {
    setTitle: (section, company_name = null) ->
      if company_name
        title = "#{prefix}.#{company_name}.#{sectionTitles[section]}"
      else
        title = "#{prefix}.#{sectionTitles[section]}"
    getTitle: () -> title
  }
)
