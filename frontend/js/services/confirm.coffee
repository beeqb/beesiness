class App.Confirm extends IdFly.AngularClass

  @_import = [
    'ModalService',
  ]

  app.factory('Confirm', @class())

  @confirm: (config, success, cancel) ->
    @_ModalService.showModal({
      templateUrl: 'assets/views/modals/confirm.html',
      controller: 'ModalsConfirmCtrl',
      inputs: {
        config: _.extend({
          title: 'Подтвердите действие',
          noText: 'Нет',
          yesText: 'Да',
        }, config),
      }
    }).then((modal) ->
      modal.close.then((confirm) ->
        if confirm == true
          success && success()
        if confirm == false
          cancel && cancel()
      )
    )
