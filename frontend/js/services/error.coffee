class App.Error extends IdFly.AngularClass

  app.factory('Error', @class())

  @raise: (type, message) ->
    error = new Error(message)
    error.type = type
    raise error

  @bindCleaner: (scope, target) ->
    watcher = (current, old) =>
      object = scope
      for item in target.split('.')
        object = object[item]

      if !object.errors
        return

      for key in @_compare(current, old)
        if object.errors[key]
          object.errors[key] = undefined

    scope.$watch(target, watcher, true)

  @_compare: (current, old, path = []) ->
    if typeof current == 'object' && typeof old == 'object'
      result = _.inject(
        current,
        ((memo, value, key) =>
          if path.length == 0 && (key == 'errors' || key == 'success')
            return memo

          currentResult = @_compare(
            value,
            (old || {})[key],
            path.concat([key]),
          )

          return memo.concat(currentResult)
        ),
        [],
      )

      return result

    if current == old
      return []

    return [path.join('.')]
