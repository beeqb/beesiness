angular.module('app').factory('Employee', ($resource) ->
  return $resource(
    '/api/companies/:company_id/employees/:id',
    {
      company_id: '@company_id',
    },
    {
      query: {
        method: 'GET',
        isArray: true,
        transformResponse: ((data) ->
          data = angular.fromJson(data)
          return data.entities
        )
      }
    }
  )
)
