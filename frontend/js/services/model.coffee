class App.Model extends IdFly.AngularModel

  @_result: (promise) ->
    model = super(promise)

    promise.then(
      ((response) =>
        if response.data.result
          model.extend(response.data.result)

        return response
      ),
      ((response) =>
        model.success = false
        model.error = 'Ошибка: ' + response.statusText
      ),
    )

    return model
