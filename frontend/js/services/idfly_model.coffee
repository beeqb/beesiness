class IdFly.AngularModel extends IdFly.AngularClass

  @_import: [
    '$http',
  ]

  @url = undefined

  @list: (query) ->
    promise = @_http(
      url: @getUrl(),
      method: 'GET',
      params: query,
    )

    result = []

    Object.defineProperties(
      result,
      {
        httpPromise: {configurable: true, value: promise},
        httpStatus: {configurable: true, value: promise.$$state},
      },
    )

    result.then = ((success, error) ->
      return promise.then(success, error)
    )

    promise.then((response) =>
      for item in response.data
        result.push(new this(item))
    )

    return result

  @get: (id, options) ->
    promise = @_http({
      url: @getUrl(id),
      method: 'GET',
      params: options,
    })

    return @_result(promise)

  @create: (data) ->
    promise = @_http.post(@getUrl(), data)
    return @_result(promise)

  @update: (id, data) ->
    promise = @_http.post(@getUrl(id), data)
    return @_result(promise)

  @delete: (id) ->
    promise = @_http.delete(@getUrl(id))
    return @_result(promise)

  @getUrl: (id) ->
    if @url == undefined
      throw new Error('@url should be defined in child class')

    result = @url
    if id
      result += '/' + id

    return result

  @_result: (promise) ->
    model = new this()

    Object.defineProperties(
      model,
      {
        httpPromise: {configurable: true, value: promise},
        httpStatus: {configurable: true, value: promise.$$state},
      },
    )

    promise.then((response) =>
      model.extend(response.data)
      return response
    )

    return model

  httpPromise: null
  httpStatus: null

  extend: (attributes) ->
    for key, value of attributes
      if !@hasOwnProperty(key) && this[key] != undefined
        continue

      this[key] = value

  constructor: (attributes) ->
    if attributes
      @extend(attributes)

  then: (success, error) ->
    if !@httpPromise
      throw new Error('httpPromise is not defined in model: no action was ' +
        'done before calling then')

    return @httpPromise.then(success, error)

  getId: () ->
    return @id

  getUrl: () ->
    return @constructor.getUrl(@getId())

  getData: () ->
    result = {}
    for key, value of this
      if !@propertyIsEnumerable(key)
        continue

      result[key] = value

    return result

  save: (attributes) ->
    if attributes
      @extend(attributes)

    id = @getId()
    data = @getData()

    if id
      model = @constructor.update(id, data)
    else
      model = @constructor.create(data)

    @_result(model, data)

    return model

  delete: () ->
    model = @constructor.delete(@getId())
    @_result(model)
    return model

  _result: (model, data) ->
    Object.defineProperties(
      this,
      {
        httpPromise: {configurable: true, value: model.httpPromise},
        httpStatus: {configurable: true, value: model.httpStatus},
      },
    )

    model.extend(data)
    extend = (() =>
      @extend(model.getData())
    )

    model.then(extend, extend)