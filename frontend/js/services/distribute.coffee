angular.module('app').factory('distribute', () ->
  (target, n) ->
    share = Math.floor(target / n)
    residue = target - n * share

    result = new Array(n)
    result.fill(share)
    result[-1..] = share + residue

    result
)
