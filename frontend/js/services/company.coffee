angular.module('app').factory('CompanyService', ($http) ->
  url = ((id, path, includes) ->
    result = "/api/companies/#{id}/#{path}"

    if includes.length isnt 0
      result += '?include=' + includes.join(',')

    return result
  )

  return {
    getDepartments: ((id, includes = []) ->
      url = url(id, 'departments', includes)
      $http.get(url)
    ),
    getEmployees: ((id, includes) ->
    )
  }
)
