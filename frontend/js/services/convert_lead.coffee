angular.module('app').factory('convertLead', ($http, $location, leadProducts, Confirm) =>
  (lead) =>
    lead.status = 'client_new'
    lead.created_at = moment().format()

    url = "/api/companies/#{lead.company_id}/leads/#{lead.id}/convert"

    $http
    .put(url, {lead: lead})
    .then((response) =>
      if _.isEmpty(lead.lead_products)
        $location.path("/companies/#{lead.company_id}/clients/#{response.data.id}")
        return

      Confirm.confirm(
        {
          question: 'Для лида были добавлены товары. Оформить сделку? Если нет, привязка будет удалена.',
          title: null,
        },
        (() =>
          leadProducts.addProducts(lead.id, lead.lead_products)
          $location.path("/companies/#{lead.company_id}/deals/new")
          $location.search({from_lead: lead.id, client_id: response.data.id})
        ),
        (() => $location.path("/companies/#{lead.company_id}/clients/#{response.data.id}"))
      )
    )
)
