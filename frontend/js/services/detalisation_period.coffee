angular.module('app').factory('detalisationPeriod', () ->
	return {
		day: {
			id: 'day',
			name: 'День',
		},
		week: {
			id: 'week',
			name: 'Неделя',
		},
		month: {
			id: 'month',
			name: 'Месяц',
		},
		quarter: {
			id: 'quarter',
			name: 'Квартал',
		},
		year: {
			id: 'year',
			name: 'Год',
		}
	}
)
