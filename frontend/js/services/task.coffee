angular.module('app').factory('Task', ($resource) ->
  return $resource(
    '/api/tasks/:id',
    {
      id: '@id',
    },
    {
      query: {
        url: '/api/tasks',
        method: 'GET',
        isArray: false,
        transformResponse: ((data) ->
          data = angular.fromJson(data)
          for task in data.entities
            task.completed = task.status == 'done'

          return data
        )
      },
      finishAll: {
        url: '/api/tasks/finish_all',
        isArray: true,
        method: 'POST',
      },
      save: {
        method: 'PUT',
        transformRequest: ((data) ->
          data = {task: data}

          return angular.toJson(data)
        )
      }
    }
  )
)
