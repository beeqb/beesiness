angular.module('app').factory('ResourceLoader', ($http) ->
  class ResourceLoader
    constructor: (@companyId) ->
      @resetAllParams()

    buildUrl: ((resourceNamePlural, id = null) ->
      if @companyId
        url = "/api/companies/#{@companyId}/#{resourceNamePlural}"
      else
        url = "/api/#{resourceNamePlural}"

      if id
        return url + '/' + id

      return url
    )

    buildParams: (() ->
      params = @paramsObj || {}

      unless _.isEmpty(@includeParam)
        params.include = @includeParam.join(',')

      unless _.isEmpty(@expandParam)
        params.expand = @expandParam.join(',')

      return params
    )

    params: (object) ->
      @paramsObj = object || {}
      return @

    include: (include...) ->
      @includeParam = include
      return @

    expand: (expand...) ->
      @expandParam = @expandParam.concat(expand)
      return @

    expandReferencesAndLabels: () ->
      @expandParam = ['references', 'labels']
      return @

    expandLabelsAndReferences: () ->
      @expandReferencesAndLabels()

    resetAllParams: () ->
      @expandParam = []
      @includeParam = []
      @paramsObj = {}

    query: (resourceNamePlural, params = {}) ->
      url = @buildUrl(resourceNamePlural)
      _.extend(params, @buildParams())

      promise = $http.get(url, {params: params})

      @resetAllParams()

      return promise

    get: (resourceNamePlural, id) ->
      url = @buildUrl(resourceNamePlural, id)

      params = @buildParams()

      promise = $http.get(url, {params: params})

      @resetAllParams()

      return promise

    post: (resourceNamePlural, data) ->
      url = @buildUrl(resourceNamePlural)
      params = @buildParams()

      promise = $http.post(url, data, {params: params})

      @resetAllParams()

      return promise

    put: (resourceNamePlural, id,  data) ->
      url = @buildUrl(resourceNamePlural, id)
      params = @buildParams()

      promise = $http.put(url, data, {params: params})

      @resetAllParams()

      return promise

    delete: (resourceNamePlural, id) ->
      url = @buildUrl(resourceNamePlural, id)
      params = @buildParams()

      promise = $http.delete(url, {params: params})

      @resetAllParams()

      return promise

  ResourceLoader
)
