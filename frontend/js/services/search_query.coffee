angular.module('app').factory('SearchQuery', ($http) ->
  {
    perform: (token, companyId, section = null) ->
      search_url = "/api/companies/#{companyId}/search_entities"
      $http
      .get(
        search_url,
        {
          params: {token: token, section: section},
        },
      )
      .then((response) ->
        return response.data
      )
  }
)
