angular.module('app').factory('leadProducts', () ->
  leadProducts = {}

  {
    addProducts: ((leadId, data) =>
      leadProducts[Number(leadId)] = data
      leadProducts
    ),
    popProducts: ((leadId) =>
      leadId = Number(leadId)

      leadProduct = leadProducts[leadId]
      leadProducts = _(leadProducts).omit(leadId)
      console.log leadProducts
      leadProduct
    ),
  }
)
