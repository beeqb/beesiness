class App.NavCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$location',
    '$routeParams',
  ]

  app.controller('NavCtrl', @factory())

  initialize: () =>
    @_rootScope.$on('$routeChangeSuccess', (event, current, previous) =>
      return unless @_isCompanyPath(@_location.path())

      @_scope.companyId = @_routeParams.company_id || @_routeParams.id
    )

    if @_isCompanyPath(@_location.path())
      @_scope.companyId = @_routeParams.company_id || @_routeParams.id

    @_scope.is = @_is

  _is: (location) =>
    path = @_location.path()
    if !@_isCompanyPath(path)
      return false

    return path.indexOf('/' + location) != -1

  _isCompanyPath: (path) =>
    return /companies\/\d+/.test(path)
