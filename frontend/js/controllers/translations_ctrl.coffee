class App.TranslationsCtrl extends IdFly.AngularClass

  @_import: [
    '$scope',
    '$http',
    'LOCALE_RU',
    'LOCALE_EN',
  ]

  app.controller('TranslationsCtrl', @factory())

  initialize: () =>
    @_scope.data = []

    parse = (data, prefix = null) =>
      for key, value of data
        if typeof(value) == 'object'
          if prefix
            key = prefix + '.' + key
          parse(value, key)
          continue

        originalKey = prefix + '.' + key
        key = originalKey
        if originalKey.indexOf('ru.') == 0 || originalKey.indexOf('en.') == 0
          key = key.substr(3)

        if originalKey.indexOf('ru.') == 0
          lang = 'ru'
        if originalKey.indexOf('en.') == 0
          lang = 'en'

        item = _.find(@_scope.data, (i) =>
          return i.key == key
        )
        if !item
          item = {
            key: key,
            values: [],
          }
          @_scope.data.push(item)

        item.values.push({
          lang: lang,
          value: value,
        })

    parse({
      ru: @_LOCALE_RU,
      en: @_LOCALE_EN
    })

    @_http
    .get('/api/translations')
    .then((response) =>
      parse(response.data)
    )