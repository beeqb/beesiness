class App.IndexCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    'Page',
    'ResourceLoader'
  ]

  app.controller('IndexCtrl', @factory())

  initialize: () =>
    @_Page.setTitle('home')
    if @_rootScope.user && @_rootScope.user.id
      if !@_rootScope.isFirstPageLoading
        @_rootScope.$emit('user:changed')
      else
        @_rootScope.isFirstPageLoading = false
