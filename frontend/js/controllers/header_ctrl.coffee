class App.HeaderCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$routeParams',
    'ResourceLoader',
    'getCompanyIdFromCurrentRoute',
  ]

  app.controller('HeaderCtrl', @factory())

  initialize: () =>
    @_rootScope.$on('$routeChangeSuccess', @_setVariables)
    @_setVariables()

    @_resource = new @_ResourceLoader()

    @_inProgress = false
    @_scope.notifications = []
    @_scope.notificationsTotalCount = 0
    @_notificationPage = 1
    @_loadNotifications(5, @_notificationPage)
    @_scope.viewNotification = @_viewNotification
    @_scope.showMoreNotification = (() =>
      return if @_inProgress
      @_inProgress = true
      @_loadNotifications(3, ++@_notificationPage)
    )
    @_scope.markAllNotificationsAsRead = @_markAllNotificationsAsRead

    @_scope.redirectToSearchPage = @_redirectToSearchPage
    @_scope.search = {
      token: @_routeParams.search_token || '',
      section: @_routeParams.search_section || null,
      invalidToken: false,
    }

    @_scope.validateSearchToken = @_validateSearchToken

    @_scope.chartValue = 0
    @_resource
    .query('tasks')
    .then((res) =>
      done = res.data.entities.filter (x) -> x.status == "done"
      chartValue = (done.length / res.data.total_count) * 100
      @_scope.chartValue = chartValue
    )

    @_scope.chartOptions = {
      lineWidth: 50,
      trackColor: '#e8eff0',
      barColor: '#27c24c',
      scaleColor: false,
      size: 100,
      rotate: 0,
      lineCap: 'butt',
      animate: 2000
    }

  _viewNotification: (notification) =>
    notification.viewed_at = moment().toDate()
    @_http
    .patch('/api/notifications/' + notification.id, {
      notification: notification
    })
    .then(() =>
      @_scope.notificationsTotalCount--
    )

  _markAllNotificationsAsRead: () =>
    @_resource
    .post('notifications/mark_all_as_read')
    .then(() =>
      _.setAll(@_scope.notifications, 'viewed_at', moment().toDate())
      @_scope.notificationsTotalCount = 0
    )

  _loadNotifications: (page_size, page) =>
    params = {
      user_id: @_scope.user.id,
      viewed_at: null,
      page: page,
      page_size: page_size,
    }
    @_http
    .get('/api/notifications', {params: params})
    .then((result) =>
      _.each(result.data.entities, (notification) =>
        @_scope.notifications.push(notification)
      )
      @_scope.notificationsTotalCount = result.data.total_count
      @_inProgress = false
    )

  _setVariables: () =>
    @_scope.currentCompanyId = @_getCompanyIdFromCurrentRoute()
    @_scope.isCurrentRouteRelatesToCompanies = !!@_scope.currentCompanyId

  _redirectToSearchPage: () =>
    @_validateSearchToken()

    return if @_scope.search.invalidToken

    @_location
    .path(
      "/companies/#{@_scope.currentCompanyId}/search-results"
    )
    .search({
      search_token: @_scope.search.token,
      search_section: @_scope.search.section,
    })

  _validateSearchToken: () =>
    @_scope.search.invalidToken = @_scope.search.token.length < 3
