class App.UsersRestorePasswordCtrl extends IdFly.AngularClass

  @_import: [
    '$scope',
    '$location',
    '$auth',
    '$routeParams',
  ]

  app.controller('UsersRestorePasswordCtrl', @factory())

  initialize: () ->
    @_scope.submit = @_submit
    @_scope.user = {
      email: @_routeParams.email,
      errors: {},
      labels: {
        email: 'Email',
      }
    }

  _submit: () =>
    @_auth.requestPasswordReset({email: @_scope.user.email})
    .then(() =>
      @_location.path('/users/restore_password_success')
    )
    .catch((response) =>
      @_scope.user.errors.email = response.data.errors
    )
