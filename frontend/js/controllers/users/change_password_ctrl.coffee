class App.UsersChangePasswordCtrl extends IdFly.AngularClass

  @_import: [
    '$scope',
    '$rootScope',
    '$http',
    '$location',
    '$routeParams',
    '$timeout',
  ]

  app.controller('UsersChangePasswordCtrl', @factory())

  initialize: () ->
    @_scope.submit = @_submit
    @_scope.changeSuccessMessage = false

    @_scope.changePasswordForm = {
      password: '',
      password_confirmation: '',
      errors: {}
    }

  _submit: () =>
    @_scope.updatePassword(@_scope.changePasswordForm)
    .then((response) =>
      @_scope.changeSuccessMessage = response.data.message
      @_rootScope.user.allow_change_password = false
      @_timeout(
        (() =>
          @_location.path('profile')
        ),
        2000
      )
    )
    .catch((response) =>
      if response.data
        _.extend(@_scope.changePasswordForm.errors, response.data.errors)
    )
