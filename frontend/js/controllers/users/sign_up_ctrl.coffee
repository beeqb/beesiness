class App.UsersSignUpCtrl extends App.FacebookAuthCtrl

  @_import: [
    '$scope',
    '$rootScope',
    '$http',
    '$location',
    '$auth',
  ]

  app.controller('UsersSignUpCtrl', @factory())

  initialize: () ->
    super()

    @_scope.submit = @_submit
    @_scope.user = {
      name: '',
      email: '',
      password: '',
      password_confirmation: '',
      agree: 1,
      errors: {},
      labels: {
        name: 'Name'
        email: 'Email',
        password: 'Password',
        password_confirmation: 'Repeat password',
        agree: 'Согласен с <a href="/policy" target="_blank">правилами и политикой</a>',
      }
    }

    @_scope.$on('auth:registration-email-error', (e, reason) =>
      _.extend(@_scope.user.errors, reason.errors)
    )

  _submit: () =>
    @_scope.user.password_confirmation = @_scope.user.password
    @_auth.submitRegistration(@_scope.user)
    .then(
      (() =>
        @_location.path('/users/sign_up_success')
      ),
      ((response) =>
        _.extend(@_scope.user.errors, response.data.errors)
      ),
    )

