class App.UsersSignOutCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$http',
    '$window',
    '$location',
  ]

  app.controller('UsersSignOutCtrl', @factory())

  initialize: () ->
    @_scope.signOut()
    @_location.path('/users/sign_in')
