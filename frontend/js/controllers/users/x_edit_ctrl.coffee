class App.UsersXEditCtrl extends App.UsersLoadCtrl

  app.controller('UsersXEditCtrl', @factory())

  initialize: () ->
    super()

    @_Page.setTitle('user_profile')

    @_scope.changePasswordInstructionsSent = false
    @_scope.sendChangePasswordInstructions = @_sendChangePasswordInstructions

    @_scope.updatePhoneCodeByCountry = @_updatePhoneCodeByCountry
    @_scope.submit = @_submit
    @_scope.validate = @_validate
    @_scope.unbindFacebook = @_unbindFacebook
    @_scope.showWorkPhonesLabel = false
    @_scope.showMobileWorkPhonesLabel = false
    @_validationTimeout = 500 #msec

    # customizimg ui-mask
    @_scope.maskOptions = {
      maskDefinitions: {
        'x': /([0-9]|\-)/
      },
      clearOnBlur: false,
      allowInvalidValue: true
    }

    if @_routeParams.edit == 'true'
      stopUsersViewFormWatching = @_scope.$watch('UsersViewForm', () =>
        @_scope.UsersViewForm.$show()
        stopUsersViewFormWatching()
      )

    @_initWatchers()
    @_scope.avatarChartOptions = {
      lineWidth: 5,
      trackColor: '#e8eff0',
      barColor: '#27c24c',
      scaleColor: false,
      color: '#3a3f51',
      size: 134,
      lineCap: 'butt',
      rotate: -90,
      animate: 1000
    }

  _sendChangePasswordInstructions: () =>
    @_auth.requestPasswordReset({email: @_scope.user.email})
    .then(() =>
      @_scope.changePasswordInstructionsSent = true
    )
    .catch((response) =>
    )

  _initWatchers: () =>
    @_scope.watchedData = {}
    @_scope.$watch('user.new_image', () =>
      if @_scope.user.new_image
        @_scope.user.image.url = 'assets/img/preloader.gif'
        @_scope.preloaderClass = 'profile-avatar--preloader'
    )

    @_scope.$watch('user.image.url', () =>
      return unless @_scope.user.image

      @_scope.user.remove_image =
        @_scope.user.image.url == @_rootScope.defaultAvatar

      if @_scope.user.remove_image
        delete @_scope.user.new_image
    )

    @_scope.$watch('user.employees', () =>
      return unless @_scope.user.employees

      for employee in @_scope.user.employees
        if _.filter(employee.employee_phones, (emp) -> emp.kind == 'work').length > 0
          @_scope.showWorkPhonesLabel = true

        if _.filter(employee.employee_phones, (emp) -> emp.kind == 'cell').length > 0
          @_scope.showMobileWorkPhonesLabel = true
    )

  _setValidationError: (attribute, errors) =>
    @_scope.UsersViewForm.$setError(attribute, '')

    _.extend(@_scope.user.errors[attribute], errors)
    @_scope.UsersViewForm.$setError(attribute, errors.join('; '))

  _setValidationErrors: (errors) =>
    for attr, error of errors
      @_setValidationError(attr, error)

  _validate: (attribute, value) =>
    if !@_scope.UsersViewForm || !@_scope.UsersViewForm.$visible
      return

    if @_validateTimeoutPromise
      @_timeout.cancel(@_validateTimeoutPromise)

    @_validateTimeoutPromise = @_timeout(
      (() =>
        data = {}
        _.extend(data, @_scope.user)

        data[attribute] = value

        @_resource
        .params({
          id: @_scope.user.id,
        })
        .post('users/validate', {user: data})
        .then((response) =>
          @_setValidationError(attribute, [])
        )
        .catch((response) =>
          errors = []
          for attr, error of response.data.errors
            if attr == attribute
              errors.push(error)

          @_setValidationError(attribute, errors)
        )
      ),
      @_validationTimeout
    )

  _submit: (data) =>
    userData = {}
    userData = _.extend(userData, @_scope.user)
    userData = _.extend(userData, data)
    # todo:refactor - format date for save into db
    userData.birthday = moment(userData.birthday).format('YYYY-MM-DD')

    defer = @_q.defer()

    resource = new @_ResourceLoader()

    resource
    .include('profile_fullness')
    .put('users', @_scope.user.id, {user: userData})
    .then(
      ((response) =>
        @_scope.preloaderClass = ''
        languageChanged = response.data.lang != @_scope.user.lang
        @_scope.user.load(response.data)
        if !@_scope.user.new_image || !(@_scope.user.new_image instanceof File)
          defer.resolve(response)
          if languageChanged
            @_cookies.put('lang', '"' + response.data.lang + '"')
            @_window.location.reload()
          else
            if @_rootScope.user.id == @_scope.user.id
              @_rootScope.$emit('user:changed')
          return

        @_Upload
        .upload({
          url: '/api/users/' + @_scope.user.id + '/upload_image',
          method: 'PUT',
          headers: @_auth.retrieveData('auth_headers'),
          data: {
            file: @_scope.user.new_image,
          },
        })
        .then(
          ((response) =>
            @_scope.user.new_image = null
            if languageChanged
              @_cookies.put('lang', '"' + response.data.lang + '"')
              @_window.location.reload()
              return

            if @_routeParams.edit
              @_location.search({})

            @_scope.user.load(response.data)
            if @_rootScope.user.id == @_scope.user.id
              @_rootScope.$emit('user:changed')

            delete @_scope.user.new_image
            defer.resolve(response)
          ),
          ((response) =>
            @_setValidationErrors(response.data.errors)
            defer.resolve('error')
          )
        )
      ),
      ((response) =>
        @_setValidationErrors(response.data.errors)
        defer.resolve('error')
      )
    )

    return defer.promise

  _unbindFacebook: () =>
    @_scope.user.facebook_link = null
    @_scope.user.facebook_identity = null

  _updatePhoneCodeByCountry: (countryCode) =>
    code = _.findWhere(
      @_scope.userStatic.references.country_phone_codes,
      {country_code: countryCode},
    )
    @_scope.user.cell_phone_code_id = code?.id
