class App.FacebookAuthCtrl extends IdFly.AngularClass

  @_import: [
    '$scope',
    '$rootScope',
    '$http',
    '$location',
    '$routeParams',
    '$timeout',
  ]

  app.controller('FacebookAuthCtrl', @factory())

  initialize: () ->
    @_scope.signFacebook = (data) =>
      @_scope.user.name = data.first_name
      @_scope.user.email = data.email
      @_scope.user.password = '--------'
      @_scope.user.password_confirmation = @_scope.user.password

      @_scope.user.facebook_data = data

      @_scope.submitRegistration(@_scope.user)
      .then(
        ((response) =>
          if response.data.is_registration
            if response.data.is_facebook
              @_location.path('/profile')
              @_location.search({edit: 'true'})
            else
              @_location.path('/users/sign_up_success')
          else
            @_location.path('/')
        ),
        ((response) =>
          _.extend(@_scope.user.errors, response.data.errors)
        ),
      )