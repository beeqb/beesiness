class App.UsersSignInCtrl extends App.FacebookAuthCtrl

  @_import: [
    '$scope',
    '$rootScope',
    '$http',
    '$location',
    '$routeParams',
    '$timeout',
  ]

  app.controller('UsersSignInCtrl', @factory())

  initialize: () ->
    super()

    @_scope.submit = @_submit
    @_scope.user = {
      email: '',
      password: '',
      errors: {},
      labels: {
        email: 'Email',
        password: 'Пароль',
        remember: 'Запомнить',
      }
    }

    @_scope.showResendConfirmationButton = false
    @_scope.confirmationEmailWasResend = false

    @_rootScope.$on('auth:login-error', (e, reason) =>
      @_scope.showResendConfirmationButton = reason.not_confirmed
    )
    @_scope.resendConfirmationEmail = @_resendConfirmationEmail

    @_scope.$on('auth:login-error', (e, reason) =>
      _.extend(@_scope.user.errors, {
        password: reason.errors,
      })
      if reason.errors
        @_scope.form.$invalid = true
    )

  _resendConfirmationEmail: () =>
    @_http.post('/api/users/resend_confirmation', {
      email: @_scope.user.email,
      password: @_scope.user.password,
    })
    .then(() =>
      @_scope.showResendConfirmationButton = false
      @_scope.confirmationEmailWasResend = true
      @_timeout(
        (() =>
          @_scope.confirmationEmailWasResend = false
        ),
        3000
      )
    )

  _submit: () =>
    @_scope.submitLogin(@_scope.user)
