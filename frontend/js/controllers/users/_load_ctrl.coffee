class App.UsersLoadCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$location',
    '$routeParams',
    '$q',
    '$auth',
    'Upload',
    '$translate',
    '$cookies',
    '$window',
    '$timeout',
    'ResourceLoader',
    'mapConfig',
    'Page',
  ]

  app.controller('UsersLoadCtrl', @factory())

  initialize: () ->
    @_resource = new @_ResourceLoader()

    @_scope.map = @_mapConfig
    @_scope.userStatic = {}
    @_scope.user = {
      errors: {},
      load: (data) =>
        _.extend(@_scope.user, data)
        if @_scope.user.birthday
          @_scope.user.birthday = moment(@_scope.user.birthday).toDate()
        if !@_scope.user.lang
          @_scope.user.lang = @_cookies.get('lang').replace(/"/g, '')
    }

    @_resolveUserId().then(@_load)

  _resolveUserId: () =>
    deferred = @_q.defer()

    if @_routeParams.id
      deferred.resolve(@_routeParams.id)
      return deferred.promise

    @_watchCleaner = @_rootScope.$watch('user.id', () =>
      if !@_rootScope.user.id
        return
      @_watchCleaner()
      deferred.resolve(@_rootScope.user.id)
    )

    return deferred.promise

  _load: (userId) =>
    @_resource
    .include(
      'departments_in_companies',
      'employees',
      'profile_fullness',
      'companies_client_in',
    )
    .expandReferencesAndLabels()
    .get('users', userId)
    .then((response) =>
      @_scope.userStatic = {
        labels: response.data.labels,
        references: response.data.references,
      }
      @_scope.user.load(response.data.entity)
    )
