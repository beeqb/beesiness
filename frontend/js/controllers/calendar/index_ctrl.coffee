class App.CalendarCtrl extends IdFly.AngularClass

  @_import: [
    '$scope',
    '$auth',
    '$http',
    'ResourceLoader',
    'uiCalendarConfig',
  ]

  app.controller('CalendarCtrl', @factory())

  initialize: () ->
    @_resource = new @_ResourceLoader()

    @_scope.newEvent = {
      startDate: moment().startOf('day').toDate(),
      startTime: moment().startOf('hour').toDate(),
      user_id: @_scope.user.id,
    }

    @_scope.addEvent = @_addEvent
    @_scope.changeView = @_changeView
    @_scope.today = @_today
    @_scope.remove = @_remove

    @_scope.events = @_events

    @_scope.eventSources = [{
      url: '/api/calendar_events',
      beforeSend: (xhr) =>
        for key, value of @_auth.retrieveData('auth_headers')
          xhr.setRequestHeader(key, value)
    }]

    @_scope.calendarConfig = {
      height: 450,
      editable: true,
      header: {
        left: 'prev',
        center: 'title',
        right: 'next',
      },
      dayClick: @_scope.alertOnEventClick,
      eventDrop: @_scope.alertOnDrop,
      eventResize: @_scope.alertOnResize,
      eventMouseover: @_scope.alertOnMouseOver,
      eventDataTransform: ((event) =>
        event.allDay = event.kind in ['vacation', 'task']
        event
      ),
      loading: ((isLoading) =>
        @_scope.events = _.map(@_fullCalendar('clientEvents'), (event) ->
          event.start = moment(event.start).toDate()
          event.end = moment(event.end).toDate() if event.end
          event
        )
      ),
      timeFormat: 'H(:mm)'
    }

    ### alert on dayClick ###

    @_scope.precision = 400
    @_scope.lastClickTime = 0

    @_scope.alertOnEventClick = (date, jsEvent, view) ->
      time = (new Date).getTime()
      if time - (@_scope.lastClickTime) <= @_scope.precision
        @_scope.events.push({
          title: 'New Event',
          start: date,
          className: [ 'b-l b-2x b-info' ],
        })
      @_scope.lastClickTime = time

    ### alert on Drop ###

    @_scope.alertOnDrop = (event, delta, revertFunc, jsEvent, ui, view) ->
      @_scope.alertMessage = 'Event Droped to make dayDelta ' + delta
      return

    ### alert on Resize ###

    @_scope.alertOnResize = (event, delta, revertFunc, jsEvent, ui, view) ->
      @_scope.alertMessage = 'Event Resized to make dayDelta ' + delta
      return

    @_scope.overlay = $('.fc-overlay')

    @_scope.alertOnMouseOver = (event, jsEvent, view) ->
      @_scope.event = event
      @_scope.overlay.removeClass('left right top').find('.arrow').removeClass 'left right top pull-up'
      wrap = $(jsEvent.target).closest('.fc-event')
      cal = wrap.closest('.calendar')
      left = wrap.offset().left - (cal.offset().left)
      right = cal.width() - (wrap.offset().left - (cal.offset().left) + wrap.width())
      top = cal.height() - (wrap.offset().top - (cal.offset().top) + wrap.height())
      if right > @_scope.overlay.width()
        @_scope.overlay.addClass('left').find('.arrow').addClass 'left pull-up'
      else if left > @_scope.overlay.width()
        @_scope.overlay.addClass('right').find('.arrow').addClass 'right pull-up'
      else
        @_scope.overlay.find('.arrow').addClass 'top'
      if top < @_scope.overlay.height()
        @_scope.overlay.addClass('top').find('.arrow').removeClass('pull-up').addClass 'pull-down'
      wrap.find('.fc-overlay').length == 0 and wrap.append(@_scope.overlay)
      return

  _remove: (event) =>
    @_resource
    .post('calendar_events/remove', {calendar_event: event})
    .then(() => @_fullCalendar('refetchEvents'))

  _changeView: (view, calendar) =>
    @_fullCalendar('changeView', view)

  _today: (calendar) =>
    @_fullCalendar('today')

  _fullCalendar: (args...) =>
    calendar = @_uiCalendarConfig.calendars?.personalCalendar
    return unless calendar

    calendar.fullCalendar.apply(calendar, args)

  _addEvent: () =>
    data = @_scope.newEvent

    data.start = moment(data.startDate).set({
      hour: data.startTime.getHours(),
      minutes: data.startTime.getMinutes(),
      seconds: 0,
    }).format('YYYY-MM-DD HH:mm:ss')

    data.end = moment(data.endDate).set({
      hour: data.endTime.getHours(),
      minute: data.endTime.getMinutes(),
      seconds: 0,
    }).format('YYYY-MM-DD HH:mm:ss')

    delete data.startDate
    delete data.startTime
    delete data.endDate
    delete data.endTime

    @_resource
    .post('calendar_events', {calendar_event: @_scope.newEvent})
    .then(() => @_fullCalendar('refetchEvents'))
