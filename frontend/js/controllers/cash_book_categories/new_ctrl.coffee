class App.CompaniesCashBookCategoriesNewCtrl extends App.CompaniesCashBookCategoriesLoadCtrl

  @_import: [
    '$scope',
    '$element'
    'close',
    'companyId',
    'ResourceLoader',
  ]

  app.controller('CompaniesCashBookCategoriesNewCtrl', @factory())

  initialize: () ->
    super()

  _load: () =>
    companyResource = new @_ResourceLoader(@_companyId)

    companyResource
    .expand('labels')
    .query('cash_book_categories/new')
    .then((response) =>
      @_scope.labels = response.data.labels
      _.extend(@_scope.category, response.data.entity)
      @_scope.category.company_id = @_companyId
      @_scope.category.expense = false
    )
