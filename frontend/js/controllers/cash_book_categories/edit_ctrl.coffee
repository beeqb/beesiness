class App.CompaniesCashBookCategoriesEditCtrl extends App.CompaniesCashBookCategoriesLoadCtrl

  @_import: [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$element',
    'close',
    'companyId',
    'categoryId',
    'existingCategories',
    'ResourceLoader',
  ]

  app.controller('CompaniesCashBookCategoriesEditCtrl', @factory())

  initialize: () ->
    @_existingCategories = JSON.parse(@_existingCategories)
    @_companyResource = new @_ResourceLoader(@_companyId)

    super()

    @_scope.delete = @_delete

    @_scope.$watchGroup(
      ['categories', 'category'],
      (
        () =>
          return unless @_scope.category.id

          @_scope.availableCategories = _.filter(
            @_existingCategories,
            (category) =>
              category.id != @_scope.category.id &&
              category.expense == @_scope.category.expense
          )
      ),
      true
    )

  _load: () =>
    @_companyResource
    .expand('labels')
    .get('cash_book_categories', @_categoryId)
    .then((response) =>
      @_scope.labels = response.data.labels
      @_scope.category = response.data.entity
      @_scope.category.category_after_delete = null
    )

  _delete: () =>
    confirmText = 'Уверены, что хотите удалить категорию?'

    return unless confirm(confirmText)

    @_element.modal('hide')

    # need for parent (ProductIndexCtrl)
    @_scope.category.delete = true
    @_close(@_scope.category)
