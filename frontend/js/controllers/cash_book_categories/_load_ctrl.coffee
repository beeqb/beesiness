class App.CompaniesCashBookCategoriesLoadCtrl extends IdFly.AngularClass

  app.controller('CompaniesCashBookCategoriesLoadCtrl', @factory())

  initialize: () ->
    @_scope.category = {errors: {}}
    @_scope.categories = []
    @_scope.availableCategories = []
    @_scope.submit = @_submitModal
    @_load()

  _submitModal: () =>
    companyResource = new @_ResourceLoader(@_companyId)

    companyResource
    .post(
      'cash_book_categories/validate',
      {cash_book_category: @_scope.category}
    )
    .then(
      (() =>
        @_element.modal('hide')
        @_close(@_scope.category)
      ),
      ((response) =>
        _.extend(@_scope.category.errors, response.data.errors)
      )
    )
