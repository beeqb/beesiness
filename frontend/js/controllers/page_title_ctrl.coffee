class App.PageTitleCtrl extends IdFly.AngularClass

  @_import: [
    '$scope',
    'Page',
  ]

  app.controller('PageTitleCtrl', @factory())

  initialize: () ->
    @_scope.getTitle = @_Page.getTitle
