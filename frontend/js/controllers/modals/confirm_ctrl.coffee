class App.ModalsConfirmCtrl extends IdFly.AngularClass

  @_import = [
    '$scope',
    '$http',
    'close',
    'config',
  ]

  app.controller('ModalsConfirmCtrl', @factory())

  initialize: () =>
    @_scope.config = @_config
    @_scope.yes = @_yes
    @_scope.no = @_no
    @_scope.close = @_close

  _no: =>
    @_close(false)

  _yes: =>
    @_close(true)
