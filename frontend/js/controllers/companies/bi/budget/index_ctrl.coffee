class App.CompaniesBiBudgetIndexCtrl extends IdFly.AngularClass
  @_import: [
    '$scope',
    '$routeParams',
    '$filter',
    'ResourceLoader',
    'Page',
    'detalisationPeriod'
  ]

  app.controller('CompaniesBiBudgetIndexCtrl', @factory())

  initialize: () ->
    @companyId = @_routeParams.company_id
    @_scope.companyId = @companyId
    @_scope.currentMonth = moment().month() + 1
    @companyResource = new @_ResourceLoader(@companyId)

    @_scope.detalisationPeriod = @_detalisationPeriod
    @_scope.selectedPeriod = @_detalisationPeriod.year

    @_scope.budgetChartStartDate = moment().startOf('year')
    @_scope.budgetChartEndDate = moment()
    @_scope.budgetChartRangeOptions = {
      startDate: moment(@_scope.budgetChartStartDate),
      endDate: moment(@_scope.budgetChartEndDate)
    }

    @_scope.budgetChartCallBack = (start, end) =>
      @_scope.budgetChartStartDate = start
      @_scope.budgetChartEndDate = end
      @_buildBudgetChart()
      # Workaroung to trigger onChanges events in components that depends on @_buildPlotData
      @_scope.$digest()
      return

    @_scope.changePeriod = (period) =>
      @_scope.selectedPeriod = period
      @_buildBudgetChart()
      return

    @_scope.actualBudgetData = {}
    @_scope.budgetChartData  = {}
    @_scope.budgetChartAxis  = {}
    @_scope.incomeAndOutcomeChartData = {}

    @_scope.incomeAndOutcomeChartDateOptions = {
      isOpen: false,
      datepickerMode: 'year',
      minMode: 'year',
      maxMode: 'year'
    }
    @_scope.incomeAndOutcomeChartDate = new Date
    @_scope.changeIncomeAndOutcomeChartDate = () =>
      @_buildIncomeAndOutcomeChart()

    @_scope.incomeAndOutcomeChartDateMode = "all"
    @_setIncomeAndOutcomeChartDateMode()

    @_scope.changeIncomeAndOutcomeChartDateMode = () =>
      @_setIncomeAndOutcomeChartDateMode()
      @_buildIncomeAndOutcomeChart()

    @_loadCompany().then(@_loadBudgetStatistics)

  _setIncomeAndOutcomeChartDateMode: () =>
    if @_scope.incomeAndOutcomeChartDateMode == "month"
      @_scope.incomeAndOutcomeChartDateFormat = "yyyy.MM"
      @_scope.incomeAndOutcomeChartDateOptions.minMode = "month"
    else if @_scope.incomeAndOutcomeChartDateMode == "year"
      @_scope.incomeAndOutcomeChartDateFormat = "yyyy"
      @_scope.incomeAndOutcomeChartDateOptions.minMode = "year"
    else
      @_scope.incomeAndOutcomeChartDateFormat = "yyyy"
      @_scope.incomeAndOutcomeChartDateOptions.minMode = "year"

  _loadBudgetStatistics: () =>
    indicators = [
      'monthly_budget',
      'current_month_income',
      'current_month_outcome',
      'current_month_loans',
      'current_month_loan_returns',
      'annual_plan_implementation',
    ]

    @companyResource
    .query('budget_statistics_by_days', {indicators: indicators.join(',')})
    .then((response) =>
      data = response.data.data
      budgetChartStartDate = moment(data.stats_by_days[0].created_at)
      budgetChartEndDate = moment(data.stats_by_days[data.stats_by_days.length - 1].created_at)

      #BUDGET ZOOM
      @_scope.detalisationData = {}
      @_scope.detalisationData.daysNum = data.stats_by_days.length
      @_scope.detalisationData.weeksNum = budgetChartEndDate.diff(budgetChartStartDate, 'weeks', true)
      @_scope.detalisationData.monthNum = budgetChartEndDate.diff(budgetChartStartDate, 'months', true)
      @_scope.detalisationData.quarterNum = budgetChartEndDate.diff(budgetChartStartDate, 'quarters', true)
      @_scope.detalisationData.yearsNum = budgetChartEndDate.diff(budgetChartStartDate, 'years', true)

      getDetalisationArray = (lengthOfPeriod, typeOfMoment) =>
        return _.times(parseInt(Math.ceil(lengthOfPeriod).toString()) + 1, (index) =>
          startMoment = budgetChartStartDate.clone().add(index, typeOfMoment)
          startIndex = startMoment.diff(budgetChartStartDate, 'days')
          endMoment = startMoment.clone().add(1, typeOfMoment)
          endIndex = endMoment.diff(budgetChartStartDate, 'days')
          res = _.reduce(data.stats_by_days[startIndex...endIndex], ((previousValue, currentValue, index, array) =>
            return {
              income: parseFloat(previousValue.income) + parseFloat(currentValue.income)
              outcome: parseFloat(previousValue.outcome) + parseFloat(currentValue.outcome)
              revenue: parseFloat(previousValue.revenue) + parseFloat(currentValue.revenue)
              date: startMoment.format('YYYY-MM-DD')
            }), {income: 0, outcome: 0, revenue: 0, date: startMoment.format('YYYY-MM-DD')})
          return res)

      #IN DAY
      @_scope.detalisationData.daysDetalisation = getDetalisationArray(@_scope.detalisationData.daysNum, 'd')
      #IN WEEK
      @_scope.detalisationData.weeksDetalisation = getDetalisationArray(@_scope.detalisationData.weeksNum, 'w')
      #IN MONTH
      @_scope.detalisationData.monthsDetalisation = getDetalisationArray(@_scope.detalisationData.monthNum, 'M')
      #IN QUARTER
      @_scope.detalisationData.quartersDetalisation = getDetalisationArray(@_scope.detalisationData.quarterNum, 'Q')
      #IN YEARS
      @_scope.detalisationData.yearsDetalisation = getDetalisationArray(@_scope.detalisationData.yearsNum, 'y')


      monthBudgetIndicators = [
        'current_month_income',
        'current_month_outcome',
        'current_month_loans',
        'current_month_loan_returns',
      ]

      @_scope.monthlyBudgetData = data.monthly_budget
      @_scope.monthBudgetIndicators = _.pick(data, monthBudgetIndicators)

      @_scope.annualPlanImplementation = {series: [], colors: []}

      colors = [
        '#6254b2',
        '#23b7e5',
        '#27c24c',
        '#fad733',
        '#f05050',
        '#3a3f51',
      ]

      if data.annual_plan_implementation?
        _.each(
         data.annual_plan_implementation.categories_amounts,
         (amount, categoryId) =>
           if categoryId
             category = _.findWhere(
               @_scope.company.cash_flow_budget_categories,
               {id: Number(categoryId)}
             )
           else
             category = {name: 'Нераспределен'}

           @_scope.annualPlanImplementation.series.push({
             label: category.name,
             data: amount
           })

           color = _.sample(colors)
           colors = _.without(colors, color)
           console.log colors.length

           @_scope.annualPlanImplementation.colors.push(color)
        )
        @_scope.annualPlanImplementation.series.push({
          label: 'Остаток',
          data: data.annual_plan_implementation.residue,
        })
        @_scope.annualPlanImplementation.colors.push('#7266ba')

      @_scope.references = response.data.references

      @_buildBudgetChart()
      @_buildIncomeAndOutcomeChart()
    )

  _loadCompany: () =>
    resource = new @_ResourceLoader()

    resource
    .include(
      'cash_flow_budget_categories',
      'cash_book_categories',
      'last_leads',
      'last_deals',
      'last_clients',
      'created_clients_count_by_days',
      'actual_income_by_days',
      'actual_budget_stats_by_days',
      'clients_count',
      'actual_income'
    )
    .expand('references')
    .get('companies', @companyId)
    .then((response) =>
      @_scope.company = response.data.entity

      @_Page.setTitle('budget', @_scope.company.name)

      @_scope.companyStatic = {references: response.data.references}
    )


  _buildIncomeAndOutcomeChart: () =>
    @_scope.incomeAndOutcomeChartData = {
      income: 0,
      outcome: 0
    }

    if @_scope.incomeAndOutcomeChartDateMode == "month"
      # Find right detalisation.
      targetMonth = _.find(@_scope.detalisationData.monthsDetalisation, (stat) =>
        targetMoment = moment(@_scope.incomeAndOutcomeChartDate)
        tmpMoment = moment(stat.date)
        if tmpMoment.year() == targetMoment.year() && tmpMoment.month() == targetMoment.month()
          return true
        return false
      )
      if targetMonth?
        @_scope.incomeAndOutcomeChartData.income = targetMonth.income
        @_scope.incomeAndOutcomeChartData.outcome = targetMonth.outcome
    else if @_scope.incomeAndOutcomeChartDateMode == "year"
      targetYear = _.find(@_scope.detalisationData.yearsDetalisation, (stat) =>
        targetMoment = moment(@_scope.incomeAndOutcomeChartDate)
        tmpMoment = moment(stat.date)
        if tmpMoment.year() == targetMoment.year()
          return true
        return false
      )
      if targetYear?
        @_scope.incomeAndOutcomeChartData.income = targetYear.income
        @_scope.incomeAndOutcomeChartData.outcome = targetYear.outcome
    else if @_scope.incomeAndOutcomeChartDateMode == "all"
      @_scope.incomeAndOutcomeChartData.income = _.reduce(@_scope.detalisationData.yearsDetalisation, ((previousValue, currentValue, index, array) =>
        return { income: previousValue.income + currentValue.income }
      )).income
      @_scope.incomeAndOutcomeChartData.outcome = _.reduce(@_scope.detalisationData.yearsDetalisation, ((previousValue, currentValue, index, array) =>
        return { outcome: previousValue.outcome + currentValue.outcome }
      )).outcome
    return

  _buildBudgetChart: () =>
    return unless @_scope.detalisationData?
    @_scope.budgetChartData = {
      x: 'x',
      xFormat: '%Y-%m-%d',
      columns: [],
      type: 'line'
    }

    @_scope.budgetChartAxis  = {
      x: {
        type: 'timeseries',
        tick: {}
      }
    }

    targetArray = @_scope.detalisationData.yearsDetalisation
    switch @_scope.selectedPeriod.id
      when 'day'
        targetArray = @_scope.detalisationData.daysDetalisation
        @_scope.budgetChartAxis.x.tick.format = '%Y-%m-%d'
      when 'week'
        targetArray = @_scope.detalisationData.weeksDetalisation
        @_scope.budgetChartAxis.x.tick.format = '%Y-%m-%d'
      when 'month'
        targetArray = @_scope.detalisationData.monthsDetalisation
        @_scope.budgetChartAxis.x.tick.format = '%Y-%m'
      when 'quarter'
        targetArray = @_scope.detalisationData.quartersDetalisation
        @_scope.budgetChartAxis.x.tick.format = '%Y-%m'
      when 'year'
        targetArray = @_scope.detalisationData.yearsDetalisation
        @_scope.budgetChartAxis.x.tick.format = '%Y'
      else
        targetArray = @_scope.detalisationData.yearsDetalisation
        @_scope.budgetChartAxis.x.tick.format = '%Y'

    targetArray = _.filter(targetArray, (stat) =>
      tmpMoment = moment(stat.date)
      return tmpMoment >= moment(@_scope.budgetChartStartDate) && tmpMoment <= moment(@_scope.budgetChartEndDate)
    )
    @_scope.budgetChartData.columns[0] = ['x'].concat(_.pluck(targetArray, 'date'))
    @_scope.budgetChartData.columns[1] = ['Приход'].concat(_.pluck(targetArray, 'income'))
    @_scope.budgetChartData.columns[2] = ['Расход'].concat(_.pluck(targetArray, 'outcome'))
    @_scope.budgetChartData.columns[3] = ['Доход'].concat(_.pluck(targetArray, 'revenue'))
    return
