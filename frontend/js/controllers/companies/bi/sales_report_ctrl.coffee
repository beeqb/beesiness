class App.CompaniesBiSalesReportCtrl extends IdFly.AngularClass

  @_import: [
    '$scope',
    '$routeParams',
    'ResourceLoader',
    'Page',
  ]

  app.controller('CompaniesBiSalesReportCtrl', @factory())

  initialize: () ->
    @_scope.viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)

    @_scope.companyId = @_routeParams.company_id
    @_scope.quarter = moment().quarter()
    @_scope.quarterMonth = 1
    @_scope.year = moment().year()

    @_scope.monthsOfQuarter = @_monthsOfQuarter

    @_scope.onChartPeriodChange = ((period) =>
      @_scope.selectedChartPeriod = period
      @_loadSalesPlans()
    )

    @_companyResource = new @_ResourceLoader(@_scope.companyId)

    @_scope.filter = {}

    @_scope.prevQuarter = @_prevQuarter
    @_scope.nextQuarter = @_nextQuarter

    @_scope.yearIsCurrent = @_yearIsCurrent

    @_loadCompany().then(@_loadData())

  _loadCompany: () =>
    resource = new @_ResourceLoader

    resource
    .include(
      'product_categories',
      'created_clients_count_by_days',
      'actual_income_by_days',
      'clients_count',
      'actual_income'
    )
    .get('companies', @_scope.companyId)
    .then((response) =>
      @_scope.company = response.data.entity
      @_Page.setTitle('sales_report', @_scope.company.name)
    )

  _loadData: () =>
    @_companyResource
    .query('sales_report', {quarter: @_scope.quarter, year: @_scope.year})
    .then((response) =>
      @_scope.data = response.data
      @_scope.currentMonthActualSales =
        @_scope.data.actual_sales_year[moment().month() + 1]?.money || 0
    )

  _monthsOfQuarter: () =>
    if @_scope.viewportWidth <= 767
      start = @_scope.quarterMonth
      end = @_scope.quarterMonth
    else
      start = @_scope.quarter * 3 - 2
      end = @_scope.quarter * 3

    [start..end]

  _getSalesPlanMonthsCondition: () =>
    currentMonth = moment().month() + 1
    currentQuarter = moment().quarter()

    months = []
    switch @_scope.selectedChartPeriod
      when 'day', 'week', 'month' then months = [currentMonth]
      when 'quarter' then months = @_getMonthsOfQuarter(currentQuarter)

    return months

  _loadSalesPlans: () =>
    params = {
      year: moment().year(),
      'month[]': @_getSalesPlanMonthsCondition(),
    }

    @_companyResource
    .include(
      'sales_plan_products',
      'sales_plan_products.product',
      'sales_plan_products.product.category',
      'sales_plan_employees',
      'sales_plan_employees.employee',
      'sales_plan_employees.employee.employees_departments'
    )
    .query('sales_plans', params)
    .then((response) =>
      @_scope.allSalesPlans = _.sortBy(response.data.entities, 'year')
    )

  _getMonthsOfQuarter: (quarter) =>
    switch quarter
      when 1 then [1, 2, 3]
      when 2 then [4, 5, 6]
      when 3 then [7, 8, 9]
      when 4 then [10, 11, 12]
      else []

  _nextQuarter: () =>
    if (@_scope.quarter == 4 && @_scope.viewportWidth > 767) || 
       (@_scope.quarterMonth == 12 && @_scope.viewportWidth <= 767)
      @_scope.quarter = 1
      @_scope.quarterMonth = 1
      @_scope.year += 1
    else
      @_scope.quarterMonth++

      if @_scope.quarterMonth % 3 == 1 || @_scope.viewportWidth > 767
        @_scope.quarter++

    @_loadData()

  _prevQuarter: () =>
    if (@_scope.quarter == 1 && @_scope.viewportWidth > 767) ||
       (@_scope.quarterMonth == 1 && @_scope.viewportWidth <= 767)
      if @_scope.viewportWidth <= 767
        @_scope.quarter = 1
        @_scope.quarterMonth = 12
      else 
        @_scope.quarter = 4

      @_scope.year -= 1

      @_loadSalesPlans()
    else
      @_scope.quarterMonth--
      
      if @_scope.quarterMonth % 3 == 0 || @_scope.viewportWidth > 767
        @_scope.quarter--

    @_loadData()

  _yearIsCurrent: (year) =>
    moment().year() == year
