class App.CompaniesBiBreakEvenAnalysisCtrl extends IdFly.AngularClass
  @_import: [
    '$scope',
    '$routeParams',
    '$filter',
    'ResourceLoader',
    'Page',
  ]

  app.controller('CompaniesBiBreakEvenAnalysisCtrl', @factory())

  initialize: () ->
    @_scope.companyId = @_routeParams.company_id

    @companyResource = new @_ResourceLoader(@_scope.companyId)
    @_scope.actualBudgetData = {}
    @_scope.sellsCounts = (c for c in [0..120] by 10)

    @_scope.sellsAmount = @_sellsAmount
    @_scope.totalExpenses = @_totalExpenses

    @_loadCompany().then(@_loadData())

  _loadCompany: () =>
    resource = new @_ResourceLoader()

    resource
    .include(
      'last_leads',
      'last_deals',
      'last_clients',
      'created_clients_count_by_days',
      'actual_income_by_days',
      'clients_count',
      'actual_income'
    )
    .expand('references')
    .get('companies', @_scope.companyId)
    .then((response) =>
      @_scope.company = response.data.entity

      @_Page.setTitle('break_even_analysis', @_scope.company.name)

      @_scope.companyStatic = {references: response.data.references}
    )

  _loadData: () =>
    indicators = [
      'fixed_expenses',
      'product_average_price',
      'products_increase',
      'break_even_point',
      'cost_per_customer',
    ]

    @companyResource
    .query('budget_statistics', {indicators: indicators.join(',')})
    .then((response) =>
      data = response.data.data
      
      @_scope.data = data

      @_buildPlotSeries()
    )

  _sellsAmount: (productsCount) =>
    return unless @_scope.data

    plotPoint = _.find(
      @_scope.plotSeries?[0].data,
      (point) -> point[0] == productsCount
    )

    if plotPoint
      plotPoint[1]
    else
      productsCount * @_scope.data.product_average_price

  _totalExpenses: (productsCount) =>
    return unless @_scope.data

    plotPoint = _.find(
      @_scope.plotSeries?[1].data,
      (point) -> point[0] == productsCount
    )

    if plotPoint
      plotPoint[1]
    else
      productsCount * @_scope.data.cost_per_customer

  _buildPlotSeries: () =>
    commonOptions = {
      points: {show: true},
      lines: {
        show: true,
        fill: true,
        fillColor: {
          colors: [
            {opacity: 0.0},
            {opacity: 0.0},
          ],
        },
      },
    }

    series = [
      _.extend({data: [], label: 'Продажи'}, commonOptions),
      _.extend({data: [], label: 'Общая сумма'}, commonOptions),
    ]

    for count in @_scope.sellsCounts
      series[0].data.push([count, @_sellsAmount(count)])
      series[1].data.push([count, @_totalExpenses(count)])

    @_scope.plotSeries = series

