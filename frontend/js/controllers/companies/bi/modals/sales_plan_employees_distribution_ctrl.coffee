class App.CompaniesBiSalesPlanEmployeesDistributionCtrl extends IdFly.AngularClass
  @_import: [
    '$scope',
    '$element',
    'close',
    'salesPlans',
    'salesmen',
    'category'
  ]

  app.controller('CompaniesBiSalesPlanEmployeesDistributionCtrl', @factory())

  initialize: () ->
    @_scope.salesPlans = JSON.parse(@_salesPlans)
    @_scope.salesmen = JSON.parse(@_salesmen)
    @_scope.category = @_category?.id || null

    @_scope.submit = @_submit
    @_scope.distributionIsInvalid = (() =>
      !_.isEmpty(@_scope.errors)
    )
    @_scope.validate = @_validate

    @_scope.errors = {}

    @_scope.foo = ((plan, salesman, part) =>
      return if plan.employeesHash[salesman.id]?[@_scope.category]

      plan.employeesHash[salesman.id] = {}
      plan.employeesHash[salesman.id][@_scope.category] = {part: part}
    )

    @_scope.part = ((plan, salesman) =>
      _.find(plan.sales_plan_employees, (salesPlanEmployee) =>
        result = salesPlanEmployee.employee_id == salesman.id

        if @_category
          result &&= salesPlanEmployee.product_category_id == @_category.id

        result
      )
    )

    @_scope.categoryIsAvailableToSalesman = (salesman) =>
      return true unless @_category

      @_category.id not in _(salesman.forbidden_product_categories).pluck('id')

  _submit: () =>
    @_validate()

    return if @_scope.distributionIsInvalid()

    @_element.modal('hide')

    @_close(@_scope.salesPlans)

  _validate: () =>
    @_scope.errors = {}

    for plan in @_scope.salesPlans
      sumOfParts = 0

      for employeeId, partObjects of plan.employeesHash
        for categoryId, object of partObjects
          continue unless (categoryId == 'null' && @_scope.category == null) || (Number(categoryId) == @_scope.category)
          sumOfParts += Number(object.part || 0)

      if sumOfParts != 100 && sumOfParts != 0
        @_scope.errors[plan.id] = 'Распределение невалидно'
