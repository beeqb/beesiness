class App.CompaniesBiSalesPlansManageCtrl extends IdFly.AngularClass

  @_import: [
    '$scope',
    '$http',
    '$routeParams',
    '$location',
  ]

  app.controller('CompaniesBiSalesPlansManageCtrl', @factory())

  initialize: () ->
    @_scope.companyId = @_routeParams.company_id

    @_scope.company = {}
    @_scope.products = []
    @_scope.categories = []

    @_scope.salesPlan = @_getNewSalesPlan({
      company_id: @_scope.companyId,
    })
    @_scope.salesPlanProduct = @_getNewSalesPlanProduct()
    @_scope.salesPlanFilter = @_salesPlanFilter

    @_scope.salesPlanDistributionIsValid = false
    @_scope.productsQuantityOrSumIsValid = false
    @_scope.salesPlanIsValid = false

    @_scope.validate = @_validate
    @_scope.submit = @_submit
    @_scope.addSalesPlanProduct = @_addSalesPlanProduct
    @_scope.removeSalesPlanProduct = @_removeSalesPlanProduct

    @_initWatchers()
    @_loadProducts()
    @_loadSalesPlan()
    @_loadCompany()

  _initWatchers: () =>
    @_scope.$watch('salesPlan.date', () =>
      if !@_scope.salesPlan.date
        return

      date = @_scope.salesPlan.date
      @_scope.salesPlan = @_getNewSalesPlan({
        company_id: @_scope.companyId,
        date: date,
      })
      @_loadSalesPlan(date.year, date.month)
    )

  _loadProducts: () =>
    @_http
    .get('/api/companies/' + @_scope.companyId + '/products?include=categories')
    .then((response) =>
      @_scope.products = response.data.entities
      for product in @_scope.products
        product.selected = false
    )

  _loadSalesPlan: (year = null, month = null) =>
    url = '/api/companies/' + @_scope.companyId + '/sales_plans/new?'
    loadExists = year && month
    if loadExists
      url = '/api/companies/' + @_scope.companyId + '/sales_plans' +
          '?year=' + year +
          '&month=' + month +
          '&include=sales_plan_products,sales_plan_products.product,' +
          'sales_plan_employees,sales_plan_employees.employee' +
          '&'

    @_http
    .get(url + 'expand=labels,references')
    .then((response) =>
      @_scope.salesPlanStatic = {
        labels: response.data.labels,
        references: response.data.references,
      }
      if loadExists
        if response.data.entities.length > 0
          _.extend(@_scope.salesPlan, response.data.entities[0])
          @_createManagersDistribution()
        else if response.data.entities.length == 0
          @_scope.salesPlan.kind = 'by_quantity'
          @_createManagersDistribution()
      else
        _.extend(@_scope.salesPlan, response.data.entity)
        @_scope.salesPlan.kind = 'by_quantity'

      @_scope.salesPlan.month = month
      @_scope.salesPlan.year = year
      @_scope.salesPlan.editable = @_isEditable(@_scope.salesPlan)

      @_validate()
    )

  _isEditable: (plan) =>
    now = moment()
    month = now.month() + 1
    year = now.year()

    return true if plan.year == null && plan.month == null

    return false if plan.year < year

    if plan.year == year
      return false if plan.month < month
      return false if plan.month == month && plan.id

    return true

  _loadCompany: () =>
    @_http
    .get('/api/companies/' + @_scope.companyId + '?include=sales_managers,product_categories')
    .then((response) =>
      @_scope.company = response.data.entity
      @_scope.categories = response.data.entity.product_categories
      @_scope.salesPlanProduct.category = @_scope.categories[0]
      @_createManagersDistribution()
    )

  _createManagersDistribution: () =>
    if !@_scope.company
      return
    for manager in @_scope.company.sales_managers
      exists = -1 != _.findIndex(@_scope.salesPlan.sales_plan_employees, {
        employee_id: manager.id,
      })
      if exists
        continue

      @_scope.salesPlan.sales_plan_employees.push({
        sales_plan_id: @_scope.salesPlan.id,
        employee_id: manager.id,
        part: 0,
        employee: manager, # extra field. it needs for showing
      })

  _getNewSalesPlanProduct: () ->
    return {
      sales_plan_id: null,
      product_id: null,
      sum: 0,
      quantity: 0,
    }

  _getNewSalesPlan: (data) ->
    return _.extend(data, {
      sales_plan_products: [],
      sales_plan_employees: [],
    })

  _getNewSalesPlanEmployee: () ->
    return {
      sales_plan_id: null,
      employee_id: null,
      part: 0,
    }

  _addSalesPlanProduct: () =>
    @_scope.salesPlanProduct.temp_id = moment().unix()
    @_scope.salesPlanProduct.product_id = @_scope.salesPlanProduct.product.id
    @_scope.salesPlan.sales_plan_products.push(@_scope.salesPlanProduct)
    @_scope.salesPlanProduct = _.extend(
      @_getNewSalesPlanProduct(),
      category: @_scope.salesPlanProduct.category,
    )
    @_validate()

  _removeSalesPlanProduct: (id) =>
    for salesPlanProduct in @_scope.salesPlan.sales_plan_products
      if salesPlanProduct.id == id || salesPlanProduct.temp_id == id
        salesPlanProduct._destroy = 1
        break

  _validate: () =>
    @_scope.salesPlanDistributionIsValid = @_validateSalesPlanDistribution()

    #@_validateProductsQuantityOrSum()
    @_scope.productsQuantityOrSumIsValid = true

    @_scope.salesPlanIsValid = @_scope.salesPlanDistributionIsValid &&
        @_scope.productsQuantityOrSumIsValid

  _validateProductsQuantityOrSum: () =>
    isValid = @_scope.salesPlan.sales_plan_products.length > 0
    for salesPlanProduct in @_scope.salesPlan.sales_plan_products
      if @_scope.salesPlan.kind == 'by_quantity'
        existsQuantity = salesPlanProduct.product.quantity
        if parseFloat(salesPlanProduct.quantity) == 0 || salesPlanProduct.quantity > existsQuantity
          isValid = false
          break
      else if @_scope.salesPlan.kind == 'by_sum'
        existsSum = salesPlanProduct.product.quantity *
          salesPlanProduct.product.price
        if parseFloat(salesPlanProduct.sum) == 0 || salesPlanProduct.sum > existsSum
          isValid = false
          break

    return isValid

  _validateSalesPlanDistribution: () =>
    sumOfParts = 0
    for salesPlanEmployee in @_scope.salesPlan.sales_plan_employees
      sumOfParts += salesPlanEmployee.part
    return sumOfParts == 100

  _submit: () =>
    data = @_scope.salesPlan
    data.sales_plan_products_attributes = @_scope.salesPlan.sales_plan_products
    data.sales_plan_employees_attributes = @_scope.salesPlan.sales_plan_employees

    method = @_http.post
    url = '/api/companies/' + @_scope.companyId + '/sales_plans'
    if @_scope.salesPlan.id
      method = @_http.put
      url += '/' + @_scope.salesPlan.id

    method(url, {sales_plan: data})
    .then(() =>
      @_location.path('/companies/' + @_scope.companyId + '/sales_plan')
    )

  _salesPlanFilter: (product) =>
    inCategory = @_scope.salesPlanProduct.category.id in product.categories
    notSelected = !_.any(@_scope.salesPlan.sales_plan_products, (salesPlanProduct) =>
      salesPlanProduct.product_id == product.id &&
      @_scope.salesPlanProduct.category.id == salesPlanProduct.category.id &&
      salesPlanProduct._destroy != 1
    )

    inCategory and notSelected
