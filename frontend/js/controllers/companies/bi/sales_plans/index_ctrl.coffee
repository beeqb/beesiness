class App.CompaniesBiSalesPlansIndexCtrl extends IdFly.AngularClass

  @_import: [
    '$scope',
    '$routeParams',
    '$q',
    'ModalService',
    'ResourceLoader',
    'Page',
    'distribute',
  ]

  app.controller('CompaniesBiSalesPlansIndexCtrl', @factory())

  initialize: () ->
    @_scope.viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0)

    @_scope.companyId = Number(@_routeParams.company_id)
    @_companyResource = new @_ResourceLoader(@_scope.companyId)

    @_salesPlansIncludes = [
      'sales_plan_products',
      'sales_plan_products.product',
      'sales_plan_products.product.product_categories',
      'sales_plan_product_categories',
      'sales_plan_employees',
    ]

    @_scope.quantity = (plan) =>
      if @_scope.currentCategory
        _.chain(plan.sales_plan_products)
          .filter((salesPlanProduct) =>
            salesPlanProduct.product.category_id == @_scope.currentCategory.id
          )
          .sumProperty('quantity')
          .value()
      else
        plan.quantity

    @_scope.categoryIsAvailableToSalesman = (salesman, category) =>
      category.id not in _(salesman.forbidden_product_categories).pluck('id')

    @_scope.company = {}
    @_scope.totalSum = 0

    @_scope.allSalesPlans = []
    @_scope.salesPlans = []
    @_scope.selectedPeriod = "year"

    @_scope.onPeriodChange = ((period) =>
      @_scope.selectedPeriod = period
      @_loadSalesPlans()
    )

    @_scope.loadSalesPlans = @_loadSalesPlans

    @_scope.updateOrCreateSalesPlanByQuantity = @_updateOrCreateSalesPlanByQuantity

    @_scope.updateOrCreateSalesPlanBySum = @_updateOrCreateSalesPlanBySum

    @_scope.editEmployeesDistribution = @_editEmployeesDistribution
    @_scope.isInFuture = @_isInFuture

    @_scope.selectedQuarter = moment().quarter()
    @_scope.selectedYear = moment().year()

    @_scope.toggleSelectedProductCategories = @_toggleSelectedProductCategories
    @_scope.prevQuarter = @_prevQuarter
    @_scope.nextQuarter = @_nextQuarter

    @_scope.isProductPlanEditable = @_isProductPlanEditable

    @_scope.filterProductsByCategory = ((product) =>
      return true unless @_scope.currentCategory

      product.category_id == @_scope.currentCategory.id
    )

    @_loadCompany().then(@_loadProducts).then(@_calculateProductByCategories)

    @_scope.salesmen = []
    @_loadSalesmen()

  _toggleSelectedProductCategories: (category) =>
    if @_scope.currentCategory && @_scope.currentCategory.id == category.id
      @_scope.currentCategory = undefined
    else
      @_scope.currentCategory = category

    @_updateView()

  _editEmployeesDistribution: () =>
    templateUrl = 'assets/views/companies/bi/modals/sales_plan_employees_distribution.html'

    modalInputs = {
      salesPlans: JSON.stringify(@_scope.salesPlans),
      salesmen: JSON.stringify(@_scope.salesmen)
      category: @_scope.currentCategory,
    }

    @_ModalService
    .showModal({
      templateUrl: templateUrl,
      controller: 'CompaniesBiSalesPlanEmployeesDistributionCtrl',
      inputs: modalInputs
    })
    .then((modal) =>
      modal.close.then((salesPlans) =>
        @_updateEmployeesDistribution(salesPlans)
      )
      modal.element.modal()
    )

  _updateEmployeesDistribution: (salesPlans) =>
    data = []

    for plan in salesPlans
      for employeeId, partObjects of plan.employeesHash
        for categoryId, object of partObjects
          data.push({
            id: object.id,
            employee_id: employeeId,
            sales_plan_id: plan.id,
            part: object.part,
            product_category_id: categoryId,
          })

    @_companyResource
    .post('sales_plan_employees/bulk_update', {sales_plan_employees: data})
    .then((response) =>
      for plan in @_scope.salesPlans
        plan.sales_plan_employees = _.where(
          response.data,
          {sales_plan_id: plan.id}
        )
        plan = @_processSalesPlan(plan)

      @_updateView()
    )

  _nextQuarter: () =>
    if (@_scope.selectedQuarter == 4 && @_scope.viewportWidth > 991) ||
       (@_scope.selectedQuarter == 12 && @_scope.viewportWidth <= 991)
      @_scope.selectedQuarter = 1
      @_scope.selectedYear += 1

      @_loadSalesPlans()
    else
      @_scope.selectedQuarter++

    @_updateView()

  _prevQuarter: () =>
    if @_scope.selectedQuarter == 1
      if @_scope.viewportWidth <= 991
        @_scope.selectedQuarter = 12
      else
        @_scope.selectedQuarter = 4

      @_scope.selectedYear -= 1

      @_loadSalesPlans()
    else
      @_scope.selectedQuarter--

    @_updateView()

  _loadCompany: () =>
    resource = new @_ResourceLoader()

    resource
    .include(
      'product_categories',
      'created_clients_count_by_days',
      'actual_income_by_days',
      'clients_count',
      'actual_income'
    )
    .get('companies', @_scope.companyId)
    .then((response) =>
      @_scope.company = response.data.entity
      @_Page.setTitle('sales_plans', @_scope.company.name)
    )

  _loadSalesmen: () =>
    @_companyResource
    .include(
      'full_name',
      'employees_departments',
      'image',
      'forbidden_product_categories'
    )
    .query('employees', {participation_in_sales: 'yes'})
    .then((response)=>
      @_scope.salesmen = response.data.entities
      console.log(@_scope.salesmen)
    )

  _loadProducts: () =>
    @_companyResource
      .expand('references')
      .query('products')
    .then((response)=>
      @_scope.products = response.data.entities
      @_scope.productStatic = {references: response.data.references}
    )

  _getMonthsOfQuarter: (quarter) =>
    if @_scope.viewportWidth <= 991
      switch quarter
        when 1 then [1]
        when 2 then [2]
        when 3 then [3]
        when 4 then [4]
        when 5 then [5]
        when 6 then [6]
        when 7 then [7]
        when 8 then [8]
        when 9 then [9]
        when 10 then [10]
        when 11 then [11]
        when 12 then [12]
        else
          []
    else
      switch quarter
        when 1 then [1, 2, 3]
        when 2 then [4, 5, 6]
        when 3 then [7, 8, 9]
        when 4 then [10, 11, 12]
        else
          []

  _loadSalesPlans: () =>
    params = {
      year: @_scope.selectedYear,
    }

    #return if _.findWhere(@_scope.allSalesPlans, params)

    @_companyResource
    .include(@_salesPlansIncludes...)
    .query('sales_plans')
    .then((response) =>
      @_scope.allSalesPlans = _.chain(response.data.entities).sortBy('year').map(@_processSalesPlan).value()
      @_updateView()
    )

  _processSalesPlan: (plan) =>
    plan.productsHash = _.indexBy(plan.sales_plan_products, 'product_id')
    plan.employeesHash = _.groupBy(plan.sales_plan_employees, 'employee_id')

    for id, employees of plan.employeesHash
      plan.employeesHash[id] = _.indexBy(employees, 'product_category_id')

    plan.categoriesHash =
      _.indexBy(plan.sales_plan_product_categories, 'product_category_id')

    plan

  _updateOrCreateSalesPlanBySum: (salesPlan, sum) =>
    if salesPlan.kind == 'by_quantity' &&
       !confirm('План в штуках будет сброшен. Продолжить?')
      return false

    data = salesPlan
    data.kind = 'by_sum'
    data.sales_plan_product_categories_attributes = data.sales_plan_product_categories || []

    if @_scope.currentCategory
      salesPlanProductCategory = _.findWhere(
        data.sales_plan_product_categories_attributes,
        {product_category_id: @_scope.currentCategory.id}
      )

      if salesPlanProductCategory
        salesPlanProductCategory.sum = Number(sum)
      else
        data.sales_plan_product_categories_attributes.push({
          product_category_id: @_scope.currentCategory.id,
          sum: sum,
          sales_plan_id: data.id,
        })

      # Preserve products
      data.sales_plan_products_attributes = _.each(data.sales_plan_products, (element, index, list) => element._destroy = 1 if element.product.category_id == @_scope.currentCategory.id) || []

      data.sum = _(data.sales_plan_product_categories_attributes).sumProperty('sum')
    else
      shares = @_distribute(sum, @_scope.company.product_categories.length)
      _.setAll(data.sales_plan_product_categories_attributes, '_destroy', 1)

      if data.sales_plan_products_attributes?
        data.sales_plan_products_attributes = data.sales_plan_products
        _.setAll(data.sales_plan_products_attributes, '_destroy', 1)

      [].push.apply(
        data.sales_plan_product_categories_attributes,
        _(@_scope.company.product_categories).map((category, index, list) =>
          {
            product_category_id: category.id,
            sum: shares[index],
            sales_plan_id: data.id,
          }
        )
      )
      data.sum = sum

    defer = @_q.defer()

    promise = if salesPlan.id
                @_updateSalesPlan(data)
              else
                @_createSalesPlan(data)

    promise
    .then(() =>
      defer.resolve()
    )
    .catch((response) =>
      defer.reject(response.data.errors)
    )

    defer.promise

  _updateOrCreateSalesPlanByQuantity: (salesPlan, productId, quantity) =>
    if salesPlan.kind == 'by_sum' &&
       !confirm('План в деньгах будет сброшен. Продолжить?')
      return false

    data = salesPlan
    delete data.sales_plan_product_categories_attributes
    data.kind = 'by_quantity'
    data.sales_plan_products_attributes = data.sales_plan_products || []

    salesPlanProduct = _.findWhere(
      data.sales_plan_products_attributes,
      {product_id: productId}
    )

    if salesPlanProduct
      salesPlanProduct.quantity = Number(quantity).toString()
    else
      data.sales_plan_products_attributes.push({
        product_id: Number(productId).toString(),
        quantity: Number(quantity).toString(),
        sales_plan_id: Number(data.id).toString(),
      })

    defer = @_q.defer()

    promise = if salesPlan.id
                @_updateSalesPlan(data)
              else
                @_createSalesPlan(data)

    promise
    .then(defer.resolve)
    .catch((response) =>
      defer.reject(response.data.errors)
    )

    defer.promise

  _updateSalesPlan: (salesPlan) =>
    @_companyResource
    .include(@_salesPlansIncludes...)
    .put('sales_plans', salesPlan.id, {sales_plan: salesPlan})
    .then((response) =>
      plan = _.findWhere(@_scope.salesPlans, {id: salesPlan.id})
      _.extend(plan, @_processSalesPlan(response.data))
    )

  _createSalesPlan: (salesPlan) =>
    @_companyResource
    .include(@_salesPlansIncludes...)
    .post('sales_plans', {sales_plan: salesPlan})
    .then((response) =>
      @_scope.allSalesPlans.push(
        _.extend(salesPlan, @_processSalesPlan(response.data))
      )
    )

  _updateView: () =>
    @_scope.salesPlans = @_selectSalesPlans(@_scope.allSalesPlans)
    @_scope.totalSum = @_calculateTotalSum(@_scope.salesPlans)

    @_mergeSalesPlansEmployees(@_scope.salesPlans)

  _selectSalesPlans: (plans) =>
    filteredPlans = []

    for month in @_getMonthsOfQuarter(@_scope.selectedQuarter)
      finded = _.findWhere(plans, {month: month, year: @_scope.selectedYear})

      if finded
        filteredPlans.push(finded)
      else if @_scope.selectedPeriod == 'year'
        filteredPlans.push({
          month: month,
          year: @_scope.selectedYear,
          company_id: @_scope.companyId,
        })

    return filteredPlans

  _calculateProductByCategories: () =>
    grouppedProducts = _.groupBy(@_scope.products, _.property('category_id'))
    _.setAll(@_scope.company.product_categories, 'count', (category) ->
      grouppedProducts[category.id].length
    )

  _salesPlanValuesDelimiter: () =>
    currentMonthLength = moment().endOf('month').date()
    delimiter = 1.0
    if @_scope.selectedPeriod == 'day'
      delimiter = currentMonthLength
    if @_scope.selectedPeriod == 'week'
      delimiter = currentMonthLength / 7.0
    return delimiter

  _calculateTotalSum: (plans) =>
    sum = 0.0
    delimiter = @_salesPlanValuesDelimiter()

    for salesPlan in plans
      sum += parseFloat(salesPlan.sum) || 0
      sum /= delimiter

    return sum

  _mergeSalesPlansEmployees: (salesPlans) =>
    _.setAll(@_scope.salesmen, 'part', 0)
    _.setAll(@_scope.salesmen, 'count', 0)

    for salesPlan in salesPlans
      for salesPlanEmployee in (salesPlan.sales_plan_employees || [])
        salesman = _.findWhere(
          @_scope.salesmen,
          {id: salesPlanEmployee.employee_id}
        )

        continue unless salesman
        if @_scope.currentCategory &&
           salesPlanEmployee.product_category_id != @_scope.currentCategory.id
          continue

        salesman.part += salesPlanEmployee.part
        salesman.count += 1

    for salesman in @_scope.salesmen
      salesman.part = if salesman.count != 0
                        parseInt(salesman.part / salesman.count)
                      else
                        0
  _isProductPlanEditable: (salesPlan, product) =>
    @_scope.user.can('manage', 'SalesPlan', {company_id: @_scope.companyId}) &&
        @_isInFuture(salesPlan) &&
        product.activity != 'archive' &&
        product.activity != 'no'

  _isInFuture: (salesPlan) =>
    moment({
      year: salesPlan.year,
      month: salesPlan.month - 1,
    })
    .isSameOrAfter({}, 'month')
