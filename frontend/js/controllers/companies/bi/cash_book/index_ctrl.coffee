class App.CompaniesBiCashBookIndexCtrl extends IdFly.AngularClass
  @_import: [
    '$rootScope',
    '$scope',
    '$routeParams',
    '$filter',
    '$timeout',
    '$q',
    '$auth',
    'ResourceLoader',
    'Upload',
    'ModalService',
    'Page',
  ]

  app.controller('CompaniesBiCashBookIndexCtrl', @factory())

  initialize: () ->
    @_scope.companyId = @_companyId = @_routeParams.company_id
    @_companyResource = new @_ResourceLoader(@_companyId)
    @departmentsWithEmployees = {}

    @_scope.companyId = @_companyId
    @_scope.cashBookRecords = []
    @_scope.loadCashBookRecords = @_loadCashBookRecords
    @_scope.createNewRecord = @_createNewRecord
    @_scope.loadEmployees = @_loadEmployees
    @_scope.references = {}
    @_scope.filter = {categories: []}
    @_scope.statistics = {}
    @_scope.updateRecord = @_updateRecord
    @_scope.toggleCategory = @_toggleCategory

    @_scope.createCategory = @_createCategory
    @_scope.editCategory = @_editCategory

    @_scope.isCategoryAllowed = @_isCategoryAllowed

    @_scope.manageCashFlowBudgetCategories = @_manageCashFlowBudgetCategories
    @_scope.chooseCashFlowBudgetCategory = @_chooseCashFlowBudgetCategory

    @_scope.$watch(
      'filter',
      ((newValue, oldValue) =>
        return if newValue == oldValue
        @_scope.cashBookRecords = []
        @_loadCashBookRecords()
      ),
      true
    )

    @_scope.$watch('user.employees', (newValue, oldValue) =>
      return if newValue == oldValue

      currentEmployee = @_scope.user.employeeInCompany(@_companyId)

      return if !currentEmployee || currentEmployee.dismissed

      @_scope.selectedDepartment =
        currentEmployee.employees_departments[0].department_id

      @_loadEmployees(@_scope.selectedDepartment)
      @_scope.newRecord.creator_id = currentEmployee.id
    )

    @_initNewRecord()
    @_loadCashBookRecords()
    @_loadDepartments()
    @_loadCompany()
    @_loadCompanyIndicators()

    @_companyResource.query('cash_flow_budget_categories').then((response) =>
      @_scope.cash_flow_budget_categories = {
        income: response.data.income,
        outcome: response.data.outcome,
      }
      @_scope.cash_flow_budget_references = response.data.references
    )

  _loadCompanyIndicators: () =>
    @_companyResource
    .query(
      'budget_statistics',
      {indicators: 'avg_month_outcome,avg_month_income,avg_monthly_income,avg_monthly_outcome,total_income,total_outcome'}
    )
    .then((response) =>
      @_scope.statistics = response.data.data
      @_scope.statistics.avg_month_outcome = Math.abs(@_scope.statistics.avg_month_outcome)
      @_scope.statistics.avg_monthly_outcome = Math.abs(@_scope.statistics.avg_monthly_outcome)
      @_scope.statistics.total_outcome = Math.abs(@_scope.statistics.total_outcome)
    )

  _loadCashBookRecords: (startDate, endDate) =>
    @startDate = if startDate? then startDate else moment().startOf('year')
    @endDate = if endDate? then endDate else moment()

    @_scope.loading = true
    filter = _.pick(@_scope.filter, _.identity)

    @_companyResource
    .include('deal', 'creator')
    .expandReferencesAndLabels()
    .params(_.extend(
      {
        'between_dates[min]': @startDate.format('YYYY-MM-DD'),
        'between_dates[max]': @endDate.format('YYYY-MM-DD')
      },
      filter
    ))
    .query('cash_book_records')
    .then((response) =>
      data = response.data
      @_scope.cashBookRecords = _.map(data.entities, @_processRecord)
      _.extend(@_scope.references, data.references)
      @_scope.labels = data.labels
      @_scope.loading = false
      @_scope.$broadcast('cashBookRecordsLoaded', @_scope.cashBookRecords)
    )

  _loadExistingSources: () =>
    @_companyResource
    .query('cash_book_records/existing_sources')
    .then((response) =>
      @_scope.existingSources = response.data
    )

  _initNewRecord: () =>
    @_scope.newRecord = {
      category: 'income',
      bill_kind: 'cash',
      company_id: @_companyId,
      created_at: new Date(),
      user_id: @_scope.user.id,
      personal: false,
      currency: @_scope.company?.currency,
    }

  _processRecord: (record) =>
    record.created_at = Date.parse(record.created_at)

    record.amount = Math.abs(record.amount)

    record.department_names =
      _.chain(record.creator.employees_departments)
       .pluck('department')
       .pluck('name')
       .value()

    record

  _loadEmployees: (departmentId) =>
    if _.has(@departmentsWithEmployees, departmentId)
      @_scope.references.employees = @departmentsWithEmployees[departmentId]
      return

    @_companyResource
    .include('user')
    .query('employees', {departments: departmentId})
    .then((response) =>
      employees =
        _
        .chain(response.data.entities)
        .where({dismissed: false})
        .map((employee) =>
          {
            id: employee.id,
            fullname: @_filter('fullname')(employee.user),
          }
        )
        .value()
      @departmentsWithEmployees[departmentId] = employees
      @_scope.references.employees = employees
    )

  _loadDepartments: () =>
    @_companyResource
    .query('departments')
    .then((response) =>
      @_scope.references.departments = response.data.entities
    )

  _loadCompany: () =>
    resource = new @_ResourceLoader

    resource
    .include(
      'cash_book_categories',
      'cash_book_records_counts_by_categories',
      'cash_flow_budget_categories',
      'created_clients_count_by_days',
      'actual_income_by_days',
      'clients_count',
      'actual_income'
    )
    .get('companies', @_companyId)
    .then((response) =>
      @_scope.company = response.data.entity

      @_Page.setTitle('cash_book', @_scope.company.name)

      @_scope.references.categories = @_scope.company.cash_book_categories
      delete @_scope.company.cash_book_categories
      @_scope.references.cash_flow_budget_categories =
        @_scope.company.cash_flow_budget_categories
      delete @_scope.company.cash_flow_budget_categories

      @_scope.newRecord.currency = @_scope.company.currency
      @_scope.counts = @_scope.company.cash_book_records_counts_by_categories
    )

  _createNewRecord: () =>
    @_companyResource
    .include('creator')
    .post('cash_book_records', {cash_book_record: @_scope.newRecord})
    .then((response) =>
      @_uploadDocument(response.data.id, @_scope.newRecord.reporting_document)
      @_initNewRecord()
      @_rootScope.$emit('user:reloadPermissions')
      @_loadCashBookRecords()
    )
    .catch((response) =>
      @_scope.newRecord.errors = response.data.errors
    )

  _updateRecord: (record, attribute, value) =>
    data = {}

    _.extend(data, record)

    if attribute == 'created_at'
      data.created_at = moment(value).format('YYYY-MM-DD')
    else
      data[attribute] = value
      delete data.created_at

    defer = @_q.defer()

    @_companyResource
    .include('creator', 'deal')
    .put('cash_book_records', record.id, {cash_book_record: data})
    .then((response) =>
      updatedRecord = response.data
      record = _.findWhere(@_scope.cashBookRecords, {id: record.id})
      _.extend(record, @_processRecord(updatedRecord))

      if attribute == 'amount'
        @_refreshRecordsAfter(record.created_at)

      @_scope.$broadcast('cashBookRecordsLoaded', @_scope.cashBookRecords)
      defer.resolve()
    )
    .catch((response) ->
      defer.reject(response.data.errors[attribute].join('; '))
    )

    return defer.promise

  _refreshRecordsAfter: (date) =>
    records = @_scope.cashBookRecords

    ids =
      _
      .chain(records)
      .filter((record) -> record.created_at >= date)
      .pluck('id')
      .value()

    @_companyResource
    .include('deal', 'creator')
    .query('cash_book_records', { 'id[]': ids, })
    .then((response) =>
      data = response.data.entities

      for record in data
        existedRecord = _.findWhere(@_scope.cashBookRecords, {id: record.id})
        _.extend(existedRecord, @_processRecord(record))
    )

  _uploadDocument: (recordId, document) =>
    return unless document

    url = @_companyResource.buildUrl('cash_book_records', recordId)

    @_Upload.upload({
      url: url + '/upload_document',
      method: 'PUT',
      headers: @_auth.retrieveData('auth_headers'),
      data: {
        document: document
      },
    }).then((response) =>
      existedRecord = _.findWhere(
        @_scope.cashBookRecords,
        {id: response.data.id}
      )
      existedRecord.reporting_document = response.data.reporting_document
    )

  _isCategoryAllowed: (category) =>
    return true if @_scope.user.hasRole('employee_investor', @_companyId)

    category.id in ['income', 'outcome'] || !category.system

  _toggleCategory: (category) =>
    if not category in @_scope.filter.categories
      @_scope.filter.categories.push(category)
    else
      @_scope.filter.categories = _.without(@_scope.filter.categories, category)

  _createCategory: () =>
    @_ModalService.showModal({
      templateUrl: '/assets/views/companies/cash_book_categories/new.html',
      controller: 'CompaniesCashBookCategoriesNewCtrl',
      inputs: {
        companyId: @_companyId,
      },
    })
    .then((modal) =>
      modal.element.modal()
      modal.close.then((result) =>
        @_companyResource
        .post('cash_book_categories', {cash_book_category: result})
        .then((response) =>
          @_scope.references.categories.push(response.data)
        )
      )
    )

  _editCategory: (categoryId) =>
    @_ModalService.showModal({
      templateUrl: "/assets/views/companies/cash_book_categories/new.html",
      controller: 'CompaniesCashBookCategoriesEditCtrl',
      inputs: {
        companyId: @_companyId,
        categoryId: categoryId,
        existingCategories: JSON.stringify(@_scope.references.categories),
      },
    })
    .then((modal) =>
      modal.element.modal()
      modal.close.then((result) =>
        if result.delete
          @_deleteCategory(categoryId, result)
        else
          @_updateCategory(categoryId, result)
      )
    )

  _updateCategory: (categoryId, data) =>
    @_companyResource
    .put('cash_book_categories', categoryId, {cash_book_category: data})
    .then((response) =>
      categories = @_scope.references.categories

      categoryIndex = _.findIndex(categories, {id: response.data.id})
      categories.splice(categoryIndex, 1, response.data)
    )

  _deleteCategory: (categoryId, data) =>
    @_companyResource
    .post(
      "cash_book_categories/#{categoryId}/delete_safely",
      {category_after_delete: data.category_after_delete}
    )
    .then(() =>
      categories = @_scope.references.categories

      @_scope.references.categories = _.filter(
        categories,
        (category) -> category.id != categoryId
      )
    )

  _manageCashFlowBudgetCategories: () =>
    @_ModalService.showModal({
      templateUrl: '/assets/views/companies/cash_flow_budget_categories/manage.html',
      controller: 'CompaniesCashFlowBudgetCategoriesManageCtrl',
      inputs: {
        companyId: @_companyId,
        categories: JSON.stringify(@_scope.cash_flow_budget_categories),
        references: JSON.stringify(@_scope.cash_flow_budget_references),
      },
    })
    .then((modal) =>
      modal.element.modal()
    )

  _chooseCashFlowBudgetCategory: () =>
    @_ModalService.showModal({
      templateUrl: '/assets/views/companies/cash_flow_budget_categories/choose.html',
      controller: 'CompaniesCashFlowBudgetCategoriesChooseCtrl',
      inputs: {
        categories: JSON.stringify(@_scope.cash_flow_budget_categories),
      },
    })
    .then((modal) =>
      modal.element.modal()
      modal.close.then((result) =>
        @_scope.newRecord.cash_flow_budget_category_id = result.category.id
      )
    )
