class App.CompaniesProductCategoriesNewCtrl extends App.CompaniesProductCategoriesLoadCtrl

  @_import: [
    '$scope',
    '$element'
    'close',
    'companyId',
    'ResourceLoader',
  ]

  app.controller('CompaniesProductCategoriesNewCtrl', @factory())

  initialize: () ->
    super()

  _load: () =>
    companyResource = new @_ResourceLoader(@_companyId)

    companyResource
    .expand('labels')
    .query('product_categories/new')
    .then((response) =>
      @_scope.categoryStatic = {
        labels: response.data.labels,
      }
      _.extend(@_scope.category, response.data.entity)
      @_scope.category.company_id = @_companyId
      @_scope.category.has_part_in_average_price = true
    )
