class App.CompaniesProductCategoriesLoadCtrl extends IdFly.AngularClass

  app.controller('CompaniesProductCategoriesLoadCtrl', @factory())

  initialize: () ->
    @_scope.category = {
      errors: {},
    }
    @_scope.categories = []
    @_scope.availableCategories = []
    @_scope.submit = @_submitModal
    @_scope.colors = [
      '6254b2',
      '23b7e5',
      '27c24c',
      'fad733',
      'f05050',
      '3a3f51',
    ]
    @_load()

  _submitModal: () =>
    companyResource = new @_ResourceLoader(@_companyId)

    companyResource
    .post('product_categories/validate', {product_category: @_scope.category})
    .then(
      (() =>
        @_element.modal('hide')
        @_close(@_scope.category)
      ),
      ((response) =>
        _.extend(@_scope.category.errors, response.data.errors)
      )
    )
