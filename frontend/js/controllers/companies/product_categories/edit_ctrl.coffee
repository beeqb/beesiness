class App.CompaniesProductCategoriesEditCtrl extends App.CompaniesProductCategoriesLoadCtrl

  @_import: [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$element',
    'close',
    'companyId',
    'productCategoryId',
    'ResourceLoader',
  ]

  app.controller('CompaniesProductCategoriesEditCtrl', @factory())

  initialize: () ->
    @_companyResource = new @_ResourceLoader(@_companyId)

    super()

    # Нужно, чтобы при показе модального окна кнопка "Удалить" не "моргала"
    @_scope.loaded = false

    @_scope.delete = @_delete

    @_scope.$watchGroup(
      ['categories', 'category'],
      (
        () =>
          return unless @_scope.category.id

          @_scope.availableCategories = _.filter(
            @_scope.categories,
            (category) =>
              category.id != @_scope.category.id
          )
      ),
      true
    )

  _load: () =>
    @_companyResource
    .expand('labels')
    .get('product_categories', @_productCategoryId)
    .then((response) =>
      @_scope.categoryStatic = {
        labels: response.data.labels,
      }
      @_scope.category = response.data.entity
      @_scope.category.category_after_delete = null

      @_loadCategories().then(() => @_scope.loaded = true)
    )

  _loadCategories: () =>
    @_companyResource
    .query('product_categories')
    .then((response) =>
      @_scope.categories = response.data.entities
    )

  _delete: () =>
    @_scope.category.delete_with_products = false
    confirmText = 'Уверены, что хотите удалить категорию товаров?'

    if @_scope.availableCategories.length == 0
      @_scope.category.delete_with_products = true
      confirmText = 'Уверены, что хотите удалить категорию товаров вместе с товарами?'

    if !confirm(confirmText)
      return

    @_element.modal('hide')

    # need for parent (ProductIndexCtrl)
    @_scope.category.delete = true
    @_close(@_scope.category)
