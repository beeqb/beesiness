class App.TasksNewCtrl extends IdFly.AngularClass

  @_import: [
    '$scope',
    '$routeParams',
    '$location',
    'SearchQuery',
  ]

  app.controller('CompaniesSearchResultsCtrl', @factory())

  initialize: () ->
    @companyId = @_routeParams.company_id
    @_scope.isSearchGroupEmpty = @_isSearchGroupEmpty
    @_scope.token = @_routeParams.search_token
    @_scope.searchSection = @_routeParams.search_section
    @_scope.redirectTo = (path) =>
      @_location.path("/companies/#{@companyId}/#{path}").search({})

    @_performSearch()

  _performSearch: () =>
    @_SearchQuery
    .perform(@_routeParams.search_token, @companyId, @_scope.searchSection)
    .then((searchResponse) =>
      @_scope.searchResults = searchResponse.search_results
      @_scope.isEmptySearch =
        _
        .chain(@_scope.searchResults)
        .map((results) -> results)
        .flatten()
        .compact()
        .isEmpty()
        .value()

      @_scope.references = searchResponse.references
    )

  _isSearchGroupEmpty: (groupName) =>
    !@_scope.searchResults || _.isEmpty(@_scope.searchResults[groupName])
