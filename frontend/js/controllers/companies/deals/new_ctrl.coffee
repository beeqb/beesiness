class App.CompaniesDealsNewCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$location',
    '$routeParams',
    '$auth',
    '$cookies',
    '$timeout',
    '$q',
    'Upload',
    'ModalService',
    'mapConfig',
    'dealCalculator',
    'ResourceLoader',
    'leadProducts',
  ]

  app.controller('CompaniesDealsNewCtrl', @factory())

  initialize: () ->
    @_scope.companyId = @_companyId = @_routeParams.company_id

    @_resource = new @_ResourceLoader()
    @_companyResource = new @_ResourceLoader(@_companyId)

    @_scope.map = @_mapConfig
    @_scope.updatePhoneCodeByCountry = @_updatePhoneCodeByCountry
    @_scope.deal = {deal_products: [], errors: {}}

    @_scope.companies = []
    @_scope.employees = []
    @_scope.employeesForSecondManager = []
    @_scope.clients = []
    @_scope.removeDealProduct = @_removeDealProduct

    @_scope.submit = @_submit
    @_scope.addDealProduct = @_addDealProduct

    @_scope.startedAtMaxDate = new Date()

    @_loadCompany()
    @_loadCashFlowBudgetCategories()
    @_loadDeal().then(() =>
      @_loadClients()
      @_loadEmployees()
      @_initWatchers()

      return unless @_routeParams.from_lead

      leadProducts = @_leadProducts.popProducts(@_routeParams.from_lead)

      _(leadProducts).each((leadProduct) =>
        @_addDealProduct({
          product_id: leadProduct.product_id,
          count: leadProduct.count,
          price: leadProduct.product.price,
          product: leadProduct.product,
        })
      )
    )

  _initWatchers: () =>
    @_scope.$watchGroup(
      ['employees', 'deal.manager_id'],
      @_getUsersForSecondManager,
      true
    )
    @_scope.$watchCollection('deal.deal_products', @_recalculate, true)
    @_scope.$watch('deal.deal_products', @_recalculate, true)
    @_scope.$watch('company.id', @_recalculate, true)
    @_scope.$watch('user.employees', () =>
      @_scope.deal.manager_id = @_scope.user.employeeInCompany(@_companyId)?.id
    )
    @_scope.$watch('deal.payment_method', (newValue, oldValue) =>
      return if newValue == oldValue
      delete @_scope.deal.interest_rate unless newValue == 'credit'
    )
    @_scope.$watch('deal.payment_period', (newValue, oldValue) =>
      return if newValue == oldValue
      delete @_scope.deal.payment_method if newValue == 'single'
    )

  _removeDealProduct: (dealProductId) =>
    @_scope.deal.deal_products = _.filter(
      @_scope.deal.deal_products,
      ((deal_product) ->
        return deal_product.temp_id != dealProductId
      )
    )

  _loadDeal: () =>
    @_companyResource
    .expandLabelsAndReferences()
    .query('deals/new')
    .then((response) =>
      @_scope.dealStatic = {
        labels: response.data.labels
        references: response.data.references
      }
      _.extend(@_scope.deal, response.data.entity)

      @_scope.deal.started_at = moment().toDate()
    )

  _loadClients: () =>
    @_companyResource
    .query('clients')
    .then((response) =>
      @_scope.clients = response.data.entities

      client = _.findWhere(@_scope.clients, {
        id: Number(@_routeParams.client_id)
      })

      if client
        @_scope.deal.email = client.email
        @_scope.deal.phone = client.work_phone
        @_scope.deal.phone_code_id = client.work_phone_code_id
        @_scope.deal.address = client.address
        @_scope.deal.discount = client.discount
        @_scope.deal.legal_form = client.legal_form
        @_scope.deal.client_id = client.id
        @_recalculate()

        @_resource
        .get('companies', client.company_id)
        .then((response) => @_recalculate())
    )

  _loadEmployees: () =>
    @_companyResource
    .include('user')
    .query('employees')
    .then((response) =>
      @_scope.employees = _.map(response.data.entities, (employee) =>
        user = employee.user
        employee.name = user.name + ' ' + user.surname
        return employee
      )
    )

  _loadCompany: () =>
    @_resource
    .get('companies', @_companyId)
    .then((response) =>
      @_scope.company = response.data.entity
    )

  _getUsersForSecondManager: () =>
    if !@_scope.deal.manager_id || !@_scope.employees.length > 0
      return

    @_scope.employeesForSecondManager =
      _.filter(@_scope.employees, (item) =>
        return item.id != @_scope.deal.manager_id
      )

  _recalculate: () =>
    @_dealCalculator.calculate(
      @_scope.deal,
      @_scope.deal.deal_products,
      (@_scope.company || {}).vat,
      @_scope.deal.discount
    )

  _addDealProduct: (dealProduct) =>
    dealProduct.temp_id = moment.unix()
    @_scope.deal.deal_products.push(dealProduct)

  _submit: () =>
    @_scope.deal.selected_products = @_scope.deal.deal_products
    _.each(@_scope.deal.selected_products, (deal_product) ->
      deal_product.product_id = deal_product.product.id
      deal_product.price = deal_product.product.price
    )

    @_companyResource
    .post('deals', {deal: @_scope.deal})
    .then(() =>
      @_rootScope.$emit('user:reloadPermissions')
      @_location.path('/companies/' + @_companyId + '/deals')
    )
    .catch((response) =>
      _.extend(@_scope.deal.errors, response.data.errors)
    )

  _updatePhoneCodeByCountry: (countryCode) =>
    code = _.findWhere(
      @_scope.dealStatic?.references.country_phone_codes,
      {country_code: countryCode},
    )
    @_scope.deal.phone_code_id = code?.id

  _loadCashFlowBudgetCategories: () =>
    @_companyResource.query('cash_flow_budget_categories').then((response) =>
      @_scope.cash_flow_budget_categories = {
        all: response.data.income.concat(response.data.outcome),
        income: response.data.income,
        outcome: response.data.outcome,
      }
    )
