class App.CompaniesDealsXEditCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$routeParams',
    '$auth',
    '$cookies',
    '$timeout',
    '$q',
    'Upload',
    'ModalService',
    'mapConfig',
    'dealCalculator',
    'ResourceLoader',
  ]

  app.controller('CompaniesDealsXEditCtrl', @factory())

  initialize: () ->
    @_scope.companyId = @_companyId = @_routeParams.company_id
    @_companyResource = new @_ResourceLoader(@_companyId)

    if @_routeParams.deal_id
      @_scope.deal = {}
      @_scope.dealStatic = {}
      @_scope.companyStatic = {}
      @_loadCompany()
      @_companyResource
      .include(
        'deal_products',
        'monthly_payments',
        'client',
        'client.rf_status',
        'client.rm_status',
        'history_length',
        'related_tasks_count',
        'receipts'
      )
      .expandLabelsAndReferences()
      .get('deals', @_routeParams.deal_id)
      .then((response) =>
        @_scope.deal = response.data.entity
        @_scope.dealStatic = {
          labels: response.data.labels,
          references: response.data.references,
        }

        @_scope.deal.expanded = true
        @_scope.deal.canBeExpanded = false
        @_scope.deal.total_sum = Number(@_scope.deal.total_sum)
        @_scope.deal.monthly_payments ?= []
        @_scope.deal.payments_count = @_scope.deal.monthly_payments.length

        @_initWatchers()
      )
    else
      @_initWatchers()
      @_scope.deal.canBeExpanded = true

    @_scope.deal.errors = {}
    @_scope.map = @_mapConfig
    @_scope.updatePhoneCodeByCountry = @_updatePhoneCodeByCountry
    @_scope.dealProductsAreEditable = @_dealProductsAreEditable
    @_scope.removeDealProduct = @_removeDealProduct
    @_scope.updateDealProductCount = @_updateDealProductCount
    @_scope.isContractActive = @_isContractActive
    @_scope.submit = @_submit
    @_scope.showReceipt = @_showReceipt

    @_scope.validate = @_validate
    @_scope.validateMonthlyPayments = @_validateMonthlyPayments

    @_scope.setStatus = @_setStatus
    @_scope.addDealProduct = @_addDealProduct
    @_scope.section = {active: 'products'}
    @_scope.employeesForSecondManager = []
    @_scope.startedAtMaxDate = new Date()
    @_scope.initMonthlyPayments = @_initMonthlyPayments
    @_scope.confirmMonthlyPayment = @_confirmMonthlyPayment
    @_scope.discardMonthlyPayment = @_discardMonthlyPayment

    @_scope.chooseCashFlowBudgetCategory = @_chooseCashFlowBudgetCategory

    @_scope.monthlyPaymentsDistributionIsValid = true

    @_scope.loadTasks = @_loadTasks

    @_scope.formIsShown = () =>
      @_routeParams.edit == 'true' &&
      +@_routeParams.deal_id == @_scope.deal.id

    @_initValidation()

  _loadCompany: () =>
    resource = new @_ResourceLoader()

    resource
    .include(
      'deals_count_by_statuses',
      'deal_statuses',
    )
    .expand('references')
    .get('companies', @_companyId)
    .then((response) =>
      @_scope.companyStatic = {
        labels: response.data.labels,
        references: response.data.references,
      }

      @_scope.companyStatic.references.deal_statuses =
        response.data.entity.deal_statuses

      delete response.data.entity.deal_statuses

      @_scope.company = response.data.entity
    )

  _initWatchers: () =>
    @_scope.watchedData = {}

    @_scope.$watchGroup(['users', 'deal.manager_id'],
      (() =>
        if !@_scope.employees || !@_scope.deal
          return
        @_scope.employeesForSecondManager = _.filter(@_scope.employees, (item) =>
          return (item.id != @_scope.deal.manager_id) && (item.id != @_scope.deal.client_id)
        )
      ),
      true
    )

    @_scope.$watchGroup(['deal.started_at', 'deal.closed_at'], () =>
      if !@_scope.deal.started_at || !@_scope.deal.closed_at
        return

      startedAt = moment(@_scope.deal.started_at)
      closedAt = moment(@_scope.deal.closed_at)

      @_scope.deal.duration = Math.abs(closedAt.diff(startedAt, 'days'))
    )

  _initValidation: () =>
    @_validationTimeout = 500 #msec
    @_scope.$watchCollection('deal', (newCollection, oldCollection) =>
      attr = null
      for k,v of newCollection
        if v != oldCollection[k]
          attr = k
          break

      if !attr || attr == 'errors'
        return

      @_validate(attr)
    )

  _validate: (attribute, value) =>
    return if !@_scope.DealForm || !@_scope.DealForm.$visible

    if @_validateTimeoutPromise
      @_timeout.cancel(@_validateTimeoutPromise)

    @_validateTimeoutPromise = @_timeout(
      (() =>
        data = {}
        _.extend(data, @_scope.deal)
        data[attribute] = value

        url = '/api/companies/' + @_companyId + '/deals/validate'
        if @_scope.deal.id
          url += '?id=' + @_scope.deal.id

        @_http
        .post(url, {deal: data})
        .then(
          (() =>
            @_setValidationError(attribute, [])
          ),
          ((response) =>
            errors = []
            for attr, error of response.data.errors
              if attr == attribute
                errors.push(error)

            @_setValidationError(attribute, errors)
          )
        )
      ),
      @_validationTimeout
    )

  _validateMonthlyPayments: () =>
    return true unless @_scope.deal.payment_period == 'monthly'

    paymentsAreValid = true

    paymentsTotal = _.reduce(
      @_scope.deal.monthly_payments,
      ((memo, payment) ->
        if payment._destroy == 1
          memo
        else
          memo + Number(payment.amount)
      ),
      0
    )

    @_scope.monthlyPaymentsDistributionIsValid =
      paymentsTotal == Number(@_scope.deal.total_sum)

    for payment in @_scope.deal.monthly_payments
      payment.error = {}
      payment.amount = Number(payment.amount)

      amountIsValid = payment.amount or payment.amount == 0
      # isNaN is a check for invalid date
      dateIsValid = payment.date and !isNaN(payment.date)

      if not amountIsValid
        paymentsAreValid = false
        payment.error.amount = ['Укажите сумму']

      if not dateIsValid
        paymentsAreValid = false
        payment.error.date = ['Укажите дату']

    @_scope.monthlyPaymentsDistributionIsValid and paymentsAreValid

  _setValidationError: (attribute, errors) =>
    @_scope.DealForm.$setError(attribute, '')
    @_scope.DealForm.$setError(attribute, errors.join('; '))

  _setValidationErrors: (errors) =>
    for attr, error of errors
      @_setValidationError(attr, error)

  _submit: (data) =>
    defer = @_q.defer()

    unless @_scope.validateMonthlyPayments()
      defer.resolve('error')
      return defer.promise

    dealData = {}
    _.extend(dealData, @_scope.deal)
    _.extend(dealData, data)

    unless dealData.payment_method == 'credit'
      dealData.interest_rate = null

    if dealData.payment_period == 'single'
      dealData.payment_method = null

    if dealData.contract_ended_at
      dealData.contract_ended_at.setMinutes(
        dealData.contract_ended_at.getMinutes() -
        dealData.contract_ended_at.getTimezoneOffset()
      )

    if dealData.contract_started_at
      dealData.contract_started_at.setMinutes(
        dealData.contract_started_at.getMinutes() -
        dealData.contract_started_at.getTimezoneOffset()
      )

    dealData.deal_monthly_payments_attributes = @_scope.deal.monthly_payments

    @_save(dealData).then((response) =>
      defer.resolve(response)
      @_rootScope.$emit('company:reload')

      currentActiveSection = @_scope.section.active
      @_scope.section.active = null
      @_timeout((() => @_scope.section.active = currentActiveSection), 0)
    )
    return defer.promise


  _save: (dealData) =>
    defer = @_q.defer()

    @_companyResource
    .include('monthly_payments', 'receipts')
    .put('deals', @_scope.deal.id, {deal: dealData})
    .then((response) =>
      _.extend(@_scope.deal, response.data)

      for payment in @_scope.deal.monthly_payments
        payment.date = moment(payment.date).toDate()

      @_scope.deal.payments_count = @_scope.deal.monthly_payments.length
      defer.resolve(response)
    )
    .catch((response) =>
      @_setValidationErrors(response.data.errors)
      defer.resolve('error')
    )

    return defer.promise

  _setStatus: (statusId) =>
    return if @_scope.deal.status == statusId

    url = '/api/companies/' + @_companyId +
      '/deals/' + @_scope.deal.id + '/set_status?include=receipts'

    @_http
    .put(url, {status: statusId})
    .then(
      ((response) =>
        _.extend(@_scope.deal, response.data)
        @_rootScope.$emit('company:reload')

        if statusId == 'payed' || statusId == 'returned'
          @_rootScope.$emit('milestone:reload')
      ),
      ((response) =>
        @_setValidationErrors(response.data.errors)
        defer.resolve('error')
      )
    )

  _addDealProduct: () =>
    @_ModalService.showModal({
      templateUrl: "/assets/views/companies/deals/new_deal_product.html",
      controller: "CompaniesDealsNewDealProductCtrl",
      inputs: {
        companyId: @_companyId,
        companyCurrency: @_scope.$parent.$parent.company.currency,
        entityId: @_scope.deal.id,
        entityName: 'deal',
        product: null,
      },
    })
    .then((modal) =>
      modal.element.modal()
      modal.close.then((result) =>
        @_scope.deal.deal_products.push(result)
        @_recalculate()
      )
    )

  _recalculate:  () =>
    @_dealCalculator.calculate(
      @_scope.deal,
      @_scope.deal.deal_products,
      @_scope.$parent.$parent.company.vat,
      @_scope.deal.discount
    )
    @_dealCalculator.saveCalculation(@_scope.deal)

  _updatePhoneCodeByCountry: (countryCode) =>
    code = _.findWhere(
      @_scope.dealStatic.references.country_phone_codes,
      {country_code: countryCode},
    )
    @_scope.deal.phone_code_id = code?.id

  _initMonthlyPayments: () =>
    @_scope.deal.monthly_payments ?= []

    count = @_scope.deal.payments_count
    monthlyPaymentAmount = @_scope.deal.total_sum / count

    if monthlyPaymentAmount % 1 == 0
      @_scope.monthlyPaymentsDistributionIsValid = true
    else
      @_scope.monthlyPaymentsDistributionIsValid = false
      monthlyPaymentAmount = 0

    length = @_scope.deal.monthly_payments.length

    for payment in @_scope.deal.monthly_payments
      payment._destroy = undefined

    if count > length
      for i in [0...count]
        if i < length
          @_scope.deal.monthly_payments[i].amount = monthlyPaymentAmount
        else
          @_scope.deal.monthly_payments[i] = {
            amount: monthlyPaymentAmount,
          }
    else
      for payment in @_scope.deal.monthly_payments[-(length - count)..]
        payment._destroy = 1

  _loadTasks: () =>
    return unless @_scope.deal.client_id

    url = "/api/tasks?client_id=#{@_scope.deal.client_id}&expand=references"

    @_http.get(url).then((response) =>
      @_scope.taskStatic = {references: response.data.references}
      @_scope.deal.related_tasks = response.data.entities
      @_scope.deal.related_tasks_count = @_scope.deal.related_tasks.length
    )

  _confirmMonthlyPayment: (index) =>
    payment = @_scope.deal.monthly_payments[index]

    return unless payment

    path =
      "deals/#{payment.deal_id}/monthly_payments/#{payment.id}/confirm"

    @_companyResource
    .post(path)
    .then((response) =>
      _.extend(payment, response.data)

      @_rootScope.$emit('milestone:reload')
      if response.data.deal_status
        @_scope.deal.status = response.data.deal_status
    )

  _discardMonthlyPayment: (index) =>
    payment = @_scope.deal.monthly_payments[index]

    return unless payment

    path =
      "deals/#{payment.deal_id}/monthly_payments/#{payment.id}/discard"

    @_companyResource
    .post(path)
    .then((response) =>
      _.extend(payment, response.data)
      @_rootScope.$emit('milestone:reload')
    )

  _chooseCashFlowBudgetCategory: () =>
    @_ModalService.showModal({
      templateUrl: '/assets/views/companies/cash_flow_budget_categories/choose.html',
      controller: 'CompaniesCashFlowBudgetCategoriesChooseCtrl',
      inputs: {
        categories: JSON.stringify(@_scope.cash_flow_budget_categories),
      },
    })
    .then((modal) =>
      modal.element.modal()
      modal.close.then((result) =>
        @_scope.deal.cash_flow_budget_category_id = result.category.id
        @_scope.deal.cash_flow_budget_category_name = result.path
      )
    )

  _isContractActive: () =>
    moment(@_scope.deal.contract_started_at).isSameOrBefore({}, 'day') &&
    moment(@_scope.deal.contract_ended_at).isSameOrAfter({}, 'day')

  _removeDealProduct: (dealProduct) =>
    return unless confirm(
      'Вы действительно хотите удалить этот товар из сделки?'
    )

    @_companyResource
    .delete("deals/#{dealProduct.deal_id}/deal_products", dealProduct.id)
    .then(() =>
      @_scope.deal.deal_products = _.filter(
        @_scope.deal.deal_products,
        ((deal_product) ->
          return deal_product.id != dealProduct.id
        )
      )
      @_recalculate()
    )

  _updateDealProductCount: (dealProduct, newCount) =>
    data = angular.copy(dealProduct)
    data.count = newCount

    @_companyResource
    .put("deals/#{dealProduct.deal_id}/deal_products", dealProduct.id,  data)
    .then((response) =>
      _.extend(dealProduct, response.data)
      @_recalculate()
    )

  _dealProductsAreEditable: () =>
    @_scope.deal.status not in ['lost', 'payed', 'returned']

  _showReceipt: (content) =>
    @_ModalService.showModal({
      templateUrl: '/assets/views/companies/deals/receipt.html',
      controller: 'CompaniesDealsShowReceiptCtrl',
      inputs: { content: content },
    })
    .then((modal) ->
      modal.element.modal()
    )
