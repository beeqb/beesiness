class App.CompaniesDealsIndexCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$location',
    '$routeParams',
    '$anchorScroll',
    'ModalService',
    'References',
    '$timeout',
    'ResourceLoader',
    'Page',
  ]

  app.controller('CompaniesDealsIndexCtrl', @factory())

  initialize: () ->
    @_References.sync()

    @_scope.companyId = +@_routeParams.company_id

    @_companyResource = new @_ResourceLoader(@_scope.companyId)

    @_scope.company = {}
    @_scope.companyStatic = {}
    @_scope.cachedDeals = {}
    @_scope.deals = []
    @_scope.dealStatic = {}
    @_scope.employees = []
    @_scope.filter = {
      statuses: [],
      clearFilterField: @_clearFilterField,
      rerenderSlider: @_rerenderSlider,
      toggleDatepickers: @_toggleDatepickers,
      startRange: {
        min: null,
        max: null
      },
      conclusionRange: {
        min: null,
        max: null
      },
      closeRange: {
        min: null,
        max: null
      },
      sumRange: {
        min: 0,
        max: 100000000,
      },
      sliderRange: {
        min: 0,
        max: 100000000,
      },
      search: {},
    }

    @_scope.toggleStatus = @_toggleStatus
    @_scope.createStatus = @_createStatus
    @_scope.editStatus = @_editStatus

    @_scope.page = 1
    @_scope.per = 0
    @_scope.total_count = 0

    @_loadCompany()
    @_loadCashFlowBudgetCategories()
    @_loadEmployees()

    @_loadDeals(@_location.search()?.page || @_scope.page)

    @_scope.filterDeals = @_filterDeals

    @_scope.$watch('page', (newValue, oldValue) =>
      return if newValue == oldValue

      if @_scope.cachedDeals[@_scope.page]
        @_scope.deals = @_scope.cachedDeals[@_scope.page]
        return

      @_loadDeals(@_scope.page)
    )

    @_rootScope.$on('company:reload', @_loadCompany)

  _loadCompany: () =>
    resource = new @_ResourceLoader()

    resource
    .include(
      'deals_count_by_statuses',
      'deal_statuses',
      'cash_flow_budget_categories'
    )
    .expand('references')
    .get('companies', @_scope.companyId)
    .then((response) =>
      @_scope.companyStatic = {
        labels: response.data.labels,
        references: response.data.references,
      }
      @_scope.companyStatic.references.deal_statuses =
        response.data.entity.deal_statuses

      @_scope.companyStatic.references.cash_flow_budget_categories =
        response.data.entity.cash_flow_budget_categories

      delete response.data.entity.deal_statuses
      delete response.data.entity.cash_flow_budget_categories

      @_scope.company = response.data.entity
      @_Page.setTitle('deals', @_scope.company.name)
    )

  _resetCache: () =>
    @_scope.cachedDeals = {}

  _toggleStatus: (status) =>
    if @_scope.filter.statuses.indexOf(status) == -1
      @_scope.filter.statuses.push(status)
    else
      @_scope.filter.statuses = _.without(@_scope.filter.statuses, status)

    @_scope.page = 1
    @_resetCache()
    @_loadDeals(@_scope.page)

  _filterParams: () =>
    filter = @_scope.filter

    dateRanges = {}

    for rangeType in ['start', 'close', 'conclusion']
      filterObj = filter[rangeType + 'Range']
      dateRanges[rangeType] = {}

      if filterObj.min
        dateRanges[rangeType].min =
          moment(filterObj.min).format('YYYY-MM-DD')

      if filterObj.max
        dateRanges[rangeType].max =
          moment(filterObj.max).format('YYYY-MM-DD')

    {
      by_client_name: filter.search.client_name,
      'by_contract_conclusion_date[min]': dateRanges.conclusion?.min,
      'by_contract_conclusion_date[max]': dateRanges.conclusion?.max,
      'by_started_at[min]': dateRanges.start?.min,
      'by_started_at[max]': dateRanges.start?.max,
      'by_closed_at[min]': dateRanges.close?.min,
      'by_closed_at[max]': dateRanges.close?.max,
      'by_total_sum[min]': filter.sumRange.min,
      'by_total_sum[max]': filter.sumRange.max,
      'by_manager': filter.search.manager_id,
    }

  _loadDeals: (page) =>
    @_companyResource
    .expandLabelsAndReferences()
    .include(
      'deal_products',
      'monthly_payments',
      'client',
      'client.rf_status',
      'client.rm_status',
      'history_length',
      'related_tasks_count',
      'receipts'
    )
    .params(_.extend({
      page: page,
      'status[]': @_scope.filter.statuses,
    }, @_filterParams()))
    .query('deals')
    .then((response) =>
      @_scope.dealStatic = {
        labels: response.data.labels
        references: response.data.references
      }
      @_scope.deals = response.data.entities
      @_scope.per = response.data.per
      @_scope.total_count = response.data.total_count
      @_scope.page = response.data.page

      for deal in @_scope.deals
        deal.total_sum = Number(deal.total_sum)
        deal.monthly_payments ?= []
        deal.payments_count = deal.monthly_payments.length

      @_scope.cachedDeals[@_scope.page] = @_scope.deals
    )

  _loadEmployees: () =>
    @_companyResource
    .include('user')
    .query('employees')
    .then((response) =>
      @_scope.employees =
        _.chain(response.data.entities)
          .where({dismissed: false})
          .map((employee) =>
            user = employee.user
            employee.name = user.name + ' ' + user.surname
            return employee
          )
          .value()
    )

  _toggleDatepickers: (range) =>
    states = [
      'openStartRangeFrom', 'openStartRangeTo',
      'openConclusionRangeFrom', 'openConclusionRangeTo',
      'openCloseRangeFrom', 'openCloseRangeTo'
    ]

    @_scope[range] = !@_scope[range]
    _.chain(states).without(range).each((r) =>
      @_scope[r] = false
    )

  _rerenderSlider: () =>
    angular.element(window).triggerHandler('resize')

  _clearFilterField: (field) =>
    if field == 'sumRange'
      @_scope.filter.sumRange = @_scope.filter.sliderRange
    else if field.endsWith('Range')
      @_scope.filter[field]['min'] = @_scope.filter[field]['max'] = null
    else
      @_scope.filter.search[field] = undefined if @_scope.filter.search[field]?

    @_filterDeals()

  _createStatus: () =>
    @_ModalService.showModal({
      templateUrl: "/assets/views/companies/generic_statuses/new.html",
      controller: "CompaniesGenericStatusesNewCtrl",
      inputs: {
        companyId: @_routeParams.company_id,
        entityName: 'deal_status',
      },
    })
    .then((modal) =>
      modal.element.modal()
      modal.close.then((result) =>
        @_companyResource
        .post('deal_statuses', {deal_status: result})
        .then((response) =>
          @_scope.companyStatic.references.deal_statuses.push(response.data)
        )
      )
    )

  _editStatus: (statusId) =>
    @_ModalService.showModal({
      templateUrl: "/assets/views/companies/generic_statuses/new.html",
      controller: 'CompaniesGenericStatusesEditCtrl',
      inputs: {
        companyId: @_routeParams.company_id,
        statusId: statusId,
        entityName: 'deal_status',
        existingStatuses: JSON.stringify(
          @_scope.companyStatic.references.deal_statuses
        ),
      },
    })
    .then((modal) =>
      modal.element.modal()
      modal.close.then((result) =>
        if result.delete
          @_deleteStatus(statusId, result)
        else
          @_updateStatus(statusId, result)
      )
    )

  _updateStatus: (statusId, data) =>
    @_companyResource
    .put('deal_statuses', statusId, {deal_status: data})
    .then((response) =>
      statuses = @_scope.companyStatic.references.deal_statuses

      statusIndex = _.findIndex(statuses, {id: response.data.id})
      statuses.splice(statusIndex, 1, response.data)
    )

  _deleteStatus: (statusId, data) =>
    @_companyResource
    .post(
      "deal_statuses/#{statusId}/delete_safely",
      {status_after_delete: data.status_after_delete}
    )
    .then(() =>
      statuses = @_scope.companyStatic.references.deal_statuses

      @_scope.companyStatic.references.deal_statuses = _.filter(
        statuses,
        (status) -> status.id != statusId
      )
    )

  _filterDeals: () =>
    @_scope.page = 1
    @_loadDeals(@_scope.page)

  _loadCashFlowBudgetCategories: () =>
    @_companyResource.query('cash_flow_budget_categories').then((response) =>
      @_scope.cash_flow_budget_categories = {
        all: response.data.income.concat(response.data.outcome),
        income: response.data.income,
        outcome: response.data.outcome,
      }
    )
