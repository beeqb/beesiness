class App.CompaniesDealsNewDealProductCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$element',
    'close',
    'companyId',
    'companyCurrency',
    'entityName',
    'entityId',
    'product',
    'toaster',
  ]

  app.controller('CompaniesDealsNewDealProductCtrl', @factory())

  initialize: () ->
    @_scope.companyId = @_companyId
    @_scope.companyCurrency = @_companyCurrency
    @_scope.product = JSON.parse(@_product)
    @_scope.submit = @_submitModal

  _submitModal: (entity_product) =>
    productAttribute = "#{@_entityName}_product"
    product = entity_product.product

    entity_product.product_id = product.id
    productQuantity = Number(product.quantity)

    if productQuantity && entity_product.count > productQuantity
      entity_product.count = productQuantity
      @_toaster.pop({
        type: 'warning',
        body: "Товара «#{product.name}» осталось только #{product.quantity} штук",
      })
      return false

    if @_entityId == null
      @_element.modal('hide')
      @_close(entity_product)
      return

    idAttribute = "#{@_entityName}_id"
    entity_product[idAttribute] = @_entityId

    url = [
      "/api/companies/#{@_companyId}",
      "#{@_entityName}s/#{@_entityId}",
      "#{productAttribute}s"
    ].join('/')

    data = {}
    data[productAttribute] = entity_product

    @_http
    .post(url, data)
    .then((response) =>
      @_element.modal('hide')
      @_close(response.data)
    )
