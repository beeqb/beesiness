class App.CompaniesDealsShowReceiptCtrl extends IdFly.AngularClass
  @_import: [
    '$scope',
    '$element',
    'close',
    'content',
    'printHtml',
  ]

  app.controller('CompaniesDealsShowReceiptCtrl', @factory())

  initialize: () ->
    @_scope.content = @_content

    @_scope.print = @_printHtml
