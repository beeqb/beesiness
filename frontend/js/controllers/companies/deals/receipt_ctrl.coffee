class App.CompaniesDealsReceiptCtrl extends IdFly.AngularClass
  @_import: [
    '$rootScope',
    '$scope',
    '$routeParams',
    'ResourceLoader',
  ]

  app.controller('CompaniesDealsReceiptCtrl', @factory())

  initialize: () ->
    companyId = @_routeParams.company_id
    dealId = @_routeParams.deal_id
    companyResource = new @_ResourceLoader(companyId)

    companyResource
    .query("deals/#{dealId}/receipt")
    .then((response) =>
      @_scope.content = response.data.content
    )
