class App.CompaniesSuppliersIndexCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$routeParams',
    'ModalService',
    'References',
    'ResourceLoader',
    'buildFullPhone',
    'Page',
  ]

  app.controller('CompaniesSuppliersIndexCtrl', @factory())

  initialize: () ->
    @_References.sync()

    @_scope.companyId = +@_routeParams.company_id
    @_companyResource = new @_ResourceLoader(@_scope.companyId)

    @_scope.company = {}
    @_scope.suppliers = []
    @_scope.cachedSuppliers = {}
    @_scope.employees = []
    @_scope.filter = {
      statuses: [],
    }
    @_scope.toggleStatus = @_toggleStatus
    @_scope.createStatus = @_createStatus
    @_scope.editStatus = @_editStatus

    @_scope.$watch('page', (newValue, oldValue) =>
      return if newValue == oldValue

      if @_scope.cachedSuppliers[@_scope.page]
        @_scope.suppliers = @_scope.cachedSuppliers[@_scope.page]
        return

      @_loadSuppliers(@_scope.page)
    )

    @_scope.page = 1
    @_scope.per = 0
    @_scope.total_count = 0

    @_loadCompany()
    @_loadSuppliers(@_location.search()?.page || @_scope.page)
    @_loadEmployees()
    @_rootScope.$on('company:reload', @_loadCompany)

  _loadCompany: () =>
    resource = new @_ResourceLoader()
    resource
    .include('suppliers_count_by_statuses', 'supplier_statuses')
    .expand('references')
    .get('companies', @_scope.companyId)
    .then((response) =>
      @_scope.companyStatic = {references: response.data.references}
      @_scope.companyStatic.references.supplier_statuses =
        response.data.entity.supplier_statuses

      delete response.data.entity.supplier_statuses

      @_scope.company = response.data.entity
      @_Page.setTitle('suppliers', @_scope.company.name)
    )

  _toggleStatus: (status) =>
    if @_scope.filter.statuses.indexOf(status) == -1
      @_scope.filter.statuses.push(status)
    else
      @_scope.filter.statuses = _.without(@_scope.filter.statuses, status)

    @_scope.page = 1
    @_resetCache()
    @_loadSuppliers(@_scop.page)

  _resetCache: () =>
    @_scope.cachedSuppliers = {}

  _loadSuppliers: (page) =>
    @_companyResource
    .expandReferencesAndLabels()
    .include(
      'history_length',
      'deal_events',
      'deals_stats',
      'related_tasks_count',
      'current_month_income',
      'current_month_income_by_days',
      'sales_dynamic'
    )
    .query('suppliers', {
      'status[]': @_scope.filter.statuses,
      page: page,
    })
    .then((response) =>
      @_scope.supplierStatic = {
        labels: response.data.labels,
        references: response.data.references,
      }

      @_scope.suppliers = response.data.entities
      @_scope.per = response.data.per
      @_scope.total_count = response.data.total_count
      @_scope.page = response.data.page

      for supplier in @_scope.suppliers
        for index, value of supplier.sales_dynamic.sum
          supplier.sales_dynamic.sum[index][1] = parseFloat(value[1])

      @_scope.cachedSuppliers[page] = @_scope.suppliers
    )

  _loadEmployees: () =>
    @_companyResource
    .include('user')
    .query('employees')
    .then((response) =>
      @_scope.employees = _.where(response.data.entities, {dismissed: false})
    )

  _createStatus: () =>
    @_ModalService.showModal({
      templateUrl: "/assets/views/companies/generic_statuses/new.html",
      controller: "CompaniesGenericStatusesNewCtrl",
      inputs: {
        companyId: @_routeParams.company_id,
        entityName: 'supplier_status',
      },
    })
    .then((modal) =>
      modal.element.modal()
      modal.close.then((result) =>
        @_companyResource
        .post('supplier_statuses', {supplier_status: result})
        .then((response) =>
          @_scope.companyStatic.references.supplier_statuses.push(response.data)
        )
      )
    )

  _editStatus: (statusId) =>
    @_ModalService.showModal({
      templateUrl: "/assets/views/companies/generic_statuses/new.html",
      controller: 'CompaniesGenericStatusesEditCtrl',
      inputs: {
        companyId: @_routeParams.company_id,
        statusId: statusId,
        entityName: 'supplier_status',
        existingStatuses: JSON.stringify(
          @_scope.companyStatic.references.supplier_statuses
        ),
      },
    })
    .then((modal) =>
      modal.element.modal()
      modal.close.then((result) =>
        if result.delete
          @_deleteStatus(statusId, result)
        else
          @_updateStatus(statusId, result)
      )
    )

  _updateStatus: (statusId, data) =>
    @_companyResource
    .put('supplier_statuses', statusId, {supplier_status: data})
    .then((response) =>
      statuses = @_scope.companyStatic.references.supplier_statuses

      statusIndex = _.findIndex(statuses, {id: response.data.id})
      statuses.splice(statusIndex, 1, response.data)
    )

  _deleteStatus: (statusId, data) =>
    @_companyResource
    .post(
      "supplier_statuses/#{statusId}/delete_safely",
      {status_after_delete: data.status_after_delete}
    )
    .then(() =>
      statuses = @_scope.companyStatic.references.supplier_statuses

      @_scope.companyStatic.references.supplier_statuses = _.filter(
        statuses,
        (status) -> status.id != statusId
      )
    )
