class App.CompaniesSuppliersXEditCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$routeParams',
    '$auth',
    '$cookies',
    '$timeout',
    '$q',
    'Upload',
    'ModalService',
    'mapConfig',
    'ResourceLoader',
    'buildFullPhone',
  ]

  app.controller('CompaniesSuppliersXEditCtrl', @factory())

  initialize: () ->
    @_companyResource = new @_ResourceLoader(@_routeParams.company_id)
    if @_routeParams.supplier_id
      @_scope.supplier = {}
      @_scope.supplierStatic = {}
      @_loadCompany()
      @_companyResource
      .include(
        'history_length',
        'deal_events',
        'deals_stats',
        'related_tasks_count'
      )
      .expandLabelsAndReferences()
      .get('suppliers', @_routeParams.supplier_id)
      .then((response) =>
        @_scope.supplier = response.data.entity
        @_scope.supplierStatic = {
          labels: response.data.labels,
          references: response.data.references,
        }
  
        @_scope.supplier.expanded = true
        @_scope.supplier.canBeExpanded = false
      )
    else
      @_scope.supplier.canBeExpanded = true
      @_scope.supplier.expanded = false
      
    @_scope.supplier.errors = {}
    @_scope.expand = @_expand

    @_scope.map = @_mapConfig
    @_scope.updatePhoneCodeByCountry = @_updatePhoneCodeByCountry
    @_scope.submit = @_submit
    @_scope.validate = @_validate
    @_scope.delete = @_delete
    @_scope.employeesForSecondManager = []
    @_scope.section = {active: 'deals'}

    @_scope.loadTasks = @_loadTasks

    @_initValidation()
    @_initWatchers()

  _loadCompany: () =>
    @_companyResource
    .include('suppliers_count_by_statuses')
    .expand('references')
    .query('')
    .then((response) =>
      @_scope.companyStatic = {
        references: response.data.references
      }
      @_scope.company = response.data.entity
    )

  _initWatchers: () =>
    @_scope.$watchGroup(['employees', 'supplier.manager_id'], () =>
      return unless @_scope.employees and  @_scope.supplier

      @_scope.employeesForSecondManager = _.filter(@_scope.employees, (item) =>
        return item.id != @_scope.supplier.manager_id
      )
    )

  _initValidation: () =>
    @_validationTimeout = 500 #msec
    @_scope.$watchCollection('supplier', (newCollection, oldCollection) =>
      attr = null
      for k,v of newCollection
        if v != oldCollection[k]
          attr = k
          break

      if !attr || attr == 'errors'
        return

      @_validate(attr)
    )

  _validate: (attribute, value) =>
    if !@_scope.SupplierForm || !@_scope.SupplierForm.$visible
      return

    if @_validateTimeoutPromise
      @_timeout.cancel(@_validateTimeoutPromise)

    @_validateTimeoutPromise = @_timeout(
      (() =>
        data = {}
        _.extend(data, @_scope.supplier)
        data[attribute] = value

        url = '/api/companies/' + @_routeParams.company_id + '/suppliers/validate'
        if @_scope.supplier.id
          url += '?id=' + @_scope.supplier.id

        @_http
        .post(url, {supplier: data})
        .then(
          (() =>
            @_setValidationError(attribute, [])
          ),
          ((response) =>
            errors = []
            for attr, error of response.data.errors
              if attr == attribute
                errors.push(error)

            @_setValidationError(attribute, errors)
          )
        )
      ),
      @_validationTimeout
    )

  _setValidationError: (attribute, errors) =>
    if !@_scope.SupplierForm
      return
    @_scope.SupplierForm.$setError(attribute, '')
    @_scope.SupplierForm.$setError(attribute, errors.join('; '))

  _setValidationErrors: (errors) =>
    for attr, error of errors
      @_setValidationError(attr, error)

  _submit: (data) =>
    defer = @_q.defer()

    supplierData = {}
    _.extend(supplierData, @_scope.supplier)
    _.extend(supplierData, data)
    supplierData.birthday = moment(supplierData.birthday).format('YYYY-MM-DD')

    @_save(supplierData).then((response) =>
      defer.resolve(response)
      @_rootScope.$emit('company:reload')

      currentActiveSection = @_scope.section.active
      @_scope.section.active = null
      @_timeout((() => @_scope.section.active = currentActiveSection), 0)
    )
    return defer.promise

  _save: (supplierData) =>
    defer = @_q.defer()

    @companyResource
    .include('history_length')
    .put('suppliers', @_scope.supplier.id, {supplier: supplierData})
    .then((response) =>
      _.extend(@_scope.supplier, response.data)

      if !@_scope.supplier.new_images || @_scope.supplier.new_images.length == 0
        defer.resolve(response)
        return

      if !(@_scope.supplier.new_images[0] instanceof File)
        defer.resolve(response)
        return

      @_uploadImages(response.data.id, @_scope.supplier.new_images)
      .then(
        ((response) =>
          @_scope.supplier.new_images = []
          @_scope.supplier.images = response.data.images
          defer.resolve(response)
        ),
        ((response) =>
          @_setValidationErrors(response.data.errors)
          defer.resolve('error')
        )
      )
    )
    .catch((response) =>
      @_setValidationErrors(response.data.errors)
      defer.resolve('error')
    )

    return defer.promise

  _uploadImages: (supplierId, images) =>
    url = '/api/companies/' + @_routeParams.company_id +
      '/suppliers/' + supplierId + '/upload_images'

    @_Upload.upload({
      url: url,
      method: 'PUT',
      headers: @_auth.retrieveData('auth_headers'),
      data: {
        images: images
      },
    })

  _delete: () =>
    if !confirm('Вы уверены?')
      return

    url = '/api/companies/' + @_routeParams.company_id +
      '/suppliers/' + @_scope.supplier.id
    @_http
    .delete(url)
    .then(() =>
      @_deleteFromList(@_scope.supplier)
    )

  _deleteFromList: (supplierForRemove) =>
    @_scope.$parent.$parent.suppliers = _.filter(
      @_scope.$parent.$parent.suppliers,
      ((supplier) =>
        return supplier.id != supplierForRemove.id
      )
    )

  _updatePhoneCodeByCountry: (countryCode) =>
    code = _.findWhere(
      @_scope.supplierStatic.references.country_phone_codes,
      {country_code: countryCode},
    )
    @_scope.supplier.work_phone_code_id = code?.id
    @_scope.supplier.cell_phone_code_id = code?.id

  _loadTasks: () =>
    url = "/api/tasks?client_id=#{@_scope.supplier.id}"

    @_http.get(url).then((response) =>
      @_scope.supplier.related_tasks = response.data.entities
      @_scope.supplier.related_tasks_count = @_scope.supplier.related_tasks.length
    )
