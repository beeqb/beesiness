class App.CompaniesSuppliersNewCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$routeParams',
    '$auth',
    '$cookies',
    '$timeout',
    '$q',
    'Upload',
    'ModalService',
    'mapConfig',
    'ResourceLoader',
    'buildFullPhone',
  ]

  app.controller('CompaniesSuppliersNewCtrl', @factory())

  initialize: () ->
    @_scope.companyId = @_routeParams.company_id
    @companyResource = new @_ResourceLoader(@_scope.companyId)

    @_scope.loadCompanyData = @_loadCompanyData

    @_scope.supplier = {
      company_id: @_scope.companyId,
      legal_form: 'individual',
      errors: {},
    }
    @_scope.updatePhoneCodeByCountry = @_updatePhoneCodeByCountry
    @_scope.map = @_mapConfig
    @_scope.submit = @_submit
    @_scope.validate = @_submit
    @_load()
    @_initWatchers()

  _clearSupplier: (caller) =>
    fields = [
      'city_id',
      'birthday',
      'birthday_uib',
      'cell_phone',
      'cell_phone_code_id',
      'work_phone',
      'work_phone_code_id',
      'skype',
      'facebook',
      'viber',
      'country_id',
      'current_job',
      'selected_job',
      'current_department',
      'selected_department',
      'current_position',
      'current_is_head',
      'address',
      'latitude',
      'longitude'
    ]

    if caller == 'company'
      fields.push('legal_entity_id')
      fields.push('legal_entity_email')
      fields.push('site')
      fields.push('name')
      fields.push('surname')
      fields.push('email')
    else if caller == 'user'
      fields.push('user_id')
      fields.push('email')

    for field in fields
      @_scope.supplier[field] = null

  _initWatchers: () =>
    @_scope.$watchGroup(['supplier.name', 'supplier.surname'], () =>
      if @_scope.supplier.name && @_scope.supplier.surname
        @_loadSupplierData()
    )

    @_scope.$watch('supplier.selected_job', () =>
      return unless @_scope.supplier.selected_job

      company = _.findWhere(@_scope.references.companies, {
        id: @_scope.supplier.selected_job,
      })

      return unless company

      @_scope.supplier.current_job = company.name
      @_scope.supplier.current_department = ''
      @_scope.supplier.current_position = ''
      @_scope.supplier.current_is_head = 0

      @companyResource
      .include('employees_departments', 'employees_departments.department')
      .get("companies/#{company.id}/employees", company.employee_id)
      .then((response) =>
        @_scope.references.departments = _.map(
          response.data.entity.employees_departments,
          ((employee_department) =>
            return {
              id: employee_department.department.id,
              name: employee_department.department.name,
              position: employee_department.position,
              is_head: employee_department.is_head,
            }
          )
        )

        if @_scope.references.departments.length > 0
          @_scope.supplier.selected_department =
            @_scope.references.departments[0].id
      )
    )

    @_scope.$watch('supplier.selected_department', () =>
      return unless @_scope.supplier.selected_department

      department = _.findWhere(@_scope.references.departments, {
        id: @_scope.supplier.selected_department
      })

      return unless department

      @_scope.supplier.current_department = department.name
      @_scope.supplier.current_position = department.position
      @_scope.supplier.current_is_head = +department.is_head
    )

  _loadCompanyData: () =>
    return unless @_scope.supplier.legal_form == 'legal_entity'

    search = @_scope.supplier.legal_entity_name

    return if !search || search.length < 3

    if @_loadCompanyDataPromise
      @_timeout.cancel(@_loadCompanyDataPromise)

    @_loadCompanyDataPromise = @_timeout(
      (() =>
        resource = new @_ResourceLoader()

        @_rootScope.showOverlay('Поиск компаний...')
        resource
        .include('director')
        .post('companies/search', { name: search })
        .then((response) =>
          @_scope.supplier.errors = {}
          @_rootScope.hideOverlay()

          return if response.data.length == 0

          @_ModalService.showModal({
            templateUrl: '/assets/views/companies/suppliers/modals/company_search_results.html',
            controller: 'CompaniesClientCompanySearchResultCtrl',
            inputs: {
              companies: JSON.stringify(response.data),
              search: search,
              labels: @_scope.labels,
              countryPhoneCodes: @_scope.references.country_phone_codes,
            },
          })
          .then((modal) =>
            modal.element.modal()
            modal.close.then((company) =>
              @_scope.supplier.legal_entity_id = company.id
              @_scope.supplier.legal_entity_email = company.email
              @_scope.supplier.legal_entity_name = company.name
              @_scope.supplier.address = company.address
              @_scope.supplier.latitude = company.latitude
              @_scope.supplier.longitude = company.longitude
              @_scope.supplier.site = company.site
              @_scope.supplier.work_phone = company.phone
              @_scope.supplier.work_phone_code_id = company.phone_code_id

              return unless company.director

              @_scope.supplier.name = company.director.name
              @_scope.supplier.surname = company.director.surname
              @_scope.supplier.email = company.director.email
            )
          )
        )
        .finally(@_rootScope.hideOverlay)
      ),
      500
    )


  _loadSupplierData: () =>
    return unless @_scope.supplier.legal_form == 'individual'

    if @_loadSupplierDataPromise
      @_timeout.cancel(@_loadSupplierDataPromise)

    @_loadSupplierDataPromise = @_timeout(
      (() =>
        @_rootScope.showOverlay('Поиск пользователей...')

        resource = new @_ResourceLoader()

        resource
        .include('employees')
        .post('users/search', {
          name: @_scope.supplier.name,
          surname: @_scope.supplier.surname,
        })
        .then((response) =>
          @_scope.supplier.errors = {}
          @_rootScope.hideOverlay()

          return if response.data.length == 0

          @_ModalService.showModal({
            templateUrl: "/assets/views/companies/suppliers/modals/user_search_results.html",
            controller: "CompaniesSupplierUserSearchResultCtrl",
            inputs: {
              users: JSON.stringify(response.data),
              labels: @_scope.labels,
              countryPhoneCodes: @_scope.references.country_phone_codes,
            },
          })
          .then((modal) =>
            modal.element.modal()
            modal.close.then((user) =>
              if user
                @_clearSupplier('user')
                @_substituteUser(user)
            )
          )
          return
        )
        .finally(@_rootScope.hideOverlay)
      ),
      500
    )


  _initValidation: () =>
    @_validationTimeout = 500 #msec
    @_scope.$watchCollection('supplier', (newCollection, oldCollection) =>
      attr = null
      for k,v of newCollection
        if v != oldCollection[k]
          attr = k
          break

      if !attr || attr == 'errors'
        return

      @_validate(attr)
    )

  _validate: (attribute) =>
    if @_validateTimeoutPromise
      @_timeout.cancel(@_validateTimeoutPromise)

    @_validateTimeoutPromise = @_timeout(
      (() =>
        url = "/api/companies/#{@_scope.companyId}/suppliers/validate"
        if @_routeParams.id
          url += '?id=' + @_routeParams.id

        @_http
        .post(url, {supplier: @_scope.supplier})
        .then(
          (() =>
            @_scope.supplier.errors = {}
          ),
          ((response) =>
            for attr, error of response.data.errors
              if attr != attribute
                continue
              @_scope.supplier.errors[attribute] = error
          )
        )
      ),
      @_validationTimeout
    )

  _load: () =>
    @companyResource
    .expandReferencesAndLabels()
    .get('suppliers/new')
    .then((response) =>
      @_scope.references = response.data.references
      @_scope.labels = response.data.labels

      _.extend(@_scope.supplier, response.data.entity)

      @_scope.supplier.legal_form = 'individual' #default
      @_initValidation()
    )

  _getManagerId: () =>
    @_scope.user.employeeInCompany(@_scope.companyId)?.id

  _substituteUser: (user) =>
    @_scope.supplier.user_id = user.id
    @_scope.supplier.email = user.email

    if @_scope.supplier.legal_form == 'individual'
      @_scope.supplier.birthday = user.birthday
      @_scope.supplier.cell_phone = user.cell_phone
      @_scope.supplier.cell_phone_code_id = user.cell_phone_code_id
      @_scope.supplier.site = user.site
      @_scope.supplier.skype = user.skype
      @_scope.supplier.facebook = user.facebook_link
      @_scope.supplier.viber = user.viber
      @_scope.supplier.address = user.address
      @_scope.supplier.latitude = user.latitude
      @_scope.supplier.longitude = user.longitude

      @_scope.references['companies'] = _.map(
        user.employees,
        ((employee) =>
          return {
            id: employee.company.id,
            name: employee.company.name,
            employee_id: employee.id,
          }
        )
      )

      if @_scope.references.companies.length > 0
        @_scope.supplier.selected_job = @_scope.references.companies[0].id

  _submit: () =>
    @_scope.supplier.manager_id = @_getManagerId()
    @_scope.supplier.work_phone_full = @_buildFullPhone(
      @_scope.supplier,
      'work_phone',
      @_scope.references.country_phone_codes
    )
    @_scope.supplier.cell_phone_full = @_buildFullPhone(
      @_scope.supplier,
      'cell_phone',
      @_scope.references.country_phone_codes
    )

    @companyResource
    .post('suppliers', {supplier: @_scope.supplier})
    .then((client) =>
      @_rootScope.$emit('user:reloadPermissions')
      @_location.path("/companies/#{@_scope.companyId}/suppliers")
    )
    .catch((response) =>
      _.extend(@_scope.supplier.errors, response.data.errors)
    )

  _updatePhoneCodeByCountry: (countryCode) =>
    return unless @_scope.references

    code = _.findWhere(
      @_scope.references.country_phone_codes,
      {country_code: countryCode},
    )
    @_scope.supplier.work_phone_code_id = code?.id
    @_scope.supplier.cell_phone_code_id = code?.id
