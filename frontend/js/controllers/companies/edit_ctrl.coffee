class App.CompaniesEditCtrl extends App.CompaniesLoadCtrl

  app.controller('CompaniesEditCtrl', @factory())

  initialize: () ->
    super()

    @_scope.submit = @_submit
    @_scope.saveReceiptTemplate = @_saveReceiptTemplate
    @_scope.delete = @_delete
    @_scope.employees = []
    @_scope.company.dirty = false
    @_scope.savingInProgress = false
    @_scope.updatePhoneCodeByCountry = @_updatePhoneCodeByCountry

    @_setupReceiptEditorsToolbars()
    @_loadEmployees()

    @_scope.$on('$locationChangeStart', (e) =>
      unsaved_changes = e.currentScope.company.dirty && !e.currentScope.savingInProgress
      if unsaved_changes && !confirm('Вы уверены, что хотите покинуть страницу? Все несохраненные изменения будут удалены.')
        e.preventDefault()
        return
    )

    @_scope.$watchCollection(
      'company.receipt_template',
      (newValue) =>
        return unless newValue

        @_companyResource
        .post('deals/receipt_preview', {
          templates: @_scope.company.receipt_template,
        })
        .then((response) =>
          @_scope.receiptPreview = response.data.content
        )
    )
    unwatch = @_scope.$watch('company.loaded', (newValue, oldValue) =>
      return if newValue == oldValue

      if newValue
        @_Page.setTitle('companies', @_scope.company.name)
        unwatch()
    )

  _loadEmployees: () =>
    url = '/api/companies/' + @_routeParams.id +
      '/employees?include=user'

    @_http
    .get(url)
    .then((response) =>
      @_scope.employees_users =
        _.map(
          _.pluck(response.data.entities, 'user'),
          ((user) =>
            fullName = user.name + ' ' + user.surname
            return _.extend(user, {name: fullName})
          )
        )
    )

  _saveReceiptTemplate: () =>
    data = {company: { receipt_template: @_scope.company.receipt_template }}
    @_http
    .patch('/api/companies/' + @_scope.company.id, data)
    .then((response) =>
      @_toaster.pop({type: 'success', body: 'Шаблон сохранен'})
    )

  _submit: () =>
    @_scope.savingInProgress = true
    @_http
    .put(
      '/api/companies/' + @_scope.company.id,
      {company: _.omit(@_scope.company, 'receipt_template')}
    )
    .then(
      (() =>
        if !@_scope.company.logo || !(@_scope.company.logo instanceof File)
          @_rootScope.$emit('user:changed')
          @_location.path('/companies/' + @_scope.company.id + '/crm')
          return

        @_Upload.upload({
          url: '/api/companies/' + @_scope.company.id + '/upload_logo',
          method: 'PUT',
          headers: @_auth.retrieveData('auth_headers'),
          data: {
            file: @_scope.company.logo,
          },
        })
        .then(
          ((response) =>
            @_scope.savingInProgress = false
            @_scope.company.dirty = false
            @_rootScope.$emit('user:changed')
            @_location.path('/companies/' + response.data.id + '/crm')
          ),
          ((response) =>
            @_scope.savingInProgress = false
            _.extend(@_scope.company.errors, response.data.errors)
          ),
          ((e) =>
            @_scope.progressPercentage = parseInt(100.0 * e.loaded / e.total);
          )
        )
      ),
      ((response) =>
        @_scope.savingInProgress = false
        _.extend(@_scope.company.errors, response.data.errors)
      )
    )

  _delete: () =>
    if !confirm("Вы уверены, что хотите удалить компанию?")
      return

    @_http
    .delete('/api/companies/' + @_scope.company.id)
    .then(() =>
      @_rootScope.$emit('user:changed')
      @_location.path('/')
    )

  _updatePhoneCodeByCountry: (countryCode) =>
    code = _.findWhere(
      @_scope.companyStatic.references.country_phone_codes,
      {country_code: countryCode},
    )
    @_scope.company.phone_code_id = code?.id

  _setupReceiptEditorsToolbars: () =>
    commonTools = [
      'bold',
      'italics',
      'justifyLeft',
      'justifyRight',
      'justifyCenter',
      'floatLeft',
      'floatRight',
    ]

    nonProductRelatedTools = [
      'companyName',
      'companyInn',
      'companyAddress',
      'companyVat',
      'paymentOption',
      'managerFullname',
      'payedAt',
      'paidAmount',
      'change',
      'cashboxName',
      'receiptKind',
      'discountSum',
      'vatSum',
      'totalSum',
    ]

    @_scope.receiptHeaderToolbar = [commonTools, nonProductRelatedTools]
    @_scope.receiptFooterToolbar = [commonTools, nonProductRelatedTools]
    @_scope.receiptItemToolbar = [
      commonTools,
      ['productName', 'productQuantity', 'productPrice'],
    ]
