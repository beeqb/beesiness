class App.CompaniesViewCtrl extends App.CompaniesLoadCtrl

  app.controller('CompaniesViewCtrl', @factory())

  initialize: () ->
    @_scope.companyId = +@_routeParams.id
    super()

    unwatch = @_scope.$watch('company.loaded', (newValue, oldValue) =>
      return if newValue == oldValue

      if newValue
        @_Page.setTitle('dashboard', @_scope.company.name)
        unwatch()
    )

