class App.CompaniesNewCtrl extends App.CompaniesLoadCtrl

  app.controller('CompaniesNewCtrl', @factory())

  initialize: () ->
    @_scope.submit = @_submit
    @_scope.updatePhoneCodeByCountry = @_updatePhoneCodeByCountry

    @_Page.setTitle('companies')

    super()

  _load: () =>
    @_http
    .get('/api/companies/new?expand=labels,references')
    .then((response) =>
      @_scope.companyStatic = {
        labels: response.data.labels,
        references: response.data.references,
      }
      _.extend(@_scope.company, response.data.entity)
      @_scope.company.creator_id = @_rootScope.user.id
      @_scope.company.vat = 0
      @_scope.company.inn = null
      @_scope.company.loaded = true
      @_initValidation()
    )

  _submit: () =>
    @_scope.savingInProgress = true
    @_http
    .post('/api/companies', {company: @_scope.company})
    .then(
      ((response) =>
        @_scope.company.id = response.data.id

        @_Upload.upload({
          url: '/api/companies/' + @_scope.company.id + '/upload_logo',
          method: 'PUT',
          headers: @_auth.retrieveData('auth_headers'),
          data: {
            file: @_scope.company.logo,
          },
        })
        .then(
          (() =>
            @_scope.savingInProgress = false
            @_rootScope.$emit('user:changed')
            @_location.path('/companies/' + @_scope.company.id + '/crm')
          ),
          ((response) =>
            @_scope.savingInProgress = false
            _.extend(@_scope.company.errors, response.data.errors)
          ),
          ((e) =>
            @_scope.progressPercentage = parseInt(100.0 * e.loaded / e.total);
          )
        )
      ),
      ((response) =>
        @_scope.savingInProgress = false
        _.extend(@_scope.company.errors, response.data.errors)
      )
    )

  _updatePhoneCodeByCountry: (countryCode) =>
    code = _.findWhere(
      @_scope.companyStatic.references.country_phone_codes,
      {country_code: countryCode},
    )
    @_scope.company.phone_code_id = code?.id
