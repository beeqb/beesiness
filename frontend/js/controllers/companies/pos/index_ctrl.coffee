class App.CompaniesPosIndexCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$routeParams',
    '$timeout',
    '$location',
    'ModalService',
    'dealCalculator',
    'localStorageService',
    'ResourceLoader',
    'Page',
    'toaster',
    'printHtml',
  ]

  app.controller('CompaniesPosIndexCtrl', @factory())

  initialize: () ->
    @_scope.companyId = +@_routeParams.company_id
    
    if @_rootScope.user.employees
      @_scope.userEmployee =
        @_rootScope.user.employeeInCompany(@_scope.companyId)
    else
      unwatch = @_scope.$watch('user.employees', (newVal, oldVal) =>
        return if newVal == oldVal

        @_scope.userEmployee =
          @_rootScope.user.employeeInCompany(@_scope.companyId)

        unwatch()
      )

    @_companyResource = new @_ResourceLoader(@_scope.companyId)

    @_scope.company = {}

    @_scope.searchedClients = []
    @_scope.dealProduct = {}

    @_scope.addProduct = @_addProduct
    @_scope.pay = @_pay

    @_scope.removeLastProduct = @_removeLastProduct
    @_scope.returns = @_returns
    @_scope.selectRecommendedProducts = @_selectRecommendedProducts
    @_scope.selectDealProduct = @_selectDealProduct

    @_scope.recommendedProducts = []

    @_scope.searchClient = @_searchClient
    @_scope.clearClient = @_clearClient

    @_resetForm()

    @_key = 'deal_products_for_pos_form_' + @_scope.companyId
    @_key_client = 'client_for_pos_form_' + @_scope.companyId

    @_scope.client = @_localStorageService.get(@_key_client) || {}

    @_loadPreselectedDealProducts()

    @_loadCompany()
    @_loadDeal()
    @_loadRecommendedProducts()
    @_initWatchers()

  _loadPreselectedDealProducts: () =>
    dealProducts = @_localStorageService.get(@_key)
    return unless dealProducts

    @_scope.deal.deal_products = _(dealProducts).filter(_.property('product'))

  _clearPreselectedDealProducts: () =>
      @_localStorageService.remove(@_key)

  _clearClient: () =>
      @_localStorageService.remove(@_key_client)

  _selectDealProduct: (product) =>
    @_scope.dealProduct = {
      product: product,
      count: 1,
      returned_at: null,
    }

  _selectRecommendedProducts: () =>
    @_ModalService
    .showModal({
      templateUrl: 'assets/views/companies/pos/select_recommended.html',
      controller: 'CompaniesPosSelectRecommendedCtrl',
      inputs: {
        companyId: @_routeParams.company_id,
        recommendedProducts: JSON.stringify(@_scope.recommendedProducts),
      }
    })
    .then((modal) =>
      modal.close.then(@_loadRecommendedProducts)
      modal.element.modal()
    )

  _returns: () =>
    @_ModalService
    .showModal({
      templateUrl: 'assets/views/companies/pos/returns.html',
      controller: 'CompaniesPosReturnsCtrl',
      inputs: {
        company: JSON.stringify(@_scope.company),
      }
    })
    .then((modal) =>
      modal.element.modal()
    )

  _removeLastProduct: () =>
    if @_scope.deal.deal_products.length == 0
      return

    @_scope.deal.deal_products.pop()
    @_localStorageService.set(@_key, @_scope.deal.deal_products)

  _resetForm: () =>
    @_scope.client = {}
    @_scope.deal = {
      company_id: @_routeParams.company_id,
      check_sum: 0,
      discount_sum: 0,
      vat_sum: 0,
      total_sum: 0,
      deal_products: [],
      errors: {},
    }

  _pay: () =>
    @_ModalService
    .showModal({
      templateUrl: 'assets/views/companies/pos/modals/payment.html',
      controller: 'CompaniesPosPaymentCtrl',
      controllerAs: 'payment',
      inputs: {
        sum: @_scope.deal.total_sum,
        currency: @_scope.company.currency,
        paymentOptions: angular.toJson(@_scope.dealStatic.references.payment_options),
      }
    })
    .then((modal) =>
      modal.close.then((data) =>
        @_scope.deal.payment_status = data.status
        @_scope.deal.payment_option = data.option
        @_scope.deal.printReceipt = data.printReceipt
        @_scope.deal.paid_cash_amount = data.depositedAmount

        @_createDeal()
      )
      modal.element.modal()
    )

  _createDeal: () =>
    @_scope.deal.manager_id = @_scope.userEmployee.id
    @_scope.deal.started_at = moment().toDate()

    # default values
    @_scope.deal.visibility = 'all'
    @_scope.deal.legal_form = 'individual'

    switch @_scope.deal.payment_status
      when 'pending' then @_scope.deal.status = 'won'
      when 'paid'
        @_scope.deal.status = 'payed'
        @_scope.deal.payment_period = 'single'

    if @_scope.client.id
      @_scope.deal.client_id = @_scope.client.id
      _.extend(
        @_scope.deal,
        _.pick(@_scope.client, 'email', 'legal_form', 'address', 'discount')
      )
      @_scope.deal.phone = @_scope.client.cell_phone ||
          @_scope.client.work_phone
      @_scope.deal.phone_code_id = @_scope.client.cell_phone_code_id ||
          @_scope.client.work_phone_code_id

    @_scope.deal.selected_products = @_scope.deal.deal_products

    _.each(@_scope.deal.selected_products, (deal_product) ->
      deal_product.product_id = deal_product.product.id
      deal_product.price = deal_product.product.price
    )

    printReceipt = @_scope.deal.printReceipt

    if printReceipt
      @_companyResource.include('receipts')

    @_companyResource
    .post('deals', {deal: @_scope.deal})
    .then((response) =>
      paymentStatus = @_scope.deal.payment_status

      @_resetForm()
      @_clearPreselectedDealProducts()
      @_clearClient()

      deal = response.data

      if paymentStatus == 'pending'
        @_location
        .search('edit', 'true')
        .path("/companies/#{@_scope.companyId}/deals/#{deal.id}")
      else
        @_rootScope.$emit('milestone:reload')
        @_printHtml(deal.receipts[0].content) if deal.receipts
    )
    .catch((response) =>
      _.extend(@_scope.deal.errors, response.data.errors)
    )

  _addProduct: (dealProduct) =>
    @_scope.deal.deal_products.push(dealProduct)

    dealProducts = @_localStorageService.get(@_key)

    dealProducts = [] unless dealProducts
    product = dealProduct.product

    if product.quantity && dealProduct.count > product.quantity
      dealProduct.count = product.quantity
      @_toaster.pop({
        type: 'warning',
        body: "Товара «#{product.name}» осталось только #{product.quantity} штук",
      })

    dealProducts.push(dealProduct)
    @_localStorageService.set(@_key, dealProducts)

  _calculate: () =>
    @_dealCalculator.calculate(
      @_scope.deal,
      @_scope.deal.deal_products,
      @_scope.company.vat,
      @_scope.client?.discount
    )

  _initWatchers: () =>
    @_scope.$watchCollection('deal.deal_products', @_calculate)
    @_scope.$watch('client.id', @_calculate)

  _loadDeal: () =>
    @_companyResource
    .expand('references')
    .query('deals/new')
    .then((response) =>
      @_scope.dealStatic = {references: response.data.references}
    )

  _loadCompany: () =>
    resource = new @_ResourceLoader()

    resource
    .include(
      'clients_count',
      'created_clients_count_by_days',
      'actual_income',
      'actual_income_by_days'
    )
    .get('companies', @_routeParams.company_id)
    .then((response) =>
      @_scope.company = response.data.entity
      @_calculate()
      @_Page.setTitle('pos', @_scope.company.name)
    )

  _loadRecommendedProducts: () =>
    @_companyResource
    .include('category')
    .query('products/recommended')
    .then((response) =>
      @_scope.recommendedProducts = response.data.entities
      _.setAll(
          @_scope.recommendedProducts,
          'quantity',
        (product) -> parseFloat(product.quantity)
      )
    )

  _searchClientPromise: null,

  _searchClient: (search) =>
    if @_searchClientPromise
      @_timeout.cancel(@_searchClientPromise)

    @_searchClientPromise = @_timeout(
      (() =>
        @_scope.client = {}

        @_companyResource
        .post('clients/search', {
          or: {
            name: search,
            surname: search,
            legal_entity_name: search,
          }
        })
        .then((response) =>
          @_scope.searchedClients = response.data

          @_localStorageService.set(@_key_client, @_scope.searchedClients[0])
        )
      ),
      500
    )

  _clearClient: () =>
    @_scope.client = {}
    @_localStorageService.remove(@_key_client)

