class App.CompaniesPosSelectRecommendedCtrl extends IdFly.AngularClass

  @_import: [
    '$scope',
    '$http',
    '$timeout',
    '$element',
    'close',
    'companyId',
    'recommendedProducts',
  ]

  app.controller('CompaniesPosSelectRecommendedCtrl', @factory())

  initialize: () ->
    @_scope._ = _

    @_scope.close = @_close

    @_scope.maxRecommendedProductsCount = 6

    @_scope.recommendedProducts = JSON.parse(@_recommendedProducts)
    @_scope.searchedProducts = []

    @_scope.search = {
      product: '',
    }

    @_scope.addRecommendedProduct = @_addRecommendedProduct
    @_scope.removeRecommendedProduct = @_removeRecommendedProduct

    @_initWatchers()

  _initWatchers: () =>
    @_scope.$watch('search.product', () =>
      if !@_scope.search.product
        return
      @_scope.searchedProducts = []
      @_searchProduct()
    )

  _addRecommendedProduct: (product) =>
    url = '/api/companies/' + @_companyId +
      '/products/' + product.id +
      '/set_recommended'
    @_http
    .post(url, {
      is_recommended: 1,
    })
    .then(() =>
      @_scope.search.product = ''
      @_scope.recommendedProducts.push(product)
    )

  _removeRecommendedProduct: (id) =>
    url = '/api/companies/' + @_companyId +
      '/products/' + id +
      '/set_recommended'
    @_http
    .post(url, {
      is_recommended: 0,
    })
    .then(() =>
      @_scope.recommendedProducts = _.filter(@_scope.recommendedProducts, (product) =>
        return product.id != id
      )
    )

  _searchProductPromise: null,
  _searchProduct: () =>
    if @_searchProductPromise
      @_timeout.cancel(@_searchProductPromise)
    @_searchProductPromise = @_timeout(
      (() =>
        url = '/api/companies/' + @_companyId + '/products/search'
        @_http
        .post(url, {
          or: {
            vendor_code: @_scope.search.product,
            name: @_scope.search.product,
          }
        })
        .then((response) =>
          @_scope.searchedProducts = response.data
        )
      ),
      500
    )