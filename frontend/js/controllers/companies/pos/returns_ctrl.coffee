class App.CompaniesPosReturnsCtrl extends IdFly.AngularClass

  @_import: [
    '$scope',
    '$http',
    '$timeout',
    '$element',
    'close',
    'company',
    'dealCalculator',
    'ResourceLoader',
  ]

  app.controller('CompaniesPosReturnsCtrl', @factory())

  initialize: () ->
    @_scope.company = JSON.parse(@_company)
    @_companyResource = new @_ResourceLoader(@_scope.company.id)

    @_scope.searchedDeals = []
    @_scope.deal = {}
    @_scope.dealProducts = []

    @_scope.search = {
      deal: '',
    }

    @_scope.selectDeal = @_selectDeal
    @_scope.returnDeal = @_returnDeal
    @_scope.returnProduct = @_returnProduct
    @_scope.isReturnCountValid = @_isReturnCountValid

    @_initWatchers()

  _initWatchers: () =>
    @_scope.$watch('search.deal', () =>
      if !@_scope.search.deal
        return
      @_searchDeal()
    )

  _selectDeal: (deal) =>
    @_scope.deal = deal
    @_scope.searchedDeals = []
    @_scope.search.deal = ''
    @_scope.dealProducts = []

    url = '/api/companies/' + @_scope.company.id +
      '/deals/' + @_scope.deal.id +
      '/deal_products'
    @_http
    .get(url)
    .then((response) =>
      @_scope.dealProducts = response.data.entities
      _.setAll(@_scope.dealProducts, 'returnCount', 1)
    )

  _searchDealPromise: null,
  _searchDeal: () =>
    if @_searchDealPromise
      @_timeout.cancel(@_searchDealPromise)
    @_searchDealPromise = @_timeout(
      (() =>
        @_scope.deal = {}
        @_scope.dealProducts = []

        url = '/api/companies/' + @_scope.company.id + '/deals/omni_search'
        @_http
        .post(url, {
          q: @_scope.search.deal,
        })
        .then((response) =>
          @_scope.searchedDeals = response.data
        )
      ),
      500
    )

  _returnProduct: (dealProduct) =>
    @_companyResource
    .post(
      "deals/#{@_scope.deal.id}/deal_products/#{dealProduct.id}/return",
      {count: dealProduct.returnCount}
    )
    .then((response) =>
      _.extend(dealProduct, response.data)

      dealProduct.returnCount = 1

      if _.every(@_scope.dealProducts, _.property('returned_at'))
        @_scope.deal.status = 'returned'

      @_dealCalculator.calculate(
        @_scope.deal,
        @_scope.dealProducts,
        @_scope.company.vat,
        @_scope.deal.discount
      )
      @_dealCalculator.saveCalculation(@_scope.deal)
    )
    .catch((response) => console.log(response))

  _returnDeal: () =>
    url = '/api/companies/' + @_scope.company.id +
      '/deals/' + @_scope.deal.id +
      '/return'
    @_http
    .post(url)
    .then(
      ((response) =>
        @_scope.deal = response.data

        _.each(@_scope.dealProducts, (dealProduct) ->
          dealProduct.returned_at = moment().toDate()
        )

        @_dealCalculator.calculate(
          @_scope.deal,
          @_scope.dealProducts,
          @_scope.company.vat,
          @_scope.deal.discount
        )
        @_dealCalculator.saveCalculation(@_scope.deal)
      ),
      ((response) =>
        console.log(response)
      ),
    )

  _isReturnCountValid: (dealProduct) =>
    return false if dealProduct.returnCount <= 0

    dealProduct.returnCount <= dealProduct.count_minus_returns
