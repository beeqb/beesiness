class App.CompaniesPosPaymentCtrl extends IdFly.AngularClass
  @_import: [
    '$scope',
    '$element',
    'close',
    'sum',
    'currency',
    'paymentOptions',
  ]

  app.controller('CompaniesPosPaymentCtrl', @factory())

  initialize: () ->
    @errors = {}

    @options = angular.fromJson(@_paymentOptions)
    @sum = @_sum
    @currency = @_currency

    @checkAmount = @_checkAmount
    @submit = @_submit

  _submit: () =>
    return unless @_validate()

    @_element.modal('hide')

    @_close({
      status: @status,
      option: @selectedOption,
      printReceipt: @printReceipt,
      depositedAmount: @depositedAmount,
    })

  _validate: () =>
    @errors = {}

    if !@status
      @errors.status = ['обязательно к заполнению']

    if !@selectedOption
      @errors.option = ['обязательно к заполнению']

    @_checkAmount() if @selectedOption is 'cash'

    return not _.any(@errors, (error) -> !_.isEmpty(error))

  _checkAmount: () =>
    if !@depositedAmount || @depositedAmount < @sum
      @errors.depositedAmount = ['внесенной суммы недостаточно']
    else
      @errors.depositedAmount = []
