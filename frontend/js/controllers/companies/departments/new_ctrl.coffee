class App.CompaniesDepartmentsNewCtrl extends App.CompaniesDepartmentsLoadCtrl

  @_import: [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$element',
    'close',
    'companyId',
  ]

  app.controller('CompaniesDepartmentsNewCtrl', @factory())

  initialize: () ->
    super()

  _load: () =>
    url = '/api/companies/' + @_companyId + '/departments/new?expand=labels'
    @_http
      .get(url)
      .then((response) =>
        @_scope.departmentStatic = {
          labels: response.data.labels,
        }
        _.extend(@_scope.department, response.data.entity)
        @_scope.department.company_id = @_companyId
      )