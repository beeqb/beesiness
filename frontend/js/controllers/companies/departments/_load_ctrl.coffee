class App.CompaniesDepartmentsLoadCtrl extends IdFly.AngularClass


  app.controller('CompaniesDepartmentsLoadCtrl', @factory())

  initialize: () ->
    @_scope.department = {
      errors: {},
    }
    @_scope.submit = @_submitModal
    @_scope.colors = [
      '6254b2',
      '23b7e5',
      '27c24c',
      'fad733',
      'f05050',
      '3a3f51',
    ]
    @_load()


  _submitModal: () =>
    url = '/api/companies/' + @_companyId + '/departments/validate'
    @_http
    .post(url, {department: @_scope.department})
    .then(
      (() =>
        @_element.modal('hide');
        @_close(@_scope.department)
      ),
      ((response) =>
        _.extend(@_scope.department.errors, response.data.errors)
      )
    )