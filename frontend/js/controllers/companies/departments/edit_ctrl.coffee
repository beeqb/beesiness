class App.CompaniesDepartmentsEditCtrl extends App.CompaniesDepartmentsLoadCtrl

  @_import: [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$element',
    'close',
    'companyId',
    'departmentId',
  ]

  app.controller('CompaniesDepartmentsEditCtrl', @factory())

  initialize: () ->
    super()
    @_scope.delete = @_delete

  _load: () =>
    url = '/api/companies/' + @_companyId +
      '/departments/' + @_departmentId + '?expand=labels'
    @_http
      .get(url)
      .then((response) =>
        @_scope.departmentStatic = {
          labels: response.data.labels,
        }
        @_scope.department = response.data.entity
      )

  _delete: () =>
    if !confirm('Уверены, что хотите удалить отдел?')
      return

    @_element.modal('hide');

    @_scope.department.delete = true # need for parent EmployeeIndexCtrl)
    @_close(@_scope.department)