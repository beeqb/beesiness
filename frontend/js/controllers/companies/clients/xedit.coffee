class App.CompaniesClientsXEditCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$routeParams',
    '$auth',
    '$cookies',
    '$timeout',
    '$q',
    'Upload',
    'ModalService',
    'mapConfig',
    'ResourceLoader',
  ]

  app.controller('CompaniesClientsXEditCtrl', @factory())

  initialize: () ->
    @_scope.companyId = +@_routeParams.company_id
    @_companyResource = new @_ResourceLoader(@_scope.companyId)

    if @_routeParams.client_id
      @_scope.client = {}
      @_loadCompany()
      @_companyResource
      .include(
        'history_length',
        'deals',
        'related_tasks_count',
        'rf_status',
        'rm_status',
        'income',
        'income_by_days',
        'deals_stats',
        'sales_dynamic',
        'products'
      )
      .expandLabelsAndReferences()
      .get('clients', @_routeParams.client_id)
      .then((response) =>
        @_scope.client = response.data.entity
        @_scope.client.expanded = true
        @_scope.client.canBeExpanded = false

        @_scope.clientStatic = {
          labels: response.data.labels,
          references: response.data.references,
        }

        for index, value of @_scope.client.sales_dynamic.sum
          @_scope.client.sales_dynamic.sum[index][1] = parseFloat(value[1])
      )
    else
      @_scope.client.canBeExpanded = true

    @_scope.client.errors = {}

    @_scope.map = @_mapConfig
    @_scope.updatePhoneCodeByCountry = @_updatePhoneCodeByCountry
    @_scope.submit = @_submit
    @_scope.validate = @_validate
    @_scope.delete = @_delete
    @_scope.employeesForSecondManager = []
    @_scope.section = {active: 'deals'}

    @_scope.loadTasks = @_loadTasks

    @_initValidation()
    @_initWatchers()

  _loadCompany: () =>
    @_companyResource
    .include('clients_count_by_statuses')
    .expand('references')
    .query('')
    .then((response) =>
      @_scope.companyStatic = {
        references: response.data.references
      }
      @_scope.company = response.data.entity
    )

  _initWatchers: () =>
    @_scope.watchedData = {}

    @_scope.$watchGroup(['employees', 'client.manager_id'], () =>
      return if !@_scope.employees || !@_scope.client

      @_scope.employeesForSecondManager = _.filter(@_scope.employees, (item) =>
        return item.id != @_scope.client.manager_id
      )
    )

  _initValidation: () =>
    @_validationTimeout = 500 #msec
    @_scope.$watchCollection('client', (newCollection, oldCollection) =>
      attr = null
      for k,v of newCollection
        if v != oldCollection[k]
          attr = k
          break

      if !attr || attr == 'errors'
        return

      @_validate(attr)
    )

  _validate: (attribute, value) =>
    if !@_scope.ClientForm || !@_scope.ClientForm.$visible
      return

    if @_validateTimeoutPromise
      @_timeout.cancel(@_validateTimeoutPromise)

    @_validateTimeoutPromise = @_timeout(
      (() =>
        data = {}
        _.extend(data, @_scope.client)
        data[attribute] = value

        if @_scope.client.id
          @_companyResource.params({id: @_scope.client.id})

        @_companyResource
        .post('clients/validate', {client: data})
        .then(() =>
          @_setValidationError(attribute, [])
        )
        .catch((response) =>
          errors = []
          for attr, error of response.data.errors
            if attr == attribute
              errors.push(error)

          @_setValidationError(attribute, errors)
        )
      ),
      @_validationTimeout
    )

  _setValidationError: (attribute, errors) =>
    if !@_scope.ClientForm
      return
    @_scope.ClientForm.$setError(attribute, '')
    @_scope.ClientForm.$setError(attribute, errors.join('; '))

  _setValidationErrors: (errors) =>
    for attr, error of errors
      @_setValidationError(attr, error)

  _submit: (data) =>
    defer = @_q.defer()

    clientData = {}
    _.extend(clientData, @_scope.client)
    _.extend(clientData, data)
    clientData.birthday = moment(clientData.birthday).format('YYYY-MM-DD')

    @_save(clientData).then((response) =>
      defer.resolve(response)
      @_rootScope.$emit('company:reload')

      # Перезагружаем клиента на случай, если был изменен депозит.
      # В этом случае нам нужны свежие данные для графика дохода
      @_reloadClient()

      currentActiveSection = @_scope.section.active
      @_scope.section.active = null
      @_timeout((() => @_scope.section.active = currentActiveSection), 0)
    )
    return defer.promise

  _save: (clientData) =>
    defer = @_q.defer()

    @_companyResource
    .include('history_length')
    .put('clients', @_scope.client.id, {client: clientData})
    .then(
      ((response) =>
        _.extend(@_scope.client, response.data)

        if !@_scope.client.new_images || @_scope.client.new_images.length == 0
          defer.resolve(response)
          return

        if !(@_scope.client.new_images[0] instanceof File)
          defer.resolve(response)
          return

        @_uploadImages(response.data.id, @_scope.client.new_images)
        .then(
          ((response) =>
            @_scope.client.new_images = []
            @_scope.client.images = response.data.images
            defer.resolve(response)
          ),
          ((response) =>
            @_setValidationErrors(response.data.errors)
            defer.resolve('error')
          )
        )

      ),
      ((response) =>
        @_setValidationErrors(response.data.errors)
        defer.resolve('error')
      )
    )

    return defer.promise

  _uploadImages: (clientId, images) =>
    url = '/api/companies/' + @_routeParams.company_id +
      '/clients/' + clientId + '/upload_images'

    @_Upload.upload({
      url: url,
      method: 'PUT',
      headers: @_auth.retrieveData('auth_headers'),
      data: {
        images: images
      },
    })

  _delete: () =>
    if !confirm('Вы уверены?')
      return

    @_companyResource
    .delete('clients', @_scope.client.id)
    .then(() =>
      @_deleteFromList(@_scope.client)
    )

  _deleteFromList: (clientForRemove) =>
    @_scope.$parent.$parent.clients = _.filter(
      @_scope.$parent.$parent.clients,
      ((client) =>
        return client.id != clientForRemove.id
      )
    )

  _updatePhoneCodeByCountry: (countryCode) =>
    code = _.findWhere(
      @_scope.clientStatic.references.country_phone_codes,
      {country_code: countryCode},
    )
    @_scope.client.work_phone_code_id = code?.id
    @_scope.client.cell_phone_code_id = code?.id

  _loadTasks: () =>
    url = "/api/tasks?client_id=#{@_scope.client.id}"

    @_http.get(url).then((response) =>
      @_scope.client.related_tasks = response.data.entities
      @_scope.client.related_tasks_count = @_scope.client.related_tasks.length
    )

  _reloadClient: () =>
    @_companyResource
      .include(
        'income',
        'income_by_days'
      )
      .get('clients', @_scope.client.id)
      .then((response) =>
        data = response.data.entity

        # Пока перезагрузка используется только для
        # обновления информации о доходах от клиента.
        # Добавлять поля по необходимости
        @_scope.client.income = data.income
        @_scope.client.income_by_days = data.income_by_days
      )
