class App.CompaniesClientCompanySearchResultCtrl extends IdFly.AngularClass
  @_import: [
    '$rootScope',
    '$scope',
    '$element',
    'close',
    'companies',
    'search',
    'labels',
    'countryPhoneCodes',
  ]

  app.controller('CompaniesClientCompanySearchResultCtrl', @factory())

  initialize: () ->
    @_scope.companies = JSON.parse(@_companies)
    @_scope.labels = @_labels

    @_scope.titleI18nParams = {
      count: @_scope.companies.length,
      search: @_search,
    }

    @_scope.choose = ((company) =>
      @_element.modal('hide')
      @_close(company)
    )

    @_scope.createNewCompany = (() =>
      @_element.modal('hide')
      @_close()
    )
