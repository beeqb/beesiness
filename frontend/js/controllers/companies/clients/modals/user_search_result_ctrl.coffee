class App.CompaniesClientUserSearchResultCtrl extends IdFly.AngularClass
  @_import: [
    '$rootScope',
    '$scope',
    '$element',
    'close',
    'users',
    'labels',
    'countryPhoneCodes',
  ]

  app.controller('CompaniesClientUserSearchResultCtrl', @factory())

  initialize: () ->
    @_scope.users = JSON.parse(@_users)
    @_scope.labels = @_labels

    for user in @_scope.users
      user.cell_phone_code =
        _.findWhere(@_countryPhoneCodes, {id: user.cell_phone_code_id})

    @_scope.titleI18nParams = {
      count: @_scope.users.length,
      name: "#{@_scope.users[0].name} #{@_scope.users[0].surname}",
    }

    @_scope.choose = ((user) =>
      @_element.modal('hide')
      @_close(user)
    )

    @_scope.createNewUser = (() =>
      @_element.modal('hide')
      @_close()
    )
