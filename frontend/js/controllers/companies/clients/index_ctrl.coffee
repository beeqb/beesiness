class App.CompaniesClientsIndexCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$location',
    '$routeParams',
    '$anchorScroll',
    'References',
    'ModalService',
    'ResourceLoader',
    'Page',
  ]

  app.controller('CompaniesClientsIndexCtrl', @factory())

  initialize: () ->
    @_References.sync()

    @_scope.companyId = +@_routeParams.company_id

    @_companyResource = new @_ResourceLoader(@_scope.companyId)

    @_scope.company = {}
    @_scope.cachedClients = {}
    @_scope.clients = []
    @_scope.employees = []
    @_scope.filter = {
      statuses: [],
    }
    @_scope.toggleStatus = @_toggleStatus
    @_scope.createStatus = @_createStatus
    @_scope.editStatus = @_editStatus

    @_scope.page = 1
    @_scope.per = 0
    @_scope.total_count = 0

    @_loadCompany()
    @_loadClients(@_location.search()?.page || @_scope.page)
    @_loadEmployees()

    @_scope.$watch('page', (newValue, oldValue) =>
      return if newValue == oldValue

      if @_scope.cachedClients[@_scope.page]
        @_scope.clients = @_scope.cachedClients[@_scope.page]
        return

      @_loadClients(@_scope.page)
    )

    @_rootScope.$on('company:reload', @_loadCompany)

  _loadCompany: () =>
    resource = new @_ResourceLoader()

    resource
    .include('clients_count_by_statuses', 'client_statuses', 'deal_statuses')
    .expand('references')
    .get('companies', @_scope.companyId)
    .then((response) =>
      @_scope.companyStatic = {references: response.data.references}
      @_scope.companyStatic.references.client_statuses =
        response.data.entity.client_statuses

      @_scope.companyStatic.references.deal_statuses =
        response.data.entity.deal_statuses

      delete response.data.entity.client_statuses
      delete response.data.entity.deal_statuses

      @_scope.company = response.data.entity
      @_Page.setTitle('clients', @_scope.company.name)
    )

  _resetCache: () =>
    @_scope.cachedClients = {}

  _toggleStatus: (status) =>
    if @_scope.filter.statuses.indexOf(status) == -1
      @_scope.filter.statuses.push(status)
    else
      @_scope.filter.statuses = _.without(@_scope.filter.statuses, status)

    @_scope.page = 1
    @_resetCache()
    @_loadClients(@_scope.page)

  _loadClients: (page) =>
    @_companyResource
    .expand('references', 'labels')
    .params({page: page, 'status[]': @_scope.filter.statuses})
    .include(
      'history_length',
      'deals',
      'related_tasks_count',
      'rf_status',
      'rm_status',
      'income',
      'income_by_days',
      'deals_stats',
      'products',
      'sales_dynamic'
    )
    .query('clients')
    .then((response) =>
      @_scope.clientStatic = {
        labels: response.data.labels
        references: response.data.references
      }
      @_scope.clients = response.data.entities
      @_scope.per = response.data.per
      @_scope.total_count = response.data.total_count
      @_scope.page = response.data.page

      for client in @_scope.clients
        for index, value of client.sales_dynamic.sum
          client.sales_dynamic.sum[index][1] = parseFloat(value[1])

      @_scope.cachedClients[@_scope.page] = @_scope.clients
    )

  _loadEmployees: () =>
    @_companyResource
    .include('user')
    .query('employees')
    .then((response) =>
      @_scope.employees = _.where(response.data.entities, {dismissed: false})
    )

  _createStatus: () =>
    @_ModalService.showModal({
      templateUrl: "/assets/views/companies/generic_statuses/new.html",
      controller: "CompaniesGenericStatusesNewCtrl",
      inputs: {
        companyId: @_routeParams.company_id,
        entityName: 'client_status',
      },
    })
    .then((modal) =>
      modal.element.modal()
      modal.close.then((result) =>
        @_companyResource
        .post('client_statuses', {client_status: result})
        .then((response) =>
          @_scope.companyStatic.references.client_statuses.push(response.data)
        )
      )
    )

  _editStatus: (statusId) =>
    @_ModalService.showModal({
      templateUrl: "/assets/views/companies/generic_statuses/new.html",
      controller: 'CompaniesGenericStatusesEditCtrl',
      inputs: {
        companyId: @_routeParams.company_id,
        statusId: statusId,
        entityName: 'client_status',
        existingStatuses: JSON.stringify(
          @_scope.companyStatic.references.client_statuses
        ),
      },
    })
    .then((modal) =>
      modal.element.modal()
      modal.close.then((result) =>
        if result.delete
          @_deleteStatus(statusId, result)
        else
          @_updateStatus(statusId, result)
      )
    )

  _updateStatus: (statusId, data) =>
    @_companyResource
    .put('client_statuses', statusId, {client_status: data})
    .then((response) =>
      statuses = @_scope.companyStatic.references.client_statuses

      statusIndex = _.findIndex(statuses, {id: response.data.id})
      statuses.splice(statusIndex, 1, response.data)
    )

  _deleteStatus: (statusId, data) =>
    @_companyResource
    .post(
      "client_statuses/#{statusId}/delete_safely",
      {status_after_delete: data.status_after_delete}
    )
    .then(() =>
      statuses = @_scope.companyStatic.references.client_statuses

      @_scope.companyStatic.references.client_statuses = _.filter(
        statuses,
        (status) -> status.id != statusId
      )
    )
