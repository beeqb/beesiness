class App.CompaniesClientsNewCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$routeParams',
    '$auth',
    '$cookies',
    '$timeout',
    '$q',
    'Upload',
    'ModalService',
    'ResourceLoader',
    'mapConfig',
  ]

  app.controller('CompaniesClientsNewCtrl', @factory())

  initialize: () ->
    @_resource = new @_ResourceLoader()

    @_scope.companyId = @_routeParams.company_id
    @_companyResource = new @_ResourceLoader(@_scope.companyId)

    @_scope.loadCompanyData = @_loadCompanyData

    @_scope.client = {
      company_id: @_routeParams.company_id,
      legal_form: 'individual',
      errors: {},
    }
    @_scope.updatePhoneCodeByCountry = @_updatePhoneCodeByCountry
    @_scope.map = @_mapConfig
    @_scope.submit = @_submit
    @_scope.validate = @_submit
    @_load()
    @_initWatchers()

  _clearClient: (caller) =>
    fields = [
      'city_id',
      'birthday',
      'birthday_uib',
      'cell_phone',
      'cell_phone_code_id',
      'work_phone',
      'work_phone_code_id',
      'skype',
      'facebook',
      'viber',
      'country_id',
      'current_job',
      'selected_job',
      'current_department',
      'selected_department',
      'current_position',
      'current_is_head',
      'address',
      'latitude',
      'longitude'
    ]

    if caller == 'company'
      fields.push('legal_entity_id')
      fields.push('legal_entity_email')
      fields.push('site')
      fields.push('name')
      fields.push('surname')
      fields.push('email')
    else if caller == 'user'
      fields.push('user_id')
      fields.push('email')

    for field in fields
      @_scope.client[field] = null

  _initWatchers: () =>
    @_scope.$watchGroup(['client.name', 'client.surname'], () =>
      if @_scope.client.name && @_scope.client.surname
        @_loadClientData()
    )

    @_scope.$watch('client.selected_job', () =>
      return unless @_scope.client.selected_job

      company = _.findWhere(@_scope.clientStatic.references.companies, {
        id: @_scope.client.selected_job
      })

      return unless company

      @_scope.client.current_job = company.name
      @_scope.client.current_department = ''
      @_scope.client.current_position = ''
      @_scope.client.current_is_head = 0

      companyResource = new @_ResourceLoader(company.id)

      companyResource
      .include('employees_departments', 'employees_departments.department')
      .get('employees', company.employee_id)
      .then((response) =>
        @_scope.clientStatic.references.departments = _.map(
          response.data.entity.employees_departments,
          ((employee_department) =>
            return {
              id: employee_department.department.id,
              name: employee_department.department.name,
              position: employee_department.position,
              is_head: employee_department.is_head,
            }
          )
        )
        if @_scope.clientStatic.references.departments.length > 0
          @_scope.client.selected_department =
            @_scope.clientStatic.references.departments[0].id
      )
    )

    @_scope.$watch('client.selected_department', () =>
      if !@_scope.client.selected_department
        return
      department = _.findWhere(@_scope.clientStatic.references.departments, {
        id: @_scope.client.selected_department
      })
      if department
        @_scope.client.current_department = department.name
        @_scope.client.current_position = department.position
        @_scope.client.current_is_head = +department.is_head
    )

    @_scope.$watch('client.work_phone_code_id', () =>
      return unless @_scope.client.work_phone_code_id

      @_scope.client.work_phone_code = _.find(
        @_scope.clientStatic.references.country_phone_codes,
        {
          id: @_scope.client.work_phone_code_id,
        }
      ).code
    )

    @_scope.$watch('client.cell_phone_code_id', () =>
      return unless @_scope.client.cell_phone_code_id

      @_scope.client.cell_phone_code = _.find(
        @_scope.clientStatic.references.country_phone_codes,
        {
          id: @_scope.client.cell_phone_code_id,
        }
      ).code
    )

  _loadCompanyData: () =>
    return unless @_scope.client.legal_form == 'legal_entity'

    search = @_scope.client.legal_entity_name

    return if !search || search.length < 3

    if @_loadCompanyDataPromise
      @_timeout.cancel(@_loadCompanyDataPromise)

    @_loadCompanyDataPromise = @_timeout(
      (() =>
        @_rootScope.showOverlay('Поиск компаний...')
        @_resource
        .include('director')
        .post('companies/search', {name: search})
        .then((response) =>
          @_scope.client.errors = {}
          @_rootScope.hideOverlay()

          return if response.data.length == 0

          @_ModalService.showModal({
            templateUrl: '/assets/views/companies/clients/modals/company_search_results.html',
            controller: 'CompaniesClientCompanySearchResultCtrl',
            inputs: {
              companies: JSON.stringify(response.data),
              search: search,
              labels: @_scope.clientStatic.labels,
              countryPhoneCodes: @_scope.clientStatic.references.country_phone_codes,
            },
          })
          .then((modal) =>
            modal.element.modal()
            modal.close.then((company) =>
              return unless company

              @_clearClient('company')

              @_scope.client.legal_entity_id = company.id
              @_scope.client.legal_entity_email = company.email
              @_scope.client.legal_entity_name = company.name
              @_scope.client.address = company.address
              @_scope.client.latitude = company.latitude
              @_scope.client.longitude = company.longitude
              @_scope.client.site = company.site
              @_scope.client.work_phone = company.phone
              @_scope.client.work_phone_code_id = company.phone_code_id

              if company.director
                @_scope.client.name = company.director.name
                @_scope.client.surname = company.director.surname
                @_scope.client.email = company.director.email
            )
          )
        )
        .finally(@_rootScope.hideOverlay)
      ),
      500
    )

  _loadClientData: () =>
    return unless @_scope.client.legal_form == 'individual'

    if @_loadClientDataPromise
      @_timeout.cancel(@_loadClientDataPromise)

    @_loadClientDataPromise = @_timeout(
      (() =>
        @_rootScope.showOverlay('Поиск пользователей...')

        @_resource
        .include('employees')
        .post('users/search', {
          name: @_scope.client.name,
          surname: @_scope.client.surname,
        })
        .then((response) =>
          @_scope.client.errors = {}
          @_rootScope.hideOverlay()

          return if response.data.length == 0

          @_ModalService.showModal({
            templateUrl: "/assets/views/companies/clients/modals/user_search_results.html",
            controller: "CompaniesClientUserSearchResultCtrl",
            inputs: {
              users: JSON.stringify(response.data),
              labels: @_scope.clientStatic.labels,
              countryPhoneCodes: @_scope.clientStatic.references.country_phone_codes,
            },
          })
          .then((modal) =>
            modal.element.modal()
            modal.close.then((user) =>
              if user
                @_clearClient('user')
                @_substituteUser(user)
            )
          )
        )
        .finally(() =>
          @_rootScope.hideOverlay()
        )
      ),
      500
    )


  _initValidation: () =>
    @_validationTimeout = 500 #msec
    @_scope.$watchCollection('client', (newCollection, oldCollection) =>
      attr = null
      for k,v of newCollection
        if v != oldCollection[k]
          attr = k
          break

      if !attr || attr == 'errors'
        return

      @_validate(attr)
    )

  _validate: (attribute) =>
    if @_validateTimeoutPromise
      @_timeout.cancel(@_validateTimeoutPromise)

    @_validateTimeoutPromise = @_timeout(
      (() =>
        if @_routeParams.id
          @_companyResource.params(id: @_routeParams.id)

        @_companyResource
        .post('clients/validate', {client: @_scope.client})
        .then(() =>
          @_scope.client.errors = {}
        )
        .catch((response) =>
          for attr, error of response.data.errors
            if attr != attribute
              continue
            @_scope.client.errors[attribute] = error
        )
      ),
      @_validationTimeout
    )

  _load: () =>
    @_companyResource
    .expandReferencesAndLabels()
    .query('clients/new')
    .then((response) =>
      @_scope.clientStatic = {
        references: response.data.references,
        labels: response.data.labels,
      }

      # убираем null из объекта, чтобы не затереть address
      data = _.pick(response.data.entity, _.identity)

      _.extend(@_scope.client, data)
      @_scope.client.legal_form = 'individual' #default
      @_initValidation()
    )

  _getManagerId: () =>
    employee = _.findWhere(@_scope.user.employees, {
      company_id: +@_routeParams.company_id,
    })
    if employee
      return employee.id
    return null

  _substituteUser: (user) =>
    @_scope.client.user_id = user.id
    @_scope.client.email = user.email

    if @_scope.client.legal_form == 'individual'
      @_scope.client.birthday = user.birthday
      @_scope.client.cell_phone = user.cell_phone
      @_scope.client.cell_phone_code_id = user.cell_phone_code_id
      @_scope.client.site = user.site
      @_scope.client.skype = user.skype
      @_scope.client.facebook = user.facebook_link
      @_scope.client.viber = user.viber
      @_scope.client.address = user.address
      @_scope.client.latitude = user.latitude
      @_scope.client.longitude = user.longitude

      @_scope.clientStatic.references.companies = _.map(
        user.employees,
        ((employee) =>
          return {
            id: employee.company.id,
            name: employee.company.name,
            employee_id: employee.id,
          }
        )
      )

      if @_scope.clientStatic.references.companies.length > 0
        @_scope.client.selected_job =
          @_scope.clientStatic.references.companies[0].id

  _submit: () =>
    @_scope.client.manager_id = @_getManagerId()

    data = @_scope.client

    @_companyResource
    .post('clients', {client: @_scope.client})
    .then((response) =>
      @_rootScope.$emit('user:reloadPermissions')
      @_location.path('/companies/' + @_routeParams.company_id + '/clients')
    )
    .catch((response) =>
      _.extend(@_scope.client.errors, response.data.errors)
    )
  _updatePhoneCodeByCountry: (countryCode) =>
    code = _.findWhere(
      @_scope.clientStatic?.references.country_phone_codes,
      {country_code: countryCode},
    )
    @_scope.client.work_phone_code_id = code?.id
    @_scope.client.cell_phone_code_id = code?.id
