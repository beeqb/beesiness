class App.CompaniesLeadStatusesLoadCtrl extends IdFly.AngularClass

  app.controller('CompaniesLeadStatusesLoadCtrl', @factory())

  initialize: () ->
    @_companyResource = new @_ResourceLoader(@_companyId)

    @_scope.status = {
      errors: {},
    }
    @_scope.submit = @_submitModal
    @_scope.colors = [
      '#6254b2',
      '#23b7e5',
      '#27c24c',
      '#fad733',
      '#f05050',
      '#3a3f51',
    ]

    @_load()

  _submitModal: () =>
    url = '/api/companies/' + @_companyId + '/departments/validate'

    @_companyResource
    .post('lead_statuses/validate', {lead_status: @_scope.status})
    .then(() =>
        @_element.modal('hide')
        @_close(@_scope.status)
    )
    .catch((response) =>
      _.extend(@_scope.status.errors, response.data.errors)
    )
