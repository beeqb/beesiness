class App.CompaniesLeadStatusesNewCtrl extends App.CompaniesLeadStatusesLoadCtrl

  @_import: [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$element',
    'close',
    'companyId',
    'ResourceLoader',
  ]

  app.controller('CompaniesLeadStatusesNewCtrl', @factory())

  initialize: () ->
    super()

  _load: () =>
    @_companyResource
    .expand('labels')
    .query('lead_statuses/new')
    .then((response) =>
      @_scope.labels = response.data.labels
      _.extend(@_scope.status, response.data.entity)
      @_scope.status.company_id = @_companyId
    )
