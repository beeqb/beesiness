class App.CompaniesLoadCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$routeParams',
    '$auth',
    '$timeout',
    '$log',
    'Upload',
    'mapConfig',
    'Page',
    'ResourceLoader',
    'toaster',
  ]

  app.controller('CompaniesLoadCtrl', @factory())

  initialize: () ->
    @_companyId = @_routeParams.id
    @_companyResource = new @_ResourceLoader(@_companyId)

    @_scope.map = @_mapConfig
    @_scope.company = {
      errors: {},
      loaded: false,
    }
    @_scope.sliderSettings = {
      floor: 0.0,
      ceiling: 40.0,
      step: 0.5,
      format: ((value) ->
        return value.toString() + '%'
      )
    }
    @_load()

  _initValidation: () =>
    @_validationTimeout = 500 #msec
    @_scope.$watchCollection('company', (newCollection, oldCollection) =>
      attr = null
      for k,v of newCollection
        if v != oldCollection[k]
          attr = k
          break

      if !attr || attr == 'errors'
        return

      @_validate(attr)
    )

  _validate: (attribute) =>
    if @_validateTimeoutPromise
      @_timeout.cancel(@_validateTimeoutPromise)

    @_validateTimeoutPromise = @_timeout(
      (() =>
        url = '/api/companies/validate'
        if @_routeParams.id
          url += '?id=' + @_scope.company.id

        @_http
        .post(url, {company: @_scope.company})
        .then(
          (() =>
            @_scope.company.errors = {}
          ),
          ((response) =>
            for attr, error of response.data.errors
              if attr != attribute
                continue
              @_scope.company.errors[attribute] = error
          )
        )
      ),
      @_validationTimeout
    )

  _load: () =>
    @_http
    .get('/api/companies/' + @_companyId +
        '?expand=labels,references' +
        '&include=creator'
    )
    .then((response) =>
      @_scope.companyStatic = {
        labels: response.data.labels,
        references: response.data.references,
      }
      _.extend(@_scope.company, response.data.entity)
      @_scope.company.loaded = true
      @_initValidation()
    )
