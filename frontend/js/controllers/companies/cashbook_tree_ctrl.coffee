class App.CompaniesCashbookTreeCtrl extends IdFly.AngularClass

  @_import: [
    '$scope',
    'ResourceLoader'
  ]

  app.controller('CompaniesCashbookTreeCtrl', @factory())

  initialize: () ->
    @_companyId               = @_scope.companyId
    @_companyResource         = new @_ResourceLoader(@_companyId)

    @treeStartDate = moment().startOf('year')
    @treeEndDate   = moment()

    @treeDateRangeOptions = {
      startDate: moment(@treeStartDate),
      endDate: moment(@treeEndDate)
    }

    @treeDateRangeCallback = ((start, end) =>
      @treeStartDate = start
      @treeEndDate = end

      @_scope.loadCashBookRecords(@treeStartDate, @treeEndDate)
    )

    @treeDataObject = []

    @toggleCashbookTreeNode = (scope) ->
      scope.toggle()

    @_scope.$on('cashBookRecordsLoaded', (event, args) => @_createTree(args))

  _createTree: (cashBookRecords) =>
    @treeDataObject.length = 0
    for year in [@treeStartDate.year()..@treeEndDate.year()]
      yearRecord = {
        year: year.toString() + '-й Год'
        data: {
          income: 0,
          outcome: 0,
          balance: 0,
          currency: ''
        }
        quarters: []
      }

      currentYearRecords = cashBookRecords.filter((record) =>
        if moment(record.created_at).year() == year
          if record.category == "income"
            yearRecord.data.income += parseFloat(record.amount)
            yearRecord.data.balance += parseFloat(record.amount)
          else if record.category == "outcome"
            yearRecord.data.outcome += parseFloat(record.amount)
            yearRecord.data.balance -= parseFloat(record.amount)
      )

      currMonthNum = 0
      for quarter in [1..4]
        quarterRecord = yearRecord.quarters[quarter - 1] = {
          quarter: quarter.toString() + '-й Квартал'
          data: {
            income: 0,
            outcome: 0,
            balance: 0,
            currency: ''
          }
          monthes: []
        }

        currentQuaterRecords = currentYearRecords.filter((record) ->
          if moment(record.created_at).quarter() == quarter
            if record.category == "income"
              quarterRecord.data.income += parseFloat(record.amount)
              quarterRecord.data.balance += parseFloat(record.amount)
            else if record.category == "outcome"
              quarterRecord.data.outcome += parseFloat(record.amount)
              quarterRecord.data.balance -= parseFloat(record.amount)
        )

        for month in [0..2]
          monthRecord = yearRecord.quarters[quarter - 1].monthes[month] = {
            monthNum: currMonthNum
            month: moment([year, currMonthNum]).format("MMMM").capitalize()
            data: {
              income: 0,
              outcome: 0,
              balance: 0,
              currency: ''
            }
            emptyArray: [{}]
          }

          monthRecord["records"] = currentQuaterRecords.filter((record) ->
            if moment(record.created_at).month() == monthRecord.monthNum
              if record.category == "income"
                monthRecord.data.income += parseFloat(record.amount)
                monthRecord.data.balance += parseFloat(record.amount)
              else if record.category == "outcome"
                monthRecord.data.outcome += parseFloat(record.amount)
                monthRecord.data.balance -= parseFloat(record.amount)
          )
          currMonthNum++

      @treeDataObject.push(yearRecord)
    @treeDataObject.reverse()
