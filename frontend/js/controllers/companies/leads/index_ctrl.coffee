class App.CompaniesLeadsIndexCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$location',
    '$routeParams',
    '$anchorScroll',
    'ModalService',
    'References',
    'ResourceLoader',
    'Page',
  ]

  app.controller('CompaniesLeadsIndexCtrl', @factory())

  initialize: () ->
    @_References.sync()

    @_scope.companyId = +@_routeParams.company_id

    @_companyResource = new @_ResourceLoader(@_scope.companyId)

    @_scope.company = {}
    @_scope.leads = []
    @_scope.cachedLeads = {}
    @_scope.employees = []
    @_scope.filter =  {
      statuses: [],
    }
    @_scope.range =  {
      min: 0,
      max: 500000,
    }
    @_scope.search = {}
    @_scope.sliderRange =  {
      min: 0,
      max: 500000,
    }

    @_scope.page = 1
    @_scope.per = 0
    @_scope.total_count = 0

    @_scope.toggleStatus = @_toggleStatus
    @_scope.rerenderSlider = @_rerenderSlider
    @_scope.clearFilterField = @_clearFilterField
    @_scope.createStatus = @_createStatus
    @_scope.editStatus = @_editStatus

    @_loadCompany()
    @_loadLeads(@_location.search()?.page || @_scope.page)
    @_loadEmployees()
    @_rootScope.$on('company:reload', @_loadCompany)

    @_scope.$watch('page', (newValue, oldValue) =>
      return if newValue == oldValue

      if @_scope.cachedLeads[@_scope.page]
        @_scope.leads = @_scope.cachedLeads[@_scope.page]
        return

      @_loadLeads(@_scope.page)
    )

  _loadCompany: () =>
    resource = new @_ResourceLoader()

    resource
    .include('leads_count_by_statuses', 'lead_statuses')
    .expand('references')
    .get('companies', @_scope.companyId)
    .then((response) =>
      @_scope.companyStatic = {references: response.data.references}
      @_scope.companyStatic.references.lead_statuses =
        response.data.entity.lead_statuses

      delete response.data.entity.lead_statuses

      @_scope.company = response.data.entity

      @_Page.setTitle('leads', @_scope.company.name)
    )

  _toggleStatus: (status) =>
    if @_scope.filter.statuses.indexOf(status) == -1
      @_scope.filter.statuses.push(status)
    else
      @_scope.filter.statuses = _.without(@_scope.filter.statuses, status)

    @_scope.page = 1
    @_resetCache()
    @_loadLeads(@_scope.page)

  _resetCache: () =>
    @_scope.cachedLeads = {}

  _loadLeads: (page) =>
    @_companyResource
    .expandLabelsAndReferences()
    .include('history_length', 'lead_products', 'related_tasks_count')
    .query('leads', {'status[]': @_scope.filter.statuses, page: page})
    .then((response) =>
      @_scope.leadStatic = {
        labels: response.data.labels,
        references: response.data.references,
      }
      @_scope.leads = response.data.entities
      @_scope.per = response.data.per
      @_scope.total_count = response.data.total_count
      @_scope.page = response.data.page

      for lead in @_scope.leads
        lead.productsTotal =
          _
          .chain(lead.lead_products)
          .map((leadProduct) ->
            leadProduct.product.price * leadProduct.count
          )
          .reduce((memo, price) -> memo + price)
          .value()

        lead.vatSum =
          lead.productsTotal * ((@_scope.company.vat || 0) / 100)

        lead.potential_amount = Number(lead.potential_amount)

        lead.expanded = false

      @_scope.cachedLeads[@_scope.page] = @_scope.leads
    )

  _loadEmployees: () =>
    @_companyResource
    .include('user')
    .query('employees')
    .then((response) =>
      @_scope.employees = _.where(response.data.entities, {dismissed: false})
    )

  _rerenderSlider: () =>
    angular.element(window).triggerHandler('resize')

  _clearFilterField: (field) =>
    if field == 'range'
      @_scope.range = @_scope.sliderRange
    else
      @_scope.search[field] = '' if @_scope.search[field]?

  _createStatus: () =>
    @_ModalService.showModal({
      templateUrl: "/assets/views/companies/generic_statuses/new.html",
      controller: "CompaniesGenericStatusesNewCtrl",
      inputs: {
        companyId: @_routeParams.company_id,
        entityName: 'lead_status',
      },
    })
    .then((modal) =>
      modal.element.modal()
      modal.close.then((result) =>
        @_companyResource
        .post('lead_statuses', {lead_status: result})
        .then((response) =>
          @_scope.companyStatic.references.lead_statuses.push(response.data)
        )
      )
    )

  _editStatus: (statusId) =>
    @_ModalService.showModal({
      templateUrl: "/assets/views/companies/generic_statuses/new.html",
      controller: 'CompaniesGenericStatusesEditCtrl',
      inputs: {
        companyId: @_routeParams.company_id,
        statusId: statusId,
        entityName: 'lead_status',
        existingStatuses: JSON.stringify(
          @_scope.companyStatic.references.lead_statuses
        ),
      },
    })
    .then((modal) =>
      modal.element.modal()
      modal.close.then((result) =>
        if result.delete
          @_deleteStatus(statusId, result)
        else
          @_updateStatus(statusId, result)
      )
    )

  _updateStatus: (statusId, data) =>
    @_companyResource
    .put('lead_statuses', statusId, {lead_status: data})
    .then((response) =>
      statuses = @_scope.companyStatic.references.lead_statuses

      statusIndex = _.findIndex(statuses, {id: response.data.id})
      statuses.splice(statusIndex, 1, response.data)
    )

  _deleteStatus: (statusId, data) =>
    @_companyResource
    .post(
      "lead_statuses/#{statusId}/delete_safely",
      {status_after_delete: data.status_after_delete}
    )
    .then(() =>
      statuses = @_scope.companyStatic.references.lead_statuses

      @_scope.companyStatic.references.lead_statuses = _.filter(
        statuses,
        (status) -> status.id != statusId
      )
    )
