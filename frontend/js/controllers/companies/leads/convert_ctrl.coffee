class App.CompaniesLeadsConvertCtrl extends App.CompaniesLeadsNewCtrl

  app.controller('CompaniesLeadsConvertCtrl', @factory())

  initialize: () ->
    super()
    @_stopWatchLead && @_stopWatchLead()
    @_stopWatchCompany && @_stopWatchCompany()
    @_scope.companyId = @_routeParams.company_id
    @_scope.convertation = true

  _getValidateUrl: () =>
    return '/api/companies/' + @_routeParams.company_id +
        '/leads/validate_lead_as_client'

  _load: () =>
    @_companyResource
    .expandLabelsAndReferences()
    .include('lead_products')
    .get('leads', @_routeParams.id)
    .then((response) =>
      @_scope.leadStatic = {
        labels: response.data.labels,
        references: response.data.references,
      }
      _.extend(@_scope.lead, response.data.entity)
      @_initValidation()
    )

  _submit: () =>
    @_scope.lead.manager_id = @_getManagerId()

    @_convertLead(@_scope.lead)
    .catch((response) =>
      _.extend(@_scope.lead.errors, response.data.errors)
    )
