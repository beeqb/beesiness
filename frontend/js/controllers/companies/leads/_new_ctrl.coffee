class App.CompaniesLeadsNewCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$routeParams',
    '$auth',
    '$cookies',
    '$timeout',
    '$q',
    'Upload',
    'ModalService',
    'mapConfig',
    'ResourceLoader',
    'leadProducts',
    'Confirm',
    'convertLead',
  ]

  app.controller('CompaniesLeadsNewCtrl', @factory())

  initialize: () ->
    @_scope.companyId = @_routeParams.company_id
    @_companyResource = new @_ResourceLoader(@_scope.companyId)

    @_scope.lead = {
      company_id: @_routeParams.company_id,
      legal_form: 'individual',
      errors: {},
    }
    @_scope.leadStatic = {
      references: {
        companies: [],
      },
    }
    @_scope.updatePhoneCodeByCountry = @_updatePhoneCodeByCountry
    @_scope.loadCompanyData = @_loadCompanyData

    @_scope.map = @_mapConfig
    @_scope.submit = @_submit
    @_scope.validate = @_submit
    @_initWatchers()
    @_load()

  _clearLead: (caller) =>
    fields = [
      'city_id',
      'birthday',
      'birthday_uib',
      'cell_phone',
      'cell_phone_code_id',
      'work_phone',
      'work_phone_code_id',
      'skype',
      'facebook',
      'viber',
      'country_id',
      'current_job',
      'selected_job',
      'current_department',
      'selected_department',
      'current_position',
      'current_is_head',
      'address',
      'latitude',
      'longitude'
    ]

    if caller == 'company'
      fields.push('legal_entity_id')
      fields.push('legal_entity_email')
      fields.push('site')
      fields.push('name')
      fields.push('surname')
      fields.push('email')
    else if caller == 'user'
      fields.push('user_id')
      fields.push('email')

    for field in fields
      @_scope.lead[field] = null

  _initWatchers: () =>
    @_stopWatchLead = @_scope.$watchGroup(['lead.name', 'lead.surname'], () =>
      if @_scope.lead.name && @_scope.lead.surname
        @_loadLeadData()
    )

    @_scope.$watch('lead.selected_job', () =>
      if !@_scope.lead.selected_job
        return
      company = _.findWhere(@_scope.leadStatic.references.companies, {
        id: @_scope.lead.selected_job
      })
      if company
        @_scope.lead.current_job = company.name
        @_scope.lead.current_department = ''
        @_scope.lead.current_position = ''
        @_scope.lead.current_is_head = 0
        if company.work_phones
          @_scope.lead.work_phone = company.work_phones[0]?.phone
          @_scope.lead.work_phone_code_id =
            company.work_phones[0]?.country_phone_code_id

        @_http
        .get('/api/companies/' + company.id +
            '/employees/' + company.employee_id +
            '?include=employees_departments,employees_departments.department'
        )
        .then((response) =>
          @_scope.leadStatic.references['departments'] = _.map(
            response.data.entity.employees_departments,
            ((employee_department) =>
              return {
                id: employee_department.department.id,
                name: employee_department.department.name,
                position: employee_department.position,
                is_head: employee_department.is_head,
              }
            )
          )
          if @_scope.leadStatic.references.departments.length > 0
            @_scope.lead.selected_department =
              @_scope.leadStatic.references.departments[0].id
        )
    )

    @_scope.$watch('lead.selected_department', () =>
      if !@_scope.lead.selected_department
        return
      department = _.findWhere(@_scope.leadStatic.references.departments, {
        id: @_scope.lead.selected_department
      })
      if department
        @_scope.lead.current_department = department.name
        @_scope.lead.current_position = department.position
        @_scope.lead.current_is_head = +department.is_head
    )


  _loadCompanyData: () =>
    return unless @_scope.lead.legal_form == 'legal_entity'

    search = @_scope.lead.legal_entity_name

    return if !search || search.length < 3

    if @_loadCompanyDataPromise
      @_timeout.cancel(@_loadCompanyDataPromise)

    @_loadCompanyDataPromise = @_timeout(
      (() =>
        @_rootScope.showOverlay('Поиск компаний...')
        @_http
        .post('/api/companies/search?include=director', {name: search})
        .then((response) =>
          @_scope.lead.errors = {}
          @_rootScope.hideOverlay()

          return if response.data.length == 0

          @_ModalService.showModal({
            templateUrl: '/assets/views/companies/leads/modals/company_search_results.html',
            controller: 'CompaniesClientCompanySearchResultCtrl',
            inputs: {
              companies: JSON.stringify(response.data),
              search: search,
              labels: @_scope.leadStatic.labels,
              countryPhoneCodes: @_scope.leadStatic.references.country_phone_codes,
            },
          })
          .then((modal) =>
            modal.element.modal()
            modal.close.then((company) =>
              return unless company

              @_clearLead('company')

              @_scope.lead.legal_entity_id = company.id
              @_scope.lead.legal_entity_email = company.email
              @_scope.lead.legal_entity_name = company.name
              @_scope.lead.address = company.address
              @_scope.lead.latitude = company.latitude
              @_scope.lead.longitude = company.longitude
              @_scope.lead.site = company.site
              @_scope.lead.work_phone = company.phone

              if company.director
                @_scope.lead.name = company.director.name
                @_scope.lead.surname = company.director.surname
                @_scope.lead.email = company.director.email
            )
          )
        )
        .finally(() => @_rootScope.hideOverlay())
      )
      500
    )


  _loadLeadData: () =>
    if @_scope.lead.legal_form != 'individual'
      return

    if @_loadLeadDataPromise
      @_timeout.cancel(@_loadLeadDataPromise)

    @_loadLeadDataPromise = @_timeout(
      (() =>
        @_rootScope.showOverlay('Поиск пользователей...')
        @_http
        .post('/api/users/search?include=employees', {
          name: @_scope.lead.name,
          surname: @_scope.lead.surname,
        })
        .then((response) =>
          @_scope.lead.errors = {}
          @_rootScope.hideOverlay()

          return if response.data.length == 0

          @_ModalService.showModal({
            templateUrl: '/assets/views/companies/leads/modals/user_search_results.html',
            controller: 'CompaniesClientUserSearchResultCtrl',
            inputs: {
              users: JSON.stringify(response.data),
              labels: @_scope.leadStatic.labels,
              countryPhoneCodes: @_scope.leadStatic.references.country_phone_codes,
            },
          })
          .then((modal) =>
            modal.element.modal()
            modal.close.then((user) =>
              return unless user
              @_clearLead('user')
              @_substituteUser(user)
            )
          )
        )
        .finally(@_rootScope.hideOverlay)
      ),
      500
    )


  _initValidation: () =>
    @_validationTimeout = 500 #msec
    @_scope.$watchCollection('lead', (newCollection, oldCollection) =>
      attr = null
      for k,v of newCollection
        if v != oldCollection[k]
          attr = k
          break

      if !attr || attr == 'errors'
        return

      @_validate(attr)
    )

  _getValidateUrl: () =>
    return '/api/companies/' + @_routeParams.company_id + '/leads/validate'

  _validate: (attribute) =>
    if @_validateTimeoutPromise
      @_timeout.cancel(@_validateTimeoutPromise)

    @_validateTimeoutPromise = @_timeout(
      (() =>
        url = @_getValidateUrl()
        if @_routeParams.id
          url += '?id=' + @_routeParams.id

        @_http
        .post(url, {lead: @_scope.lead})
        .then(
          (() =>
            @_scope.lead.errors = {}
          ),
          ((response) =>
            for attr, error of response.data.errors
              if attr == attribute
                @_scope.lead.errors[attribute] = error
          )
        )
      ),
      @_validationTimeout
    )

  _load: () =>
    @_http
    .get('/api/companies/' + @_routeParams.company_id + '/leads/new' +
        '?expand=labels,references')
    .then((response) =>
      @_scope.leadStatic = {
        labels: response.data.labels,
        references: response.data.references,
      }
      _.extend(@_scope.lead, response.data.entity)
      @_scope.lead.legal_form = 'individual' #default
      @_initValidation()
    )

  _getManagerId: () =>
    @_scope.user.employeeInCompany(@_routeParams.company_id)?.id

  _substituteUser: (user) =>
    @_scope.lead.user_id = user.id
    @_scope.lead.email = user.email
    if @_scope.lead.legal_form == 'individual'
      @_scope.lead.birthday = user.birthday
      @_scope.lead.cell_phone = user.cell_phone
      @_scope.lead.cell_phone_code_id = user.cell_phone_code_id
      @_scope.lead.site = user.site
      @_scope.lead.skype = user.skype
      @_scope.lead.facebook = user.facebook_link
      @_scope.lead.viber = user.viber
      @_scope.lead.address = user.address
      @_scope.lead.latitude = user.latitude
      @_scope.lead.longitude = user.longitude

      @_scope.leadStatic.references.companies = _.map(
        user.employees,
        ((employee) =>
          return {
            id: employee.company.id,
            name: employee.company.name,
            employee_id: employee.id,
            work_phones: employee.employee_phones,
          }
        )
      )

      if @_scope.leadStatic.references.companies.length > 0
        @_scope.lead.selected_job =
          @_scope.leadStatic.references.companies[0].id

  _submit: () =>
    @_scope.lead.manager_id = @_getManagerId()

    url = '/api/companies/' + @_routeParams.company_id + '/leads'
    @_http
    .post(url, {lead: @_scope.lead})
    .then(
      (() =>
        @_rootScope.$emit('user:reloadPermissions')
        @_location.path('/companies/' + @_routeParams.company_id + '/leads')
      ),
      ((response) =>
        _.extend(@_scope.lead.errors, response.data.errors)
        @_scope.$broadcast('aform:errors')
      )
    )

  _updatePhoneCodeByCountry: (countryCode) =>
    code = _.findWhere(
      @_scope.leadStatic.references.country_phone_codes,
      {country_code: countryCode},
    )
    @_scope.lead.work_phone_code_id = code?.id
    @_scope.lead.cell_phone_code_id = code?.id
