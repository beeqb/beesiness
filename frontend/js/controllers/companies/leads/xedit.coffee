class App.CompaniesLeadsXEditCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$location',
    '$routeParams',
    '$auth',
    '$cookies',
    '$timeout',
    '$q',
    'Upload',
    'ModalService'
    'mapConfig',
    'ResourceLoader',
    'Confirm',
    'convertLead',
  ]

  app.controller('CompaniesLeadsXEditCtrl', @factory())

  initialize: () ->
    @_scope.companyId = @_routeParams.company_id
    @_companyResource = new @_ResourceLoader(@_scope.companyId)

    if @_routeParams.lead_id
      @_scope.lead = {}
      @_loadCompany()
      @_companyResource
      .include(
        'history_length',
        'lead_products',
        'related_tasks_count'
      )
      .expandLabelsAndReferences()
      .get('leads', @_routeParams.lead_id)
      .then((response) =>
        @_scope.leadStatic = {
          labels: response.data.labels,
          references: response.data.references,
        }
        @_scope.lead = response.data.entity
        @_scope.lead.expanded = true
        @_scope.lead.canBeExpanded = false
      )
    else
      @_scope.lead.canBeExpanded = true

    @_scope.lead.errors = {}
    @_scope.map = @_mapConfig
    @_scope.updatePhoneCodeByCountry = @_updatePhoneCodeByCountry
    @_scope.addLeadProduct = @_addLeadProduct
    @_scope.setStatus = @_setStatus
    @_scope.submit = @_submit
    @_scope.validate = @_validate
    @_scope.delete = @_delete
    @_scope.employeesForSecondManager = []
    @_scope.loadTasks = @_loadTasks
    @_scope.convert = @_convert

    @_initValidation()
    @_initWatchers()

  _loadCompany: () =>
    @_companyResource
    .include('leads_count_by_statuses')
    .expand('references')
    .query('')
    .then((response) =>
      @_scope.companyStatic = {
        references: response.data.references,
      }
      @_scope.company = response.data.entity
    )

  _initWatchers: () =>
    @_scope.watchedData = {}

    @_scope.$watchGroup(['employees', 'lead.manager_id'], () =>
      if !@_scope.employees || !@_scope.lead
        return
      @_scope.employeesForSecondManager = _.filter(@_scope.employees, (item) =>
        return item.id != @_scope.lead.manager_id
      )
    )

  _initValidation: () =>
    @_validationTimeout = 500 #msec
    @_scope.$watchCollection('lead', (newCollection, oldCollection) =>
      attr = null
      for k,v of newCollection
        if v != oldCollection[k]
          attr = k
          break

      if !attr || attr == 'errors'
        return

      @_validate(attr)
    )

  _validate: (attribute, value) =>
    if !@_scope.LeadForm || !@_scope.LeadForm.$visible
      return

    if @_validateTimeoutPromise
      @_timeout.cancel(@_validateTimeoutPromise)

    @_validateTimeoutPromise = @_timeout(
      (() =>
        data = {}
        _.extend(data, @_scope.lead)
        data[attribute] = value

        if @_scope.lead.id
          @_companyResource.params({id: @_scope.lead.id})

        @_companyResource
        .post('leads/validate', {lead: data})
        .then(() =>
          @_setValidationError(attribute, [])
        )
        .catch((response) =>
          errors = []
          for attr, error of response.data.errors
            if attr == attribute
              errors.push(error)

          @_setValidationError(attribute, errors)
        )
      ),
      @_validationTimeout
    )

  _setValidationError: (attribute, errors) =>
    return unless @_scope.LeadForm

    @_scope.LeadForm.$setError(attribute, '')
    @_scope.LeadForm.$setError(attribute, errors.join('; '))

  _setValidationErrors: (errors) =>
    for attr, error of errors
      @_setValidationError(attr, error)

  _submit: (data) =>
    defer = @_q.defer()

    leadData = {}
    _.extend(leadData, @_scope.lead)
    _.extend(leadData, data)
    leadData.birthday = moment(leadData.birthday).format('YYYY-MM-DD')

    @_save(leadData).then((response) =>
      defer.resolve(response)
      @_rootScope.$emit('company:reload')
    )
    return defer.promise


  _save: (leadData) =>
    defer = @_q.defer()

    @_companyResource
    .put('leads', @_scope.lead.id, {lead: leadData})
    .then((response) =>
      _.extend(@_scope.lead, response.data)
      @_scope.lead.lead_categories =
        _.filter(@_scope.leadCategories, (category) =>
          return @_scope.lead.categories.indexOf(category.id) != -1
        )

      if !@_scope.lead.new_images || @_scope.lead.new_images.length == 0
        defer.resolve(response)
        return

      if !(@_scope.lead.new_images[0] instanceof File)
        defer.resolve(response)
        return

      @_uploadImages(response.data.id, @_scope.lead.new_images)
      .then(
        ((response) =>
          @_scope.lead.new_images = []
          @_scope.lead.images = response.data.images
          defer.resolve(response)
        ),
        ((response) =>
          @_setValidationErrors(response.data.errors)
          defer.resolve('error')
        )
      )
    )
    .catch((response) =>
      @_setValidationErrors(response.data.errors)
      defer.resolve('error')
    )

    return defer.promise

  _uploadImages: (leadId, images) =>
    url = "/api/companies/#{@scope.companyId}/leads/#{leadId}/upload_images"

    @_Upload.upload({
      url: url,
      method: 'PUT',
      headers: @_auth.retrieveData('auth_headers'),
      data: {
        images: images
      },
    })

  _delete: () =>
    return unless confirm('Вы уверены?')

    @_companyResource
    .delete('leads', @_scope.lead.id)
    .then(() =>
      @_deleteFromList(@_scope.lead)
    )

  _deleteFromList: (leadForRemove) =>
    @_scope.$parent.$parent.leads = _.filter(
      @_scope.$parent.$parent.leads,
      ((lead) =>
        return lead.id != leadForRemove.id
      )
    )

  _setStatus: (lead, status) =>
    lead = @_scope.lead

    return unless lead && lead.status != status.id

    lead.status = status.id

    @_companyResource
    .put('leads', lead.id, {status: status.id})
    .then(() =>
      @_rootScope.$emit('company:reload')
    )

  _updatePhoneCodeByCountry: (countryCode) =>
    code = _.findWhere(
      @_scope.leadStatic.references.country_phone_codes,
      {country_code: countryCode},
    )
    @_scope.lead.work_phone_code_id = code?.id
    @_scope.lead.cell_phone_code_id = code?.id

  _addLeadProduct: () =>
    @_ModalService.showModal({
      templateUrl: "/assets/views/companies/deals/new_deal_product.html",
      controller: "CompaniesDealsNewDealProductCtrl",
      inputs: {
        companyId: @_scope.companyId,
        companyCurrency: @_scope.$parent.company.currency,
        entityId: @_scope.lead.id,
        entityName: 'lead',
        product: null,
      },
    })
    .then((modal) =>
      modal.element.modal()
      modal.close.then((result) =>
        @_scope.lead.lead_products.push(result)
        @_scope.lead.potential_amount += result.product.price * result.count
        @_recalculate()
        @_companyResource.put('leads', @_scope.lead.id, {lead: @_scope.lead})
      )
    )

  _recalculate: () =>
    @_scope.lead.productsTotal = 0

    for leadProduct in @_scope.lead.lead_products
      continue if leadProduct.returned_at

      @_scope.lead.productsTotal +=
        leadProduct.product.price * leadProduct.count

    @_scope.lead.vatSum =
      @_scope.lead.productsTotal * ((@_scope.company.vat || 0) / 100)

  _loadTasks: () =>
    new @_ResourceLoader()
    .params({
      company_id: @_routeParams.company_id,
      client_id: @_scope.lead.id,
    })
    .query('tasks')
    .then((response) =>
      @_scope.lead.related_tasks = response.data.entities
      @_scope.lead.related_tasks_count = @_scope.lead.related_tasks.length
    )

  _convert: () =>
    actuallyConvert = () =>
      managerId = @_scope.user.employeeInCompany(@_scope.companyId)?.id
      @_scope.lead.manager_id = managerId

      @_convertLead(@_scope.lead)

    contactsAreFilled =
      @_scope.lead.email &&
      @_scope.lead.work_phone &&
      @_scope.lead.cell_phone

    if contactsAreFilled
      actuallyConvert()
      return

    @_Confirm.confirm({
      title: null,
      question: 'Дозаполнить контакты?',
    },
    (() =>
      @_location.path(
        "/companies/#{@_scope.companyId}/leads/#{@_scope.lead.id}/convert"
      )
    ),
    actuallyConvert)

