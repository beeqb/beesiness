class App.CompaniesGenericStatusesNewCtrl extends App.CompaniesGenericStatusesLoadCtrl

  @_import: [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$element',
    'close',
    'companyId',
    'entityName',
    'ResourceLoader',
  ]

  app.controller('CompaniesGenericStatusesNewCtrl', @factory())

  initialize: () ->
    super()

  _load: () =>
    @_companyResource
    .expand('labels')
    .query("#{@_entityName}es/new")
    .then((response) =>
      @_scope.labels = response.data.labels
      _.extend(@_scope.status, response.data.entity)
      @_scope.status.company_id = @_companyId
    )
