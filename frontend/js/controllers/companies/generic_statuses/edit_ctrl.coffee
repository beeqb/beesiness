class App.CompaniesGenericStatusesEditCtrl extends App.CompaniesGenericStatusesLoadCtrl

  @_import: [
    '$rootScope',
    '$scope',
    '$location',
    '$element',
    'ResourceLoader',
    'close',
    'companyId',
    'statusId',
    'existingStatuses',
    'entityName',
  ]

  app.controller('CompaniesGenericStatusesEditCtrl', @factory())

  initialize: () ->
    super()
    @_existingStatuses = JSON.parse(@_existingStatuses)

    @_scope.delete = @_delete

    @_scope.$watch(
      'status',
      () =>
        return unless @_scope.status.id

        @_scope.availableStatuses = _.filter(
          @_existingStatuses,
          (status) => status.id != @_scope.status.id
        )
    )

  _load: () =>
    @_companyResource
    .expand('labels')
    .get("#{@_entityName}es", @_statusId)
    .then((response) =>
      @_scope.labels = response.data.labels
      @_scope.status = response.data.entity
    )

  _delete: () =>
    return unless confirm('Уверены, что хотите удалить статус?')

    @_element.modal('hide')

    @_scope.status.delete = true # need for parent LeadsIndexCtrl
    @_close(@_scope.status)
