class App.CompaniesGenericStatusesLoadCtrl extends IdFly.AngularClass

  app.controller('CompaniesGenericStatusesLoadCtrl', @factory())

  initialize: () ->
    @_companyResource = new @_ResourceLoader(@_companyId)

    @_scope.entityClass = @_entityName.toCamelCase().capitalize()

    @_scope.status = {
      errors: {},
    }
    @_scope.submit = @_submitModal
    @_scope.colors = [
      '#6254b2',
      '#23b7e5',
      '#27c24c',
      '#fad733',
      '#f05050',
      '#3a3f51',
    ]

    @_load()

  _submitModal: () =>
    data = {}
    data[@_entityName] = @_scope.status

    @_companyResource
    .post("#{@_entityName}es/validate", data)
    .then(() =>
        @_element.modal('hide')
        @_close(@_scope.status)
    )
    .catch((response) =>
      _.extend(@_scope.status.errors, response.data.errors)
    )
