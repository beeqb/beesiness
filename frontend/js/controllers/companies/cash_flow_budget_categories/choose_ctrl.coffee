class App.CompaniesCashFlowBudgetCategoriesChooseCtrl extends IdFly.AngularClass

  @_import: [
    '$scope',
    '$element'
    'close',
    'categories',
    'branch',
    'selectedId',
    '$filter',
    'ResourceLoader',
  ]

  app.controller('CompaniesCashFlowBudgetCategoriesChooseCtrl', @factory())

  initialize: () ->
    @_scope.categories = JSON.parse(@_categories)
    @_scope.branch = @_branch
    @_scope.selectedNode = {}

    if @_selectedId
      @_scope.selectedNode.id = @_selectedId

    @_scope.selectNode = (category) =>
      @_scope.selectedNode = category

    @_scope.selectNone = () =>
      @_scope.selectedNode = {}

    @_scope.submitChoose = () =>
      @_element.modal('hide')

      @_close({
        category: @_scope.selectedNode,
      })
