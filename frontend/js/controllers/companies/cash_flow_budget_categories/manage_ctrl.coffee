class App.CompaniesCashFlowBudgetCategoriesManageCtrl extends IdFly.AngularClass

  @_import: [
    '$scope',
    '$element'
    '$q',
    'close',
    'categories',
    'references',
    'companyId',
    'ResourceLoader',
  ]

  app.controller('CompaniesCashFlowBudgetCategoriesManageCtrl', @factory())

  initialize: () ->
    @_scope.categories = JSON.parse(@_categories)

    @_companyResource = new @_ResourceLoader(@_companyId)
    @_scope.references = JSON.parse(@_references)

    @_scope.treeOptions = {
      # accept: (sourceNodeScope, destNodesScope, destIndex) =>
      #   source = sourceNodeScope.$modelValue
      #   dest = destNodesScope.$parent.$modelValue

      #   console.log source.parent_id, dest?.id
      #   if !source.parent_id && !dest
      #     return false

      #   if source.parent_id && dest && source.parent_id == dest.id
      #     return false

      #   return true

      beforeDrop: (event) =>
        source = event.source.nodeScope.$modelValue
        destination = event.dest.nodesScope.$parent.$modelValue

        cashBookCategory =
          event.dest.nodesScope.$treeScope.$element[0].dataset.category

        data = {
          parent_id: destination?.id || null,
          cash_book_category: cashBookCategory,
        }

        @_updateCategory(source, data)
    }

    @_scope.addCategory = @_addCategory
    @_scope.cancelEditing = @_cancelEditing
    @_scope.updateCategory = @_updateCategory
    @_scope.deleteCategory = (category) =>
      @_scope.categoryToDelete = category

    @_scope.actuallyDeleteCategory = @_actuallyDeleteCategory

    @_scope.cancelDeleteCategory = () =>
      delete @_scope.categoryToDelete

    @_flattenedCategories = {}

    @_scope.searchCategory = @_searchCategory

  _addCategory: (parent, cashBookCategory = null) =>
    if parent
      parent.children.push({
        name: 'Новая статья',
        cash_book_category: parent.cash_book_category,
        kind: 'operating',
        parent_id: parent.id,
        company_id: parent.company_id,
        parent: parent,
        children: [],
      })
    else
      @_scope.categories[cashBookCategory].unshift({
        name: 'Новая статья',
        cash_book_category: cashBookCategory,
        company_id: @_companyId,
        kind: 'operating',
        children: [],
      })

  _setValidationError: (attribute, errors) =>
    @_scope.editCategoryForm.$setError(attribute, '')
    @_scope.editCategoryForm.$setError(attribute, errors.join('; '))

  _setValidationErrors: (errors) =>
    for attr, error of errors
      @_setValidationError(attr, error)

  _cancelEditing: (category) =>
    unless category.id
      if category.parent
        category.parent.children = _.without(category.parent.children, category)
      else
        @_scope.categories[category.cash_book_category] =
          _.without(@_scope.categories[category.cash_book_category], category)

  _updateCategory: (category, data, form) =>
    data = _.chain(category).omit('parent').extend(data).value()

    defer = @_q.defer()

    if category.id
      @_companyResource
      .put(
        'cash_flow_budget_categories',
        category.id,
        {cash_flow_budget_category: data}
      )
      .then((response) =>
        defer.resolve()
      )
      .catch((response) =>
        #FIXME
        for key, messages of response.data.errors
          form.$setError(key, messages.join('; '))

        defer.reject()
      )
    else
      @_companyResource
      .post(
        'cash_flow_budget_categories',
        {cash_flow_budget_category: data}
      )
      .then((response) =>
        delete category.parent
        category.id = response.data.id
        defer.resolve()
      )
      .catch((response) =>
        #FIXME
        for key, messages of response.data.errors
          form.$setError(key, messages.join('; '))

        defer.reject()
      )

      defer.promise

  _actuallyDeleteCategory: (category) =>
    @_companyResource
    .post(
      "cash_flow_budget_categories/#{category.id}/delete_safely",
      {category_after_delete: @_scope.categoryToDelete.categoryAfter.id}
    )
    .then((response) =>
      cashBookCategory = category.cash_book_category
      categoriesTree = @_scope.categories[cashBookCategory]

      if category.parent_id
        parent = @_findInTheTreeById(categoriesTree, category.parent_id)
        parent.children = _.without(parent.children, category)
      else
        @_scope.categories[cashBookCategory] =
          _.without(categoriesTree, category)
    )

  _findInTheTreeById: (categories, id) =>
    for category in categories
      return category if category.id == id

      found = @_findInTheTreeById(category.children, id)

      return found if found

    return null

  _searchCategory: (term, cashBookCategory) =>
      unless @_flattenedCategories[cashBookCategory]
        @_flattenedCategories[cashBookCategory] = @_flattenTree(
          @_scope.categories[cashBookCategory]
        )

      @_scope.searchedCategories = _.filter(
        @_flattenedCategories[cashBookCategory],
        (category) ->
          regexp = new RegExp(term, 'i')
          regexp.test(category.name)
      )

  _flattenTree: (tree) =>
    result = []
    for node in tree
      result.push({id: node.id, name: node.name})
      continue if _.isEmpty(node.children)

      result.push(@_flattenTree(node.children))

    return _.flatten(result)
