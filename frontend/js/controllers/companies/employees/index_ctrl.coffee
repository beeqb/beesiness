class App.CompaniesEmployeesIndexCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$routeParams',
    'ModalService',
    'References',
    '$cookies',
    '$anchorScroll',
    'ResourceLoader',
    'Page',
  ]

  app.controller('CompaniesEmployeesIndexCtrl', @factory())

  initialize: () ->
    @_References.sync()

    @_scope.companyId = +@_routeParams.company_id
    @_companyResource = new @_ResourceLoader(@_scope.companyId)

    @_scope.company = {}
    @_scope.departments = []
    @_scope.employees = []
    @_scope.cachedEmployees = {}

    @_rootScope.$on('departments:reload', @_loadDepartments)

    @_scope.filter = {
      departments: [],
    }

    @cookieName = 'departments[' + @_routeParams.company_id + ']'
    departments = @_cookies.get()
    if departments
      @_scope.filter.departments = JSON.parse(departments)


    @_scope.openInviteDialog = @_openInviteDialog
    @_scope.createDepartment = @_createDepartment
    @_scope.editDepartment = @_editDepartment
    @_scope.deleteDepartment = @_deleteDepartment

    @_scope.toggleDepartment = @_toggleDepartment

    @_scope.page = 1
    @_scope.per = 0
    @_scope.total_count = 0

    @_loadCompany().then(() => @_loadEmployees(@_scope.page))
    @_loadDepartments()
    @_loadProductCategories()

    @_scope.$watch('page', () =>
      if @_scope.cachedEmployees[@_scope.page]
        @_scope.employees = @_scope.cachedEmployees[@_scope.page]
        return

      @_loadEmployees(@_scope.page)
    )

  _loadCompany: () =>
    url = '/api/companies/' + @_routeParams.company_id +
      '?expand=references,selectable_roles'
    @_http
    .get(url)
    .then((response) =>
      @_scope.companyStatic = {
        references: response.data.references,
        selectableRoles: response.data.selectable_roles,
      }
      @_scope.company = response.data.entity
      @_Page.setTitle('employees', @_scope.company.name)
    )

  _loadDepartments: () =>
    @_companyResource
    .include('employees_count', 'head')
    .query('departments')
    .then((response) =>
      @_scope.departments = response.data.entities
    )

  _loadProductCategories: () =>
    @_companyResource
    .query('product_categories')
    .then((response) =>
      @_scope.product_categories = response.data.entities
    )

  _loadEmployees: (page) =>
    @_companyResource
    .expandLabelsAndReferences()
    .include(
      'roles',
      'user',
      'employees_departments',
      'forbidden_product_categories',
      'employee_phones',
      'tasks_statistic',
      'income',
      'income_by_days'
    )
    .params({
      page: page,
      departments: @_scope.filter.departments,
    })
    .query('employees')
    .then((response) =>
      @_scope.employees = response.data.entities
      @_scope.per = response.data.per
      @_scope.total_count = response.data.total_count

      @_scope.cachedEmployees[page] = @_scope.employees

      @_scope.employeeStatic = {
        labels: response.data.labels,
        references: response.data.references,
      }

      for employee in @_scope.employees
        employee.company = @_scope.company

        for department in employee.employees_departments
          department.department = _.findWhere(
            @_scope.departments,
            {id: department.department_id},
          )

        employee.forbidden_product_category_ids =
          _(employee.forbidden_product_categories).pluck('id')
    )

  _openInviteDialog: () =>
    @_ModalService
    .showModal({
      templateUrl: 'assets/views/companies/employees/invite.html',
      controller: 'CompaniesEmployeesInviteCtrl',
      inputs: {
        departments: JSON.stringify(@_scope.departments),
        companyId: @_routeParams.company_id
      }
    })
    .then((modal) =>
      modal.element.modal()
    )

  _createDepartment: () =>
    @_ModalService.showModal({
      templateUrl: "/assets/views/companies/departments/new.html",
      controller: "CompaniesDepartmentsNewCtrl",
      inputs: {
        companyId: @_routeParams.company_id,
      },
    })
    .then((modal) =>
      modal.element.modal()
      modal.close.then((result) =>
        url = '/api/companies/' + @_routeParams.company_id + '/departments'
        @_http
        .post(url, {department: result})
        .then((response) =>
          response.data.employees_count = 0
          @_scope.departments.push(response.data)
        )
      )
    )

  _editDepartment: (departmentId) =>
    @_ModalService.showModal({
      templateUrl: "/assets/views/companies/departments/new.html",
      controller: "CompaniesDepartmentsEditCtrl",
      inputs: {
        companyId: @_routeParams.company_id,
        departmentId: departmentId,
      },
    })
    .then((modal) =>
      modal.element.modal()
      modal.close.then((result) =>
        if result.delete
          @_deleteDepartment(departmentId)
        else
          @_updateDepartment(departmentId, result)
      )
    )

  _updateDepartment: (departmentId, data) =>
    url = '/api/companies/' + @_routeParams.company_id +
      '/departments/' + departmentId
    @_http
    .put(url, {department: data})
    .then((response) =>
      departmentIndex = _.findIndex(@_scope.departments, {
        id: response.data.id
      })
      _.extend(@_scope.departments[departmentIndex], response.data)
    )

  _deleteDepartment: (departmentId) =>
    url = '/api/companies/' + @_routeParams.company_id +
      '/departments/' + departmentId
    @_http
    .delete(url)
    .then(() =>
      @_scope.departments = _.filter(
        @_scope.departments,
        ((department) =>
          return department.id != departmentId
        )
      )
    )

  _resetCache: () =>
    @_scope.cachedEmployees = {}

  _toggleDepartment: (departmentId) =>
    if @_scope.filter.departments.indexOf(departmentId) == -1
      @_scope.filter.departments.push(departmentId)
    else
      @_scope.filter.departments = _.without(@_scope.filter.departments, departmentId)

    if @_scope.filter.departments.length > 0
      @_cookies.put(@cookieName, JSON.stringify(@_scope.filter.departments))
    else
      @_cookies.remove(@cookieName)

    @_scope.page = 1
    @_resetCache()
    @_loadEmployees(@_scope.page)
