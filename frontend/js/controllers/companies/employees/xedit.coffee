class App.CompaniesEmployeesXEditCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$location',
    '$routeParams',
    '$timeout',
    '$q',
    'mapConfig',
    'References',
    'ResourceLoader',
  ]

  app.controller('CompaniesEmployeesXEditCtrl', @factory())

  initialize: () =>
    @_companyResource = new @_ResourceLoader(@_routeParams.company_id)

    if @_routeParams.employee_id
      @_scope.employee = {}
      @_scope.employee.user = {}
      @_loadCompany().then(() =>
        @_companyResource
        .include(
          'roles',
          'user',
          'employees_departments',
          'employee_phones',
          'tasks_statistic'
        )
        .expandLabelsAndReferences()
        .get('employees', @_routeParams.employee_id)
        .then((response) =>
          @_scope.employee = response.data.entity
          @_scope.employee.company = @_scope.company
          @_scope.employeeStatic = {
            labels: response.data.labels,
            references: response.data.references,
          }
          @_scope.employee.expanded = true
          @_scope.employee.canBeExpanded = false
        )
      )
    else
      @_scope.employee.canBeExpanded = true

    @_scope.employee.errors = {}
    @_scope.map = @_mapConfig

    @_scope.$watchCollection('departments', (newValue, oldValue) =>
      return if newValue == oldValue

      for employee_department in @_scope.employee.employees_departments
        employee_department.department = _.findWhere(
          @_scope.departments,
          {id: employee_department.department_id}
        )

      @_scope.employee.selected_employees_departments =
        @_scope.employee.employees_departments
    )

    @_scope.loadTasks = @_loadTasks
    @_scope.submit = @_submit
    @_scope.addPhone = @_addPhone
    @_scope.deletePhone = @_deletePhone
    @_scope.changePhone = @_changePhone
    @_scope.dismiss = @_dismiss
    @_scope.validate = @_validate
    @_scope.isBirthdayEditable = @_isBirthdayEditable
    @_scope.isCashboxNameEditable = @_isCashboxNameEditable
    @_scope.employee.selected_phones = @_scope.employee.employee_phones

    @_scope.addEmployeeDepartment = @_addEmployeeDepartment
    @_scope.changeEmployeeDepartment = @_changeEmployeeDepartment
    @_scope.deleteEmployeeDepartment = @_deleteEmployeeDepartment
    @_scope.getAvailableDepartments = @_getAvailableDepartments
    @_scope.employee.selected_employees_departments = @_scope.employee.employees_departments

    @_scope.isOnVacation = ((employee) -> employee.user_activity_id == 2)

    @_initValidation()
    @_initWatchers()

    @_scope.workPhone = _.find(@_scope.employee.employee_phones, ((phone) => return phone.kind == 'work'))
    @_scope.mobilePhone = _.find(@_scope.employee.employee_phones, ((phone) => return phone.kind == 'cell'))
    #debugger

  _loadCompany: () =>
    @_companyResource
    .expand('references,selectable_roles')
    .query('')
    .then((response) =>
      @_scope.companyStatic = {
        references: response.data.references,
        selectableRoles: response.data.selectable_roles,
      }
      @_scope.company = response.data.entity
    )

  _loadEmployeeActivity: () =>
    @_companyResource
    .query("employees/#{@_scope.employee.id}/activity")
    .then((response) =>
      employee = angular.copy(@_scope.employee)
      employee.activity = response.data
      @_scope.employee = employee
    )

  _getAvailableDepartments: (currentDepartmentId = null) =>
    busyIds = _.pluck(
      @_scope.employee.selected_employees_departments,
      'department_id'
    )

    availableDepartments = _.filter(
      @_scope.departments,
      ((department) =>
        return busyIds.indexOf(department.id) == -1
      )
    )

    if currentDepartmentId
      currentDepartment = _.findWhere(@_scope.departments, {
        id: currentDepartmentId,
      })
      if currentDepartment
        availableDepartments.push(currentDepartment)

    return availableDepartments


  _validate: (attribute, value) =>
    if !@_scope.EmployeesViewForm || !@_scope.EmployeesViewForm.$visible
      return

    if @_validateTimeoutPromise
      @_timeout.cancel(@_validateTimeoutPromise)

    @_validateTimeoutPromise = @_timeout(
      (() =>
        data = {}
        _.extend(data, @_scope.employee)
        data[attribute] = value

        if @_scope.employee.id
          @_companyResource.params({id: @_scope.employee.id})

        @_companyResource
        .post('employees/validate', {employee: data})
        .then(() =>
          @_setValidationError(attribute, [])
        )
        .catch((response) =>
          errors = []
          for attr, error of response.data.errors
            if attr == attribute
              errors.push(error)

          @_setValidationError(attribute, errors)
        )
      ),
      @_validationTimeout
    )

  _initValidation: () =>
    @_validationTimeout = 500 #msec
    @_scope.$watchCollection('employee', (newCollection, oldCollection) =>
      attr = null
      for k,v of newCollection
        if v != oldCollection[k]
          attr = k
          break

      if !attr || attr == 'errors'
        return

      @_validate(attr)
    )

  _setValidationError: (attribute, errors) =>
    @_scope.EmployeesViewForm.$setError(attribute, '')
    @_scope.EmployeesViewForm.$setError(attribute, errors.join('; '))

  _setValidationErrors: (errors) =>
    for attr, error of errors
      @_setValidationError(attr, error)

  _initWatchers: () =>
    @_scope.watchedData = {}

    expandedWatchUnbind = @_scope.$watch('employee.expanded', () =>
      return if !@_scope.employee || !@_scope.employee.expanded

      @_loadEmployeeActivity()
      expandedWatchUnbind()
    )

    @_scope.$watch('employee.user.roles_in_companies', () =>
      return unless @_scope.employee.user.roles_in_companies

      @_scope.employee.selected_roles = []
      @_scope.employee.roles = []

      rolesInCompany = _.find(
        @_scope.employee.user.roles_in_companies,
        (item) =>
          found = false
          for role in item.roles
            if role.name.indexOf('employee') != -1 && role.resource_id == @_scope.employee.company_id
              found = true
              break
          return found
      )

      if rolesInCompany
        @_scope.employee.selected_roles = _.pluck(rolesInCompany.roles, 'name')
        @_scope.employee.roles = rolesInCompany.roles
    )

  _submit: (data) =>
    employeeData = {}
    _.extend(employeeData, @_scope.employee)
    _.extend(employeeData, data)

    cashboxNameChanged = data.cashbox_name != @_scope.employee.cashbox_name

    employeeData.user_attributes = angular.copy(employeeData.user)

    employeeData.vacation_start =
      moment(employeeData.vacation_start).format('YYYY-MM-DD')
    employeeData.vacation_end =
      moment(employeeData.vacation_end).format('YYYY-MM-DD')

    employeeData.user_attributes.birthday =
      moment(data.birthday).format('YYYY-MM-DD')

    unless @_isCashboxNameEditable(employeeData)
      employeeData.cashbox_name = null

    delete employeeData.user

    @_companyResource
    .include('user', 'employees_departments', 'forbidden_product_categories')
    .put('employees', @_scope.employee.id, {employee: employeeData})
    .then((response) =>
      _.extend(@_scope.employee, response.data)

      if 'employee_director' in @_scope.employee.selected_roles
        @_removeOtherDirectorRoles(@_scope.employee.id)

      @_rootScope.$emit('departments:reload')

      return unless @_scope.employee.user.id == @_rootScope.user.id

      if cashboxNameChanged
        @_rootScope.$emit('user:changed')
      else
        @_rootScope.$emit('user:reloadPermissions')
    )
    .catch((response) =>
      @_setValidationErrors(response.data.errors)
      @_q.reject(response.data.errors)
    )

  _removeOtherDirectorRoles: (exceptEmployeeId) =>
    selectedRoleRemoved = false
    roleRemoved = false
    for employee in @_scope.employees
      if employee.id == exceptEmployeeId
        continue
      if selectedRoleRemoved == false
        index = employee.selected_roles.indexOf('employee_director')
        if index != -1
          @_scope.$parent.$applyAsync(()->
            delete employee.selected_roles[index]
          )
          selectedRoleRemoved = true

      if roleRemoved == false
        role = _.findWhere(employee.roles, {name: 'employee_director'})
        if role
          @_scope.$parent.$applyAsync(() ->
            employee.roles = _.filter(employee.roles, (role) ->
              return role.name != 'employee_director'
            )
          )
          roleRemoved = true

      if roleRemoved && selectedRoleRemoved
        break

  _addPhone: (kind) =>
    @_scope.employee.selected_phones.push({
      id: (new Date).getTime()
      employee_id: @_scope.employee.id,
      country_phone_code_id: null,
      phone: '',
      kind: kind,
    })

  _changePhone: (id, attr, data) =>
    phone = _.find(@_scope.employee.selected_phones, (phone) =>
      return phone.id == id
    )
    if phone
      phone[attr] = data

    if attr == 'country_phone_code_id'
      phone.country_phone_code = _.findWhere(
        @_scope.employeeStatic.references.country_phone_codes, {
          id: data,
        })

  _deletePhone: (phone) =>
    @_scope.employee.selected_phones = _.without(
      @_scope.employee.selected_phones,
      phone
    )

  _addEmployeeDepartment: () =>
    @_scope.employee.selected_employees_departments.push({
      id: (new Date).getTime()
      company_id: @_scope.company.id,
      employee_id: @_scope.employee.id,
      is_head: false,
      position: '',
    })

  _changeEmployeeDepartment: (id, attr, data) =>
    employeeDepartment = _.find(@_scope.employee.selected_employees_departments, (ed) =>
      return ed.id == id
    )
    if employeeDepartment
      employeeDepartment[attr] = data

    if attr == 'department_id'
      employeeDepartment.department = _.findWhere(
        @_scope.departments, {
          id: data,
        })

  _deleteEmployeeDepartment: (employeeDepartment) =>
    @_scope.employee.selected_employees_departments = _.without(
      @_scope.employee.selected_employees_departments,
      employeeDepartment
    )

  _isBirthdayEditable: () =>
    user = @_scope.user
    employee = @_scope.employee

    canUpdateBirthday = user.can('update_birthday', 'Employee', employee.id)
    canAddBirthday = user.can('add_birthday', 'Employee', employee.id)

    employee.user.birthday && canUpdateBirthday ||
    !employee.user.birthday && canAddBirthday

  _isCashboxNameEditable: (data) =>
    unless data.participation_in_sales == 'yes'
      return false

    allowed_roles = [
      'employee_hr',
      'employee_investor',
      'employee_director',
      'employee_accountant',
      'employee_head_of_sales_department',
      'employee_manager_of_sales_department',
      'employee_head_of_marketing_department',
      'employee_marketer',
      'employee_purchasing_manager',
      'employee_admin',
    ]

    _.intersection(data.selected_roles, allowed_roles).length != 0

  _dismiss: () =>
    return unless confirm('После увольнения сотрудника все его задачи будут назначены на вас.')

    @_companyResource
    .post("employees/#{@_scope.employee.id}/dismiss")
    .then((response) =>
      _.extend(@_scope.employee, response.data)
    )

  _loadTasks: () =>
    resource = new @_ResourceLoader()

    resource
    .query('tasks', {executor_id: @_scope.employee.id})
    .then((response) =>
      @_scope.employee.assigned_tasks = response.data.entities
    )
