class App.CompaniesEmployeesInviteCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$element',
    'close',
    'departments',
    'companyId'
  ]

  app.controller('CompaniesEmployeesInviteCtrl', @factory())

  initialize: () ->
    @_scope.data = {
      role: null,
      email: null,
      errors: {},
      labels: {
        email: 'E-mail',
        position: 'Должность',
        department_id: 'Департамент',
      }
    }
    @_scope.departments = JSON.parse(@_departments)
    @_scope.invite = @_invite

  _invite: () =>
    url = '/api/companies/' + @_companyId + '/invite_user'
    @_http
    .post(url, @_scope.data)
    .then(
      (() =>
        @_element.modal('hide');
        @_close()
      ),
      ((response) =>
        _.extend(@_scope.data.errors, response.data.errors)
      ),

    )
