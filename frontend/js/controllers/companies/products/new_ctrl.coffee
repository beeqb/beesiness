class App.CompaniesProductsNewCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$routeParams',
    '$auth',
    '$cookies',
    '$timeout',
    '$q',
    'Upload',
    'ModalService',
  ]

  app.controller('CompaniesProductsNewCtrl', @factory())

  initialize: () ->
    @_scope.companyId = @_routeParams.company_id
    @_scope.product = {
      categories: [],
      errors: {},
    }
    @_scope.submit = @_submit
    @_scope.validate = @_submit
    @_scope.createCategory = @_createCategory
    @_load()
    @_loadCompany()

  _initValidation: () =>
    @_validationTimeout = 500 #msec
    @_scope.$watchCollection('product', (newCollection, oldCollection) =>
      attr = null
      for k,v of newCollection
        if v != oldCollection[k]
          attr = k
          break

      if !attr || attr == 'errors'
        return

      @_validate(attr)
    )

  _validate: (attribute) =>
    if @_validateTimeoutPromise
      @_timeout.cancel(@_validateTimeoutPromise)

    @_validateTimeoutPromise = @_timeout(
      (() =>
        url = '/api/companies/' + @_routeParams.company_id + '/products/validate'
        if @_routeParams.id
          url += '?id=' + @_routeParams.id

        @_http
        .post(url, {product: @_scope.product})
        .then(
          (() =>
            @_scope.product.errors = {}
          ),
          ((response) =>
            for attr, error of response.data.errors
              if attr == attribute
                @_scope.product.errors[attribute] = error
          )
        )
      ),
      @_validationTimeout
    )

  _load: () =>
    @_http
    .get('/api/companies/' + @_routeParams.company_id + '/products/new' +
        '?expand=labels,references')
    .then((response) =>
      @_scope.productStatic = {
        labels: response.data.labels,
        references: response.data.references,
      }
      _.extend(@_scope.product, response.data.entity)
      cookieName = 'product_categories[' + @_scope.companyId + ']'
      productCategories = @_cookies.get(cookieName)
      if productCategories
        @_scope.product.categories = JSON.parse(productCategories)

      @_initValidation()
    )

  _loadCompany: () =>
    @_http
    .get('/api/companies/' + @_routeParams.company_id +
        '?include=product_categories')
    .then((response) =>
      @_scope.company = response.data.entity
    )

  _submit: () =>
    @_checkProductExistence({
      barcode: @_scope.product.barcode,
      price: @_scope.product.price,
    })
    .then((response) =>
      if response.data.entities.length == 0
        @_save()
      else
        @_showCheckProductExistenceModal(response.data.entities[0])
    )

  _showCheckProductExistenceModal: (product) =>
    @_ModalService.showModal({
      templateUrl: "/assets/views/companies/products/modals/checkProductExistence.html",
      controller: "CompaniesProductCheckProductExistenceCtrl",
      inputs: {
        product: product,
      },
    })
    .then((modal) =>
      modal.element.modal()
    )

  _save: () =>
    url = '/api/companies/' + @_routeParams.company_id + '/products'
    @_http
    .post(url, {product: @_scope.product})
    .then(
      ((response) =>
        if !@_scope.product.images || @_scope.product.images.length == 0
          @_rootScope.$emit('user:reloadPermissions')
          @_location.path('/companies/' + @_routeParams.company_id + '/products')
          return

        @_uploadImages(response.data.id, @_scope.product.images)

      ),
      ((response) =>
        _.extend(@_scope.product.errors, response.data.errors)
      )
    )

  _checkProductExistence: (params) =>
    url = '/api/companies/' + @_routeParams.company_id + '/products'
    for k, v of params
      if url.indexOf('?') == -1
        url += '?'
      else
        url += '&'
      url += (k + '=' + v)

    @_http.get(url)

  _uploadImages: (productId, images) =>
    url = '/api/companies/' + @_routeParams.company_id +
      '/products/' + productId + '/upload_images'

    @_Upload.upload({
      url: url,
      method: 'PUT',
      headers: @_auth.retrieveData('auth_headers'),
      data: {
        images: images
      },
    })
    .then(
      ((response) =>
        @_rootScope.$emit('user:reloadPermissions')
        @_location.path('/companies/' + @_routeParams.company_id + '/products')
      ),
      ((response) =>
        _.extend(@_scope.product.errors, response.data.errors)
      )
    )


  _createCategory: () =>
    @_ModalService.showModal({
      templateUrl: "/assets/views/companies/product_categories/new.html",
      controller: "CompaniesProductCategoriesNewCtrl",
      inputs: {
        companyId: @_routeParams.company_id,
      },
    })
    .then((modal) =>
      modal.element.modal()
      modal.close.then((result) =>
        url = '/api/companies/' + @_routeParams.company_id + '/product_categories'
        @_http
        .post(url, {product_category: result})
        .then((response) =>
          @_scope.company.product_categories.push(response.data)
          @_scope.product.categories.push(response.data.id)
        )
      )
    )
