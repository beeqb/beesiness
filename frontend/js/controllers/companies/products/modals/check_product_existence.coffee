class App.CompaniesProductCheckProductExistenceCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$element',
    'close',
    'product',
  ]

  app.controller('CompaniesProductCheckProductExistenceCtrl', @factory())

  initialize: () ->
    @_scope.product = @_product
    @_scope.submit = @_submit

  _submit: () =>
    url = '/api/companies/' + @_scope.product.company_id +
      '/products/' + @_scope.product.id
    @_http
    .delete(url)
    .then((response) =>
      @_element.modal('hide');
      @_close()
    )

