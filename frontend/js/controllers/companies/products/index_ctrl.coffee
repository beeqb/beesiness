class App.CompaniesProductsIndexCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$routeParams',
    '$cookies',
    '$location',
    'ModalService',
    'localStorageService',
    'ResourceLoader',
    'Page',
  ]

  app.controller('CompaniesProductsIndexCtrl', @factory())

  initialize: () ->
    @_scope.companyId = +@_routeParams.company_id
    @_companyResource = new @_ResourceLoader(@_scope.companyId)
    @_scope.company = {}
    @_scope.products = []
    @_scope.cachedProducts = {}
    @_scope.productCategories = []
    @_scope.filter = {
      categories: [],
    }

    @cookieName = 'product_categories[' + @_scope.companyId + ']'
    productCategories = @_cookies.get(@cookieName)
    if productCategories
      @_scope.filter.categories = JSON.parse(productCategories)

    @_scope.createCategory = @_createCategory
    @_scope.editCategory = @_editCategory
    @_scope.toggleCategory = @_toggleCategory

    @_scope.page = 1
    @_scope.per = 10
    @_scope.total_count = 0

    @_loadCompany()
    @_loadCategories()
    @_loadProducts(@_location.search()?.page || @_scope.page)
    @_rootScope.$on('categories:reload', @_loadCategories)

    @_scope.$watch('page', (newValue, oldValue) =>
      return if newValue == oldValue

      if @_scope.cachedProducts[@_scope.page]
        @_scope.products = @_scope.cachedProducts[@_scope.page]
        return

      @_loadProducts(@_scope.page)
    )

  _loadCompany: () =>
    resource = new @_ResourceLoader()

    resource
    .get('companies', @_scope.companyId)
    .then((response) =>
      @_scope.company = response.data.entity
      @_Page.setTitle('products', @_scope.company.name)
    )

  _resetCache: () =>
    @_scope.cachedProducts = {}

  _toggleCategory: (categoryId) =>
    if categoryId in @_scope.filter.categories
      @_scope.filter.categories = _.without(@_scope.filter.categories, categoryId)
    else
      @_scope.filter.categories.push(categoryId)

    if @_scope.filter.categories.length > 0
      @_cookies.put(@cookieName, JSON.stringify(@_scope.filter.categories))
    else
      @_cookies.remove(@cookieName)

    @_resetCache()
    @_scope.page = 1
    @_loadProducts(@_scope.page)

  _loadCategories: () =>
    @_companyResource
    .include('products_count')
    .query('product_categories')
    .then((response) =>
      @_scope.productCategories = response.data.entities
    )

  _loadProducts: (page) =>
    params = {page: page, page_size: @_scope.per}

    if @_scope.filter.categories.length > 0
      _.extend(params, {'by_categories[]': @_scope.filter.categories})

    @_companyResource
    .params(params)
    .include(
      'category',
      'deals_count',
      'current_month_income',
      'current_month_income_by_days',
      'current_month_plan',
      'popularity',
      'history_length',
      'sales_percent',
      'income_percent',
      'clients_purchased_percent'
    )
    .expandLabelsAndReferences()
    .query('products')
    .then((response) =>
      data = response.data

      @_scope.productStatic = {
        labels: data.labels,
        references: data.references,
      }
      @_scope.products = data.entities
      _.setAll(
          @_scope.products,
          'quantity',
        (product) -> parseFloat(product.quantity)
      )
      @_scope.total_count = data.total_count
      @_scope.cachedProducts[@_scope.page] = @_scope.products
    )

  _createCategory: () =>
    @_ModalService.showModal({
      templateUrl: "/assets/views/companies/product_categories/new.html",
      controller: "CompaniesProductCategoriesNewCtrl",
      inputs: {
        companyId: @_routeParams.company_id,
      },
    })
    .then((modal) =>
      modal.element.modal()
      modal.close.then((result) =>
        @_companyResource
        .post('product_categories', {product_category: result})
        .then((response) =>
          response.data.products_count = 0
          @_scope.productCategories.push(response.data)
        )
      )
    )

  _editCategory: (categoryId) =>
    @_ModalService.showModal({
      templateUrl: "/assets/views/companies/product_categories/new.html",
      controller: "CompaniesProductCategoriesEditCtrl",
      inputs: {
        companyId: @_routeParams.company_id,
        productCategoryId: categoryId,
      },
    })
    .then((modal) =>
      modal.element.modal()
      modal.close.then((result) =>
        if result.delete
          @_deleteCategory(categoryId, result)
        else
          @_updateCategory(categoryId, result)
      )
    )

  _updateCategory: (categoryId, data) =>
    @_companyResource
    .put('product_categories', categoryId, {product_category: data})
    .then((response) =>
      categoryIndex = _.findIndex(@_scope.productCategories, {
        id: response.data.id
      })
      _.extend(@_scope.productCategories[categoryIndex], response.data)
    )

  _deleteCategory: (categoryId, data) =>
    @_companyResource
    .post(
      "product_categories/#{categoryId}/delete_safely",
      {
        category_after_delete: data.category_after_delete,
        delete_with_products: data.delete_with_products,
      }
    )
    .then(() =>
      @_scope.productCategories = _.filter(
        @_scope.productCategories,
        ((category) =>
          return category.id != categoryId
        )
      )
      @_loadCategories()
      @_loadProducts()
    )
