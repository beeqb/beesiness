class App.CompaniesProductsXEditCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    '$routeParams',
    '$auth',
    '$cookies',
    '$timeout',
    '$q',
    '$filter',
    'Upload',
    'ModalService',
    'localStorageService',
    'ResourceLoader',
  ]

  app.controller('CompaniesProductsXEditCtrl', @factory())

  initialize: () ->
    @_companyResource = new @_ResourceLoader(@_routeParams.company_id)
    if @_routeParams.product_id
      @_scope.product = {}
      @_scope.productStatic = {}
      @_loadCompany()

      @_companyResource
      .query('product_categories')
      .then((response) =>
        @_scope.productCategories = response.data.entities
      )

      @_companyResource
      .include(
        'category',
        'deals_count',
        'history_length',
        'current_month_income',
        'current_month_income_by_days',
        'current_month_plan',
        'popularity',
        'clients_purchased_percent',
        'sales_percent',
        'income_percent'
      )
      .expandLabelsAndReferences()
      .get('products', @_routeParams.product_id).then((response) =>
        @_scope.product = response.data.entity
        @_scope.productStatic = {
          labels: response.data.labels,
          references: response.data.references,
        }

        @_scope.product.expanded = true
        @_scope.product.canBeExpanded = false
      )
    else
      @_scope.product.canBeExpanded = true

    @_scope.canBeAddedToDeal = @_canBeAddedToDeal
    @_scope.expand = @_expand
    @_scope.product.errors = {}
    @_scope.submit = @_submit
    @_scope.validate = @_validate
    @_scope.delete = @_delete
    @_scope.removeImage = @_removeImage

    @_scope.togglePreview = @_togglePreview
    @_scope.addProductToDeal = @_addProductToDeal

    @_scope.tradeMargin = @_tradeMargin
    @_scope.margin = @_margin

    @_scope.isEditable = @_isEditable
    @_scope.section = {}

    @_initValidation()

  _loadCompany: () =>
    @_companyResource
    .include('product_categories')
    .query('')
    .then((response) =>
      @_scope.company = response.data.entity
    )

  _addProductToDeal: (product) =>
    @_ModalService.showModal({
      templateUrl: "/assets/views/companies/deals/new_deal_product.html",
      controller: "CompaniesDealsNewDealProductCtrl",
      inputs: {
        companyId: @_scope.companyId,
        companyCurrency: @_scope.company.currency,
        product: JSON.stringify(product),
        entityName: 'deal',
        entityId: null,
      },
    })
    .then((modal) =>
      debugger
      modal.element.modal()
      modal.close.then((result) =>
        key = 'deal_products_for_pos_form_' + @_scope.companyId
        dealProducts = @_localStorageService.get(key)
        if dealProducts == null
          dealProducts = []
        dealProducts.push(result)
        @_localStorageService.set(key, dealProducts)
      )
    )

  _initValidation: () =>
    @_validationTimeout = 500 #msec
    @_scope.$watchCollection('product', (newCollection, oldCollection) =>
      attr = null
      for k,v of newCollection
        if v != oldCollection[k]
          attr = k
          break

      if !attr || attr == 'errors'
        return

      @_validate(attr)
    )

  _validate: (attribute, value) =>
    if !@_scope.ProductForm || !@_scope.ProductForm.$visible
      return

    if @_validateTimeoutPromise
      @_timeout.cancel(@_validateTimeoutPromise)

    @_validateTimeoutPromise = @_timeout(
      (() =>
        data = {}
        _.extend(data, @_scope.product)
        data[attribute] = value

        url = '/api/companies/' + @_routeParams.company_id + '/products/validate'
        if @_scope.product.id
          url += '?id=' + @_scope.product.id

        @_http
        .post(url, {product: data})
        .then(
          (() =>
            @_setValidationError(attribute, [])
          ),
          ((response) =>
            errors = []
            for attr, error of response.data.errors
              if attr == attribute
                errors.push(error)

            @_setValidationError(attribute, errors)
          )
        )
      ),
      @_validationTimeout
    )

  _setValidationError: (attribute, errors) =>
    @_scope.ProductForm.$setError(attribute, '')
    @_scope.ProductForm.$setError(attribute, errors.join('; '))

  _setValidationErrors: (errors) =>
    for attr, error of errors
      @_setValidationError(attr, error)

  _submit: (data) =>
    productData = {}
    _.extend(productData, @_scope.product)
    _.extend(productData, data)

    deferred = @_q.defer()
    @_checkProductExistence({
      barcode: productData.barcode,
      price: productData.price,
    })
    .then((response) =>
      if response.data.entities.length == 0 || (
        response.data.entities.length == 1 && response.data.entities[0].id == @_scope.product.id
      )
        @_save(productData).then(
          ((response) =>
            deferred.resolve(response)
            @_rootScope.$emit('categories:reload')

            currentActiveSection = @_scope.section.active
            @_scope.section.active = null
            @_timeout((() => @_scope.section.active = currentActiveSection), 0)
          ),
          (() =>
            deferred.resolve('error')
          )
        )
      else
        deferred.resolve('error')
        @_showCheckProductExistenceModal(response.data.entities[0])
    )

    return deferred.promise

  _checkProductExistence: (params) =>
    url = '/api/companies/' + @_routeParams.company_id + '/products'
    for k, v of params
      if url.indexOf('?') == -1
        url += '?'
      else
        url += '&'
      url += (k + '=' + v)

    @_http.get(url)

  _showCheckProductExistenceModal: (existedProduct) =>
    @_ModalService.showModal({
      templateUrl: "/assets/views/companies/products/modals/checkProductExistence.html",
      controller: "CompaniesProductCheckProductExistenceCtrl",
      inputs: {
        product: existedProduct,
      },
    })
    .then((modal) =>
      modal.element.modal()
      modal.close.then(() =>
        @_deleteFromList(existedProduct)
      )
    )

  _save: (productData) =>
    defer = @_q.defer()

    @_companyResource
    .include('category')
    .put('products', @_scope.product.id, {product: productData})
    .then(
      ((response) =>
        _.extend(@_scope.product, response.data)

        if !@_scope.product.new_images || @_scope.product.new_images.length == 0
          defer.resolve(response)
          return

        if !(@_scope.product.new_images[0] instanceof File)
          defer.resolve(response)
          return

        @_uploadImages(response.data.id, @_scope.product.new_images)
        .then(
          ((response) =>
            @_scope.product.new_images = []
            @_scope.product.images = response.data.images
            defer.resolve(response)
          ),
          ((response) =>
            @_setValidationErrors(response.data.errors)
            defer.reject('error')
          )
        )

      ),
      ((response) =>
        @_setValidationErrors(response.data.errors)
        defer.reject('error')
      )
    )

    return defer.promise

  _uploadImages: (productId, images) =>
    url = '/api/companies/' + @_routeParams.company_id +
      '/products/' + productId + '/upload_images'

    @_Upload.upload({
      url: url,
      method: 'PUT',
      headers: @_auth.retrieveData('auth_headers'),
      data: {
        images: images
      },
    })

  _removeImage: (index) =>
    url = '/api/companies/' + @_routeParams.company_id +
      '/products/' + @_scope.product.id + '/images/' + index

    @_http.delete(url).then((response) =>
      @_scope.product.images = response.data.images

      previewIndex = @_scope.product.preview_index

      # при удалении картинки, стоящей в списке
      # до превью, нужно подправить индекс превью
      if index <= previewIndex and previewIndex isnt 0
        @_scope.product.preview_index -= 1
    )

  _togglePreview: (index) =>
    @_scope.product.preview_index = index

  _delete: () =>
    if !confirm('Вы уверены?')
      return

    url = '/api/companies/' + @_routeParams.company_id +
      '/products/' + @_scope.product.id

    @_http
    .delete(url)
    .then((response) =>
      @_deleteFromList(@_scope.product)
    )

  _deleteFromList: (productForRemove) =>
    @_scope.$parent.$parent.products = _.filter(
      @_scope.$parent.$parent.products,
      ((product) =>
        return product.id != productForRemove.id
      )
    )

  _isEditable: () =>
    product = @_scope.product
    product.id && @_rootScope.user.can('update', 'Product', product.id)

  _tradeMargin: () =>
    product = @_scope.product

    return unless product.price && product.first_cost

    (product.price - product.first_cost) / product.first_cost * 100

  _margin: () =>
    product = @_scope.product

    return unless product.price && product.first_cost

    (product.price - product.first_cost) / product.price * 100

  _canBeAddedToDeal: (product) =>
    !@_categoryIsForbidden(product.category.id) &&
      product.activity not in ['no', 'archive']

  _categoryIsForbidden: (categoryId) =>
    employee = @_scope.user.employeeInCompany(@_scope.companyId)

    return true unless employee

    categoryId in _(employee.forbidden_product_categories).pluck('id')
