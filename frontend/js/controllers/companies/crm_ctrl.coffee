class App.CompaniesCRMCtrl extends IdFly.AngularClass
  @_import: [
    '$routeParams',
    '$scope',
    'ResourceLoader',
    'Page',
  ]

  app.controller('CompaniesCRMCtrl', @factory())

  initialize: () =>
    @_scope.companyId = +@_routeParams.company_id

    @_companyResource = new @_ResourceLoader(@_scope.companyId)

    @_scope.incomePercentDone = 0
    @_scope.incomePercentNotDone = 0
    @_scope.salesFunnelWidgetData = {}

    @_scope.$watch('user.employees', (newValue, oldValue) =>
      return if oldValue == newValue

      employee = @_scope.user.employeeInCompany(@_scope.companyId)

      @_companyResource
      .query("employees/#{employee.id}/activity")
      .then((response) =>
        employee.activity = response.data
        @_scope.userEmployee = employee
      )
    )

    @_load()

  _load: () =>
    resource = new @_ResourceLoader()

    resource
    .include(
      'leads_count',
      'clients_count',
      'products_count',
      'deals_count',
      'last_clients',
      'last_deals',
      'last_leads',
      'indicators_dynamic',
      'plan_income',
      'actual_income',
      'created_clients_count_by_days',
      'actual_income_by_days',
      'lead_statuses',
      'client_statuses',
      'deal_statuses'
    )
    .expand('references')
    .get('companies', @_scope.companyId)
    .then((response) =>
      @_scope.company = response.data.entity

      @_Page.setTitle('crm', @_scope.company.name)

      @_scope.companyStatic = {
        references: _.extend(
          response.data.references,
          {lead_statuses: @_scope.company.lead_statuses},
          {client_statuses: @_scope.company.client_statuses},
          {deal_statuses: @_scope.company.deal_statuses},
        )
      }

      delete @_scope.company.lead_statuses
      delete @_scope.company.client_statuses
      delete @_scope.company.deal_statuses

      @_calculateIncomePercent()
    )

    resource
    .expandLabelsAndReferences()
    .query('tasks', {company_id: @_scope.companyId})
    .then((response) =>
      @_scope.tasks = @_scope.filteredTasks = response.data.entities
      @_scope.taskStatic = {
        executors: _.chain(@_scope.tasks)
          .pluck('executor')
          .uniq(_.property('id'))
          .value(),
        creators: _.chain(@_scope.tasks)
          .pluck('creator')
          .uniq(_.property('id'))
          .value(),
        references: response.data.references,
        labels: response.data.labels,
      }
    )

    @_companyResource
    .query('departments')
    .then((response) =>
      @_scope.departments = response.data.entities
    )

    @_companyResource
    .query('clients')
    .then((response) =>
      @_scope.clients = response.data.entities
    )

  _calculateIncomePercent: () =>
    return unless parseFloat(@_scope.company.plan_income) > 0

    @_scope.incomePercentDone =
      @_scope.company.actual_income / @_scope.company.plan_income * 100
    @_scope.incomePercentNotDone = 100 - @_scope.incomePercentDone

