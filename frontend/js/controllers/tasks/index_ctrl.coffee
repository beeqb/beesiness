class App.TasksIndexCtrl extends IdFly.AngularClass
  @_import: [
    '$scope',
    '$location',
    'ResourceLoader',
    'Page',
  ]

  app.controller('TasksIndexCtrl', @factory())

  initialize: () ->
    @_Page.setTitle('tasks')

    @_resource = new @_ResourceLoader()
    @_load()

    @_scope.redirectToCreateTask = () =>
      @_location.path("companies/#{@_scope.taskCreateCompany}/tasks/new")

  _load: () =>
    @_resource
    .expandLabelsAndReferences()
    .query('tasks')
    .then((response) =>
      @_scope.filteredTasks = @_scope.tasks = response.data.entities

      @_scope.taskStatic = {
        executors: _.chain(@_scope.tasks)
          .pluck('executor')
          .uniq(_.property('id'))
          .value(),
        creators: _.chain(@_scope.tasks)
          .pluck('creator')
          .uniq(_.property('id'))
          .value(),
        references: response.data.references,
        labels: response.data.labels,
      }
    )

    @_resource
    .include('client_statuses')
    .query('companies')
    .then((response) =>
      @_scope.companies = response.data.entities
    )


