class App.TasksXEditCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$auth',
    '$http',
    '$q',
    '$interval',
    'Department',
    'Employee',
    'Client',
    'Task',
    'Upload',
    'References',
    'ResourceLoader',
  ]

  app.controller('TasksXEditCtrl', @factory())

  initialize: () ->
    @_scope.references = @_References.all()
    @_companyId = @_scope.task.company_id

    @_scope.$watch('companies', (newValue, oldValue) =>
      @_scope.task.company = _.findWhere(@_scope.companies, {id: @_companyId})
    )

    @_scope.task.errors = {}

    @_scope.selectedDepartments = {
      executor: @_scope.task.executor.employees_departments[0].department_id,
      assistant: @_scope.task.assistant?.employees_departments?[0]?.department_id,
    }

    @_scope.employeesCandidates = {
      watchers: @_scope.task.watchers,
    }

    @_scope.loadEmployeesFor = @_loadEmployeesFor
    @_scope.populateAssociation = @_populateAssociation
    @_scope.removeAttachment = @_removeAttachment
    @_scope.changeStatus = @_changeStatus
    @_scope.submit = @_submit

    @_scope.task.assessment ?= "0"
    @_scope.task.assessment = parseInt(@_scope.task.assessment)

    @_scope.taskFormattedSpentTime = @_getFormattedTaskSpentTime(@_scope.task)
    @_timerUpdater = @_interval((() =>
      return unless @_scope.task.status is "in_progress"
      @_scope.taskFormattedSpentTime = @_getFormattedTaskSpentTime(@_scope.task)
      #console.log @_scope.taskFormattedSpentTime
    ), 500);
    @_scope.$on('$destroy', () => @_interval.cancel(@_timerUpdater))

    @_scope.onFormShow = (() =>
      @_loadEmployeesFor('executor')
      @_loadEmployeesFor('assistant')

      @_scope.departments = @_Department.query({
        company_id: @_companyId,
      })

      @_companyResource = new @_ResourceLoader(@_companyId)
      leadsPromise = @_companyResource.query('leads')
      clientsPromise = @_companyResource.query('clients')
      suppliersPromise = @_companyResource.query('suppliers')

      @_q
      .all([
        clientsPromise,
        leadsPromise,
        suppliersPromise
      ])
      .then((responses) =>
        clients = responses[0].data.entities
        _.setAll(clients, 'human_type', 'Клиенты')

        leads = responses[1].data.entities
        _.setAll(leads, 'human_type', 'Лиды')

        suppliers = responses[2].data.entities
        _.setAll(suppliers, 'human_type', 'Поставщики')

        @_scope.clientLikeEntities = clients.concat(leads, suppliers)
      )
    )

  _getFormattedTaskSpentTime: (task) =>
    # Convert to hh:mm representation string with leading zeros if needed
    task.spent_time ?= 0
    nowMomentSec = moment().unix()
    lastPauseAtMomentSec = moment(task.last_pause_at).unix()
    actualSpentTime = 0
    if @_scope.task.status is "in_progress"
      actualSpentTime = (parseFloat(task.spent_time) + (nowMomentSec - lastPauseAtMomentSec) / 3600.0).toString()
    else
      actualSpentTime = parseFloat(task.spent_time).toString()
    h = Math.floor(parseFloat(actualSpentTime)).toString()
    m = Math.floor((parseFloat(actualSpentTime) - Math.floor(parseFloat(actualSpentTime))) * 60).toString()
    fh = "0" + h
    fm = "0" + m
    formattedTime = {
      rawHours: h
      rawMinutes: m
      formattedHours: fh.substr(fh.length - 2)
      formattedMinutes: fm.substr(fm.length - 2)
    }
    return formattedTime

  _loadEmployeesFor: (field) =>
    @_Employee.query({
      company_id: @_companyId,
      departments: @_scope.selectedDepartments[field],
      include: 'user,employees_departments',
    }).$promise.then((employees) =>
      if field is 'watchers'
        newEmployees = _.filter(employees, (employee) =>
          !_.findWhere(@_scope.employeesCandidates[field], {id: employee.id})
        )

        @_scope.employeesCandidates[field] =
          @_scope.employeesCandidates[field].concat(newEmployees)
      else
        @_scope.employeesCandidates[field] = employees
    )

  _submit: (data) =>
    taskData = {}
    _.extend(taskData, @_scope.task)
    _.extend(taskData, data)

    resource = new @_Task(taskData)

    resource
    .$save()
    .then(() =>
      if _.isEmpty(@_scope.task.new_attachments)
        @_rootScope.$emit('user:changed')
        return

      @_uploadAttachments().then((response) =>
        @_rootScope.$emit('user:changed')
        @_scope.task.new_attachments = []
        @_scope.task.attachments = response.data.attachments
      )
    )
    .catch((response) =>
      _.extend(@_scope.task.errors, response.errors)
    )

  _uploadAttachments: () =>
    url = '/api/tasks/' + @_scope.task.id + '/upload_attachments'

    @_Upload.upload({
      url: url,
      method: 'PUT',
      headers: @_auth.retrieveData('auth_headers'),
      data: {
        attachments: @_scope.task.new_attachments
      },
    })

  _populateAssociation: (field, data) =>
    employees = @_scope.employeesCandidates[field]

    if field isnt 'watchers'
      @_scope.task[field] = _.findWhere(employees, {id: data})
    else
      idField = 'watcher_ids'
      @_scope.task[field] = _.filter(
        employees,
        (employee) =>
          employee.id in @_scope.task[idField]
      )

  _removeAttachment: (index) =>
    url = '/api/tasks/' + @_scope.task.id + '/attachments/' + index

    @_http.delete(url).then((response) =>
      @_scope.task.attachments = response.data.attachments
    )

  _changeStatus: (status) =>
    url = '/api/tasks/' + @_scope.task.id + '/' + status
    @_http
    .post(url)
    .then((response) =>
      @_scope.task.status = response.data.status
      @_scope.task.started_at = response.data.started_at
      @_scope.task.done_at = response.data.done_at
      @_scope.task.spent_time = response.data.spent_time
      @_scope.task.last_pause_at = response.data.last_pause_at
      @_scope.taskFormattedSpentTime = @_getFormattedTaskSpentTime(@_scope.task)
    )
