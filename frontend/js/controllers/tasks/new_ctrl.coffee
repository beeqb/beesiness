class App.TasksNewCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$routeParams',
    '$location',
    '$auth',
    '$q',
    'Upload',
    'ResourceLoader',
  ]

  app.controller('TasksNewCtrl', @factory())

  initialize: () ->
    @companyId = @_routeParams.company_id
    @companyResource = new @_ResourceLoader(@companyId)

    @_scope.task = {
      errors: {},
    }

    @_scope.selectedDepartments = {}
    @_scope.employeesCandidates = {}

    @_scope.loadEmployeesFor = @_loadEmployeesFor
    @_scope.submit = @_submit

    @_load()

    clientsPromise = @companyResource.query('clients')
    leadsPromise = @companyResource.query('leads')
    suppliersPromise = @companyResource.query('suppliers')

    @_q
    .all([
      clientsPromise,
      leadsPromise,
      suppliersPromise,
    ])
    .then((responses) =>
      clients = responses[0].data.entities
      _.setAll(clients, 'human_type', 'Клиенты')

      leads = responses[1].data.entities
      _.setAll(leads, 'human_type', 'Лиды')

      suppliers = responses[2].data.entities
      _.setAll(suppliers, 'human_type', 'Поставщики')

      @_scope.clientLikeEntities = clients.concat(leads, suppliers)
    )

    @companyResource.query('departments').then((response) =>
      @_scope.departments = response.data.entities
    )

    @_scope.addItemToChecklist = (() =>
      @_scope.task.checklist.push({done: false})
    )

    @_scope.removeItemFromChecklist = ((index) =>
      @_scope.task.checklist.splice(index, 1)
    )

    @_scope.submittingNow = false

  _load: () =>
    resource = new @_ResourceLoader()

    resource
    .expandReferencesAndLabels()
    .query('tasks/new', {company_id: @companyId})
    .then((response) =>
      @_scope.taskStatic = {
        references: response.data.references
        labels: response.data.labels
      }
      _.extend(@_scope.task, response.data.entity)
      @_scope.task.client_id = Number(@_routeParams.client_id)
      @_scope.task.start_date = new Date()
      @_scope.task.creator_id = @_rootScope.user.id
    )

  _loadEmployeesFor: (field) =>
    @companyResource
    .include('user')
    .query(
      'employees',
      {departments: @_scope.selectedDepartments[field]}
    )
    .then((response) =>
      employees = response.data.entities

      if field is 'watchers'
        newEmployees = _.chain(employees)
          .filter((employee) =>
            !_.findWhere(@_scope.employeesCandidates[field], {id: employee.id})
          )
          .map((employee) ->
            return {id: employee.id, name: employee.user.fullname}
          )
          .value()

        @_scope.employeesCandidates[field] =
          (@_scope.employeesCandidates[field] || []).concat(newEmployees)
      else
        @_scope.employeesCandidates[field] = _.map(employees, (employee) ->
          return {id: employee.id, name: employee.user.fullname}
        )
    )

  _submit: () =>
    @_scope.submittingNow = true

    taskData = @_scope.task
    taskData.start_date = moment(taskData.start_date).format('YYYY-MM-DD')
    taskData.due_date = moment(taskData.due_date).format('YYYY-MM-DD')

    resource = new @_ResourceLoader()

    resource
    .post('tasks', {task: taskData})
    .then((response) =>
      if _.isEmpty(@_scope.task.attachments)
        @_location.path('/tasks')
        @_rootScope.$emit('user:changed')
        return

      @_scope.task.id = response.data.id
      @_uploadAttachments()
    )
    .catch((response) =>
      @_scope.submittingNow = false
      _.extend(@_scope.task.errors, response.data.errors)
    )

  _uploadAttachments: () =>
    url = '/api/tasks/' + @_scope.task.id + '/upload_attachments'

    @_Upload.upload({
      url: url,
      method: 'PUT',
      headers: @_auth.retrieveData('auth_headers'),
      data: {
        attachments: @_scope.task.attachments
      },
    })
    .then(
      ((response) =>
        @_rootScope.$emit('user:changed')
        @_location.path('/tasks')
      ),
      ((response) =>
        @_scope.submittingNow = false
        _.extend(@_scope.task.errors, response.data.errors)
      )
    )

