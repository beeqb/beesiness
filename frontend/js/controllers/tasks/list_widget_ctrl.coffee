class App.TasksListWidgetCtrl extends IdFly.AngularClass
  @_import: [
    '$rootScope',
    '$scope',
    '$location',
    '$routeParams',
    '$filter',
    'References',
    'ResourceLoader'
  ]

  app.controller('TasksListWidgetCtrl', @factory())

  initialize: () ->
    @_References.sync()
    @_scope.tasksFilter = {
      status: 'all',
    }
    @_resource = new @_ResourceLoader()

  markSelectedAsDone: () =>
    return unless confirm('Вы уверены, что хотите завершить эти задачи?')

    selectedTasks = (task for task in @_scope.filteredTasks when task.selected)
    ids = _.pluck(selectedTasks, 'id')

    @_resource
    .post('tasks/finish_all', {ids: ids})
    .then((response) =>
      for updatedTask in response.data
        task = _.findWhere(@_scope.tasks, {id: updatedTask.id})
        task = _.extend(task, updatedTask)

      _.setAll(selectedTasks, 'selected', false)
      @allSelected = false
    )

  # Возвращает true, если выбрана хотя бы одна незавершенная задача
  notAllDoneSelected: () =>
    _.any(@_scope.tasks, (task) -> task.selected && task.status != 'done')

  toggleAllSelection: () =>
    _.setAll(@_scope.tasks, 'selected', @allSelected)

  filterTasks: () =>
    @_scope.filteredTasks = _.filter(@_scope.tasks, (task) =>
      result = true

      return result unless @_scope.tasksFilter

      if @_scope.tasksFilter.status and @_scope.tasksFilter.status != 'all'
        result = result && task.status == @_scope.tasksFilter.status

      if @_scope.tasksFilter.executor
        result = result && task.executor.id == @_scope.tasksFilter.executor

      if @_scope.tasksFilter.creator
        result = result && task.creator.id == @_scope.tasksFilter.creator

      return result
    )
