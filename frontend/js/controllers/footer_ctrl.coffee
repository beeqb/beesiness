class App.FooterCtrl extends IdFly.AngularClass

  @_import: [
    '$rootScope',
    '$scope',
    '$location',
    '$anchorScroll',
    '$timeout',
  ]

  app.controller('FooterCtrl', @factory())

  initialize: () =>
    @_scope.gotoTop = @_gotoTop

  _gotoTop: () =>
    @_anchorScroll('top')
