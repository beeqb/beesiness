angular.module('app').component('incomeAndOutcomePiechart', {
  bindings: {
    data: "<",
  },
  templateUrl: "/assets/views/components/income_and_outcome_piechart.html",
  controller: ($filter) ->
    @$onChanges = ((changes) =>
      return unless @chartData
      @_updateChart()
    )

    @$onInit = (() =>
      @_createChart()
    )

    @_createChart = () =>
      @chartData = {
        bindto: "#incomeAndOutcomePiechartContainer"
        data: {
          columns: [],
          type: "pie",
          names: {
            IOPIncome: 'Приход',
            IOPOutcome: 'Расход'
          }
        }
        pie: {
          label: {
            format: (value, ratio, id) =>
              return value + "(" + $filter('roundedNumber')(ratio * 100, 2) + "%)"
          }
        }
      }

      if !@data.income?
        @data.income = 0

      if !@data.outcome?
        @data.outcome = 0

      @chartData.data.columns[0] = ["IOPIncome", @data.income]
      @chartData.data.columns[1] = ["IOPOutcome", @data.outcome]
      @chart = c3.generate(@chartData)

    @_updateChart = () =>
      if !@chartData?
        @_createChart()
        return

      @chartData.data.columns[0] = ["IOPIncome", @data.income]
      @chartData.data.columns[1] = ["IOPOutcome", @data.outcome]
      if @chart?
        @chart.load(@chartData.data)
      else
        @chart = c3.generate(@chartData)

    return
})
