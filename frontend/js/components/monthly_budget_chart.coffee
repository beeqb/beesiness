angular.module('app').component('monthlyBudgetChart', {
  bindings: {
    data: "<",
    axis: "<"
  },
  templateUrl: "/assets/views/components/monthly_budget_chart.html",
  controller: () ->
    @$onChanges = ((changes) =>
      return unless @chartData
      @_updateChart()
    )

    @$doCheck = (() =>
      #console.log "doCheck"
    )
    
    @$onInit = (() =>
      @_createChart()
    )

    @_createChart = () =>
      @chartData = {
        bindto: "#monthlyBudgetChartContainer"
        zoom: {
          enabled: true
        }
      }

      if !@data.columns?
        @data.columns = []

      @chartData.data = @data
      @chartData.axis = @axis
      @chart = c3.generate(@chartData)

    @_updateChart = () =>
      if !@chartData?
        @_createChart()
        return

      @chartData.data = @data
      @chartData.axis = @axis
      if @chart?
        @chart = @chart.destroy()
        @chart = c3.generate(@chartData)
      else
        @chart = c3.generate(@chartData)

    return
})
