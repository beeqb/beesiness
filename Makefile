export MSYS_NO_PATHCONV=1
ENV ?= beeqb.local
RELEASE ?= no
FROM ?=
PROJECT_LOCATION = `pwd`

build_image:
	docker build --rm -t ruby-app "${PROJECT_LOCATION}/config/ruby-app"
	# Build ruby with node image
	docker build --rm -t node-ruby "${PROJECT_LOCATION}/config/node-ruby"
	# Build base ruby app test image
	docker build --rm -t ruby-app-test "${PROJECT_LOCATION}/config/ruby-app-test"
	# Build main image
	docker build --rm -t beeqb.rails "${PROJECT_LOCATION}/config/ruby"

build:
	make build_image

ifeq (yes,$(shell \
	test "${FROM}" = "" || \
	test "`git diff "${FROM}"..HEAD -- backend/Gemfile`" != "" && \
	echo "yes" \
))
	docker run --rm -w "/app" -v "${PROJECT_LOCATION}/backend:/app" beeqb.rails bundle install --jobs 128 --path vendor/bundle
	docker run --rm -w "/app" -v "${PROJECT_LOCATION}/backend:/app" beeqb.rails bundle pack --all --path vendor/bundle

else
	# сборка не требуется
endif

ifeq (yes,$(shell \
	test "${FROM}" = "" || \
	test "`git diff "${FROM}"..HEAD -- frontend/Gemfile`" != "" && \
	echo "yes" \
))
	docker run --rm -w "/app" -v "${PROJECT_LOCATION}/frontend:/app" node-ruby bundle install --jobs 128 --path vendor/bundle

else
	# Сборка не требуется
endif


# ==========================
# СБОРКА ТЕСТОВОГО ОКРУЖЕНИЯ
# ==========================

ifeq (yes,$(shell \
	test "${FROM}" = "" || \
	test "`git diff "${FROM}"..HEAD -- test/Gemfile.lock`" != "" && \
	echo "yes" \
))
	docker run --rm -w "/app" -v "${PROJECT_LOCATION}/test:/app" ruby-app-test bundle install --jobs 128 --path vendor/bundle

else
	# сборка не требуется
endif

ifeq (yes,$(shell \
	test "${FROM}" = "" || \
	test "`git diff "${FROM}"..HEAD -- frontend/package.json`" != "" && \
	echo "yes" \
))
	docker run --rm -v "${PROJECT_LOCATION}/frontend:/app" -w /app node-ruby npm install

endif

ifeq (yes,$(shell \
	test "${FROM}" = "" || \
	test "`git diff "${FROM}"..HEAD -- frontend`" != "" && \
	echo "yes" \
))
	docker run --rm -v "${PROJECT_LOCATION}/frontend:/app" -w /app node-ruby sh -c "rm -rf /app/build/*"

ifeq ($(RELEASE),yes)
	docker run --rm -v "${PROJECT_LOCATION}/frontend:/app" -w /app node-ruby sh -c "VERSION=$$(git rev-parse HEAD) gulp release"

else
	docker run --rm -v "${PROJECT_LOCATION}/frontend:/app" -w /app node-ruby gulp
	docker run --rm -v "${PROJECT_LOCATION}/frontend:/app" -w /app node-ruby gulp render-index

endif

endif

start:
	docker-compose -f config/${ENV}/main.yml up -d --force-recreate --remove-orphans

stop:
	docker-compose -f config/${ENV}/main.yml stop

ps:
	docker-compose -f config/${ENV}/main.yml ps

logs:
	docker-compose -f config/${ENV}/main.yml logs --tail 100 -f -t

rspec:
	docker-compose -f config/${ENV}/main.yml run \
		--rm test  \
		bash -c "/wfi.sh -t 60 test.mysql.beeqb.dev1.idfly.ru:3306 -- \
			bundle install --path vendor/bundle && bin/rake db:migrate && bin/rake db:seed && rspec --tag ~@ability"; \
		code=$${?}; \
		docker-compose -f config/${ENV}/main.yml down; \
		exit $${code}
up:
	make build

	make start
	
dbmigrate:
	docker exec backend.${ENV} rails db:migrate

dbseed:
	docker exec backend.${ENV} rails db:seed

cleanup:
	docker rm -f $(docker ps -a -q)
	docker rmi -f $(docker images -q)

# ===============================================================
# Приложение запущено; ваши следующие действия:
# 1. Откройте http://localhost в браузере для доступа к приложению
# 2. Откройте http://localhost:1080 в браузере для доступа к почте
# 3. Выполните "make logs" для подключения к логам
# 4. Выполните "make stop" для остановки приложения
# ===============================================================

