param
(
	[parameter(Position=0, Mandatory=$true)]
	[string]$action = "help"
)

$PROJECT_LOCATION = "C:/Users/Dmytro/Workspace/beeqb-webapp"
$ENV = "beeqb.local"

function runCMD($cmdString)
{
	write-host "Running: $cmdString" -foregroundcolor "blue"
	iex $cmdString
}

function beeqbStart()
{
	#start containers
	runCMD "docker-compose -f config/$ENV/main.yml up -d --force-recreate --remove-orphans"
}

function beeqbStartSSH()
{
	#start containers
	runCMD "docker-compose -f config/$ENV/main-rubymine.yml up -d --force-recreate --remove-orphans"
}

function beeqbStop()
{
	runCMD "docker-compose -f config/$ENV/main.yml stop"
}

function beeqbStopSSH()
{
	runCMD "docker-compose -f config/$ENV/main-rubymine.yml stop"
}


function beeqbDbmigrate()
{
	runCMD "docker exec backend.$ENV rails db:migrate"
}

function beeqDbseed()
{
	runCMD "docker exec backend.$ENV rails db:seed"
}

function beeqbPs()
{
	runCMD "docker-compose -f config/$ENV/main.yml ps"
}

function beeqbPsSSH()
{
	runCMD "docker-compose -f config/$ENV/main-rubymine.yml ps"
}

function beeqbBuildRubyApp
{
	runCMD "docker build --rm -t ruby-app `"$PROJECT_LOCATION/config/ruby-app`""
}

function beeqbBuildNodeRuby
{
	runCMD "docker build --rm -t node-ruby `"$PROJECT_LOCATION/config/node-ruby`""
}

function beeqbBuildRubyAppTest
{
	runCMD "docker build --rm -t ruby-app-test `"$PROJECT_LOCATION/config/ruby-app-test`""
}

function beeqbBuildRuby
{
	runCMD "docker build --rm -t beeqb.rails `"$PROJECT_LOCATION/config/ruby`""
}

function beeqbNpmInstall
{
	runCMD "docker run --rm -v `"$PROJECT_LOCATION/frontend:/app`" -w /app node-ruby npm install"
}

function beeqbRemoveAllContainers
{
	runCMD "docker rm -f $(docker ps -a -q)"
}

function beeqbRemoveAllImages
{
	runCMD "docker rmi -f $(docker images -q)"
}

function beeqbBuildImages()
{
	# Stop containers if running
	beeqbStop
	# Remove all containers
	beeqbRemoveAllContainers
	# Remove all images
	beeqbRemoveAllImages
	
	# Build images:
	# Build base ruby app image
	beeqbBuildRubyApp
	# Build ruby with node image
	beeqbBuildNodeRuby
	# Build base ruby app test image
	beeqbBuildRubyAppTest
	# Build main image
	beeqbBuildRuby
}

function beeqbPrepare()
{
	# Stop containers if running
	beeqbStop
	# Remove all containers
	beeqbRemoveAllContainers
	# Backend gems
	runCMD "docker run --rm -w `"/app`" -v `"$PROJECT_LOCATION/backend:/app`" beeqb.rails bundle install --jobs 128 --path vendor/bundle"
	runCMD "docker run --rm -w `"/app`" -v `"$PROJECT_LOCATION/backend:/app`" beeqb.rails bundle pack --all --path vendor/bundle"
	# Frontend gems
	runCMD "docker run --rm -w `"/app`" -v `"$PROJECT_LOCATION/frontend:/app`" node-ruby bundle install --jobs 128 --path vendor/bundle"
	# Test gems
	runCMD "docker run --rm -w `"/app`" -v `"$PROJECT_LOCATION/test:/app`" ruby-app-test bundle install --jobs 128 --path vendor/bundle"
	# Node modules
	beeqbNpmInstall
	# Clear build directory
	runCMD "docker run --rm -v `"$PROJECT_LOCATION/frontend:/app`" -w /app node-ruby sh -c `"rm -rf /app/build/*`""
	# Gulp
	runCMD "docker run --rm -v `"$PROJECT_LOCATION/frontend:/app`" -w /app node-ruby gulp"
	runCMD "docker run --rm -v `"$PROJECT_LOCATION/frontend:/app`" -w /app node-ruby gulp render-index"
}

function beeqbShowHelp
{
	write-host "	Available commands:
	
	build               Builds all images.
	build-ruby-app      Builds ruby app image.
	build-node-ruby     Builds ruby image with node modules.
	build-app-test      Builds testing image.
	build-ruby          Builds main image.
	prepare             Loads vendor gems, nmps, starts gulp.
	start               Creates containers.
	stop                Stops containers.
	ps                  Containers info.
	start-ssh           Creates containers with ssh for backend without server starting.
	stop-ssh            Stops containers with ssh backend.
	ps-ssh              Containers info with ssh backend.
	dbseed              Runs rails db:seed.
	dbmigrate           Runs rails db:migrate.
	npm-install         Install nmps separately.
	netstat             Netstat on backend container.
	rm                  Removes all containers.
	rmi                 Removes all images."
}

write-host "Action is: $action"

if($action -eq "help")
{
	beeqbShowHelp
}
elseif($action -eq "build")
{
	beeqbBuildImages
	beeqbPrepare
}
elseif($action -eq "build-ruby-app")
{
	beeqbBuildRubyApp
}
elseif($action -eq "build-node-ruby")
{
	beeqbBuildNodeRuby
}
elseif($action -eq "build-app-test")
{
	beeqbBuildRubyAppTest
}
elseif($action -eq "build-ruby")
{
	beeqbBuildRuby
}
elseif($action -eq "prepare")
{
	beeqbPrepare
}
elseif($action -eq "start")
{
	beeqbStart
}
elseif($action -eq "stop")
{
	beeqbStop
}
elseif($action -eq "ps")
{
	beeqbPs
}
elseif($action -eq "start-ssh")
{
	beeqbStartSSH
}
elseif($action -eq "stop-ssh")
{
	beeqbStopSSH
}
elseif($action -eq "ps-ssh")
{
	beeqbPsSSH
}
elseif($action -eq "dbseed")
{
	beeqDbseed
}
elseif($action -eq "dbmigrate")
{
	beeqbDbmigrate
}
elseif($action -eq "npm-install")
{
	beeqbNpmInstall
}
elseif($action -eq "netstat")
{
	runCMD "docker exec -it backend.$ENV netstat -nlp"
}
elseif($action -eq "rm")
{
	beeqbRemoveAllContainers
}
elseif($action -eq "rmi")
{
	beeqbRemoveAllImages
}